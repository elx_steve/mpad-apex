Mpad Android application for APEX
=================================

Overview
--------

This repository starts from the old apex-android repository and is specifically oriented to continue
the development for APEX.
The apex-android repository is not used anymore.
A similar repository fro Mpad exists now for Mpad Northwell as well.
This repository lose the old branches used in apex-android.

Hardware supported
------------------

The code in the repository is supposed to be executed on Zebra terminals TC56 with Zebra Printer
ZQ models.

Who contact
-----------

Stefano Bodini : stefano_bodini@elyxor.com


