package com.apex.mpad.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.presenter.iface.AddPrinterPresenterInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.base.BaseActivity;
import com.zebra.sdk.printer.discovery.BluetoothDiscoverer;
import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveredPrinterBluetooth;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


public class AddPrinterActivity extends BaseActivity {

    @InjectView(R.id.button_pair) Button button;
    @InjectView(R.id.edit_scan_printer) EditText scanPrinterText;
    @InjectView(R.id.button_right) Button closeButton;
    @InjectView(R.id.text_connected_printer) TextView connectedPrinter;

    @Inject @ForApplication ApexApplication mApp;
    @Inject @ForApplication ApexPrefs mPrefs;
    @Inject AddPrinterPresenterInterface mPresenter;

    private List<DiscoveredPrinter> mPrinters;
    final UUID SerialPortServiceClass_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private DiscoveryHandler mDiscoveryHandler = null;
    private BluetoothAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_add_printer);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        mPrinters = new ArrayList<DiscoveredPrinter>();
        initView();
    }

    @Override protected void onDestroy() {
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    private void initView() {
        setupButtonBar(null, "Close", false);
        scanPrinterText.requestFocus();
        hideKeyboard(scanPrinterText);
        if (mPrefs.getPrinterName() != null) {
            connectedPrinter.setText("Connected to: "+mPrefs.getPrinterName());
        }
    }

    @OnClick(R.id.button_right) void onCloseClick() {
        finish();
    }

    @OnClick(R.id.button_pair) void onPairClick() {
        discoverPrinter();
    }

    private void discoverPrinter() {
        showLoading();
        button.setEnabled(false);

        if (scanPrinterText.getText().toString().equals(mPrefs.getMacAddress())) {
            stopLoading();
            return;
        }
        if (!mApp.isBluetoothEnabled()) {
            showErrorMessage(ApexApplication.ERROR_BLUETOOTH);
            return;
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        registerReceiver(mReceiver, filter);

        mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mAdapter.isDiscovering()) {
            mAdapter.cancelDiscovery();
        }
        mAdapter.startDiscovery();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mAdapter.cancelDiscovery();
    }

    private void updateDiscoveredPrinters() {
        for (DiscoveredPrinter printer : mPrinters) {
            if (((DiscoveredPrinterBluetooth) printer).friendlyName.equals(scanPrinterText.getText().toString())) {
                mPrefs.setMacAddress(printer.address);
                mPrefs.setPrinterName(((DiscoveredPrinterBluetooth) printer).friendlyName);
                connectedPrinter.setText("Connected to: "+mPrefs.getPrinterName());
                showFinishedMessage();
                stopLoading();
                return;
            }
        }
        showErrorMessage("Printer not located. Please make sure it is turned on and try again.");
        button.setEnabled(true);
    }

    private void showFinishedMessage() {
        stopLoading();
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                mPresenter.close();
            }
        };
        dialog.setArgs("Printer has been paired!");
        showDialog(dialog);
    }

    private void showErrorMessage(String message) {
        stopLoading();
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
            }
        };
        dialog.setArgs(message);
        showDialog(dialog);
    }

    @Override protected BasePresenterInterface getPresenter() {
        return mPresenter;
    }

    private List<BluetoothDevice> tempBtList = new ArrayList<BluetoothDevice>();

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(final Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device

            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                tempBtList.clear();
            }
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.d("BTDev", "found");
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                String name = device.getName();

                if (!tempBtList.contains(device)) {
                    tempBtList.add(device);
                }
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.d("BTDev", "done");

                connectToBluetoothDevices(context);
            }

        }
    };
    private void connectToBluetoothDevices(final Context context) {
        final CountDownLatch latch = new CountDownLatch(1);

        if (mAdapter.isDiscovering()) {
            mAdapter.cancelDiscovery();
        }
        try {
            ((AddPrinterActivity)context).unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        if (tempBtList.isEmpty()) {
            showErrorMessage("No Printers Found, please check that the printer is on and try again.");
            button.setEnabled(true);
        }
        for (BluetoothDevice device : tempBtList) {
            BluetoothSocket socket = null;
            try {
                socket = device.createInsecureRfcommSocketToServiceRecord(SerialPortServiceClass_UUID);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (socket != null) {
                if (mDiscoveryHandler == null) {
                    mDiscoveryHandler = new DiscoveryHandler() {
                        @Override public void foundPrinter(DiscoveredPrinter discoveredPrinter) {
                            if (discoveredPrinter instanceof DiscoveredPrinterBluetooth) {
                                DiscoveredPrinterBluetooth printer = (DiscoveredPrinterBluetooth) discoveredPrinter;
                                if (printer.friendlyName != null && printer.friendlyName.equals(scanPrinterText.getText().toString())) {
                                    mPrefs.setMacAddress(printer.address);
                                    mPrefs.setPrinterName(printer.friendlyName);
                                    connectedPrinter.setText("Connected to: "+mPrefs.getPrinterName());
                                    showFinishedMessage();
                                    stopLoading();
                                    latch.countDown();
                                    return;
                                }
                            }
                            mPrinters.add(discoveredPrinter);
                        }

                        @Override public void discoveryFinished() {
//                            updateDiscoveredPrinters();
                            latch.countDown();
                        }

                        @Override public void discoveryError(String s) {
                            showErrorMessage(s);
                            latch.countDown();
                        }
                    };
                    try {
                        BluetoothDiscoverer.findPrinters(context, mDiscoveryHandler);
                    } catch (Exception e) {
                        showErrorMessage(e.getMessage());
                    }
                }
            }
        }
        try {
            latch.await(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


