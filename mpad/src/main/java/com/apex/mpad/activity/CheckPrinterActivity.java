package com.apex.mpad.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.CheckPrinterPresenterInterface;
import com.apex.mpad.service.PrintService;
import com.apex.mpad.util.ApexSleeper;
import com.apex.mpad.util.base.BasePrinterActivity;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.SGD;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.printer.ZebraPrinterLinkOs;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by John on 3/9/2017.
 *
 * Switching to ZQ510 requires updated JAR and commands.  This is to ensure discovery and connectivity.
 */

public class CheckPrinterActivity extends BasePrinterActivity {

    @Inject
    CheckPrinterPresenterInterface mPresenter;

    @Inject
    PrintService mPrintService;

    @InjectView(R.id.macInput) EditText macAddressEditText;
    @InjectView(R.id.statusText) TextView statusField;
    @InjectView(R.id.testButton) Button testButton;
    @InjectView(R.id.retryButton) Button retryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_check_printer);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        showDiscoveryMessage = true;
        discoverPrinters();
        initView();
    }

    protected void initView() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (mPrefs.getMacAddress() != null)
                    macAddressEditText.setText(mPrefs.getMacAddress());

                // Disable buttons during the scan
                setupButtonBar(null, "Close", false);
                manageButtonBar( false, false, false);
                helpButton.setEnabled(false);

                testButton.setEnabled(false);
                testButton.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        new Thread(new Runnable() {
                            public void run() {
                                enableTestButton(false);
                                Looper.prepare();
                                doConnectionTest();
                                Looper.loop();
                                Looper.myLooper().quit();
                            }
                        }).start();
                    }
                });
                showLoading();
            }
        });
    }

    @OnClick(R.id.button_right) void onCloseClick() {
        finish();
    }
    @OnClick(R.id.retryButton) void onRetryClick() {
        testButton.setVisibility(View.VISIBLE);
        retryButton.setVisibility(View.GONE);
        showLoading();
        // Disable buttons during the scan
        setupButtonBar(null, "Close", false);
        manageButtonBar( false, false, false);

        helpButton.setEnabled(false);
        discoverPrinters();
    }


    @Override
    public void discoveryFinished() {
        super.discoveryFinished();
        stopLoading();
        enableTestButton(true);
        if (printersFound) {
            macAddressEditText.setText(mPrefs.getMacAddress());
            setStatus("Status: Not Connected", Color.WHITE);
        } else {
            setStatus("No Printer Found", Color.RED);
            enableTestButton(false);
            testButton.setVisibility(View.GONE);
            retryButton.setVisibility(View.VISIBLE);
            retryButton.setEnabled(true);
        }
        // Enable buttons
        setupButtonBar(null, "Close", false);
        manageButtonBar( false, true, false);

        helpButton.setEnabled(true);
    }

    public void disconnect() {
        try {
            setStatus("Disconnecting", Color.RED);
            if (connection != null) {
                connection.close();
            }
            setStatus("Not Connected", Color.RED);
        } catch (ConnectionException e) {
            setStatus("COMM Error! Disconnected", Color.RED);
        } finally {
            enableTestButton(true);
        }
    }

    private void setStatus(final String statusMessage, final int color) {
        runOnUiThread(new Runnable() {
            public void run() {
                statusField.setBackgroundColor(color);
                statusField.setText(statusMessage);
            }
        });
        ApexSleeper.sleep(1000);
        stopLoading();
    }

    @Deprecated
    private byte[] getConfigLabel() {
        byte[] configLabel = null;
        try {
            PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();
            SGD.SET("device.languages", "cpcl", connection);

            if (printerLanguage == PrinterLanguage.ZPL) {
                configLabel = "^XA^FO17,16^GB379,371,8^FS^FT65,255^A0N,135,134^FDTEST^FS^XZ".getBytes();
            } else if (printerLanguage == PrinterLanguage.CPCL) {
                String cpclConfigLabel = "! 0 200 200 406 1\r\n" + "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n" + "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";
                configLabel = cpclConfigLabel.getBytes();
            }
        } catch (ConnectionException e) {

        }
        return configLabel;
    }

    public ZebraPrinter connect() {
        setStatus("Connecting...", Color.YELLOW);
        if (connection != null) disconnect();
        connection = null;
        connection = new BluetoothConnection(mPrefs.getMacAddress());


        try {
            connection.open();
            setStatus("Connected", Color.GREEN);
        } catch (ConnectionException e) {
            setStatus("Comm Error! Disconnecting", Color.RED);
            disconnect();
        }

        ZebraPrinter printer = null;

        if (connection.isConnected()) {
            try {

                printer = ZebraPrinterFactory.getInstance(connection);
                setStatus("Determining Printer Language", Color.YELLOW);
                String pl = SGD.GET("device.languages", connection);
                setStatus("Printer Language " + pl, Color.BLUE);
            } catch (ConnectionException e) {
                setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                ApexSleeper.sleep(1000);
                disconnect();
            } catch (ZebraPrinterLanguageUnknownException e) {
                setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                ApexSleeper.sleep(1000);
                disconnect();
            }
        }

        return printer;
    }

    private void doConnectionTest() {
        printer = connect();

        if (printer != null) {
            sendTestLabel();
        } else {
            disconnect();
        }
    }
    private void sendTestLabel() {
        try {
            ZebraPrinterLinkOs linkOsPrinter = ZebraPrinterFactory.createLinkOsPrinter(printer);

            PrinterStatus printerStatus = (linkOsPrinter != null) ? linkOsPrinter.getCurrentStatus() : printer.getCurrentStatus();

            if (printerStatus.isReadyToPrint) {
                PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();
                SGD.SET("device.languages", "cpcl", connection);
                mPrintService.setConnection((BluetoothConnection)connection);
                mPrintService.sendFailedVisitLabel(printer, "Test",null,"Check Printer");
                setStatus("Sending Data", Color.BLUE);
            } else if (printerStatus.isHeadOpen) {
                setStatus("Printer Head Open", Color.RED);
            } else if (printerStatus.isPaused) {
                setStatus("Printer is Paused", Color.RED);
            } else if (printerStatus.isPaperOut) {
                setStatus("Printer Media Out", Color.RED);
            }
            if (connection instanceof BluetoothConnection) {
                String friendlyName = ((BluetoothConnection) connection).getFriendlyName();
                setStatus(friendlyName, Color.MAGENTA);
                ApexSleeper.sleep(500);
            }
        } catch (ConnectionException e) {
            setStatus(e.getMessage(), Color.RED);
        } finally {
            disconnect();
        }
    }

    private void enableTestButton(final boolean enabled) {
        runOnUiThread(new Runnable() {
            public void run() {
                testButton.setEnabled(enabled);
            }
        });
    }


}
