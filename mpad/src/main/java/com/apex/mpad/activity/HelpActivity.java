package com.apex.mpad.activity;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.HelpPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class HelpActivity extends BaseActivity {

    @InjectView(R.id.phone_link) TextView phoneLink;
    @InjectView(R.id.button_help) Button helpButton;
    @Inject HelpPresenterInterface mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_help);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
    }

    private void initView() {
        phoneLink.setPaintFlags(phoneLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        setupButtonBar(null, "Close", false);
        helpButton.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.button_right) public void onCloseClick() {
        mPresenter.close();
    }
    @OnClick(R.id.phone_link) public void onPhoneClick() {
        makeCall(phoneLink.getText().toString());
    }

    @Override public BasePresenterInterface getPresenter() {return mPresenter;}
}
