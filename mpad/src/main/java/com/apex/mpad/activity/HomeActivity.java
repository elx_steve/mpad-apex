package com.apex.mpad.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import butterknife.InjectView;
import butterknife.OnClick;
import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.HomePresenterInterface;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

//this is essentially the main menu

public class HomeActivity extends BaseActivity {

    @InjectView(R.id.button_check_printer) Button checkPrinterButton;
    @InjectView(R.id.button_add_patient) Button addPatientButton;
    @InjectView(R.id.button_refresh_orders) Button refreshOrdersButton;
    @InjectView(R.id.button_route_summary) Button routeSummaryButton;

    @Inject HomePresenterInterface mPresenter;

    private long mLastClick = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
    }
    private void initView() {
        setupButtonBar(null, "Back", false);
        handleRefreshButton();
    }

    private void goToWelcome()
    {
        // AM-29
        //startActivity(new Intent(this, WelcomeActivity.class));
        Intent i = new Intent(this, WelcomeActivity.class);
        startActivity(i);
    }

    @Override
    public void handleRefreshButton() {
        refreshOrdersButton.setBackgroundResource(R.drawable.button);
        if (mPresenter.hasMoreWorkOrdersNotification()) {
            refreshOrdersButton.setBackgroundResource(R.drawable.button_red);
        }
    }

    @OnClick(R.id.button_check_printer) public void onPrinterClick() {
        mPresenter.checkPrinter();
    }
    @OnClick(R.id.button_add_patient) public void onPatientClick() {
        mPresenter.addPatient();
    }
    @OnClick(R.id.button_refresh_orders) public void onRefreshClick() {
        if (System.currentTimeMillis() - mLastClick < 1000){
            return;
        }
        mLastClick = System.currentTimeMillis();
        mPresenter.refreshWorkOrders();
    }
    @OnClick(R.id.button_route_summary) public void onRouteSummaryClick() {
        mPresenter.routeSummary();
    }

    @OnClick(R.id.button_right) public void onCloseClick() {
        mPresenter.back();
    }

    @Override public BasePresenterInterface getPresenter() {
        return mPresenter;
    }

    public void showRefreshResultDialog(boolean isSuccessful) {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss(); goToWelcome();
            }
        };

        String text = "Your workorders have been successfully updated.";
        if (!isSuccessful) {
            text = "Your workorders could not be updated. Please try again shortly " +
                    "or contact your administrator if the problem persists";
        }
        dialog.setArgs("Refresh Orders", text);
        showDialog(dialog);
    }
}
