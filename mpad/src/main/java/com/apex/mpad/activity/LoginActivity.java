package com.apex.mpad.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.*;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.LoginPresenterInterface;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.util.*;
import com.apex.mpad.util.base.BaseActivity;
import com.google.common.io.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.apex.mpad.util.Constants.BUS_UI;


public class LoginActivity extends BaseActivity {
    private static final String TAG = "LoginActivity";

    @Optional @InjectView(R.id.login_username) EditText username;
    @Optional @InjectView(R.id.login_password) EditText password;
    @Optional @InjectView(R.id.button_login) Button loginButton;
    @Optional @InjectView(R.id.button_login_help) TextView loginHelp;
    @Optional @InjectView(R.id.progress_spinner) ProgressBar progressBar;
    @Optional @InjectView(R.id.version_text) TextView version;

    @Inject LoginPresenterInterface mPresenter;
    @Inject @ForApplication ApexApplication mApp;
    @Inject @Named(BUS_UI) ApexBus uiBus;
    @Inject @ForApplication ApexPrefs mPrefs;
    @Inject NavigationManager mNavManager;

    private PackageInfo pi;

    private static final Logger mLogger = LoggerFactory.getLogger(LoginActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLogger.debug("Activity Created: {}", this);
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);

        initView();
    }
    private void initView() {
        loginButton.setEnabled(false);
        password.setTypeface(Typeface.DEFAULT);

        try {
            pi = getPackageManager().getPackageInfo("com.apex.mpad", 0);
        } catch (PackageManager.NameNotFoundException e) {
            mLogger.warn("Package Manager error: {}", e.getMessage() != null ? e.getMessage() : "");
        }
        version.setText(pi.versionName);
    }


    @Override protected void onResume() {
        super.onResume();

        // Example of checking about EMDK status and notify user
        if (mApp.EMDKstatus == 2){
            Log.d(TAG, "EMDK problems notification");
            Toast.makeText(mApp, "Warning ! EMDK can have problems", Toast.LENGTH_LONG).show();
        }

        uiBus.register(this);
        mApp.setSessionId(null);
        username.getText().clear();
        password.getText().clear();
        username.requestFocus();
        if (mPrefs.getLocalDev()) {
            password.setText("abcd1234");
            username.setText("moto1");
            //password.setText("8p3x8dm1n!");
            //username.setText("admin");
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ExitActivity.exitApplication(this);
        }

    }

    @Override protected void onPause() {
        super.onPause();
        uiBus.unregister(this);
    }

    @Optional @OnClick(R.id.button_login) void onLoginClick() {
        mPresenter.login(username.getText().toString(), password.getText().toString());
    }
    @Optional @OnTouch(R.id.button_login_help) boolean onLoginHelpClick(View v, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                v.setPressed(true);
                startActivity(new Intent(this, HelpActivity.class));
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_OUTSIDE:
            case MotionEvent.ACTION_CANCEL:
                v.setPressed(false);
                // Stop action ...
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return true;
    }
    @Optional @OnTextChanged(R.id.login_username) void onUsernameChanged(CharSequence text) {
        setNextEnabledState();
    }
    @Optional @OnTextChanged(R.id.login_password) void onPasswordChanged(CharSequence text) {
        setNextEnabledState();
    }

    private void setNextEnabledState() {
        boolean disabled = username.getText().length() == 0 || password.getText().length() == 0;
        loginButton.setEnabled(!disabled);
    }


    public void showErrorDialog(String message) {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
            }
        };
        dialog.setArgs(message);
        showDialog(dialog);
    }

    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.bringToFront();
    }

    public void stopLoading() {
        progressBar.setVisibility(View.GONE);
    }

    public void moveToNext() {
        mApp.isLoggedIn = true;
        //startActivity(new Intent(this, WelcomeActivity.class));
        Intent i = new Intent(this, RouteSummaryListActivity.class);
        i.putExtra("isFirstLaunch", true);
        startActivity(i);
    }

    public void openPrefs() {
        startActivity(new Intent(this, SettingsActivity.class));
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
