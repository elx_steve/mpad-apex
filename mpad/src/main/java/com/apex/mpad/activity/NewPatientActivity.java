package com.apex.mpad.activity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import butterknife.*;
import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.NewPatientPresenterInterface;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.adapter.ApexSpinnerAdapter;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.DatePickerFragment;
import com.apex.mpad.util.base.BasePrinterActivity;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.List;


public class NewPatientActivity extends BasePrinterActivity implements AdapterView.OnItemSelectedListener, DatePickerFragment.DatePickerResult {

    @InjectView(R.id.draw_site_spinner) Spinner drawSiteSpinner;
    @InjectView(R.id.specimen_spinner) Spinner specimenSpinner;
    @InjectView(R.id.button_right) Button nextButton;
    @InjectView(R.id.button_left) Button menuButton;
    @InjectView(R.id.button_print) Button printButton;

    @InjectView(R.id.first_name) EditText firstName;
    @InjectView(R.id.last_name) EditText lastName;
    @InjectView(R.id.dob) EditText dob;
    @InjectView(R.id.notes) EditText notes;

    @Inject NewPatientPresenterInterface mPresenter;

    private boolean isFirstSelect = true;

    private DatePickerFragment mDatePicker;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_new_patient);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        if (mPrefs.getMacAddress() == null || mPrefs.getMacAddress().isEmpty()) {
            // Hide buttons
            menuButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.INVISIBLE);
            helpButton.setVisibility(View.INVISIBLE);

            discoverPrinters();
            showLoading();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initView();
                initLists();
            }
        });
    }
    private void initView() {
        setupButtonBar("Back", "Next", false);
        mDatePicker = new DatePickerFragment();
        mDatePicker.setListener(this);
    }

    private void initLists() {
        List<String> drawSites = mPresenter.getDrawSites();
        drawSites.add(0, "");
        ApexSpinnerAdapter failReasonAdapter = new ApexSpinnerAdapter(this, R.layout.spinner_text, drawSites);
        drawSiteSpinner.setAdapter(failReasonAdapter);
        drawSiteSpinner.setOnItemSelectedListener(this);

        specimenSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void discoveryFinished() {
        super.discoveryFinished();
        stopLoading();
        // Show buttons
        menuButton.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);
        helpButton.setVisibility(View.VISIBLE);
    }


    @OnFocusChange(R.id.first_name) void onFirstFocus(View v, boolean hasFocus) {
        if (hasFocus)
            focusOnView(v);
    }
    @OnFocusChange(R.id.last_name) void onLastFocus(View v, boolean hasFocus) {
        if (hasFocus)
            focusOnView(v);
    }
    @OnFocusChange(R.id.dob) void onDobFocus(View v, boolean hasFocus) {
        if (hasFocus)
            focusOnView(v);
    }
    @OnFocusChange(R.id.notes) void onNotesFocus(View v, boolean hasFocus) {
        if (hasFocus)
            focusOnView(v);
    }


    @OnTextChanged(R.id.first_name) public void onFirstNameTextChanged() {
        invalidateCanContinueAndPrint();
    }
    @OnTextChanged(R.id.last_name) public void onLastNameTextChanged() {
        invalidateCanContinueAndPrint();
    }
    @OnClick(R.id.button_print) public void onPrintClick() {
        mPresenter.print();
    }
    @OnClick(R.id.button_left) public void onBackClick() {
        mPresenter.back();
    }
    @OnClick(R.id.button_right) public void onNextClick() {
        if (!mPresenter.hasPrinted()) {
            ApexDialog dialog = new ApexDialog() {
                @Override public void confirm() {
                    mPresenter.next();
                }
            };
            dialog.setArgs("Confirm", "You have not printed all of the labels, are you sure you want to continue?", "Yes", "No");
            showDialog(dialog);
        } else {
            mPresenter.next();
        }
    }
    @OnTouch(R.id.dob) boolean onDobTouch(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            showDialog(mDatePicker);
            return true;
        }
        return true;
    }

    public void sendFormInfo() {
        mPresenter.setFirstName(firstName.getText().toString());
        mPresenter.setLastName(lastName.getText().toString());
        mPresenter.setDob(dob.getText().toString());
        mPresenter.setDrawSite(drawSiteSpinner.getSelectedItem().toString());
        mPresenter.setNotes(notes.getText().toString());
        mPresenter.setTubeCount(Integer.parseInt(specimenSpinner.getSelectedItem().toString() == null ? "0" : specimenSpinner.getSelectedItem().toString()));
    }

    private void invalidateCanContinueAndPrint() {
        boolean enabled = !(firstName.getText().length() == 0 || lastName.getText().length() == 0 || drawSiteSpinner.getSelectedItemPosition() == 0 || mPresenter.getTubeCount() <= 0);
        nextButton.setEnabled(enabled);
        printButton.setEnabled(enabled);
    }

    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (isFirstSelect) {
            isFirstSelect = false;
        } else {
            if (parent.getId() == drawSiteSpinner.getId()) {
                mPresenter.setDrawSite(drawSiteSpinner.getSelectedItem().toString());
            } else if (parent.getId() == specimenSpinner.getId()) {
                mPresenter.setTubeCount(Integer.parseInt(specimenSpinner.getSelectedItem().toString()));
            }
            invalidateCanContinueAndPrint();
        }
    }

    @Override public void onNothingSelected(AdapterView<?> parent) {}

    @Override public void onDateSet(DatePicker view, int year, int month, int day) {
        dob.setText((month+1)+"/"+day+"/"+year);
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}

}
