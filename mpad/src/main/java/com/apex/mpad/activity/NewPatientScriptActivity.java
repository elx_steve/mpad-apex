package com.apex.mpad.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.widget.*;
import butterknife.*;
import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.NewPatientScriptPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class NewPatientScriptActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

    private static final int CAMERA_REQUEST = 9998;

    @Inject NewPatientScriptPresenterInterface mPresenter;

    @Optional @InjectView(R.id.button_left) Button menuButton;
    @Optional @InjectView(R.id.button_right) Button addButton;

    @Optional @InjectView(R.id.captured_image) ImageView capturedImage;

    @Optional @InjectView(R.id.notes) EditText notes;

    @Optional @InjectView(R.id.radio_change) RadioButton changeScript;
    @Optional @InjectView(R.id.radio_new) RadioButton newScript;
    @Optional @InjectView(R.id.progress_spinner) ProgressBar mProgressBar;

    private Bitmap mBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_new_patient_script);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initHeader();
    }

    @Override public void onPause() {
        capturedImage.setImageURI(null);
        super.onPause();
    }

    @Override public void onNewIntent(Intent i) {
        super.onNewIntent(i);
        setIntent(i);
        resetView();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mPresenter.getFileForImage().getAbsolutePath(), bmpFactoryOptions);

            bmpFactoryOptions.inSampleSize = mPresenter.calculateInSampleSize(bmpFactoryOptions, 240, 160);

            bmpFactoryOptions.inJustDecodeBounds = false;
            mBitmap = BitmapFactory.decodeFile(mPresenter.getFileForImage().getAbsolutePath(), bmpFactoryOptions);

            capturedImage.setImageBitmap(mBitmap);
//            bmp.recycle();
            mPresenter.getFileForImage().delete();
        }
    }

    private void initView() {
        setupButtonBar("Menu", "Add", false);
        initPopupMenu(R.menu.new_patient_script);
        addButton.setEnabled(false);
    }

    private void resetView() {
        capturedImage.setImageDrawable(null);
        notes.getText().clear();
        changeScript.setChecked(false);
        newScript.setChecked(false);
    }

    @Override public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_back) {
            finish();
            return true;
        }
        return super.onMenuItemClick(item);
    }

    @Optional @OnTextChanged(R.id.notes) void onNotesTextChanged(CharSequence text){
        if (notes.getText().length() == 0) {
            addButton.setEnabled(false);
        } else if (changeScript.isChecked() || newScript.isChecked()) {
            addButton.setEnabled(true);
        }
    }
    @Optional @OnCheckedChanged(R.id.radio_new) void onNewChecked(boolean isChecked) {
        if (isChecked && notes.getText().length() > 0) {
            addButton.setEnabled(true);
        } else if (!changeScript.isChecked()) {
            addButton.setEnabled(false);
        }
    }
    @Optional @OnCheckedChanged(R.id.radio_change) void onChangeChecked(boolean isChecked) {
        if (isChecked && notes.getText().length() > 0) {
            addButton.setEnabled(true);
        } else if (!newScript.isChecked()) {
            addButton.setEnabled(false);
        }
    }
    @Optional @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }
    @Optional @OnClick(R.id.button_right) void onAddClick() {
        showLoading();
        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override public void run() {
                mPresenter.saveImage(mBitmap);
                mPresenter.setNotes(notes.getText().toString());
                mPresenter.setChangeScript(changeScript.isChecked());
                mPresenter.next();
            }
        });
    }
    @Optional @OnClick(R.id.button_capture) void onCaptureClick() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(mPresenter.getFileForImage()));
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
