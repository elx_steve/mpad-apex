package com.apex.mpad.activity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.OrderRequirementListPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class OrderRequirementListActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

    @Inject OrderRequirementListPresenterInterface mPresenter;

    @Optional @InjectView(R.id.list) ListView list;

    @Optional @InjectView(R.id.button_left) Button menuButton;
    @Optional @InjectView(R.id.button_right) Button nextButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_order_requirement_list);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initHeader();
    }

    private void initView() {
        setupButtonBar("Menu","Next",false);
        initPopupMenu(R.menu.order_requirement_list);

        String[] items = mPresenter.getOrderRequirementItems();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item_small, items);
        list.setAdapter(adapter);
    }

    @Optional @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }
    @Optional @OnClick(R.id.button_right) void onNextClick() {
        mPresenter.next();}
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
