package com.apex.mpad.activity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.apex.mpad.R;
import com.apex.mpad.adapter.ApexSpinnerAdapter;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.PatientDOBPresenter;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.PatientDOBPresenterInterface;
import com.apex.mpad.util.ApexDatePreference;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.ApexSpinner;
import com.apex.mpad.util.base.BaseAudioRecordingActivity;
import com.apex.mpad.util.base.BaseRecordingActivity;
import com.apex.mpad.util.DatePickerFragment;
import com.apex.mpad.util.DateUtil;
import com.apex.mpad.util.FileUtility;
import com.apex.mpad.util.ImageUtility;

import java.util.Arrays;
import java.util.Date;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import butterknife.OnTouch;
import butterknife.Optional;

/**
 * Created by John on 6/13/2016.
 */
public class PatientDOBActivity  extends BaseAudioRecordingActivity implements PopupMenu.OnMenuItemClickListener, AdapterView.OnItemSelectedListener, DatePickerFragment.DatePickerResult {

    @Inject
    PatientDOBPresenterInterface mPresenter;

    @Optional
    @InjectView(R.id.name)
    TextView name;

    @Optional
    @InjectView(R.id.dob)
    EditText dob;
    @Optional
    @InjectView(R.id.dob_spinner)
    ApexSpinner dob_spinner;

    @Optional
    @InjectView(R.id.notes)
    EditText notes;

    @Inject
    ImageUtility mImageUtility;

    private DatePickerFragment mDatePicker;

    @Optional
    @InjectView(R.id.dob_layout) LinearLayout dobLayout;

    @Optional
    @InjectViews({R.id.recording_button_layout, R.id.recording_label_layout}) LinearLayout[] recordingLayouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_patient_dob);
        super.onCreate(savedInstanceState);
        String defaultDate = ApexPrefs.getInstance(getApplicationContext()).getDefaultDob();
        Long defDobMillis = Long.MIN_VALUE;
        Bundle dBun = new Bundle();
        try {
            Date defDob = ApexDatePreference.FMT_DEFAULT.parse(defaultDate);
            defDobMillis = defDob.getTime();
        } catch (java.text.ParseException pex) {
        }
        dBun.putLong(DatePickerFragment.BUNDLE_ARG_DEFAULT_DATE, defDobMillis);
        mDatePicker = new DatePickerFragment();
        mDatePicker.setArguments(dBun);
        mDatePicker.setListener(this);
        initPopupMenu(R.menu.patient_information);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onCreate(this);
        initView();
        initHeader();
        initLists();
        prepareRecording();
    }

    @Override protected void initView() {
        super.initView();
        setupButtonBar("Menu", "Next", false);
        WorkOrderInterface wo = mPresenter.getWorkOrder();
        dob_spinner.setVisibility(View.VISIBLE);
        if (wo != null) {
            name.setText(wo.getPatientName());
            dob.setVisibility(View.GONE);
            if (dob.getText().length() > 0) {
                dob_spinner.setVisibility(View.GONE);
                dob.setVisibility(View.VISIBLE);
            }
        }
        notes.setEnabled(false);
        findViewById(R.id.notes_layout).setVisibility(View.INVISIBLE);

        if (!mPresenter.doVoiceRecording()) {
            ButterKnife.apply(Arrays.asList(recordingLayouts), HIDE);
        }

        if (!mPresenter.doValidateDropdown()) {
            dobLayout.setVisibility(View.GONE);
            mPresenter.setDOB(mPresenter.getDob());
            dob.setText(mPresenter.getDob());
        }

        //boolean recordingExists = FileUtility.fileExists(getAudioImageName());
        enableRightButton();
    }

    protected void initLists() {
        String prevDate = mPresenter.getWorkOrder().getNewDOB();
        boolean isBack = (prevDate != null && prevDate.length() > 0);
        if (isBack) {
            mPresenter.buildList(true); //first position already chosen
        }
        ApexSpinnerAdapter adapter = new ApexSpinnerAdapter(this, android.R.layout.simple_spinner_dropdown_item, mPresenter.getDOBList());
        dob_spinner.setAdapter(adapter);
        dob_spinner.setOnItemSelectedListener(this);
        if (isBack) setSpinnerValue(0);
    }

    private void enableRightButton() {
        boolean recordingExists = FileUtility.fileExistsNonZero(getAudioImageName());
        boolean dobEntered = (dob_spinner.getSelectedItemPosition() >=0 || dob.getText().length() > 0);
        rightButton.setEnabled(recordingExists && dobEntered);
        if (recordingExists) {
            mPresenter.setAudioFile(FileUtility.getFile(getAudioImageName()));
            recordedLabel.setText(R.string.recorded);
            enablePlay();
        } else {
            if (!mPresenter.doVoiceRecording()) {
                mPresenter.setAudioFile(null);
                rightButton.setEnabled(dobEntered);
            } else {
                disablePlay(); //cannot play a staged recording
                recordingExists = FileUtility.fileExistsNonZero(mImageUtility.getFullAudioPath(getAudioImageName()));
                rightButton.setEnabled(recordingExists && dobEntered);
                if (recordingExists) {
                    recordedLabel.setText(R.string.recorded);
                    recordedLabel.setVisibility(View.VISIBLE);
                    recordingLabel.setText(R.string.staged_label);
                }
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String newText = ((month + 1) + "/" + day + "/" + year);
        dob.setText(newText);
        mPresenter.setDOB(newText);
        enableRightButton();
        //notes.setText(RestClient.NOTES_MSG + newText);
    }

    @OnTouch(R.id.dob) boolean onDobTouch(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            showDialog(mDatePicker);
        }
        return true;
    }

    @Override
    protected void setDOB(WorkOrderInterface wo) {
        dobLbl.setVisibility(View.GONE);
        header_dob.setText("");
    }

    @Optional
    @OnClick(R.id.button_left)
    void onMenuClick() {
        mPopupMenu.show();
    }

    @Optional
    @OnClick(R.id.button_right)
    void onNextClick() {
        mPresenter.setNewNote(notes.getText().toString());
        mPresenter.next();
    }

    @Override public void onNothingSelected(AdapterView<?> parent) {}

    /**
     * When the drop-down selection is made
     * @param parent Drop-Down obj
     * @param view
     * @param position selected index
     * @param id ignored by this screen
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getId() == dob_spinner.getId()) {

            String selected = dob_spinner.getSelectedItem().toString();
            dob.setVisibility(View.GONE); //form field hidden
            if (selected.equals(PatientDOBPresenter.NOTA)) {
                dob_spinner.setVisibility(View.GONE);
                dob.setVisibility(View.VISIBLE); //form field displayed
                recordingButton.setEnabled(true);
                dob.setEnabled(true);
                dob.requestFocus();
                showDialog(mDatePicker);
            } else {
                boolean oldValid = DateUtil.areDateStringsEqual(mPresenter.getDob(), selected);
                String newDobStr = (mPresenter.getNewDob() != null && !mPresenter.getNewDob().isEmpty()) ? mPresenter.getNewDob() : null;
                boolean newValid = false;
                if (newDobStr != null) {
                    //we may have matched on the original DOB, but we have a new value we must also check
                    oldValid = false;  //new value outweighs old value
                    newValid = DateUtil.areDateStringsEqual(newDobStr, selected);
                }
                boolean valid = oldValid || newValid;
                if (valid) {
                    mPresenter.setDOB(selected);
                    enableRightButton();
                    recordingButton.setEnabled(true);
                    recordingButton.requestFocus();
                } else {
                    showBadDOBResultDialog();
                    dob_spinner.setOnItemSelectedListener(null);
                    initLists();
                }
            }

        }
    }

    @Override
    protected void setPresenterFile() {
        enableRightButton(); //also sets the file
    }

    @Override protected void setFileKey() {
        fileKey = getPresenter().getWorkOrder().getSpecimenNumber();
    }

    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}

    public void showBadDOBResultDialog() {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
            }
        };

        String text = getString(R.string.bad_dob);
        dialog.setArgs("Patient DOB", text);
        showDialog(dialog);
    }

    private void setSpinnerValue(int pos) {
        try {
            dob_spinner.setSpinnerValue(pos);
        } catch (Exception e) {
            dob_spinner.setSelection(pos);
        }
    }

    //bulk view actions from ButterKnife
    static final ButterKnife.Action<View> HIDE = new ButterKnife.Action<View>() {
        @Override public void apply(View view, int index) {
            view.setVisibility(View.GONE);
        }
    };

}
