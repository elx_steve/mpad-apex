package com.apex.mpad.activity;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.PatientInformationPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class PatientInformationActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

    @Inject PatientInformationPresenterInterface mPresenter;

    @Optional @InjectView(R.id.phone) TextView phone;
    @Optional @InjectView(R.id.alt_name) TextView altName;
    @Optional @InjectView(R.id.alt_phone) TextView altPhone;
    @Optional @InjectView(R.id.patient_info) TextView patientInfo;
    @Optional @InjectView(R.id.order_info) TextView orderInfo;
    @Optional @InjectView(R.id.status) TextView status;
    @Optional @InjectView(R.id.address) TextView address;
    @Optional @InjectView(R.id.detail) TextView detail;

    @Optional @InjectView(R.id.button_left) Button menuButton;
    @Optional @InjectView(R.id.button_right) Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_patient_information);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initHeader();
    }

    @Override
    protected void setDOB(WorkOrderInterface wo) {
        if (wo.isDobValidationReqd()) {
            dobLbl.setVisibility(View.GONE);
            header_dob.setText("");
        } else {
            super.setDOB(wo);
        }
    }

    private void initView() {
        setupButtonBar("Menu", "Next", false);
        initPopupMenu(R.menu.patient_information);
        WorkOrderInterface wo = mPresenter.getWorkOrder();
        if (wo != null) {
            phone.setText(wo.getPatientPhone());
            phone.setPaintFlags(phone.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            altName.setText(wo.getAltContactName());
            altPhone.setText(wo.getAltPhoneNo());
            altPhone.setPaintFlags(altPhone.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            patientInfo.setText(wo.getPatientComments());
            orderInfo.setText(wo.getWorkOrderNotes());
            address.setText(wo.getFullAddress());
            detail.setText(wo.getNotConfirmedReason());

            String statusText = "";
            if (wo.isConfirmed()) {
                statusText = "Confirmed";
            } else {
                statusText = "Not confirmed";
            }
            statusText += "(" + wo.getAppointmentTime() + ")";
            status.setText(statusText);
        } else {
            showNoDataDialog();
        }
    }

    @Optional @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }
    @Optional @OnClick(R.id.button_right) void onNextClick() {mPresenter.next();}

    @Optional @OnClick(R.id.phone) void onPhoneClick() {
        String number = phone.getText().toString();
        makeCall(number);
    }
    @Optional @OnClick(R.id.alt_phone) void onAltPhoneClick() {
        String number = altPhone.getText().toString();
        makeCall(number);
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}

