package com.apex.mpad.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.adapter.PatientAdapter;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.PatientListItemPresenterInterface;
import com.apex.mpad.presenter.iface.PatientListPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class PatientListActivity extends BaseActivity {

    @InjectView(R.id.list) ListView list;
    @InjectView(R.id.header_location) TextView location;

    @Optional @InjectView(R.id.button_left) Button menuButton;
    @Optional @InjectView(R.id.button_right) Button rightButton;

    @Inject PatientListPresenterInterface mPresenter;

    private PatientAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_patient_list);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
    }

    @Override protected void onResume() {
        super.onResume();
        initList();
    }

    private void initView() {
        setupButtonBar("Menu", null, false);
        initPopupMenu(R.menu.patient_list);
        location.setText(mPresenter.getFacilityName());
        rightButton.setVisibility(View.INVISIBLE);
    }

    private void initList() {
        mAdapter = new PatientAdapter(this, mPresenter.getItems());
        list.setAdapter(mAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((PatientListItemPresenterInterface)mAdapter.getItem(position)).startOrder();
            }
        });
    }

    @Optional @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }

    @Override protected BasePresenterInterface getPresenter() {
        return mPresenter;
    }
}
