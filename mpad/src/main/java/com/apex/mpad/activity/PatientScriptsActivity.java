package com.apex.mpad.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Spinner;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.PatientScriptsPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class PatientScriptsActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

    @Inject PatientScriptsPresenterInterface mPresenter;

    @InjectView(R.id.number_additional_scripts) Spinner scriptSpinner;

    @InjectView(R.id.button_right) Button nextButton;
    @InjectView(R.id.button_left) Button menuButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_patient_scripts);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initHeader();
    }

    private void initView() {
        setupButtonBar("Menu", "Next", false);
        initPopupMenu(R.menu.patient_scripts);
    }

    @Optional @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }
    @Optional @OnClick(R.id.button_right) void onNextClick() {
        mPresenter.setNumberAdditionalScripts(Integer.parseInt(scriptSpinner.getSelectedItem().toString()));
        mPresenter.next();
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
