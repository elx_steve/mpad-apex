package com.apex.mpad.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import com.apex.mpad.R;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.PaymentPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class PaymentActivity extends BaseActivity {

    @InjectView(R.id.scroll) ScrollView scroll;

    @InjectView(R.id.amount) TextView amount;
    @InjectView(R.id.reason) TextView reason;
    @InjectView(R.id.patient_info) TextView patientInfo;
    @InjectView(R.id.order_info) TextView orderInfo;

    @InjectView(R.id.collected) EditText collected;
    @InjectView(R.id.notes) EditText notes;

    @InjectView(R.id.button_right) Button nextButton;
    @InjectView(R.id.button_left) Button backButton;

    @Inject PaymentPresenterInterface mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_payment);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initHeader();
    }

    @OnFocusChange(R.id.collected) void onCollectedTouch(View v) {
        focusOnView(v);
    }
    @OnFocusChange(R.id.notes) void onNotesTouch(View v) {
        focusOnView(v);
    }
    private void initView() {
        setupButtonBar("Back", "Next", false);
        nextButton.setEnabled(false);

        WorkOrderInterface wo = mPresenter.getWorkOrder();
        if (wo != null) {
            amount.setText(wo.getCollectAmountTotal());
            reason.setText(wo.getCollectAmountDescription());

            orderInfo.setText(wo.getWorkOrderNotes());
            patientInfo.setText(wo.getPatientComments());
        } else {
            showNoDataDialog();
        }
    }

    @OnClick(R.id.button_right) void onNextClick() {
        mPresenter.next();
    }
    @OnClick(R.id.button_left) void onBackClick() {
        mPresenter.back();
    }

    public void sendFormInfo() {
        mPresenter.setAmountCollected(collected.getText().toString());
        mPresenter.setNotes(notes.getText().toString());
    }

    private void invalidateNextButton() {
        boolean enabled = !(collected.getText().length() == 0) && !(notes.getText().length() == 0);
        nextButton.setEnabled(enabled);
    }
    @OnTextChanged(R.id.collected) void onCollectedChanged() {
        invalidateNextButton();
    }
    @OnTextChanged(R.id.notes) void onNotesChanged() {
        invalidateNextButton();
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
