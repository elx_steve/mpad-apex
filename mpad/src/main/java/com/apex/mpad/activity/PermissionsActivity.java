package com.apex.mpad.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.apex.mpad.R;
import com.apex.mpad.util.ApexApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is required for ADB Debugging.  Only AppManager can force-set app permissions
 */
public class PermissionsActivity extends Activity {
    protected TextView notifications_area;

    private static final Logger mLogger = LoggerFactory.getLogger(PermissionsActivity.class);
    private static final int localIdRequestValue = 55;    // Arbitrary code to identify uniquely the permission request

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);
        final TextView notifications_area = findViewById(R.id.Notifications);

        ApexApplication app = ApexApplication.get();
        app.setCurrentActivity(this);

        mLogger.info("isInit     = " + app.isInit + "\nisLoggedIn = " + app.isLoggedIn);

        if ((ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
            (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
            (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) ||
            (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED))
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,  // Request 0
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,     // Request 1
                            Manifest.permission.READ_EXTERNAL_STORAGE,      // Request 2
                            Manifest.permission.RECORD_AUDIO,               // Request 3
                            Manifest.permission.INTERNET},                  // Request 4
                    localIdRequestValue);
        } else {
            mLogger.info("TFG - Launcher app");

            if (app.EMDKstatus == -1) {
                // Display on the screen an EMDK error notification
                notifications_area.setText(getResources().getString(R.string.emdk_error1));
            }

            if (app.isInit) {
                if (!app.isLoggedIn) {
                    startLogin();
                } else {
                    continueWork();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        mLogger.info("TFG - Received Request permissions answer !");
        ApexApplication app = ApexApplication.get();

        switch (requestCode) {
            case localIdRequestValue: {
                if (grantResults.length > 0
                        && (grantResults[0] != PackageManager.PERMISSION_GRANTED ||
                            grantResults[1] != PackageManager.PERMISSION_GRANTED ||
                            grantResults[2] != PackageManager.PERMISSION_GRANTED ||
                            grantResults[3] != PackageManager.PERMISSION_GRANTED ||
                            grantResults[4] != PackageManager.PERMISSION_GRANTED)) {
                    // Only if ALL permissions are granted the app proceed
                    ExitActivity.exitApplication(this);
                }
                else
                {
                    // All the permissions are granted - start EMDK !
                    app.startEMDK();
                }
            }
        }
    }

    public void startLogin() {
        Intent i = new Intent(this, LoginActivity.class);
        i.putExtra("SHUTDOWN", false);
        startActivity(i);
        finish();
    }

    public void continueWork() {
        Intent i = new Intent(this, WelcomeActivity.class);
        i.putExtra("SHUTDOWN", false);
        startActivity(i);
        finish();
    }
}
