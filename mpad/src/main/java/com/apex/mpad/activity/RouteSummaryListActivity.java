package com.apex.mpad.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import butterknife.InjectView;
import butterknife.OnClick;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.RouteSummaryListItemPresenterInterface;
import com.apex.mpad.presenter.iface.RouteSummaryListPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.adapter.RouteSummaryAdapter;

import javax.inject.Inject;
import java.util.List;


public class RouteSummaryListActivity extends BaseActivity {

    @Inject @ForApplication
    ApexApplication mApp;

    @InjectView(R.id.list) ListView list;
    @InjectView(R.id.button_left) Button failButton;
    @InjectView(R.id.button_right) Button closeButton;

    @Inject RouteSummaryListPresenterInterface mPresenter;

    boolean isFirstLaunch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_route_summary_list);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        //if (isFirstLaunch) mApp.startTimedEvents();
    }

    private void initView() {
        setupButtonBar(null, "Close", false);
        List<RouteSummaryListItemPresenterInterface> routeSummaryItems = mPresenter.getRouteSummaryItems();
        RouteSummaryAdapter adapter = new RouteSummaryAdapter(this, R.layout.list_item_small, routeSummaryItems);
        list.setAdapter(adapter);
        Intent i = getIntent();
        isFirstLaunch = i.getBooleanExtra("isFirstLaunch", false);
    }

    @OnClick(R.id.button_right) void onCloseClick() {
        mPresenter.close(isFirstLaunch);
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
