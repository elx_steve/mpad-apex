package com.apex.mpad.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.apex.mpad.R;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexDatePreference;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.ApexSwitch;
import com.apex.mpad.util.ApexSwitchPref;

import static android.content.SharedPreferences.OnSharedPreferenceChangeListener;

public class SettingsActivity extends PreferenceActivity implements ApexSwitchPref.OnSwitchPrefCreatedListener {
    SharedPreferences prefs;
    ApexPrefs apexPrefs;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apexPrefs = ApexPrefs.getInstance(getApplicationContext());
    }

    @Override protected void onResume() {
        super.onResume();
        getFragmentManager().beginTransaction()
                .add(android.R.id.content, new SettingsFragment())
                .commit();
        getFragmentManager().executePendingTransactions();
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }
    @Override public boolean isValidFragment(String fragName) {
        return SettingsFragment.class.getName().equals(fragName);
    }

    @Override public void onSwitchPrefCreated(ApexSwitchPref switchPref) {
        String key = switchPref.getKey();
        Switch switchView = switchPref.getSwitch();
//        switchView.setOnClickListener(switchListener);
        if (key.equals(ApexPrefs.PREF_LOCAL_DEV)) {
            switchView.setChecked(apexPrefs.getLocalDev());
        }

        switchView.setOnCheckedChangeListener(checkListener);
    }

    private class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {
        public SettingsFragment() {
        }

        @Override public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override public void onResume() {
            super.onResume();
            addPreferencesFromResource(R.xml.preferences);
            prefs.registerOnSharedPreferenceChangeListener(this);
            ApexPrefs apexPrefs = ApexPrefs.getInstance(getApplicationContext());

            ApexSwitchPref.OnSwitchPrefCreatedListener listener = (ApexSwitchPref.OnSwitchPrefCreatedListener)getActivity();

            ApexSwitchPref prefLocalDev = (ApexSwitchPref) findPreference(ApexPrefs.PREF_LOCAL_DEV);

            prefLocalDev.setSwitchText(getString(R.string.pref_toggle_on), getString(R.string.pref_toggle_off));
            prefLocalDev.setChecked(apexPrefs.getLocalDev());
            prefLocalDev.setListener(listener);

        }

        @Override public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Preference pref = findPreference(key);

            if (pref instanceof EditTextPreference) {
                EditTextPreference editPref = (EditTextPreference) pref;
                pref.setSummary(editPref.getText());
            }
            if (pref == findPreference(ApexPrefs.PREF_TRANSMIT_INTERVAL)) {
                ((ApexApplication) getApplication()).resetTimedEvents();
            }

            if (pref instanceof ApexDatePreference){
                ApexDatePreference dPref = (ApexDatePreference)pref;
                pref.setSummary(apexPrefs.getDefaultDob());
            }
        }

        public void onPause() {
            prefs.unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

    }
    public Switch.OnCheckedChangeListener checkListener = new Switch.OnCheckedChangeListener() {
        @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ApexSwitch switchView = (ApexSwitch) buttonView;
            String key = switchView.getParentKey();

            if (key.equals(ApexPrefs.PREF_LOCAL_DEV)) {
                apexPrefs.setLocalDev(isChecked);
            }
        }
    };
}
