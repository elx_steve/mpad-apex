package com.apex.mpad.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.apex.mpad.R;
import com.apex.mpad.adapter.ApexSpinnerAdapter;
import com.apex.mpad.adapter.SpecimenRequirementAdapter;
import com.apex.mpad.model.iface.DrawSiteInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.SpecimenRequirementListPresenterInterface;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.base.BasePrinterActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;


public class SpecimenRequirementListActivity extends BasePrinterActivity implements PopupMenu.OnMenuItemClickListener, Spinner.OnItemSelectedListener {
    private static final String DIALOG_NOT_PRINTED = "You have not printed all of the labels are you sure you want to continue?";

    @Inject SpecimenRequirementListPresenterInterface mPresenter;

    @InjectView(R.id.draw_site_spinner) Spinner drawSiteSpinner;
    @InjectView(R.id.list) ListView list;
    @InjectView(R.id.button_left) Button menuButton;
    @InjectView(R.id.button_right) Button nextButton;
    @InjectView(R.id.specimen_number) TextView specimenNumber;

    @InjectView(R.id.button_print_all) Button printAllButton;

    private boolean isFirstSelect = true;
    private boolean isScanPrinterFinish = false;
    private boolean isNextButtonShadow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_specimen_requirement_list);
        super.onCreate(savedInstanceState);
        initPopupMenu(R.menu.specimen_requirement);
    }

    @Override protected void onResume() {
        super.onResume();
        mPresenter.onCreate(this);
        if (mPrefs.getMacAddress() == null || mPrefs.getMacAddress().isEmpty()) {
            // Disable all buttons
            menuButton.setEnabled(false);
            helpButton.setEnabled(false);
            isNextButtonShadow = false;
            isScanPrinterFinish = false;
            manageNextButton();

            discoverPrinters();
            showLoading();
        } else {

        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initView();
                initLists();
                initHeader();
            }
        });
    }

    @Override
    public void discoveryFinished() {
        super.discoveryFinished();
        if (!printersFound) {
            showErrorDialog("Printer could not be found. Please ensure it is on and paired.");
        }
        stopLoading();
        // Enable buttons
        menuButton.setEnabled(true);
        helpButton.setEnabled(true);

        isScanPrinterFinish = true;
        manageNextButton();
    }

    private void initView() {
        setupButtonBar("Menu", "Next", false);
        nextButton.setEnabled(false);
        isNextButtonShadow = false;

        WorkOrderInterface wo = mPresenter.getWorkOrder();
        if (wo!=null) {
            specimenNumber.setText(wo.getSpecimenNumber());
        } else {
            showNoDataDialog();
        }
    }

    private void initLists() {
        List<String> drawSiteNames = mPresenter.getDrawSiteNames();
        drawSiteNames.add(0, "");

        ApexSpinnerAdapter adapter = new ApexSpinnerAdapter(this, R.layout.spinner_drop_down, drawSiteNames);
        drawSiteSpinner.setAdapter(adapter);
        drawSiteSpinner.setOnItemSelectedListener(this);

        SpecimenRequirementAdapter specimenAdapter = new SpecimenRequirementAdapter(this, mPresenter.getSpecimenRequirementList());
        list.setAdapter(specimenAdapter);
    }

    @OnClick(R.id.button_left) public void onMenuClick() {
        mPopupMenu.show();
    }
    @OnClick(R.id.button_right) public void onNextClick() {
        if (!mPresenter.hasAllPrinted()) {
            showErrorDialog(DIALOG_NOT_PRINTED);
        } else {
            mPresenter.next();
        }
    }

    @OnClick(R.id.button_print_all) public void onPrintAllClick() {
        mPresenter.printAll();
    }

    public void showErrorDialog(String dialogNotPrinted) {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
                mPresenter.next();
            }
            @Override public void cancel() {
                dismiss();
            }
        };
        dialog.setArgs("Confirm", dialogNotPrinted, "Yes", "No");
        showDialog(dialog);
    }

    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (isFirstSelect) {
            isFirstSelect = false;
        } else if (position == 0) {
            isNextButtonShadow = false;
        } else {
            String siteName = parent.getItemAtPosition(position).toString();
            DrawSiteInterface drawSite = mPresenter.getDrawSiteWithName(siteName);
            if (drawSite != null) {
                mPresenter.setDrawSite(drawSite.getId());
            }
                isNextButtonShadow = true;
        }
        manageNextButton();
    }

    @Override public void onNothingSelected(AdapterView<?> parent) {

    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}

    public void updateList() {
        ((SpecimenRequirementAdapter)list.getAdapter()).notifyDataSetChanged();
    }

    // The function manage the Next button enable/disable. It depends about the choice on onItemSelected
    // and the scan/search printer task ended
    // If the printer is found or if is not found but the search is ended, re-enable the button.
    private void manageNextButton() {
        if(isNextButtonShadow && (isScanPrinterFinish || printersFound)) {
            nextButton.setEnabled(true);
        } else {
            nextButton.setEnabled(false);
        }
    }
}
