package com.apex.mpad.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import com.apex.mpad.R;
import com.apex.mpad.adapter.ApexSpinnerAdapter;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.TubeFailReasonPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.List;


public class TubeFailReasonActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    @InjectView(R.id.button_left) Button menuButton;
    @InjectView(R.id.button_right) Button nextButton;

    @InjectView(R.id.notes) EditText notes;

    @InjectView(R.id.reason_spinner) Spinner reasonSpinner;

    @Inject TubeFailReasonPresenterInterface mPresenter;

    private boolean isFirstSelect = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_tube_fail_reason);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initList();
    }

    private void initView() {
        setupButtonBar("Back", "Next", false);
        initPopupMenu(R.menu.visit_route);
        nextButton.setEnabled(false);
    }

    private void initList() {
        List<String> failReasons = mPresenter.getFailReasons();
        failReasons.add(0, "");

        ApexSpinnerAdapter adapter = new ApexSpinnerAdapter(this, R.layout.spinner_text, failReasons);
        reasonSpinner.setAdapter(adapter);
        reasonSpinner.setOnItemSelectedListener(this);
    }

    @OnFocusChange(R.id.notes) void onNotesFocus(View v, boolean hasFocus) {
        if (hasFocus)
            focusOnView(v);
    }

    @OnClick(R.id.button_left) public void onBackClick() {
        mPresenter.back();
    }
    @OnClick(R.id.button_right) public void onNextClick() {
        mPresenter.setNotes(notes.getText().toString());
        mPresenter.next();
    }

    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (isFirstSelect) {
            isFirstSelect = false;
        } else if (position == 0) {
            resetButtonEnabled();
        } else {
            mPresenter.setFailReason(parent.getItemAtPosition(position).toString());
            resetButtonEnabled();
        }
    }

    @OnTextChanged(R.id.notes) void onNotesChanged() {
        resetButtonEnabled();
    }

    private void resetButtonEnabled() {
        boolean enabled = reasonSpinner.getSelectedItemPosition() != 0 && notes.getText().length() > 0;
        nextButton.setEnabled(enabled);
    }

    @Override public void onNothingSelected(AdapterView<?> parent) {}
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
