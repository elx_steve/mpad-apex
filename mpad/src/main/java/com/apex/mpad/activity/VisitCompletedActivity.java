package com.apex.mpad.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTouch;

import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.VisitCompletedPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.ApexScrollView;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.FileUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;


public class VisitCompletedActivity extends BaseActivity {

    @InjectView(R.id.signature) GestureOverlayView gesture;
    @InjectView(R.id.signature_bmp) ImageView gestureBmp;

    @InjectView(R.id.button_back) Button backButton;
    @InjectView(R.id.button_eraser) ImageButton eraserButton;
    @InjectView(R.id.button_left) Button menuButton;
    @InjectView(R.id.button_right) Button nextButton;
    @InjectView(R.id.button_clear) Button clearButton;
    @InjectView(R.id.gesture_layout) ViewGroup gestureLayout;

    @InjectView(R.id.total_collected) TextView totalCollected;
    @InjectView(R.id.patient_name) TextView patientName;
    @InjectView(R.id.comments) EditText comments;

    @InjectView(R.id.uts_check) CheckBox utsCheck;

    @InjectView(R.id.scroll) ApexScrollView scroll;

    @Inject @ForApplication ApexApplication mApp;
    @Inject VisitCompletedPresenterInterface mPresenter;

    private long mLastClick = 0;

    private static final Logger mLogger = LoggerFactory.getLogger(VisitCompletedActivity.class);

    private boolean sigStarted = false;

    public boolean isFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_visit_completed);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initHeader();
    }

    @Override protected void setOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isFinishing()) {
            saveSignature();
            saveFields();
        } else {
            FileUtility.deleteFile(getFileName());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initView() {
        scroll.setScrollingEnabled(false);
        gesture.setGestureStrokeType(GestureOverlayView.GESTURE_STROKE_TYPE_MULTIPLE);
        WorkOrderInterface wo = mPresenter.getWorkOrder();
        if (wo != null) {
            boolean showBackButton = wo.getPatientLastName() == null || wo.getPatientLastName().isEmpty();
            int orientation = this.getResources().getConfiguration().orientation;
            findViewById(R.id.signature_layout).setVisibility(View.VISIBLE);
            backButton.setVisibility(View.INVISIBLE);
            eraserButton.setVisibility(View.GONE);
            ViewGroup.LayoutParams lp = gestureLayout.getLayoutParams();
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                lp.height = 150;
                if (showBackButton) {
                    setupButtonBar("Back", "Done", false);
                } else {
                    setupButtonBar(null, "Done", false);
                }
            } else {
                lp.height = 200;
                if (showBackButton) {
                    setupButtonBar("Back", "Done", false);
                } else {
                    setupButtonBar(null, "Done", false);
                }
                backButton.setVisibility(View.GONE);
                eraserButton.setVisibility(View.VISIBLE);
                findViewById(R.id.signature_layout).setVisibility(View.GONE);
            }
            gestureLayout.setLayoutParams(lp);

            initPopupMenu(R.menu.visit_route);

            totalCollected.setText(wo.getAmountActuallyCollected() != null ? wo.getAmountActuallyCollected() : String.valueOf(0));
            patientName.setText(wo.getPatientName());
            comments.setText(wo.getCompletedWorkOrderNotes());

            if (FileUtility.fileExists(getFileName())) {
                gesture.setVisibility(View.GONE);
                gestureBmp.setImageBitmap(BitmapFactory.decodeFile(getFileName()));
                gestureBmp.setVisibility(View.VISIBLE);
                resizeBmp();
                mPresenter.setImageFile(FileUtility.getFile(getFileName()));
                mPresenter.setHasSignature(true);
            }
            comments.setText(mPresenter.getWorkOrder().getCompletedWorkOrderNotes());
            utsCheck.setChecked(mPresenter.getWorkOrder().isPatientUnableToSign());

            focusOnTop(gestureLayout);

        } else {
            showNoDataDialog();
        }
    }

    private void resetGesture() {
        gestureBmp.setVisibility(View.GONE);
        FileUtility.deleteFile(getFileName());
        mPresenter.setImageFile(null);
        mPresenter.setHasSignature(false);
        gesture.clear(false);
        gesture.setVisibility(View.VISIBLE);
        sigStarted = false;
    }

    private void resizeBmp() {
        Display display = getWindowManager().getDefaultDisplay();
        Point screenWidth = new Point();
        display.getSize(screenWidth);
        gestureBmp.setMinimumWidth(screenWidth.x);
        gestureBmp.refreshDrawableState();
    }

    @OnTouch(R.id.signature_bmp) public boolean onBmpTouch(MotionEvent event) {
        resizeBmp();
        return true;
    }

    @OnTouch(R.id.signature) public boolean onSigTouch(MotionEvent event) {
        sigStarted = true;
        return true;
    }

    public String getFileName() {
        return (Environment.getExternalStorageDirectory() + File.separator + mPresenter.getWorkOrder().getSpecimenNumber() + "_signature" + IMAGE_FORMAT_EXT);
    }

    private void saveSignature() {
        showLoading();
        gesture.setDrawingCacheEnabled(true);
        Bitmap cache = gesture.getDrawingCache();
        if (cache != null && sigStarted) {
            Bitmap bm = Bitmap.createBitmap(gesture.getDrawingCache());

            File f = new File(getFileName());
            try {
                FileUtility.deleteFile(getFileName());
                f.createNewFile();
                FileOutputStream os = new FileOutputStream(f);
                bm.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.close();
                mPresenter.setImageFile(f);
                mPresenter.setHasSignature(true);
            } catch (Exception e) {
                mLogger.warn("Unable to save signature: " + e.getMessage(), e);
            }
        } else {
            if (FileUtility.fileExists(getFileName())) {
                mPresenter.setImageFile(FileUtility.getFile(getFileName()));
                mPresenter.setHasSignature(true);
            }
        }
        stopLoading();
    }

    private void saveFields() {
        mPresenter.setCompletedWorkOrderNotes(comments.getText().toString());
        mPresenter.setPatientUnableToSign(utsCheck.isChecked());
    }

    @OnClick(R.id.button_right) void onNextClick() {
        if (System.currentTimeMillis() - mLastClick < 1000){
            return;
        }
        mLastClick = System.currentTimeMillis();
        Thread thread = new Thread() {
            @Override public void run() {
                saveSignature();
                saveFields();
                mPresenter.next();
            }
        };
        thread.start();

    }

    @OnClick(R.id.button_clear) void onClearClick() {
        resetGesture();
    }
    @OnClick(R.id.button_eraser) void onBackClick() {
        clearButton.performClick();
    }

    @OnClick(R.id.button_left) void onLeftClick() {mPresenter.back();}

    @OnFocusChange(R.id.comments) void onCommentFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            focusOnView(v);
        }
    }

    public void showConfirmDialog(String messageCancelNewPatient) {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                mApp.getCurrentVisit().getWorkOrders().remove(mApp.getCurrentWorkOrder());
                startActivity(new Intent(getActivity(), NewPatientScriptActivity.class));
            }
            @Override public void cancel() {
                dismiss();
            }
        };
        dialog.setArgs("Confirm", messageCancelNewPatient, "Yes", "No");
        showDialog(dialog);
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
