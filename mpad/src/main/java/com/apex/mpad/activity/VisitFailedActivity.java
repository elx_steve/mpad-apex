package com.apex.mpad.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.*;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.adapter.ApexSpinnerAdapter;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.VisitFailedPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.base.BasePrinterActivity;

import javax.inject.Inject;
import java.util.List;


public class VisitFailedActivity extends BasePrinterActivity implements AdapterView.OnItemSelectedListener {

    @Inject @ForApplication ApexApplication mApp;
    @Inject VisitFailedPresenterInterface mPresenter;

    @InjectView(R.id.reason_spinner) Spinner reasonSpinner;
    @InjectView(R.id.progress_spinner) ProgressBar mProgressBar;
    @InjectView(R.id.button_left) Button menuButton;
    @InjectView(R.id.button_right) Button nextButton;
    @InjectView(R.id.print_button) Button printButton;
    @InjectView(R.id.notes) EditText notes;
    @InjectView(R.id.header_patient_name) TextView patientName;
    @InjectView(R.id.header_dob) TextView dob;
    @InjectView(R.id.header_sex) TextView sex;

    private boolean isFirstSelect = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_visit_failed);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        if (mPrefs.getMacAddress() == null || mPrefs.getMacAddress().isEmpty()) {
            // Hide buttons
            menuButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.INVISIBLE);
            helpButton.setVisibility(View.INVISIBLE);
            discoverPrinters();
            showLoading();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initView();
                initList();
                initHeader();
            }
        });
    }

    private void initView() {
        setupButtonBar("Back", "Continue", false);
    }
    private void initList() {
        List<String> failReasons = mPresenter.getFailureReasons();
        failReasons.add(0, "");
        ApexSpinnerAdapter failReasonAdapter = new ApexSpinnerAdapter(this, R.layout.spinner_text, failReasons);
        reasonSpinner.setAdapter(failReasonAdapter);
        reasonSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void discoveryFinished() {
        super.discoveryFinished();
        stopLoading();
        // Show buttons
        menuButton.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);
        helpButton.setVisibility(View.VISIBLE);
    }


    public void showCompleteDialog() {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
                mApp.shutdown(true, false);
            }
        };
        dialog.setArgs(getResources().getString(R.string.dialog_complete_message));
        showDialog(dialog);
    }
    @Optional @OnClick(R.id.button_left) public void onBackClick() {
        mPresenter.back();
    }
    @Optional @OnClick(R.id.button_right) public void onNextClick() {
        mPresenter.setFailureComments(notes.getText().toString());
        showLoading();
        new Thread(new Runnable() {
            @Override public void run() {
                mPresenter.next();
            }
        }).start();
    }
    @Optional @OnClick(R.id.print_button) public void onPrintClick() {
        showLoading();
        Thread thread = new Thread(new Runnable() {
            @Override public void run() {
                mPresenter.print();
                runOnUiThread(new Runnable() {
                    @Override public void run() {
                        stopLoading();
                    }
                });
            }
        });
        thread.start();
    }

    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (isFirstSelect) {
            isFirstSelect = false;
        } else if (position == 0) {
            nextButton.setEnabled(false);
        } else {
            mPresenter.setFailureReason(parent.getItemAtPosition(position).toString());
            nextButton.setEnabled(true);
        }
    }

    @Override public void onNothingSelected(AdapterView<?> parent) {}
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
