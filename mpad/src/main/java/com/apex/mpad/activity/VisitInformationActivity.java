package com.apex.mpad.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.VisitInformationPresenterInterface;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class VisitInformationActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

    @Inject VisitInformationPresenterInterface mPresenter;

    @Optional @InjectView(R.id.header_patient_name) TextView headerName;
    //@Optional @InjectView(R.id.header_dob) TextView headerDob;
    @Optional @InjectView(R.id.header_sex) TextView headerSex;
    @Optional @InjectView(R.id.insurance) TextView insurance;
    @Optional @InjectView(R.id.md) TextView md;
    @Optional @InjectView(R.id.hc) TextView hc;
    @Optional @InjectView(R.id.md2) TextView md2;
    @Optional @InjectView(R.id.freq) TextView freq;
    @Optional @InjectView(R.id.fast) TextView fast;
    @Optional @InjectView(R.id.tests) TextView tests;
    @Optional @InjectView(R.id.patient_info) TextView patientInfo;
    @Optional @InjectView(R.id.order_info) TextView orderInfo;

    @Optional @InjectView(R.id.button_left) Button menuButton;
    @Optional @InjectView(R.id.button_right) Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_visit_information);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initView();
        initHeader();
    }

    private void initView() {
        setupButtonBar("Menu", "Next", false);
        initPopupMenu(R.menu.visit_information);

        WorkOrderInterface wo = mPresenter.getWorkOrder();
        if (wo != null) {
            insurance.setText(wo.getInsuranceType());

            if (wo.isCashCheck()) {
                insurance.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
            md.setText(wo.getDoctorName());
            hc.setText(wo.getAgencyName());
            md2.setText(wo.getAdditionalDoctorName1());
            freq.setText(wo.getFrequency());
            fast.setText((wo.getIsFasting().equalsIgnoreCase("true") ? "Yes" : "No"));
            tests.setText(wo.getTestNames());
            patientInfo.setText(wo.getPatientComments());
            orderInfo.setText(wo.getWorkOrderNotes());
        } else {
            showNoDataDialog();
        }
    }

    @Optional @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }
    @Optional @OnClick(R.id.button_right) void onNextClick() {mPresenter.next();}

    @Override protected BasePresenterInterface getPresenter() {
        return mPresenter;
    }
}
