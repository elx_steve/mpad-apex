package com.apex.mpad.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.VisitRoutePresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;


public class VisitRouteActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {
    private static final String MESSAGE_CHECK_CONFIRMED = "Please select that you have or have not confirmed the visit";

    @Inject VisitRoutePresenterInterface mPresenter;
    @Inject @ForApplication ApexApplication mApp;

    @Optional @InjectView(R.id.confirmed_layout) ViewGroup confirmedLayout;

    @Optional @InjectView(R.id.status) TextView status;
    @Optional @InjectView(R.id.phone) TextView phone;
    @Optional @InjectView(R.id.alt_phone) TextView altPhone;
    @Optional @InjectView(R.id.alt_name) TextView altName;
    @Optional @InjectView(R.id.address) TextView address;
    @Optional @InjectView(R.id.patient_info) TextView patientInfo;
    @Optional @InjectView(R.id.order_info) TextView orderInfo;
    @Optional @InjectView(R.id.detail) TextView detail;

    @Optional @InjectView(R.id.header_detail_layout) ViewGroup headerDetailLayout;
    @Optional @InjectView(R.id.header_patient_name) TextView patientName;

    @Optional @InjectView(R.id.button_left) Button menuButton;
    @Optional @InjectView(R.id.button_right) Button routeButton;
    @Optional @InjectView(R.id.button_radio_confirmed) RadioButton buttonConfirmed;
    @Optional @InjectView(R.id.button_radio_not_confirmed) RadioButton buttonNotConfirmed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_visit_route);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initHeader();
        initView();
    }
    @Override protected void onResume() {
        super.onResume();
        mApp.resetTimedEvents();
    }

    @Override protected void onPause() {
        //mApp.cancelTimedEvents(); //do not pause data collection
        super.onPause();
    }

    @Override protected void setDOB(WorkOrderInterface wo) {
        dobLbl.setVisibility(View.GONE);
        header_dob.setText("");
    }

    private void initView() {
        menuButton.setVisibility(View.VISIBLE);
        setupButtonBar("Menu", "Route", false);
        initPopupMenu(R.menu.visit_route);

        WorkOrderInterface wo = mPresenter.getWorkOrder();
        if (wo != null) {
            if (mPresenter.hasMultiplePatients()) {
                setPatientNameMultiPatient(wo);
                headerDetailLayout.setVisibility(View.INVISIBLE);
                status.setText(TEXT_EMPTY);
                phone.setText(TEXT_EMPTY);
                phone.setClickable(false);
                phone.setTextColor(getResources().getColor(android.R.color.black));

                altPhone.setText(TEXT_EMPTY);
                altPhone.setClickable(false);
                altPhone.setTextColor(getResources().getColor(android.R.color.black));

                altName.setText(TEXT_EMPTY);
                confirmedLayout.setVisibility(View.INVISIBLE);
                mPopupMenu.getMenu().findItem(R.id.menu_fail).setEnabled(false);
                patientInfo.setText(TEXT_MULTIPLE);
                orderInfo.setText("");
            } else {
                initHeader();
                phone.setText(wo.getPatientPhone());
                phone.setPaintFlags(phone.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                altName.setText(wo.getAltContactName());
                altPhone.setText(wo.getAltPhoneNo());
                altPhone.setPaintFlags(altPhone.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                patientInfo.setText(wo.getPatientComments());
                orderInfo.setText(wo.getWorkOrderNotes());
                detail.setText(wo.getNotConfirmedReason());

                if (mPresenter.getWorkOrder().isConfirmed()) {
                    confirmedLayout.setVisibility(View.INVISIBLE);
                } else {
                    confirmedLayout.setVisibility(View.VISIBLE);
                }
                String statusText = "";
                if (wo.isConfirmed()) {
                    statusText = "Confirmed";
                } else {
                    statusText = "Not confirmed";
                }
                statusText += "(" + wo.getAppointmentTime() + ")";
                status.setText(statusText);
            }
            address.setText(wo.getFullAddress());
        } else {
            showNoDataDialog();
        }
    }

    private void setPatientNameMultiPatient(WorkOrderInterface wo) {
        String text = TEXT_MULTIPLE;
        if (mPresenter.hasMultiplePatients()) {
            if (wo.getDrawAreaUnit() != null && !(wo.getDrawAreaUnit().isEmpty()))
                text = wo.getDrawAreaUnit();
        }
        patientName.setText(text);
    }

    public void makeCall(String number) {
        String text   = "tel:"+number;
        Uri uri = Uri.parse(text);
        startActivity(new Intent(Intent.ACTION_DIAL, uri));
    }

    @OnClick(R.id.phone) void onPhoneClick() {
        if (!mPresenter.hasMultiplePatients()) {
            String number = phone.getText().toString();
            makeCall(number);
        }
    }
    @OnClick(R.id.alt_phone) void onAltPhoneClick() {
        if (!mPresenter.hasMultiplePatients()) {
            String number = altPhone.getText().toString();
            makeCall(number);
        }
    }

    @OnClick(R.id.button_right) void onRouteButton() {
        mApp.startTimedEvents();
        if (confirmedLayout.getVisibility() == View.VISIBLE) {
            if (!buttonConfirmed.isChecked() && !buttonNotConfirmed.isChecked()) {
                showMessageDialog(MESSAGE_CHECK_CONFIRMED);
            }
            mPresenter.getWorkOrder().setConfirmed(buttonConfirmed.isChecked());
        }

        new Thread(new Runnable() {
            @Override public void run() {
                checkForUpload();
                mPresenter.next();
            }
        }).start();
    }

    @Optional @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
