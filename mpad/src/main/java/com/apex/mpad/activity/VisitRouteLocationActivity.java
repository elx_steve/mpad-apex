package com.apex.mpad.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.VisitRouteLocationPresenterInterface;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Page with "Start Visit" button, after the GPS routing finishes
 */
public class VisitRouteLocationActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {

    @Inject VisitRouteLocationPresenterInterface mPresenter;
    @Inject @ForApplication ApexApplication mApp;
    @Inject NavigationManager mNavManager;

    @Optional @InjectView(R.id.destination) TextView destination;
    @Optional @InjectView(R.id.button_left) Button menuButton;
    @Optional @InjectView(R.id.button_right) Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_visit_route_location);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
        initPopupMenu(R.menu.visit_route_location);
    }

    @Override protected void onResume() {
        super.onResume();
        mNavManager.setIsNavigating(false);
        mApp.resetTimedEvents();
        initView();
        initHeader();
    }

    @Override protected void onPause() {
        //mApp.cancelTimedEvents();
        super.onPause();
    }

    private void initView() {
        setupButtonBar("Menu", "Start Visit", false);

        WorkOrderInterface wo = mPresenter.getWorkOrder();
        if (wo != null) {
            destination.setText(wo.getFullAddress());
        } else {
            showNoDataDialog();
        }
    }

    @OnClick(R.id.button_right) void onStartClick() {
        if (mApp.isUploading()) {
            ApexDialog dialog = new ApexDialog() {
                @Override public void confirm() {
                    dismiss();
                }
            };
            dialog.setArgs("Upload in Progress", "Your data is being uploaded, please try again momentarily", "OK");
            showDialog(dialog);
        } else {
            mPresenter.next();
        }
    }

    @OnClick(R.id.button_left) void onMenuClick() {
        mPopupMenu.show();
    }

    @Override protected BasePresenterInterface getPresenter() {
        return mPresenter;
    }
}


