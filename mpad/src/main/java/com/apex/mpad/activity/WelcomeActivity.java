package com.apex.mpad.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.presenter.BasePresenter;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.presenter.iface.WelcomePresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.base.BaseActivity;

import java.util.Locale;

import javax.inject.Inject;

//This activity is the main screen which shows the number of remaining visits and allows the user to "begin"

public class WelcomeActivity extends BaseActivity {

    private static final String MESSAGE_REFRESH = "You only have skipped patients remaining. Please Refresh your work orders.";
    private static final String MESSAGE_RETURN = "Visits complete. Please return to the lab.";

    public static final String MESSAGE_REFRESH_SUCCESS = "Your work orders have been successfully updated";
    public static final String MESSAGE_REFRESH_FAIL = "Your work orders could not be updated. Please try again shortly or contact your administrator if the problem persists";

    @Inject WelcomePresenterInterface mPresenter;
    @Inject @ForApplication ApexApplication mApp;

    @Optional @InjectView(R.id.next_visit_layout) ViewGroup nextVisitLayout;
    @Optional @InjectView(R.id.stops_remaining_layout) ViewGroup stopsRemainingLayout;
    @Optional @InjectView(R.id.patients_remaining_layout) ViewGroup patientsRemainingLayout;
    @Optional @InjectView(R.id.patients_skipped_layout) ViewGroup patientsSkippedLayout;
    @Optional @InjectView(R.id.pending_uploads_layout) ViewGroup pendingUploadsLayout;

    @Optional @InjectView(R.id.next_visit) TextView nextVisit;
    @Optional @InjectView(R.id.stops_remaining) TextView stopsRemaining;
    @Optional @InjectView(R.id.patients_remaining) TextView patientsRemaining;
    @Optional @InjectView(R.id.patients_skipped) TextView patientsSkipped;
    @Optional @InjectView(R.id.pending_uploads) TextView pendingUploads;

    @Optional @InjectView(R.id.button_left) Button homeButton;
    @Optional @InjectView(R.id.button_right) Button beginButton;
    @Optional @InjectView(R.id.button_refresh) Button refreshButton;
    @Optional @InjectView(R.id.button_shutdown) Button shutdownButton;

    private boolean mIsReturning;
    private long mLastClick = 0;
    private boolean out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_welcome);
        super.onCreate(savedInstanceState);
        mPresenter.onCreate(this);
    }

    @Override protected void onResume() {
        super.onResume();
        mPresenter.onCreate(this);
        initView();
        mApp.startTimedEvents();
    }

    @Override protected void onPause() {
        mApp.cancelTimedEvents();
        super.onPause();
    }
    private void initView() {
        setupButtonBar("Home", "Begin", false);
        String message = "";
        if (mPresenter.getVisitsComplete()) {
            mPresenter.refreshOrders(false);
        }
        handleRefreshButton();
        if (mPresenter.getVisitsComplete()) {
            nextVisitLayout.setVisibility(View.INVISIBLE);
            stopsRemainingLayout.setVisibility(View.INVISIBLE);
            patientsRemainingLayout.setVisibility(View.INVISIBLE);
            patientsSkippedLayout.setVisibility(View.INVISIBLE);
            pendingUploadsLayout.setVisibility(View.INVISIBLE);
            if (mPresenter.hasSkippedPatients()) {
                refreshButton.setVisibility(View.VISIBLE);
                showSkippedRefreshDialog();
            } else if (!mIsReturning) {
                refreshButton.setVisibility(View.INVISIBLE);
                shutdownButton.setVisibility(View.VISIBLE);
                beginButton.setEnabled(false);
                showReturnDialog();
            }
        } else {
            setNextVisitText();
            stopsRemaining.setText(mPresenter.getStopsRemaining());
            patientsRemaining.setText(mPresenter.getPatientsRemaining());
            patientsSkipped.setText(mPresenter.getPatientsSkipped());

            String pendingUploadsText = mPresenter.getPendingUploads();
            if (!pendingUploadsText.equals("")) {
                pendingUploadsLayout.setVisibility(View.VISIBLE);
                pendingUploads.setText(pendingUploadsText);
                mPresenter.uploadFiles();
            } else {
                pendingUploadsLayout.setVisibility(View.INVISIBLE);
            }
        }

    }

    @Override
    public void handleRefreshButton() {
        refreshButton.setBackgroundResource(R.drawable.button);
        if (mPresenter.hasMoreWorkOrdersNotification()) {
            refreshButton.setBackgroundResource(R.drawable.button_red);
        }
    }

    private void setNextVisitText() {
        String text = mPresenter.getPatientName();
        if (mPresenter.getDrawAreaType().equals(BasePresenter.HOUSE_CALL)) {
            if (mPresenter.getNextPatientCount() > 1) {
                text = String.format(Locale.US, "%d patients", mPresenter.getNextPatientCount());
            }
        } else {
            if (mPresenter.getDrawAreaUnit() != null && !(mPresenter.getDrawAreaUnit().isEmpty()))
                text = mPresenter.getDrawAreaUnit();
        }
        nextVisit.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean out = super.onCreateOptionsMenu(menu);
        if (shutdownButton.getVisibility() == View.VISIBLE) {
            for (int m = 0; m < menu.size(); m++) {
                if (menu.getItem(m).getItemId() == R.id.action_close) {
                    menu.getItem(m).setEnabled(false);
                    break;
                }
            }
        }
        return out;
    }

    /**
     * This is the handler for the X close button
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_close:
                if (shutdownButton.getVisibility() == View.VISIBLE) {
                    showReturnDialog();
                } else {
                    mApp.shutdown(true, false);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showReturnDialog() {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
                mIsReturning = true;
                mPresenter.routeBackToApex();
            }
        };
        dialog.setArgs("Return", MESSAGE_RETURN, "Return", "Cancel");
        showDialog(dialog);
    }

    private void showSkippedRefreshDialog() {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
                mPresenter.refreshOrders(true);
            }
        };
        dialog.setArgs("Refresh", MESSAGE_REFRESH, "Refresh", "Cancel");
        showDialog(dialog);
    }

    public void showRefreshResultMessage(boolean isSuccess) {
        ApexDialog dialog;
        if (isSuccess) {
            dialog = new ApexDialog() {
                @Override public void confirm() {
                    dismiss();
                    finish();
                    startActivity(getIntent());
                }
            };
            dialog.setArgs(MESSAGE_REFRESH_SUCCESS);
        } else {
            dialog = new ApexDialog() {
                @Override public void confirm() {
                    dismiss();
                }
            };
            dialog.setArgs(MESSAGE_REFRESH_FAIL);
        }
        showDialog(dialog);
    }

    @Optional @OnClick(R.id.button_refresh) void onRefreshClick() {
        if (System.currentTimeMillis() - mLastClick < 1000){
            return;
        }
        mLastClick = System.currentTimeMillis();
        mPresenter.refreshOrders(true);
    }
    @Optional @OnClick(R.id.button_shutdown) void onShutdownButton() { mApp.shutdown(true, true); }
    @Optional @OnClick(R.id.button_left) void onHomeClick() {
        startActivity(new Intent(this, HomeActivity.class));
    }
    @Optional @OnClick(R.id.button_right) void onBeginClick() {
        new Thread(new Runnable() {
            @Override public void run() {
                checkForUpload();
                startActivity(new Intent(WelcomeActivity.this, VisitRouteActivity.class));
            }
        }).start();
    }
    @Override protected BasePresenterInterface getPresenter() {return mPresenter;}
}
