package com.apex.mpad.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.apex.mpad.R;

import java.util.List;

/**
 * Created by will on 5/11/15.
 */
public class ApexSpinnerAdapter extends ArrayAdapter<String> {
    protected Activity mActivity;
    protected List<String> mItems;

    public ApexSpinnerAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.mActivity = (Activity) context;
        this.mItems = objects;
    }

    @Override public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = mActivity.getLayoutInflater();
        View row = inflater.inflate(R.layout.spinner_drop_down, parent, false);
        TextView label = (TextView) row.findViewById(R.id.spinner_dropdown_text);
        label.setText(getItem(position));
        LinearLayout layout = (LinearLayout) row.findViewById(R.id.spinner_dropdown_layout);
        return row;
    }
    @Override public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = mActivity.getLayoutInflater();
        View row = inflater.inflate(R.layout.spinner_text, parent, false);
        TextView label = (TextView) row.findViewById(R.id.spinner_text);
        label.setText(getItem(position));
        return row;
    }

    public String removeAt(int pos) {
        return mItems.remove(pos);
    }
}

