package com.apex.mpad.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.apex.mpad.R;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.presenter.iface.PatientListItemPresenterInterface;

import java.util.List;

/**
 * Created by will on 4/23/15.
 */
public class PatientAdapter extends BaseAdapter {

    private List<PatientListItemPresenterInterface> mItems;
    private LayoutInflater mInflater;
    private Context mContext;
    private int maxWidth;

    public PatientAdapter(Context context, List<PatientListItemPresenterInterface> data) {
        super();
        Injector.INSTANCE.inject(this);
        this.mItems = data;
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
    }
    public class PatientListViewHolder {
        @InjectView(R.id.patient_list_indicator) ImageView indicator;
        @InjectView(R.id.patient_list_item_location) TextView location;
        @InjectView(R.id.patient_list_item_patient_name) TextView name;
        @InjectView(R.id.patient_list_item_dob) TextView dob;
        @InjectView(R.id.pickup_only_indicator) ImageView puIndicator;

        public PatientListViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public List<PatientListItemPresenterInterface> getBackingData() {return mItems;}

    @Override public int getCount() {
        return mItems.size();
    }
    public int getMaxItemWidth() {return maxWidth;}

    @Override public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
//        if (convertView == null) {
            view = mInflater.inflate(R.layout.layout_patient_items, parent, false);
            final PatientListViewHolder holder = new PatientListViewHolder(view);
            view.setTag(holder);
//        } else {
//            view = convertView;
//        }

//        final PatientListViewHolder holder = (PatientListViewHolder) view.getTag();
        final PatientListItemPresenterInterface item = mItems.get(position);
        int width = view.getWidth();
        if (width > maxWidth) {
            maxWidth = width;
        }

        holder.location.setText(item.getLocation());
        holder.name.setText(item.getName());
        holder.dob.setText("TBD");

        switch (item.getStatus()) {
            case Completed:
                holder.indicator.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_gray));
                break;
            case Failed:
                holder.indicator.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_red));
                break;
            default:
                holder.indicator.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_green));
                break;
        }

        if (item.getWorkOrder().isPickupOnlyPatient()) {
            holder.puIndicator.setVisibility(View.VISIBLE);
        } else {
            holder.puIndicator.setVisibility(View.INVISIBLE);
        }

        boolean enabled = item.getStatus() != WorkOrder.WorkOrderStatus.Completed;
        if (!enabled) {
            view.setOnClickListener(null);
        }
        if (!enabled) {
            view.setBackgroundColor(mContext.getResources().getColor(R.color.list_light_grey));
            holder.location.setTextColor(mContext.getResources().getColor(R.color.list_dark_grey));
            holder.name.setTextColor(mContext.getResources().getColor(R.color.list_dark_grey));
            holder.dob.setTextColor(mContext.getResources().getColor(R.color.list_dark_grey));
        }

        return view;
    }
}
