package com.apex.mpad.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.RouteSummaryListItemPresenterInterface;

import java.util.List;

/**
 * Created by will on 5/14/15.
 */
public class RouteSummaryAdapter extends ArrayAdapter<RouteSummaryListItemPresenterInterface> {
    private Activity mActivity;
    private List<RouteSummaryListItemPresenterInterface> mItems;
    private int mLayout;

    public RouteSummaryAdapter(Context context, int resource, List<RouteSummaryListItemPresenterInterface> objects) {
        super(context, resource, objects);
        this.mActivity = (Activity) context;
        this.mItems = objects;
        this.mLayout = resource;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = mActivity.getLayoutInflater();
            convertView = inflater.inflate(mLayout, parent, false);
        }
        TextView label = (TextView) convertView.findViewById(R.id.item);
        label.setText(getItem(position).getText());


        label.setTextColor(mActivity.getResources().getColor(getItem(position).isComplete() ? android.R.color.darker_gray : android.R.color.holo_green_dark));
        return convertView;
    }
}
