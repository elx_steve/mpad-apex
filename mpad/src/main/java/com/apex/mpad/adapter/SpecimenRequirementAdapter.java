package com.apex.mpad.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.TubeInfo;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import com.apex.mpad.presenter.iface.SpecimenRequirementListItemPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.async.PrintAsync;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/23/15.
 */
public class SpecimenRequirementAdapter extends BaseAdapter {

    private List<SpecimenRequirementListItemPresenterInterface> mItems;
    private LayoutInflater mInflater;
    private Context mContext;
    private boolean isFirstClick;

    @Inject @ForApplication ApexApplication mApp;

    public SpecimenRequirementAdapter(Context context, List<SpecimenRequirementListItemPresenterInterface> data) {
        super();
        Injector.INSTANCE.inject(this);
        this.mItems = data;
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.isFirstClick = true;
    }
    public class SpecimenRequirementViewHolder {
        @InjectView(R.id.specimen_list_item_button) Button button;
        @InjectView(R.id.specimen_list_item_spinner) Spinner spinner;
        @InjectView(R.id.specimen_list_item_text) TextView text;

        public SpecimenRequirementViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public List<SpecimenRequirementListItemPresenterInterface> getBackingData() {return mItems;}

    @Override public int getCount() {
        return mItems.size();
    }

    @Override public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
//        View view = null;
//        if (convertView == null) {
            View view = mInflater.inflate(R.layout.layout_specimen_items, parent, false);
            final SpecimenRequirementViewHolder holder = new SpecimenRequirementViewHolder(view);
            view.setTag(holder);
//        } else {
//            view = convertView;
//        }

//        final SpecimenRequirementViewHolder holder = (SpecimenRequirementViewHolder) view.getTag();
        final SpecimenRequirementListItemPresenterInterface item = mItems.get(position);
        SpecimenRequirementInterface specimen = item.getSpecimen();

        List<Integer> list = new ArrayList<Integer>();
        int maxTubes = specimen.getTubeTypeId() != TubeInfo.EXTRA_TUBE_ID ? specimen.getTubeCount() : 9;
        int i = 0;

        while (i <= maxTubes) {
            list.add(i++);
        }
        Integer[] array = new Integer[list.size()];
        array = list.toArray(array);
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(mContext, R.layout.list_item_small, array);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        holder.spinner.setAdapter(adapter);
        if (specimen.getTubeCountCollected() == null) {
            specimen.setTubeCountCollected(0);
        }
        holder.spinner.setSelection(specimen.getTubeCountCollected());

        holder.text.setText(specimen.getTubeType());

        holder.button.setText(item.getButtonText());
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (!mApp.isPrinting()) {
                    item.setTubeCountCollected(holder.spinner.getSelectedItemPosition());
                    PrintAsync print = new PrintAsync((BaseActivity) mContext, item, holder.button);
                    print.execute();
                }
            }
        });
        item.setButton(holder.button);
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isFirstClick) {
                    isFirstClick = false;
                } else {
                    item.setTubeCountCollected(position);
                }
            }

            @Override public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        view.setTag(holder);
        return view;
    }
}
