package com.apex.mpad.annotation;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by will on 4/9/15.
 */
@Qualifier @Retention(RetentionPolicy.RUNTIME)
public @interface ForApplication {
}
