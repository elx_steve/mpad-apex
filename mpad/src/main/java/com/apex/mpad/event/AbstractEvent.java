package com.apex.mpad.event;

/**
 * Created by will on 4/11/15.
 */
public abstract class AbstractEvent {
    private Enum mType;

    protected AbstractEvent(Enum type)
    {
        this.mType = type;
    }

    public Enum getType()
    {
        return this.mType;
    }
}
