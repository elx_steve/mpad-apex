package com.apex.mpad.event;

/**
 * Created by whansen on 4/11/15.
 * Events class for holding Events (for use with Otto)
 */
public class Events {
    public static class Login {
        public String username;
        public String password;
    }
}
