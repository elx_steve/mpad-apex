package com.apex.mpad.event;

/**
 * Created by will on 4/11/15.
 */
public class LoginEvent extends AbstractEvent {
    private String mUsername;
    private String mPassword;
    private Type mType;

    public enum Type {
        USER_CHANGE,
        PASS_CHANGE,
        LOGGING_IN
    }

    public LoginEvent(Type type, String username, String password) {
        super(type);
        this.mType = type;
        this.mUsername = username;
        this.mPassword = password;
    }

    @Override public Type getType() {
        return this.mType;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }
}
