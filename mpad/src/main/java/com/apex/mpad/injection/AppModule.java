package com.apex.mpad.injection;

import android.content.Context;
import android.location.LocationManager;
import android.os.Build;
import com.apex.mpad.adapter.PatientAdapter;
import com.apex.mpad.adapter.SpecimenRequirementAdapter;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.presenter.*;
import com.apex.mpad.service.*;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.service.rest.RestClient;
import com.apex.mpad.util.*;
import com.apex.mpad.util.async.LoginAsyncTask;
import com.apex.mpad.util.async.ProfileLoadAsyncTask;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by will on 4/9/15.
 */
@Module(
        includes = {
                ServiceModule.class
        },
        injects = {
                //Base Application Classes
                ApexApplication.class,
                ApexPrefs.class,
                BasePresenter.class,
                FileUploadBroadcastReceiver.class,
                FileUploader.class,
                FileUploaderService.class,
                FileUtility.class,
                ImageUtility.class,
                LoginAsyncTask.class,
                NavigationService.class,
                NavigationModule.class,
                NotificationBroadcastReceiver.class,
                NotificationService.class,
                //ProfileLoadAsyncTask.class,
                PrintService.class,
                RestClient.class,
                ScanService.class,

                //Presenters
                AddPrinterPresenter.class,
                CheckPrinterPresenter.class,
                HelpPresenter.class,
                HomePresenter.class,
                LoginPresenter.class,
                NewPatientPresenter.class,
                NewPatientScriptPresenter.class,
                OrderRequirementListPresenter.class,
                PatientDOBPresenter.class,
                PatientInformationPresenter.class,
                PatientListItemPresenter.class,
                PatientListPresenter.class,
                PatientScriptsPresenter.class,
                PaymentPresenter.class,
                RouteSummaryListItemPresenter.class,
                RouteSummaryListPresenter.class,
                SpecimenRequirementListItemPresenter.class,
                SpecimenRequirementListPresenter.class,
                TubeFailReasonPresenter.class,
                VisitCompletedMoreOrdersPresenter.class,
                VisitCompletedPresenter.class,
                VisitFailedPresenter.class,
                VisitInformationPresenter.class,
                VisitRouteLocationPresenter.class,
                VisitRoutePresenter.class,
                WelcomePresenter.class,

                //Adapters
                PatientAdapter.class,
                SpecimenRequirementAdapter.class
        },
        complete = false,
        library = true
)
public class AppModule {

    private final ApexApplication mApp;

    public AppModule(ApexApplication app) {
        mApp = app;
    }

    @Provides @Singleton @ForApplication Context provideApplicationContext() {
        return mApp;
    }
    @Provides @Singleton @ForApplication ApexApplication provideApplication() {return mApp;}

    @Provides @Singleton @ForApplication ApexPrefs providePrefs(@ForApplication Context context) {
        return ApexPrefs.getInstance(context);
    }

    @Provides @Named("device_id") String provideDeviceId() {
        return Build.SERIAL;
    }

    @Provides @Singleton FileUtility provideFileUtility() {
        return new FileUtility();
    }
    @Provides @Singleton ImageUtility provideImageUtility() { return new ImageUtility();}
    @Provides @Singleton PrintService providePrintService() {return new PrintService();}
    @Provides @Singleton @ForApplication LocationManager provideLocationManager(@ForApplication Context appContext) {
        return (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
    }

    @Provides @Singleton NavigationManager provideNavigationService() {
        return new NavigationManager();
    }
    @Provides @Singleton FileUploader provideFileUploader() {
        return new FileUploader();
    }

}
