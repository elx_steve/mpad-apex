package com.apex.mpad.injection;

import com.apex.mpad.util.ApexBus;
import com.squareup.otto.ThreadEnforcer;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

import static com.apex.mpad.util.Constants.BUS_REST;
import static com.apex.mpad.util.Constants.BUS_UI;

/**
 * Created by will on 4/11/15.
 */
@Module(
        library = true
)
public class BusServiceModule {

    @Provides @Singleton @Named(BUS_REST) ApexBus provideRestBus() {
        return new ApexBus(ThreadEnforcer.ANY);
    }

    @Provides @Singleton @Named(BUS_UI) ApexBus provideUiBus() {
        return new ApexBus(ThreadEnforcer.MAIN);
    }
}
