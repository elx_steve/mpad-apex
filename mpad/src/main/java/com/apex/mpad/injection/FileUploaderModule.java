package com.apex.mpad.injection;

import com.apex.mpad.presenter.WelcomePresenter;
import dagger.Module;

/**
 * Created by will on 4/22/15.
 */
@Module(
        injects = {
                WelcomePresenter.class
        },
        library = true,
        complete = false
)
public class FileUploaderModule {
}
