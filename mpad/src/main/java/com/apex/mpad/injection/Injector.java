package com.apex.mpad.injection;

import dagger.ObjectGraph;

/**
 * Created by will on 4/9/15.
 */
public enum Injector {
    INSTANCE;

    private ObjectGraph objectGraph = null;

    public void init(final Object appModule) {
        if (objectGraph == null) {
            objectGraph = objectGraph.create(appModule);
        } else {
            objectGraph = objectGraph.plus(appModule);
        }

        objectGraph.injectStatics();
    }

    public void init(final Object appModule, final Object target) {
        init(appModule);
        inject(target);
    }

    public ObjectGraph add(final Object... modules) {
        return objectGraph.plus(modules);
    }

    public void inject(final Object target) {
        objectGraph.inject(target);
    }

    public <T> T resolve(Class<T> type) {
        return objectGraph.get(type);
    }

}

