package com.apex.mpad.injection;

import com.apex.mpad.service.navigation.NavigationManager;
import dagger.Module;

/**
 * Created by will on 4/15/15.
 */
@Module(
        injects = {
                NavigationManager.class
        },
        library = true,
        complete = false
)
public class NavigationModule {

}
