package com.apex.mpad.injection;

import com.apex.mpad.activity.*;
import com.apex.mpad.presenter.*;
import com.apex.mpad.presenter.iface.*;
import com.apex.mpad.util.base.BaseActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by will on 4/9/15.
 */
@Module(
    injects = {
            AddPrinterActivity.class,
            CheckPrinterActivity.class,
            BaseActivity.class,
            HelpActivity.class,
            HomeActivity.class,
            LoginActivity.class,
            NewPatientActivity.class,
            NewPatientScriptActivity.class,
            OrderRequirementListActivity.class,
            PatientDOBActivity.class,
            PatientInformationActivity.class,
            PatientListActivity.class,
            PatientScriptsActivity.class,
            PaymentActivity.class,
            RouteSummaryListActivity.class,
            SpecimenRequirementListActivity.class,
            TubeFailReasonActivity.class,
            VisitCompletedActivity.class,
            VisitCompletedMoreOrdersActivity.class,
            VisitFailedActivity.class,
            VisitInformationActivity.class,
            VisitRouteActivity.class,
            VisitRouteLocationActivity.class,
            WelcomeActivity.class
    },
        addsTo = AppModule.class,
        complete = false,
        library = true
)
public class PresenterModule {

    BaseActivity mActivity;

    public PresenterModule(BaseActivity activity) {
        this.mActivity = activity;
    }

    @Provides public LoginPresenterInterface provideLoginPresenter(LoginPresenter presenter) {
        return presenter;
    }
    @Provides public WelcomePresenterInterface provideWelcomePresenter(WelcomePresenter presenter) {return presenter;}
    @Provides public VisitRoutePresenterInterface provideVisitPresenter(VisitRoutePresenter presenter) {return presenter;}
    @Provides public VisitRouteLocationPresenterInterface provideVisitLocationPresenter(VisitRouteLocationPresenter presenter) {return presenter;}
    @Provides public PatientDOBPresenterInterface providePatientDOBPresenter(PatientDOBPresenter presenter) {return presenter;}
    @Provides public PatientInformationPresenterInterface providePatientInformationPresenter(PatientInformationPresenter presenter) {return presenter;}
    @Provides public VisitInformationPresenterInterface provideVisitInformationPresenter(VisitInformationPresenter presenter) {return presenter;}
    @Provides public PatientScriptsPresenterInterface providePatientScriptsPresenter(PatientScriptsPresenter presenter) {return presenter;}
    @Provides public NewPatientScriptPresenterInterface provideNewPatientScriptPresenter(NewPatientScriptPresenter presenter) {return presenter;}
    @Provides public OrderRequirementListPresenterInterface provideOrderRequirementPresenter(OrderRequirementListPresenter presenter) {return presenter;}
    @Provides public SpecimenRequirementListPresenterInterface provideSpecimenListPresenter(SpecimenRequirementListPresenter presenter) {return presenter;}
    @Provides public VisitCompletedPresenterInterface provideVisitCompletedPresenter(VisitCompletedPresenter presenter) {return presenter;}
    @Provides public PatientListPresenterInterface providePatientListPresenter(PatientListPresenter presenter) {return presenter;}
    @Provides public TubeFailReasonPresenterInterface provideTubeFailPresenter(TubeFailReasonPresenter presenter) {return presenter;}
    @Provides public VisitCompletedMoreOrdersPresenterInterface provideVisitCompletedMoreOrdersPresenter(VisitCompletedMoreOrdersPresenter presenter) {return presenter;}
    @Provides public VisitFailedPresenterInterface providesVisitFailedPresenter(VisitFailedPresenter presenter) {return presenter;}
    @Provides public AddPrinterPresenterInterface providesAddPrinterPresenter(AddPrinterPresenter presenter) {return presenter;}
    @Provides public CheckPrinterPresenterInterface providesCheckPrinterPresenter(CheckPrinterPresenter presenter) {return presenter;}
    @Provides public HelpPresenterInterface providesHelpPresenter(HelpPresenter presenter) {return presenter;}
    @Provides public HomePresenterInterface provideHomePresenter(HomePresenter presenter) {return presenter;}
    @Provides public NewPatientPresenterInterface provideNewPatientPresenter(NewPatientPresenter presenter) {return presenter;}
    @Provides public PaymentPresenterInterface providePaymentPresenter(PaymentPresenter presenter) {return presenter;}
    @Provides public RouteSummaryListPresenterInterface provideRouteSummaryListPresenter(RouteSummaryListPresenter presenter) {return presenter;}
}
