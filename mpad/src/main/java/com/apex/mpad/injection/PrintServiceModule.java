package com.apex.mpad.injection;

import com.apex.mpad.service.PrintService;
import dagger.Module;
import dagger.Provides;
import org.parceler.javaxinject.Singleton;

/**
 * Created by will on 4/24/15.
 */
@Module(library = true, complete = false)
public class PrintServiceModule {
}
