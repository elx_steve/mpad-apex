package com.apex.mpad.injection;

import android.content.Context;
import android.util.Log;

import com.apex.mpad.activity.LoginActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.service.gson.instance.InterfaceAdapter;
import com.apex.mpad.service.jackson.date.DateTimeConverter;
import com.apex.mpad.service.rest.ApiService;
import com.apex.mpad.service.rest.JacksonConverter;
import com.apex.mpad.service.rest.RestClient;
import com.apex.mpad.service.rest.iface.RestClientInterface;
import com.apex.mpad.service.rest.test.LocalJsonClient;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.SSLUtil;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.Converter;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.TimeUnit;



/**
 * Created by will on 4/10/15.
 */
@Module(
        library = true, complete = false
)
public class RestServiceModule {
    private static final String TAG = "RestServiceModule";
    private static final Logger mLogger = LoggerFactory.getLogger(LoginActivity.class);

    @Inject @ForApplication Context mAppContext;
    @Inject @ForApplication ApexPrefs mPrefs;

    private int ConnTimeout = 10;   // AM-48
    private int RxTimeout = 10;
    private int TxTimeout = 10;

    @Provides ApiService provideApiService(Converter converter, @ForApplication Context context, OkClient client) {
        if (mPrefs == null)
            mPrefs = ApexPrefs.getInstance(mAppContext);

        String prefix = (mPrefs.getSsl().compareTo("1.1") == 0 || mPrefs.getSsl().compareTo("1.2") == 0) ? "https://" : "http://";

        RestAdapter localAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.1")
                .setConverter(converter)
                .setClient(new LocalJsonClient(context))
                .build();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(prefix + mPrefs.getEndpoint())
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(converter)
                .setClient(client)
                .build();

        if (mPrefs.getLocalDev())
            return localAdapter.create(ApiService.class);

        return restAdapter.create(ApiService.class);
    }

    @Provides Converter provideConverter(ObjectMapper mapper) {
        Converter converter = new JacksonConverter(mapper);
        return new JacksonConverter(mapper);
    }

    @Provides @Singleton ObjectMapper provideObjectMapper() {
        ObjectMapper result = new ObjectMapper();
        result.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        result.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
        result.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
//        result.registerModule(new JodaModule());
        result.registerModule(new DateTimeConverter());
        result.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        result.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        return result;
    }

    @Provides RestClientInterface provideRestClient(ApiService service) {
        return new RestClient(service);
    }

    @Provides @Singleton public Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeHierarchyAdapter(WorkOrderInterface.class, new InterfaceAdapter<WorkOrderInterface>())
                .create();
    }

    @Provides @Singleton public OkClient provideClient() {
        if (mPrefs == null)
            mPrefs = ApexPrefs.getInstance(mAppContext);

        ConnTimeout = mPrefs.getPrefRestConnTimeout();   // AM-48
        RxTimeout = mPrefs.getPrefRestRxTimeout();       // AM-48
        TxTimeout = mPrefs.getPrefRestTxTimeout();       // AM-48

        mLogger.debug("Timeout assigned  (Conn/Rx/Tx) : " + this.ConnTimeout + " - " + this.RxTimeout + " - " + this.TxTimeout);
        Log.d(TAG, "Timeout assigned (Conn/Rx/Tx) : " + this.ConnTimeout + " - " + this.RxTimeout + " - " + this.TxTimeout);

        OkHttpClient client = new OkHttpClient();
        client.setRetryOnConnectionFailure(false);
        client.setWriteTimeout(this.TxTimeout, TimeUnit.SECONDS);
        client.setReadTimeout(this.RxTimeout, TimeUnit.SECONDS);
        client.setConnectTimeout(this.ConnTimeout, TimeUnit.SECONDS);

        if (mPrefs.getSsl().compareTo("1.2") == 0) {
           return new OkClient(SSLUtil.enableTls12(client));
        } else if (mPrefs.getSsl().compareTo("1.1") == 0) {
           return new OkClient(SSLUtil.enableTls11(client));
        } else {
           return new OkClient(SSLUtil.enableNoTls(client));
        }
    }
}
