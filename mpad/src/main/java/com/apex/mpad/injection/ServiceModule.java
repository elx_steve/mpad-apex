package com.apex.mpad.injection;

import dagger.Module;

/**
 * Created by will on 4/9/15.
 */
@Module(
    includes = {
            RestServiceModule.class,
            BusServiceModule.class,
            NavigationModule.class,
            FileUploaderModule.class,
            PrintServiceModule.class
    }, complete = false
)
public class ServiceModule {
}
