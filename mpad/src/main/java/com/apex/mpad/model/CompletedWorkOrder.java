package com.apex.mpad.model;

import com.apex.mpad.model.iface.CompletedWorkOrderInterface;
import com.apex.mpad.model.iface.FailInfoInterface;
import com.apex.mpad.model.iface.SuccessInfoInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by will on 4/17/15.
 *
 * This is an IN-MEMORY object only....  it is converted to a JSON object before sending
 */
public class CompletedWorkOrder implements CompletedWorkOrderInterface {
    public static final String SPECIMEN_NUMBER = "SpecimenNumber";
    public static final String IS_CONFIRMED = "IsConfirmed";

    public static final String FAILED_COLLECTION = "FailedCollection";
    public static final String SUCCESS_COLLECTION = "SuccessCollection";

    public static final String HAS_ADDITIONAL_SCRIPT = "HasAdditionalScript";
    public static final String AMOUNT_COLLECTED = "AmountCollected";
    public static final String PAYMENT_COLLECTION_NOTES = "PaymentCollectionNotes";
    public static final String PATIENT_SPECIMEN_COUNT = "PatientSpecimenCount";
    public static final String PATIENT_UNABLE_TO_SIGN = "PatientUnableToSign";
    public static final String WORK_ORDER_NOTES = "CompletedWorkOrderNotes";

    public static final String IS_ADD_ON_PATIENT = "IsAddOnPatient";

    public static final String PATIENT_NAME = "PatientName";
    public static final String PATIENT_FIRST_NAME = "PatientFirstName";
    public static final String PATIENT_LAST_NAME = "PatientLastName";
    public static final String DOB = "Dob";
    public static final String IS_DRAW_SUCCESSFUL = "IsDrawSuccessful";
    public static final String TUBE_DRAW_FAIL_NOTES = "TubeDrawFailNotes";
    public static final String TUBE_DRAW_FAIL_ID = "TubeDrawFailId";

    public boolean isConfirmed;
    private String mSpecimenNumber;

    private FailInfoInterface mFailedCollection;
    private SuccessInfoInterface mSuccessCollection;
    private boolean mHasAdditionalScript;

    private String mPaymentCollectionNotes;
    private double mAmountCollected;
    private int mPatientSpecimentCount;
    private boolean mPatientUnableToSign;

    private String mCompletedWorkOrderNotes;
    private String mPatientName;
    private String mPatientFirstName;
    private String mPatientLastName;
    private boolean mIsAddOnPatient;
    private String mDob;
    private String mNewDob;
    public boolean isDrawSuccessfull;
    private String mTubeDrawFailReasonId;
    private String mTubeDrawFailNotes;

    private static final Logger mLogger = LoggerFactory.getLogger(CompletedWorkOrder.class);


    public CompletedWorkOrder() {
        mSuccessCollection = new SuccessInfo();
        mFailedCollection = new FailInfo();
    }

    public static CompletedWorkOrderInterface from(WorkOrderInterface wo) {
        if (wo == null) {
            return null;
        }

        float amountCollected = 0f;

        try {
            amountCollected = Float.parseFloat(wo.getAmountActuallyCollected() != null ? wo.getAmountActuallyCollected() : "0");
        } catch (Exception e) {
            mLogger.warn("Unable to parse float: {}", e.getMessage() != null ? e.getMessage() : "");
        }

        CompletedWorkOrderInterface result = new CompletedWorkOrder();
        result.setSpecimenNumber(wo.getSpecimenNumber());
        result.setConfirmed(wo.isConfirmed());
        if (wo.getNumberAdditionalScripts() != null) {
            result.setAdditionalScript(wo.getNumberAdditionalScripts() > 0);
        }
        result.setAmountCollected(amountCollected);
        result.setPaymentCollectionNotes(wo.getPaymentCollectionNotes());
        result.setPatientUnableToSign(wo.isPatientUnableToSign());
        result.setPatientSpecimenCount(wo.getPatientSpecimentCount());
        result.setCompletedWorkOrderNotes(wo.getCompletedWorkOrderNotes());
        result.setPatientFirstName(wo.getPatientFirstName());
        result.setPatientLastName(wo.getPatientLastName());

        boolean isAddOnPatient = !(wo.getPatientFirstName() == null || wo.getPatientFirstName().isEmpty());
        result.setAddOnPatient(isAddOnPatient);
        result.setTubeDrawFailNotes(wo.getTubeDrawFailNotes());
        result.setTubeDrawFailReasonId(wo.getTubeDrawFailReasonId());
        result.setDob(wo.getDob());
        result.setNewDOB(wo.getNewDOB());
        return result;

    }

    public void setSpecimenNumber(String specimenNumber) {
        this.mSpecimenNumber = specimenNumber;
    }
    public void setConfirmed(boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public String getSpecimenNumber() {
        return mSpecimenNumber;
    }
    public FailInfoInterface getFailedCollection() {
        return mFailedCollection;
    }

    public SuccessInfoInterface getSuccessCollection() {
        return mSuccessCollection;
    }

    public boolean hasAdditionalScript() {
        return mHasAdditionalScript;
    }

    public void setAdditionalScript(boolean mHasAdditionalScript) {
        this.mHasAdditionalScript = mHasAdditionalScript;
    }

    public String getPaymentCollectionNotes() {
        return mPaymentCollectionNotes;
    }

    @Override public Integer getPatientSpecimenCount() {
        return null;
    }

    public void setPaymentCollectionNotes(String mPaymentCollectionNotes) {
        this.mPaymentCollectionNotes = mPaymentCollectionNotes;
    }

    public double getAmountCollected() {
        return mAmountCollected;
    }

    public void setAmountCollected(double mAmountCollected) {
        this.mAmountCollected = mAmountCollected;
    }

    public int getPatientSpecimentCount() {
        return mPatientSpecimentCount;
    }

    public void setPatientSpecimentCount(int mPatientSpecimentCount) {
        this.mPatientSpecimentCount = mPatientSpecimentCount;
    }

    public boolean isPatientUnableToSign() {
        return mPatientUnableToSign;
    }

    public void setPatientUnableToSign(boolean mPatientUnableToSign) {
        this.mPatientUnableToSign = mPatientUnableToSign;
    }

    public String getCompletedWorkOrderNotes() {
        return mCompletedWorkOrderNotes;
    }

    @Override public void setPatientSpecimenCount(Integer count) {
        this.mPatientSpecimentCount = count;
    }

    public void setCompletedWorkOrderNotes(String mCompletedWorkOrderNotes) {
        this.mCompletedWorkOrderNotes = mCompletedWorkOrderNotes;
    }

    public String getPatientName() {
        return mPatientName;
    }

    public void setPatientName(String mPatientName) {
        this.mPatientName = mPatientName;
    }

    public String getPatientFirstName() {
        return mPatientFirstName;
    }

    public void setPatientFirstName(String mPatientFirstName) {
        this.mPatientFirstName = mPatientFirstName;
    }

    public String getPatientLastName() {
        return mPatientLastName;
    }

    public void setPatientLastName(String mPatientLastName) {
        this.mPatientLastName = mPatientLastName;
    }

    public boolean isAddOnPatient() {
        return mIsAddOnPatient;
    }

    public void isAddOnPatient(boolean mIsAddOnPatient) {
        this.mIsAddOnPatient = mIsAddOnPatient;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String mDob) {
        this.mDob = mDob;
    }

    @Override public void setAddOnPatient(boolean isAddOnPatient) {
        this.mIsAddOnPatient = isAddOnPatient;
    }

    public boolean isDrawSuccessfull() {
        return isDrawSuccessfull;
    }

    public String getTubeDrawFailReasonId() {
        return mTubeDrawFailReasonId;
    }

    public void setTubeDrawFailReasonId(String mTubeDrawFailReasonId) {
        this.mTubeDrawFailReasonId = mTubeDrawFailReasonId;
    }

    public String getTubeDrawFailNotes() {
        return mTubeDrawFailNotes;
    }

    public void setTubeDrawFailNotes(String mTubeDrawFailNotes) {
        this.mTubeDrawFailNotes = mTubeDrawFailNotes;
    }

    public void setSuccessCollection(SuccessInfoInterface collection) {
        this.mSuccessCollection = collection;
    }

    public void setFailedCollection(FailInfoInterface failInfo) {
        this.mFailedCollection = failInfo;
    }

    public void setDrawSuccessfull(boolean isSuccessful) {
        this.isDrawSuccessfull = isSuccessful;
    }

    public String getNewDOB() {
        return mNewDob;
    }

    public void setNewDOB(String dob) {
        mNewDob = dob;
    }
}
