package com.apex.mpad.model;

import com.apex.mpad.model.iface.DrawSiteInterface;
import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class DrawSite implements DrawSiteInterface {
    public String drawSiteId;
    public String drawSite;

    public String getId() {
        return drawSiteId;
    }

    public void setId(String drawSiteId) {
        this.drawSiteId = drawSiteId;
    }

    public String getDescription() {
        return drawSite;
    }

    public void setDescription(String description) {
        this.drawSite = description;
    }
}
