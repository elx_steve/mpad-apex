package com.apex.mpad.model;

import com.apex.mpad.model.iface.FailInfoInterface;
import com.apex.mpad.service.gson.converter.JodaDateTimeConverter;
import org.joda.time.DateTime;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class FailInfo implements FailInfoInterface {

    @ParcelPropertyConverter(JodaDateTimeConverter.class)
    public DateTime failedTime;
    public String reasonId;
    public String failComments;

    public DateTime getFailedDateTime() {
        return failedTime;
    }

    public void setFailedDateTime(DateTime failedTime) {
        this.failedTime = failedTime;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getFailComments() {
        return failComments;
    }

    public void setFailComments(String failComments) {
        this.failComments = failComments;
    }
}
