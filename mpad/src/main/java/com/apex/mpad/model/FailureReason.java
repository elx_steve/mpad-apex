package com.apex.mpad.model;

import com.apex.mpad.model.iface.FailureReasonInterface;
import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class FailureReason implements FailureReasonInterface {
    public String failReasonId;
    public String failReason;

    public String getId() {
        return failReasonId;
    }

    public void setId(String failReasonId) {
        this.failReasonId = failReasonId;
    }

    public String getDescription() {
        return failReason;
    }

    public void setDescription(String failReason) {
        this.failReason = failReason;
    }
}
