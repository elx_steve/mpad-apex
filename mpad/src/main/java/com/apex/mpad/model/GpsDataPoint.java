package com.apex.mpad.model;

import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.gson.converter.JodaDateTimeConverter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.DateTime;
import org.parceler.Parcel;
import org.parceler.ParcelConstructor;
import org.parceler.ParcelPropertyConverter;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
@JsonIgnoreProperties
public class GpsDataPoint implements GpsDataPointInterface {
    public Double longitude;
    public Double latitude;
    public Double speed;
    public Double direction;
    public String techId;

    @ParcelPropertyConverter(JodaDateTimeConverter.class)
    public DateTime dateTime;

    @ParcelConstructor public GpsDataPoint() {}

    public GpsDataPoint(double lat, double lon, double dir, double speed, DateTime dateTime, String techId) {
        this.latitude = lat;
        this.longitude = lon;
        this.direction = dir;
        this.speed = speed;
        this.dateTime = dateTime;
        this.techId = techId;
    }

    @Override public GpsDataPoint clone() {
        return new GpsDataPoint(latitude, longitude, direction, speed, dateTime, techId);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDirection() {
        return direction;
    }

    public void setDirection(double direction) {
        this.direction = direction;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime mDateTime) {
        this.dateTime = mDateTime;
    }

    public String getTechId() {
        return techId;
    }

    public void setTechId(String techId) {
        this.techId = techId;
    }

}

