package com.apex.mpad.model;

import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class SpecimenRequirement implements SpecimenRequirementInterface {
    Integer tubeTypeId;
    String tubeType;
    Integer tubeCount;
    Integer tubeCountCollected;
    String barcode;
    String barcodeName;
    boolean printBarcode;

    public SpecimenRequirement() {}

    public SpecimenRequirement(SpecimenRequirement other) {
        this.tubeTypeId = other.tubeTypeId;
        this.tubeType = other.tubeType;
        this.tubeCount = other.tubeCount;
        this.tubeCountCollected = other.tubeCountCollected;
        this.barcode = other.barcode;
        this.barcodeName = other.barcodeName;
        this.printBarcode = other.printBarcode;
    }

    @Override public SpecimenRequirement copy() {
        return new SpecimenRequirement(this);
    }

    @Override public boolean hasCountDiscrepancy() {
        return !tubeCount.equals(tubeCountCollected);
    }
    @Override public boolean isExtra() {
        return tubeTypeId.equals(TubeInfo.EXTRA_TUBE_ID);
    }

    @Override public Integer getTubeTypeId() {
        return tubeTypeId;
    }

    @Override public void setTubeTypeId(Integer id) {
        this.tubeTypeId = id;
    }

    @Override public String getTubeType() {
        return tubeType;
    }

    @Override public void setTubeType(String type) {
        this.tubeType = type;
    }

    @Override public Integer getTubeCount() {
        return tubeCount;
    }

    @Override public void setTubeCount(Integer count) {
        this.tubeCount = count;
    }

    @Override public Integer getTubeCountCollected() {
        return tubeCountCollected;
    }
    @Override public void setTubeCountCollected(Integer count) {
        this.tubeCountCollected = count;
    }

    @Override public String getBarcode() {
        return barcode;
    }

    @Override public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Override public String getBarcodeName() {
        return barcodeName;
    }

    @Override public void setBarcodeName(String barcodeName) {
        this.barcodeName = barcodeName;
    }

    @Override public boolean printBarcode() {
        return printBarcode;
    }

    @Override public void setPrintBarcode(boolean printBarcode) {
        this.printBarcode = printBarcode;
    }
}
