package com.apex.mpad.model;

import com.apex.mpad.model.iface.SuccessInfoInterface;
import com.apex.mpad.model.iface.TubeInfoInterface;
import com.apex.mpad.service.gson.converter.JodaDateTimeConverter;
import com.apex.mpad.service.gson.converter.TubeInfoListConverter;
import org.joda.time.DateTime;
import org.parceler.Parcel;
import org.parceler.ParcelConstructor;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class SuccessInfo implements SuccessInfoInterface {

    public static final String COMPLETED_DATE_TIME = "CompletedDateTime";

    public static final String DRAW_SITE = "DrawSite";

    public static final String TUBES_COLLECTED = "TubesCollected";
    public static final String TUBE_FAIL_REASON_ID = "TubeFailReasonId";
    public static final String TUBE_FAIL_REASON_COMMENT = "TubeFailReasonComment";


    @ParcelPropertyConverter(JodaDateTimeConverter.class)
    public DateTime collectionTime;
    public String drawSite;
    public boolean isDrawSuccessful;

    @ParcelPropertyConverter(TubeInfoListConverter.class)
    public List<TubeInfoInterface> tubesCollected;
    public String tubeFailReasonId;
    public String tubeFailReasonComment;

    @ParcelConstructor public SuccessInfo() {}

    public DateTime getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(DateTime collectionTime) {
        this.collectionTime = collectionTime;
    }

    public String getDrawSite() {
        return drawSite;
    }

    public void setDrawSite(String drawSite) {
        this.drawSite = drawSite;
    }

    public boolean isDrawSuccessful() {
        return isDrawSuccessful;
    }

    public void setDrawSuccessful(boolean isDrawSuccessful) {
        this.isDrawSuccessful = isDrawSuccessful;
    }

    public List<TubeInfoInterface> getTubesCollected() {
        return tubesCollected;
    }

    public void setTubesCollected(List<TubeInfoInterface> tubesCollected) {
        this.tubesCollected = tubesCollected;
    }

    public String getTubeFailReasonId() {
        return tubeFailReasonId;
    }

    public void setTubeFailReasonId(String tubeFailedReasonId) {
        this.tubeFailReasonId = tubeFailedReasonId;
    }

    public String getTubeFailReasonComment() {
        return tubeFailReasonComment;
    }

    public void setTubeFailReasonComment(String tubeFailedReasonComment) {
        this.tubeFailReasonComment = tubeFailedReasonComment;
    }
}
