package com.apex.mpad.model;

import com.apex.mpad.model.iface.TestInterface;
import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class Test implements TestInterface {

    public static final String TEST_NAME = "TestName";

    public String testName;

    public Test() {}

    public Test(Test other) {
        this.testName = other.testName;
    }

    public Test copy() {
        return new Test(this);
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}
