package com.apex.mpad.model;

import com.apex.mpad.model.iface.TubeDrawFailReasonInterface;
import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class TubeDrawFailReason implements TubeDrawFailReasonInterface {
    public String failReasonId;
    public String failReason;

    public String getFailReasonId() {
        return failReasonId;
    }

    public void setFailReasonId(String failReasonId) {
        this.failReasonId = failReasonId;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }
}
