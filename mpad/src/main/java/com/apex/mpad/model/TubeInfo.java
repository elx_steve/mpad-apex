package com.apex.mpad.model;

import com.apex.mpad.model.iface.TubeInfoInterface;
import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class TubeInfo implements TubeInfoInterface {

    public static final String TUBE_TYPE_ID = "TubeTypeId";
    public static final String TUBE_COUNT = "TubeCount";
    public static final int EXTRA_TUBE_ID = 999;

    public Integer tubeTypeId;
    public Integer tubeCount;

    public Integer getTubeTypeId() {
        return tubeTypeId;
    }

    public void setTubeTypeId(Integer tubeTypeId) {
        this.tubeTypeId = tubeTypeId;
    }

    public Integer getTubeCount() {
        return tubeCount;
    }

    public void setTubeCount(Integer tubeCount) {
        this.tubeCount = tubeCount;
    }
}
