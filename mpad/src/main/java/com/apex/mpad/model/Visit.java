package com.apex.mpad.model;

import com.apex.mpad.model.iface.VisitInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/13/15.
 */
public class Visit implements VisitInterface {
    public static final String WORK_ORDERS = "WorkOrders";
    public static final String CURRENT_ORDER = "CurrentOrder";
    public static final String IS_COMPLETE = "IsComplete";
    public static final String STOP_NUMBER = "StopNumber";

    private List<WorkOrderInterface> mWorkOrders;
    private WorkOrderInterface mCurrentOrder;
    private int mStopNumber;
    private int mNumPatients;

    private String mArea;
    private String mAreaType;

    public Visit() {
        mWorkOrders = new ArrayList<WorkOrderInterface>();
    }

    @Override public List<WorkOrderInterface> getWorkOrders() {
        return mWorkOrders;
    }

    @Override public List<WorkOrderInterface> getPatientWorkOrders() {
        return null;
    }

    public void setWorkOrders(List<WorkOrderInterface> workOrders) {
        if (workOrders == mWorkOrders) {
            return;
        } else {
            mWorkOrders = workOrders;
        }
    }

    public boolean isComplete() {
        return !hasOrdersWithStatus(WorkOrder.WorkOrderStatus.Open);
    }

    public boolean hasSkips() {
        return hasOrdersWithStatus(WorkOrder.WorkOrderStatus.Skipped);
    }
    public int getOpenWorkOrderCount() {
        int result = 0;
        for (WorkOrderInterface workOrder : mWorkOrders) {
            if (workOrder.getStatus() == WorkOrder.WorkOrderStatus.Open) {
                result++;
            }
        }
        return result;
    }
    public int getNumSkippedPatients() {
        int result = 0;
        String currentPatient = "";
        for (WorkOrderInterface workOrder : mWorkOrders) {
            if (workOrder.getPatientId().equals(currentPatient)) {
                continue;
            }
            if (workOrder.getStatus() == WorkOrder.WorkOrderStatus.Skipped) {
                result++;
            }
            currentPatient = workOrder.getPatientId();
        }
        return result;
    }

    @Override public List<WorkOrderInterface> getCurrentPatientWorkOrders() {
        List<WorkOrderInterface> result = new ArrayList<WorkOrderInterface>();
        for (WorkOrderInterface workOrder : mWorkOrders) {
            if (workOrder.getPatientId().equals(mCurrentOrder.getPatientId())) {
                result.add(workOrder);
            }
        }
        return result;
    }

    public void addWorkOrder(WorkOrderInterface workOrder) {
        mWorkOrders.add(workOrder);
        mNumPatients = 0;
        String prev = "";
        for (WorkOrderInterface wo : mWorkOrders) {
            if (!wo.getPatientId().equals(prev)) {
                mNumPatients++;
                prev = wo.getPatientId();
            }
        }
    }

    public WorkOrderInterface getNextOrder() {
        if (isComplete()) {
            return null;
        }
        WorkOrderInterface result = null;
        if (!patientHasMoreWorkOrders()) {
            result = getNextOpenWorkOrder();
        } else {
            result = getNextWorkOrderForPatient();
        }
        mCurrentOrder = result;
        return result;
    }

    public boolean hasMoreWorkOrders() {
        return !isComplete();
    }
    public boolean patientHasMoreWorkOrders() {
        if (mCurrentOrder == null) {
            return false;
        }
        for (WorkOrderInterface wo : mWorkOrders) {
            if (wo.getStatus() == WorkOrder.WorkOrderStatus.Open && wo.getPatientId().equals(mCurrentOrder.getPatientId())) {
                return true;
            }
        }
        return false;
    }

    @Override public boolean patientHasMultipleOpenWorkOrders() {
        int count = 0;
        for (WorkOrderInterface wo : mWorkOrders) {
            if (wo.getStatus() == WorkOrder.WorkOrderStatus.Open && wo.getPatientId().equals(mCurrentOrder.getPatientId())) {
                if (++count > 1) {
                    return true;
                }
            }
        }
        return false;
    }

    private WorkOrderInterface getNextWorkOrderForPatient() {
        for (WorkOrderInterface wo : mWorkOrders) {
            if (wo.getStatus() == WorkOrder.WorkOrderStatus.Open && wo.getPatientId().equals(mCurrentOrder.getPatientId())) {
                return wo;
            }
        }
        return null;
    }

    private WorkOrderInterface getNextOpenWorkOrder() {
        for (WorkOrderInterface wo : mWorkOrders) {
            if (wo.getStatus() == WorkOrder.WorkOrderStatus.Open) {
                return wo;
            }
        }
        return null;
    }

    public int getStopNumber() {
        return mStopNumber;
    }
    public void setStopNumber(int num) {
        this.mStopNumber = num;
    }

    @Override public WorkOrderInterface getCurrentWorkOrder() {
        return mCurrentOrder;
    }
    @Override public void setCurrentWorkOrder(WorkOrderInterface currentOrder) {
        this.mCurrentOrder = currentOrder;
    }

    private boolean hasOrdersWithStatus(WorkOrder.WorkOrderStatus status) {
        for (WorkOrderInterface workOrder : mWorkOrders) {
            if (workOrder.getStatus() == status) {
                return true;
            }
        }
        return false;
    }

    public int getNumPatients() {
        return mNumPatients;
    }
    public void setNumPatients(int num) {
        this.mNumPatients = num;
    }

    public String getArea() {
        return (mArea == null) ? "" : mArea;
    }

    public void setArea(String mArea) {
        this.mArea = mArea;
    }

    public String getAreaType() {
        return (mAreaType == null) ? "" : mAreaType;
    }

    public void setAreaType(String mAreaType) {
        this.mAreaType = mAreaType;
    }


}
