package com.apex.mpad.model;

import com.apex.mpad.model.iface.*;
import com.apex.mpad.service.gson.converter.JodaDateTimeConverter;
import com.apex.mpad.service.gson.converter.SpecimenRequirementListConverter;
import com.apex.mpad.service.gson.converter.TestListConverter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */

@Parcel
@JsonIgnoreProperties
public class WorkOrder implements WorkOrderInterface {
    public static final String ID = "Id";
    public static final String STATUS = "IsCompleted";
    public static final String START_DATE_TIME = "StartDateTime";
    public static final String END_DATE_TIME = "EndDateTime";

    public static final String ROUTE_NUMBER = "RouteNumber";
    public static final String STOP_NUMBER = "StopNumber";
    public static final String SPECIMEN_NUMBER = "SpecimenNumber";
    public static final String IS_CONFIRMED = "IsConfirmed";
    public static final String VISIT_DATE_TIME = "VisitDateTime";
    public static final String PATIENT_NAME = "PatientName";
    public static final String PATIENT_ID = "PatientId";
    public static final String PATIENT_FIRST_NAME = "PatientFirstName";
    public static final String PATIENT_LAST_NAME = "PatientLastName";
    public static final String DOB = "Dob";
    public static final String GENDER = "Gender";
    public static final String PATIENT_PHONE = "PatientPhone";
    public static final String ALT_CONTACT_NAME = "AltContactName";
    public static final String ALT_PHONE_NUMBER = "AltPhoneNumber";
    public static final String ADDRESS_LINE_1 = "AddressLine1";
    public static final String ADDRESS_LINE_2 = "AddressLine2";
    public static final String CITY = "City";
    public static final String STATE = "State";
    public static final String ZIP_CODE = "Zipcode";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String PATIENT_COMMENTS = "PatientComments";
    public static final String APPOINTMENT_TIME = "AppointmentTime";
    public static final String ACS_INFORMATION = "AcsInformation";
    public static final String IS_FASTING = "IsFasting";
    public static final String INSURANCE_TYPE = "InsuranceType";
    public static final String DOCTOR_NAME = "DoctorName";
    public static final String ADDITIONAL_DOCTOR_NAME_1 = "AdditionalDoctorName1";
    public static final String ADDITIONAL_DOCTOR_NAME_2 = "AdditionalDoctorName2";
    public static final String FREQUENCY = "Frequency";
    public static final String ORDER_END_DATE = "OrderEndDate";
    public static final String UPCOMING_VISIT_DATE = "UpcomingVisitDate";
    public static final String IS_CASH_CHECK = "IsCashCheck";
    public static final String COLLECT_AMOUNT_OTHER = "CollectAmountOther";
    public static final String COLLECT_AMOUNT_TOTAL = "CollectAmountTotal";
    public static final String AMOUNT_ACTUALLY_COLLECTED = "AmountCollected";
    public static final String PATIENT_SPECIMEN_COUNT = "PatientSpecimenCount";
    public static final String SPECIMEN_REQUIREMENTS = "SpecimenRequirements";
    public static final String TESTS = "Tests";
    public static final String NUMBER_ADDITIONAL_SCRIPTS = "NumberAdditionalScripts";
    public static final String NUMBER_ADDITIONAL_SCRIPTS_CAPTURED = "NumberAdditionalScriptsCaptured";
    public static final String PAYMENT_COLLECTION_NOTES = "PaymentCollectionNotes";
    public static final String DRAW_SITE = "DrawSite";
    public static final String PATIENT_UNABLE_TO_SIGN = "PatientUnableToSign";
    public static final String FACILITY_ID = "FacilityId";
    public static final String WORK_ORDER_NOTES = "CompletedWorkOrderNotes";
    public static final String COMPLETED_WORK_ORDER_NOTES = "CompletedWorkOrderNotes";
    public static final String TUBE_DRAW_FAIL_NOTES = "TubeDrawFailNotes";
    public static final String TUBE_DRAW_FAIL_ID = "TubeDrawFailId";
    public static final String AGENCY_NAME = "AgencyName";
    public static final String NOT_CONFIRMED_REASON = "NotConfirmedReason";

    public enum WorkOrderStatus {
        Open,
        Failed,
        Completed,
        Skipped
    }
    public long mId;

    public WorkOrderStatus mStatus;
    public DateTime mStartDateTime;
    public DateTime mEndDateTime;

    public String routeNumber;
    public Integer stopNumber;
    public String drawAreaUnit;
    public String drawAreaType;

    public boolean isConfirmed;
    public String specimenNumber;
    public boolean isPickupOnlyPatient;

    //properties for JSON parsing
    @ParcelPropertyConverter(JodaDateTimeConverter.class)
    public DateTime visitDate;

    public String patientName;
    public String patientId;
    public String dob;
    public String gender;
    public String patientPhone;
    public String patientFirstName;
    public String patientLastName;
    public String altContactName;

    public String newDOB;

    public String altPhoneNo;

    @JsonProperty("address_line_1")
    public String addressLine1;

    @JsonProperty("address_line_2")
    public String addressLine2;
    public String city;
    public String state;
    public String zipcode;
    public double latitude;
    public double longitude;
    public String patientComments;
    public String appointmentTime;
    public String acsInformation;
    public String isFasting;
    public String insuranceType;
    public String doctorName;

    @JsonProperty("additional_doctor_1_name")
    public String additionalDoctor1Name;

    @JsonProperty("additional_doctor_2_name")
    public String additionalDoctor2Name;
    public String frequency;
    public String orderEndDate;
    public String upcomingVisitDate;
    public boolean isCashCheckPmt;
    public String collectAmountDescription;
    public String collectAmountTotal;
    public String amountActuallyCollected;
    public Integer patientSpecimenCount;

    @ParcelPropertyConverter(SpecimenRequirementListConverter.class)
    public List<SpecimenRequirementInterface> specimenRequirements;

    @ParcelPropertyConverter(TestListConverter.class)
    public List<TestInterface> tests;

    public Integer mNumberAdditionalScripts;
    public Integer numberAdditionalScriptsCaptured;
    public String mPaymentCollectionNotes;
    public String mCompletedWorkOrderNotes;
    public String workOrderNotes;
    public String notConfirmedReason;
    public String mTubeDrawFailReasonId;
    public String mTubeDrawFailNotes;
    public String agencyName;
    public String mDrawSite;
    public boolean patientUnableToSign;
    public String facilityId;
    public boolean isDobValidationReqd;
    public boolean isVoiceRecReqd;

    public WorkOrder() {
        this.specimenRequirements = new ArrayList<SpecimenRequirementInterface>();
        this.tests = new ArrayList<TestInterface>();
        this.mStatus = WorkOrderStatus.Open;
    }

    public WorkOrder(WorkOrder other) {
        this.mId = other.mId;
        this.mStatus = other.mStatus;
        this.mStartDateTime = other.mStartDateTime;
        this.mEndDateTime = other.mEndDateTime;
        this.routeNumber = other.routeNumber;
        this.stopNumber = other.stopNumber;
        this.drawAreaUnit = other.drawAreaUnit;
        this.drawAreaType = other.drawAreaType;
        this.isConfirmed = other.isConfirmed;
        this.specimenNumber = other.specimenNumber;
        this.isPickupOnlyPatient = other.isPickupOnlyPatient;
        this.visitDate = other.visitDate;
        this.patientName = other.patientName;
        this.patientId = other.patientId;
        this.dob = other.dob;
        this.gender = other.gender;
        this.patientPhone = other.patientPhone;
        this.patientFirstName = other.patientFirstName;
        this.patientLastName = other.patientLastName;
        this.altContactName = other.altContactName;
        this.newDOB = other.newDOB;
        this.altPhoneNo = other.altPhoneNo;
        this.addressLine1 = other.addressLine1;
        this.addressLine2 = other.addressLine2;
        this.city = other.city;
        this.state = other.state;
        this.zipcode = other.zipcode;
        this.latitude = other.latitude;
        this.longitude = other.longitude;
        this.patientComments = other.patientComments;
        this.appointmentTime = other.appointmentTime;
        this.acsInformation = other.acsInformation;
        this.isFasting = other.isFasting;
        this.insuranceType = other.insuranceType;
        this.doctorName = other.doctorName;
        this.additionalDoctor1Name = other.additionalDoctor1Name;
        this.additionalDoctor2Name = other.additionalDoctor2Name;
        this.frequency = other.frequency;
        this.orderEndDate = other.orderEndDate;
        this.upcomingVisitDate = other.upcomingVisitDate;
        this.isCashCheckPmt = other.isCashCheckPmt;
        this.collectAmountDescription = other.collectAmountDescription;
        this.collectAmountTotal = other.collectAmountTotal;
        this.amountActuallyCollected = other.amountActuallyCollected;
        this.patientSpecimenCount = other.patientSpecimenCount;
        if (other.specimenRequirements != null) {
            this.specimenRequirements = new ArrayList<SpecimenRequirementInterface>(other.specimenRequirements.size());
            for (SpecimenRequirementInterface spec : other.specimenRequirements) {
                this.specimenRequirements.add(spec.copy());
            }
        }
        if (other.tests != null) {
            this.tests = new ArrayList<TestInterface>(other.tests.size());
            for (TestInterface t : other.tests) {
                this.tests.add(t.copy());
            }
        }
        this.mNumberAdditionalScripts = other.mNumberAdditionalScripts;
        this.numberAdditionalScriptsCaptured = other.numberAdditionalScriptsCaptured;
        this.mPaymentCollectionNotes = other.mPaymentCollectionNotes;
        this.mCompletedWorkOrderNotes = other.mCompletedWorkOrderNotes;
        this.workOrderNotes = other.workOrderNotes;
        this.notConfirmedReason = other.notConfirmedReason;
        this.mTubeDrawFailReasonId = other.mTubeDrawFailReasonId;
        this.mTubeDrawFailNotes = other.mTubeDrawFailNotes;
        this.agencyName = other.agencyName;
        this.mDrawSite = other.mDrawSite;
        this.patientUnableToSign = other.patientUnableToSign;
        this.facilityId = other.facilityId;
        this.isDobValidationReqd = other.isDobValidationReqd;
        this.isVoiceRecReqd = other.isVoiceRecReqd;
    }

    public WorkOrder copy() {
        return new WorkOrder(this);
    }

    public String getFullAddress() {
        String result = addressLine1 + "\n";
        if (addressLine2 != null && !addressLine2.isEmpty()) {
            result += addressLine2 + "\n";
        }
        result += city + ", " + state + " " + zipcode;
        return result;
    }

    public String getAdditionalDoctorNames() {
        String result = additionalDoctor1Name;
        if (additionalDoctor2Name != null && !additionalDoctor2Name.isEmpty()) {
            result += ", " + additionalDoctor2Name;
        }
        return result;
    }

    public String getTestNames() {
        String result = "";
        for (TestInterface test : tests) {
            if (!result.equals("")) {
                result += ", ";
            }
            result += test.getTestName();
        }
        return result;
    }

    public CompletedWorkOrderInterface getCompletedOrder() {
        return CompletedWorkOrder.from(this);
    }

    public CompletedWorkOrderInterface getFailedOrder(String failureReasonId, String failureComments) {
        CompletedWorkOrderInterface result = getCompletedOrder();
        FailInfo failInfo = new FailInfo();
        failInfo.setFailedDateTime(new DateTime());
        failInfo.setReasonId(failureReasonId);
        failInfo.setFailComments(failureComments);

        result.setFailedCollection(failInfo);
        return result;
    }

    public CompletedWorkOrderInterface getSuccessfulOrder() {
        CompletedWorkOrderInterface result = getCompletedOrder();

        SuccessInfoInterface successInfo = new SuccessInfo();
        successInfo.setCollectionTime(new DateTime());
        successInfo.setDrawSite(mDrawSite);
        successInfo.setTubesCollected(new ArrayList<TubeInfoInterface>());
        for (SpecimenRequirementInterface requirement : specimenRequirements) {
            if (requirement.getTubeTypeId() != TubeInfo.EXTRA_TUBE_ID || (requirement.getTubeCountCollected() > 0)) {
                result.setDrawSuccessfull(true);
                TubeInfoInterface tubeInfo = new TubeInfo();
                tubeInfo.setTubeTypeId(requirement.getTubeTypeId());
                tubeInfo.setTubeCount(requirement.getTubeCountCollected());
                successInfo.getTubesCollected().add(tubeInfo);
            }
        }
        successInfo.setTubeFailReasonId(this.mTubeDrawFailReasonId);
        successInfo.setTubeFailReasonComment(this.mTubeDrawFailNotes);
        result.setSuccessCollection(successInfo);
        return result;
    }

    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    public Integer getStopNumber() {
        return stopNumber;
    }

    public void setStopNumber(Integer stopNumber) {
        this.stopNumber = stopNumber;
    }

    public String getSpecimenNumber() {
        return specimenNumber;
    }

    public void setSpecimenNumber(String specimenNumber) {
        this.specimenNumber = specimenNumber;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    public DateTime getVisitDateTime() {
        return visitDate;
    }

    public void setVisitDateTime(DateTime visitDate) {
        this.visitDate = visitDate;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() { return patientId; }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getAltContactName() {
        return altContactName;
    }

    public void setAltContactName(String altContactName) {
        this.altContactName = altContactName;
    }

    public String getAltPhoneNo() {
        return altPhoneNo;
    }

    public void setAltPhoneNo(String altPhoneNo) {
        this.altPhoneNo = altPhoneNo;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPatientComments() {
        return patientComments;
    }

    public void setPatientComments(String patientComments) {
        this.patientComments = patientComments;
    }

    public String getWorkOrderNotes() {
        return workOrderNotes;
    }

    public void setWorkOrderNotes(String mWorkOrderNotes) {
        this.workOrderNotes = mWorkOrderNotes;
    }

    public String getNotConfirmedReason() {
        return notConfirmedReason;
    }

    public void setNotConfirmedReason(String notConfirmedReason) {
        this.notConfirmedReason = notConfirmedReason;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getAcsInformation() {
        return acsInformation;
    }

    public void setAcsInformation(String acsInformation) {
        this.acsInformation = acsInformation;
    }

    public String getIsFasting() {
        return isFasting;
    }

    public void setIsFasting(String isFasting) {
        this.isFasting = isFasting;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getDoctorName() {
        return doctorName;
    }

    @Override public String getAdditionalDoctorName1() {
        return null;
    }

    @Override public String getAdditionalDoctorName2() {
        return null;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public boolean isCashCheck() {
        return isCashCheckPmt;
    }

    public void setCashCheck(boolean isCashCheckPmt) {
        this.isCashCheckPmt = isCashCheckPmt;
    }

    public String getAdditionalDoctor1Name() {
        return additionalDoctor1Name;
    }

    public void setAdditionalDoctor1Name(String additionalDoctor1Name) {
        this.additionalDoctor1Name = additionalDoctor1Name;
    }

    public String getAdditionalDoctor2Name() {
        return additionalDoctor2Name;
    }

    public void setAdditionalDoctor2Name(String additionalDoctor2Name) {
        this.additionalDoctor2Name = additionalDoctor2Name;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getOrderEndDate() {
        return orderEndDate;
    }

    public void setOrderEndDate(String orderEndDate) {
        this.orderEndDate = orderEndDate;
    }

    public String getUpcomingVisitDate() {
        return upcomingVisitDate;
    }

    public void setUpcomingVisitDate(String upcomingVisitDate) {
        this.upcomingVisitDate = upcomingVisitDate;
    }

    public String getCollectAmountDescription() {
        return collectAmountDescription;
    }

    public void setCollectAmountDescription(String collectAmountDescription) {
        this.collectAmountDescription = collectAmountDescription;
    }

    public String getCollectAmountTotal() {
        return collectAmountTotal;
    }

    @Override public Integer getPatientSpecimentCount() {
        return patientSpecimenCount;
    }

    public void setCollectAmountTotal(String collectAmountTotal) {
        this.collectAmountTotal = collectAmountTotal;
    }

    public void setPatientSpecimenCount(Integer patientSpecimenCount) {
        this.patientSpecimenCount = patientSpecimenCount;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public List<SpecimenRequirementInterface> getSpecimenRequirements() {
        return specimenRequirements;
    }

    public void setSpecimenRequirements(List<SpecimenRequirementInterface> specimenRequirements) {
        this.specimenRequirements = specimenRequirements;
    }

    public List<TestInterface> getTests() {
        return tests;
    }

    @Override public Integer getNumberAdditionalScripts() {
        return mNumberAdditionalScripts;
    }

    @Override public void setNumberAdditionalScripts(Integer numberAdditionalScripts) {
        this.mNumberAdditionalScripts = numberAdditionalScripts;
    }

    @Override public Integer getNumberAdditionalScriptsCollected() {
        return numberAdditionalScriptsCaptured;
    }

    @Override public void setNumberAdditionalScriptsCollected(Integer number) {
        this.numberAdditionalScriptsCaptured = number;
    }

    public void setTests(List<TestInterface> tests) {
        this.tests = tests;
    }

    public void setStatus(WorkOrderStatus status) {
        this.mStatus = status;
    }

    @Override public WorkOrderStatus getStatus() {
        return mStatus;
    }

    public String getAmountActuallyCollected() {
        return amountActuallyCollected;
    }

    public void setAmountActuallyCollected(String amountActuallyCollected) {
        this.amountActuallyCollected = amountActuallyCollected;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public Integer getNumberAdditionalScriptsCaptured() {
        return numberAdditionalScriptsCaptured;
    }

    public void setNumberAdditionalScriptsCaptured(Integer numberAdditionalScriptsCaptured) {
        this.numberAdditionalScriptsCaptured = numberAdditionalScriptsCaptured;
    }

    public String getPaymentCollectionNotes() {
        return mPaymentCollectionNotes;
    }

    public void setPaymentCollectionNotes(String mPaymentCollectionNotes) {
        this.mPaymentCollectionNotes = mPaymentCollectionNotes;
    }

    public String getTubeDrawFailReasonId() {
        return mTubeDrawFailReasonId;
    }

    public void setTubeDrawFailReasonId(String mTubeDrawFailReasonId) {
        this.mTubeDrawFailReasonId = mTubeDrawFailReasonId;
    }

    public String getTubeDrawFailNotes() {
        return mTubeDrawFailNotes;
    }

    public void setTubeDrawFailNotes(String mTubeDrawFailNotes) {
        this.mTubeDrawFailNotes = mTubeDrawFailNotes;
    }

    public String getDrawSite() {
        return mDrawSite;
    }

    public void setDrawSite(String mDrawSite) {
        this.mDrawSite = mDrawSite;
    }

    public boolean isPatientUnableToSign() {
        return patientUnableToSign;
    }

    public void setPatientUnableToSign(boolean patientUnableToSign) {
        this.patientUnableToSign = patientUnableToSign;
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public DateTime getStartDateTime() {
        return mStartDateTime;
    }

    public void setStartDateTime(DateTime mStartDateTime) {
        this.mStartDateTime = mStartDateTime;
    }

    public DateTime getEndDateTime() {
        return mEndDateTime;
    }

    public void setEndDateTime(DateTime mEndDateTime) {
        this.mEndDateTime = mEndDateTime;
    }

    public String getCompletedWorkOrderNotes() {
        return mCompletedWorkOrderNotes;
    }
    public void setCompletedWorkOrderNotes(String completedWorkOrderNotes) {
        this.mCompletedWorkOrderNotes = completedWorkOrderNotes;
    }

    public void setNewDOB(String dob) {
        this.newDOB = dob;
    }
    public String getNewDOB() {
        return newDOB;
    }

    @Override
    public boolean isDobValidationReqd() {
        return isDobValidationReqd;
    }

    public void setDobValidationReqd(boolean b) {
        isDobValidationReqd = b;
    }

    @Override
    public boolean isVoiceRecReqd() {
        return isVoiceRecReqd;
    }

    public void setVoiceRecReqd(boolean b) {
        isVoiceRecReqd = b;
    }

    public String getDrawAreaUnit() {
        return drawAreaUnit;
    }

    public void setDrawAreaUnit(String drawAreaUnit) {
        this.drawAreaUnit = drawAreaUnit;
    }

    public boolean isPickupOnlyPatient() {
        return isPickupOnlyPatient;
    }

    public void setPickupOnlyPatient(boolean pickupOnlyPatient) {
        isPickupOnlyPatient = pickupOnlyPatient;
    }

    public String getDrawAreaType() {
        return drawAreaType;
    }

    public void setDrawAreaType(String drawAreaType) {
        this.drawAreaType = drawAreaType;
    }


}
