package com.apex.mpad.model.iface;

import com.apex.mpad.model.CompletedWorkOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = CompletedWorkOrder.class)
public interface CompletedWorkOrderInterface {
    void setFailedCollection(FailInfoInterface failCollection);
    void setDrawSuccessfull(boolean isSuccessful);
    void setSuccessCollection(SuccessInfoInterface successCollection);
    void setSpecimenNumber(String specimenNumber);
    void setConfirmed(boolean isConfirmed);
    void setAdditionalScript(boolean hasAdditionalScript);

    String getSpecimenNumber();
    boolean isConfirmed();
    FailInfoInterface getFailedCollection();
    SuccessInfoInterface getSuccessCollection();
    boolean hasAdditionalScript();
    double getAmountCollected();
    void setAmountCollected(double amount);
    String getPaymentCollectionNotes();
    void setPaymentCollectionNotes(String notes);
    Integer getPatientSpecimenCount();
    boolean isPatientUnableToSign();
    void setPatientUnableToSign(boolean isUnableToSign);
    boolean isAddOnPatient();
    String getPatientName();
    String getPatientFirstName();
    String getPatientLastName();
    String getDob();
    boolean isDrawSuccessfull();
    String getTubeDrawFailNotes();
    void setTubeDrawFailNotes(String notes);
    String getTubeDrawFailReasonId();
    void setTubeDrawFailReasonId(String id);
    String getCompletedWorkOrderNotes();
    void setPatientSpecimenCount(Integer count);
    void setCompletedWorkOrderNotes(String notes);
    void setPatientFirstName(String firstName);
    void setPatientLastName(String lastName);
    void setDob(String dob);
    void setAddOnPatient(boolean isAddOnPatient);
    void setNewDOB(String dob);
    String getNewDOB();
}
