package com.apex.mpad.model.iface;

import com.apex.mpad.model.DrawSite;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = DrawSite.class)
public interface DrawSiteInterface {
    String getId();
    String getDescription();
}
