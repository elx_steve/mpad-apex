package com.apex.mpad.model.iface;

import com.apex.mpad.model.FailInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.joda.time.DateTime;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = FailInfo.class)
public interface FailInfoInterface {
    DateTime getFailedDateTime();
    void setFailedDateTime(DateTime failedTime);
    String getReasonId();
    void setReasonId(String reasonId);
    String getFailComments();
    void setFailComments(String failComments);
}
