package com.apex.mpad.model.iface;

import com.apex.mpad.model.FailureReason;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = FailureReason.class)
public interface FailureReasonInterface {
    String getId();
    String getDescription();
}
