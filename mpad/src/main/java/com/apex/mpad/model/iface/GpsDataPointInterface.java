package com.apex.mpad.model.iface;

import com.apex.mpad.model.GpsDataPoint;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.joda.time.DateTime;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = GpsDataPoint.class)
public interface GpsDataPointInterface {
    double getLatitude();
    double getLongitude();
    double getDirection();
    double getSpeed();
    DateTime getDateTime();
    String toString();
    GpsDataPointInterface clone();
}
