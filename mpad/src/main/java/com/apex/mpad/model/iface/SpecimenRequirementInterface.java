package com.apex.mpad.model.iface;

import com.apex.mpad.model.SpecimenRequirement;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = SpecimenRequirement.class)
public interface SpecimenRequirementInterface {
    Integer getTubeTypeId();
    void setTubeTypeId(Integer id);
    String getTubeType();
    void setTubeType(String type);
    Integer getTubeCount();
    void setTubeCount(Integer count);
    Integer getTubeCountCollected();
    void setTubeCountCollected(Integer count);
    String getBarcode();
    void setBarcode(String barcode);
    String getBarcodeName();
    void setBarcodeName(String barcodeName);
    boolean printBarcode();
    void setPrintBarcode(boolean printBarcode);
    boolean hasCountDiscrepancy();
    boolean isExtra();
    SpecimenRequirement copy();
}
