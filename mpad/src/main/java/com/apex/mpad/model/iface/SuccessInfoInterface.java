package com.apex.mpad.model.iface;

import com.apex.mpad.model.SuccessInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = SuccessInfo.class)
public interface SuccessInfoInterface {
    DateTime getCollectionTime();
    void setCollectionTime(DateTime completedDateTime);
    String getDrawSite();
    void setDrawSite(String drawSite);
    void setTubesCollected(List<TubeInfoInterface> tubesCollected);
    List<TubeInfoInterface> getTubesCollected();

    void setTubeFailReasonId(String id);

    void setTubeFailReasonComment(String notes);

    String getTubeFailReasonId();

    String getTubeFailReasonComment();

}
