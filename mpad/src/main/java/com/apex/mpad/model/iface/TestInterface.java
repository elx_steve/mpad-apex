package com.apex.mpad.model.iface;

import com.apex.mpad.model.Test;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = Test.class)
public interface TestInterface {
    String getTestName();
    Test copy();
}
