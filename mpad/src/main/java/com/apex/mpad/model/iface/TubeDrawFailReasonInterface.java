package com.apex.mpad.model.iface;

import com.apex.mpad.model.TubeDrawFailReason;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = TubeDrawFailReason.class)
public interface TubeDrawFailReasonInterface {
    public String getFailReason();
    public String getFailReasonId();
}
