package com.apex.mpad.model.iface;

import com.apex.mpad.model.TubeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = TubeInfo.class)
public interface TubeInfoInterface {
    void setTubeTypeId(Integer id);
    void setTubeCount(Integer count);

    Integer getTubeTypeId();

    Integer getTubeCount();
}
