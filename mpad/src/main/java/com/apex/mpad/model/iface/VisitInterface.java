package com.apex.mpad.model.iface;

import com.apex.mpad.model.Visit;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Created by will on 4/9/15.
 */
@JsonDeserialize(as = Visit.class)
public interface VisitInterface {
    public int getStopNumber();
    public void setStopNumber(int stop);
    public WorkOrderInterface getCurrentWorkOrder();
    public void setCurrentWorkOrder(WorkOrderInterface currentOrder);
    public void addWorkOrder(WorkOrderInterface workOrder);
    public boolean isComplete();
    public boolean hasSkips();
    public boolean hasMoreWorkOrders();
    public boolean patientHasMoreWorkOrders();
    public boolean patientHasMultipleOpenWorkOrders();
    public List<WorkOrderInterface> getWorkOrders();
    public List<WorkOrderInterface> getPatientWorkOrders();
    public int getNumPatients();
    public int getNumSkippedPatients();
    public int getOpenWorkOrderCount();
    public WorkOrderInterface getNextOrder();
    public List<WorkOrderInterface> getCurrentPatientWorkOrders();
    String getArea();
    String getAreaType();
    void setArea(String area);
    void setAreaType(String type);
}
