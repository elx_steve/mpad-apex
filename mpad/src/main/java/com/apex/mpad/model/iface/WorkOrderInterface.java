package com.apex.mpad.model.iface;

import com.apex.mpad.model.WorkOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by will on 4/9/15.
 */

@JsonDeserialize(as = WorkOrder.class)
public interface WorkOrderInterface {
    long getId();
    DateTime getStartDateTime();
    DateTime getEndDateTime();
    DateTime getVisitDateTime();
    String getRouteNumber();
    String getPatientName();
    String getPatientLastName();
    String getPatientFirstName();
    String getDob();
    String getGender();
    String getPatientPhone();
    String getAltContactName();
    String getAltPhoneNo();
    String getAddressLine1();
    String getAddressLine2();
    String getCity();
    String getState();
    String getZipcode();
    double getLatitude();
    double getLongitude();
    String getPatientComments();
    void setPatientComments(String comments);
    String getAcsInformation();
    String getIsFasting();
    String getInsuranceType();
    String getDoctorName();
    String getAdditionalDoctorName1();
    String getAdditionalDoctorName2();
    String getFrequency();
    String getOrderEndDate();
    String getUpcomingVisitDate();
    boolean isCashCheck();
    String getCollectAmountDescription();
    String getCollectAmountTotal();
    Integer getPatientSpecimentCount();
    List<SpecimenRequirementInterface> getSpecimenRequirements();
    List<TestInterface> getTests();
    Integer getNumberAdditionalScripts();
    void setNumberAdditionalScripts(Integer numberAdditionalScripts);
    Integer getNumberAdditionalScriptsCollected();
    void setNumberAdditionalScriptsCollected(Integer number);
    String getFullAddress();
    String getPaymentCollectionNotes();
    void setPaymentCollectionNotes(String notes);
    String getDrawSite();
    void setDrawSite(String drawSite);
    boolean isPatientUnableToSign();
    void setPatientUnableToSign(boolean isUnable);
    String getFacilityId();
    String getCompletedWorkOrderNotes();
    void setCompletedWorkOrderNotes(String notes);
    String getWorkOrderNotes();
    String getNotConfirmedReason();
    String getTubeDrawFailNotes();
    void setTubeDrawFailNotes(String notes);
    String getTubeDrawFailReasonId();
    void setTubeDrawFailReasonId(String id);
    String getAdditionalDoctorNames();
    String getAgencyName();
    String getTestNames();
    CompletedWorkOrderInterface getSuccessfulOrder();
    CompletedWorkOrderInterface getFailedOrder(String failureReasonId, String failureReasonComments);
    WorkOrder.WorkOrderStatus getStatus();
    void setStatus(WorkOrder.WorkOrderStatus status);
    String getPatientId();
    Integer getStopNumber();
    String getDrawAreaUnit();
    String getDrawAreaType();
    String getSpecimenNumber();
    String getAmountActuallyCollected();
    void setAmountActuallyCollected(String amount);
    boolean isConfirmed();
    void setConfirmed(boolean isConfirmed);
    String getAppointmentTime();
    void setAppointmentTime(String appointmentTime);

    void setPatientFirstName(String firstName);
    void setPatientLastName(String lastName);

    void setPatientName(String s);

    void setDob(String dob);

    void setWorkOrderNotes(String notes);

    void setNewDOB(String dob);
    String getNewDOB();

    boolean isDobValidationReqd();
    boolean isVoiceRecReqd();

    boolean isPickupOnlyPatient();

    WorkOrder copy();
}
