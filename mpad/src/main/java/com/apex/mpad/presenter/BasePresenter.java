package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.VisitFailedActivity;
import com.apex.mpad.activity.WelcomeActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/24/15.
 */
public abstract class BasePresenter implements BasePresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private BaseActivity mActivity;

    public static final String HOUSE_CALL = "HC";

    public BasePresenter() {
        Injector.INSTANCE.inject(this);
    }

    @Override public void onCreate(BaseActivity activity) {
        this.mActivity = activity;
        if (mApp.getCurrentWorkOrder() == null) {
            mApp.advanceToNextWorkOrder();
        }
    }

    @Override public void failVisit() {
        mActivity.startActivity(new Intent(mActivity, VisitFailedActivity.class));
    }

    @Override public void skipVisit() {
        mApp.skipVisit();
        mActivity.startActivity(new Intent(mActivity, WelcomeActivity.class));
    }
    @Override public void back() {
        mActivity.finish();
    }
    @Override public boolean hasMultiplePatients() {
        return mApp.getCurrentVisit().getWorkOrders().size() > 1 && mApp.getCurrentVisit().getNumPatients() > 1;
    }
}
