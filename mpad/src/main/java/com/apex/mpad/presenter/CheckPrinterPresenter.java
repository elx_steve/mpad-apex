package com.apex.mpad.presenter;

import com.apex.mpad.activity.AddPrinterActivity;
import com.apex.mpad.activity.CheckPrinterActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.AddPrinterPresenterInterface;
import com.apex.mpad.presenter.iface.CheckPrinterPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class CheckPrinterPresenter extends BasePresenter implements CheckPrinterPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private CheckPrinterActivity mActivity;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (CheckPrinterActivity) activity;
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return null;
    }

    @Override public void close() {
        mActivity.finish();
    }

    @Override public void next() {}
}
