package com.apex.mpad.presenter;

import com.apex.mpad.activity.HelpActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.HelpPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class HelpPresenter extends BasePresenter implements HelpPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private HelpActivity mActivity;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (HelpActivity) activity;
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return null;
    }

    @Override public void close() {
        mActivity.finish();
    }

    @Override public void next() {

    }
}
