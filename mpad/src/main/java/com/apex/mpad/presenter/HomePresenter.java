package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.AddPrinterActivity;
import com.apex.mpad.activity.CheckPrinterActivity;
import com.apex.mpad.activity.HomeActivity;
import com.apex.mpad.activity.NewPatientActivity;
import com.apex.mpad.activity.RouteSummaryListActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.HomePresenterInterface;
import com.apex.mpad.service.FileUploader;
import com.apex.mpad.service.rest.iface.RestClientInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class HomePresenter extends BasePresenter implements HomePresenterInterface {

    @Inject @ForApplication ApexApplication mApp;
    @Inject FileUploader mUploader;
    @Inject RestClientInterface mRestClient;

    private HomeActivity mActivity;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (HomeActivity) activity;
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return null;
    }

    @Override public void addPrinter() {
        mActivity.startActivity(new Intent(mActivity, AddPrinterActivity.class));
    }

    @Override public void checkPrinter() {
        mActivity.startActivity(new Intent(mActivity, CheckPrinterActivity.class));
    }

    @Override public void addPatient() {
        mActivity.startActivity(new Intent(mActivity, NewPatientActivity.class));
    }

    @Override public void refreshWorkOrders() {
        boolean isSuccessful = false;
        mUploader.uploadFiles();
        if (mUploader.getPendingWorkOrdersCount() == 0) {
            List<WorkOrderInterface> workOrders = new ArrayList<WorkOrderInterface>();
            if ((workOrders = mRestClient.getWorkOrders(mApp.getSessionId())) != null) {
                mApp.loadWorkOrders(workOrders);
                isSuccessful = true;
            }
        }
        if (isSuccessful) mApp.setHasMoreWorkOrdersNotification(false);
        mActivity.showRefreshResultDialog(isSuccessful);
        mActivity.handleRefreshButton();
    }

    @Override public boolean hasMoreWorkOrdersNotification() {
        return mApp.hasMoreWorkOrdersNotification();
    }

    @Override public void routeSummary() {
        mActivity.startActivity(new Intent(mActivity, RouteSummaryListActivity.class));
    }

    @Override public void back() {
        mActivity.finish();
    }
    @Override public void next() {

    }
}
