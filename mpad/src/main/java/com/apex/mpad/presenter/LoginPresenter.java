package com.apex.mpad.presenter;

import com.apex.mpad.activity.LoginActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.LoginPresenterInterface;
import com.apex.mpad.service.rest.iface.RestClientInterface;
import com.apex.mpad.service.rest.message.LoginResponseMessage;
import com.apex.mpad.util.*;
import com.apex.mpad.util.async.LoginAsyncTask;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import javax.inject.Named;

import static com.apex.mpad.util.Constants.BUS_REST;
import static com.apex.mpad.util.Constants.BUS_UI;

/**
 * Created by will on 4/9/15.
 */
public class LoginPresenter extends BasePresenter implements LoginPresenterInterface, LoginAsyncTask.LoginComplete {

    @Inject @ForApplication ApexPrefs mPrefs;
    @Inject @ForApplication ApexApplication mApp;
    @Inject @Named("device_id") String deviceId;

    @Inject @Named(BUS_UI) ApexBus bus;
    @Inject @Named(BUS_REST) ApexBus restBus;

    @Inject RestClientInterface restClient;
    private LoginActivity mActivity;

    @Inject public LoginPresenter() {}

    @Override public void login(final String username, String password) {
        if (username.equals("admin") && password.equals(mPrefs.getHiddenAdminPassword())) {
            mActivity.openPrefs();
        } else {
            mActivity.showLoading();
            LoginAsyncTask task = new LoginAsyncTask(username, password, this);
            task.execute();
        }
    }

    @Override public void onLoginComplete(boolean isSuccess, LoginResponseMessage message) {
        mActivity.stopLoading();
        if (isSuccess) {
            mActivity.moveToNext();
        } else if (message == null) {
            mActivity.showErrorDialog("A Network connection is needed");
            return;
        } else if (message.getError() != null && !message.getError().getDescription().isEmpty()) {
            mActivity.showErrorDialog(message.getError().getDescription());
        }
    }

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (LoginActivity) activity;
        bus.register(this);
        restBus.register(this);
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return null;
    }
    @Override public void next() {

    }
}
