package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.NewPatientActivity;
import com.apex.mpad.activity.VisitCompletedActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.SpecimenRequirement;
import com.apex.mpad.model.TubeInfo;
import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.DrawSiteInterface;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import com.apex.mpad.model.iface.TubeInfoInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.NewPatientPresenterInterface;
import com.apex.mpad.service.PrintService;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class NewPatientPresenter extends BasePresenter implements NewPatientPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;
    @Inject PrintService mPrintService;

    private NewPatientActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    private String mFirstName;
    private String mLastName;
    private String mDob;
    private String mDrawSite;
    private String mNotes;

    private int mTubeCount;

    private List<String> mDrawSites;

    private boolean mHasPrinted;

    @Inject public NewPatientPresenter() {}

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (NewPatientActivity) activity;
        this.mWorkOrder = new WorkOrder();
        this.mHasPrinted = false;

        mDrawSites = new ArrayList<String>();
        for (DrawSiteInterface drawSite : mApp.getDrawSites()) {
            mDrawSites.add(drawSite.getDescription());
        }
    }

    private String generateBarcode() {
        DateTime dt = new DateTime();
        String barcode = "9" + mApp.getTechId() + dt.getHourOfDay() + dt.getMinuteOfHour() + dt.getSecondOfMinute();
        return barcode;
    }
    private String getDrawSiteId() {
        for (DrawSiteInterface drawSite : mApp.getDrawSites()) {
            if (mDrawSite.equals(drawSite.getDescription())) {
                return drawSite.getId();
            }
        }
        return mDrawSite;
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        TubeInfoInterface tubeInfo = new TubeInfo();
        tubeInfo.setTubeCount(mTubeCount);
        tubeInfo.setTubeTypeId(0);
        if (mApp.getPreviousVisit() != null) {
            mApp.setCurrentVisit(mApp.getPreviousVisit());
        }
        mApp.getCurrentVisit().addWorkOrder(mWorkOrder);
        mApp.setCurrentWorkOrder(mWorkOrder);

        mActivity.startActivity(new Intent(mActivity, VisitCompletedActivity.class));
    }

    @Override public List<String> getDrawSites() {
        return mDrawSites;
    }

    @Override public void print() {
        mActivity.showLoading();
        mActivity.sendFormInfo();
        mWorkOrder.setPatientFirstName(mFirstName);
        mWorkOrder.setPatientLastName(mLastName);
        mWorkOrder.setPatientName(mLastName + ", " + mFirstName);
        mWorkOrder.setDob(mDob);
        mWorkOrder.setDrawSite(getDrawSiteId());
        mWorkOrder.setCompletedWorkOrderNotes(mNotes);

        SpecimenRequirementInterface specimen = new SpecimenRequirement();
        specimen.setBarcode(generateBarcode());
        specimen.setBarcodeName(specimen.getBarcode());
        specimen.setTubeCount(mTubeCount);
        specimen.setTubeType("EXTRA");
        specimen.setTubeTypeId(TubeInfo.EXTRA_TUBE_ID);
        specimen.setPrintBarcode(true);

        boolean allPrinted = true;

        for (int i = 0; i < mTubeCount; i++) {
            if (!mPrintService.printTubeLabel(mWorkOrder, specimen)) {
                allPrinted = false;
            }
        }
        specimen.setTubeCountCollected(mTubeCount);
        mHasPrinted = allPrinted;
        if (allPrinted) mPrintService.disconnect();
        mActivity.stopLoading();
    }
    @Override public void back() {
        mActivity.finish();
    }

    @Override public void setDrawSite(String s) {
        this.mDrawSite = s;
    }
    @Override public int getTubeCount() {
        return mTubeCount;
    }
    @Override public boolean hasPrinted() {
        return mHasPrinted;
    }
    @Override public void setFirstName(String s) {
        this.mFirstName = s;
    }
    @Override public void setLastName(String s) {
        this.mLastName = s;
    }
    @Override public void setDob(String s) {
        this.mDob = s;
    }
    @Override public void setNotes(String s) {
        this.mNotes = s;
    }
    @Override public void setTubeCount(int i) {
        this.mTubeCount = i;
    }
}
