package com.apex.mpad.presenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import com.apex.mpad.activity.NewPatientScriptActivity;
import com.apex.mpad.activity.OrderRequirementListActivity;
import com.apex.mpad.activity.VisitInformationActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.NewPatientScriptPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.FileUtility;
import com.apex.mpad.util.ImageUtility;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by will on 4/10/15.
 */
public class NewPatientScriptPresenter extends BasePresenter implements NewPatientScriptPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;
    @Inject FileUtility mFileUtility;
    @Inject ImageUtility mImageUtility;

    private NewPatientScriptActivity mActivity;
    private WorkOrderInterface mWorkOrder;
    private String mNotes;
    private boolean mIsChangeScript;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (NewPatientScriptActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
    }

    public void saveImage(Bitmap bitmap) {
        String filename = getScriptImageName();

        File sd = Environment.getExternalStorageDirectory();
        File dest = new File(sd, filename);
        try {
            FileOutputStream out = new FileOutputStream(dest);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override public File getFileForImage() {
        String filename = getScriptImageName();
        File sd = Environment.getExternalStorageDirectory();
        File dest = new File(sd, filename);
        return dest;
    }
    private String getScriptImageName() {
        return "Script-" + mWorkOrder.getSpecimenNumber() + "-" + String.valueOf(getNumberCollected() + 1) + ".png";
    }

    @Override public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private int getNumberCollected() {
        return mWorkOrder.getNumberAdditionalScriptsCollected() == null ? 0 : mWorkOrder.getNumberAdditionalScriptsCollected();
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        mActivity.showLoading();
        mWorkOrder.setNumberAdditionalScriptsCollected(getNumberCollected()+1);
        File[] files = mFileUtility.getFiles("", "Script-" + mWorkOrder.getSpecimenNumber() + "-\\d+\\.png", false);
        String imageType = mIsChangeScript ? "CS" : "NS";

        for (File file : files) {
            if (mImageUtility.saveImageForUpload(mApp.getSessionId(), imageType, file.getPath(), mWorkOrder.getSpecimenNumber(), mNotes, mApp.getTechId())) {
                file.delete();
            }
        }

        if (mWorkOrder.getNumberAdditionalScriptsCollected() < mWorkOrder.getNumberAdditionalScripts()) {
            Intent i = new Intent(mActivity, NewPatientScriptActivity.class);
            mActivity.stopLoading();
            mActivity.finish();
            mActivity.startActivity(i);
        } else {
            mActivity.stopLoading();
            mActivity.startActivity(new Intent(mActivity, OrderRequirementListActivity.class));
        }
    }

    @Override public void back() {
        mActivity.startActivity(new Intent(mActivity, VisitInformationActivity.class));
    }

    @Override public void setNotes(String notes){
        this.mNotes = notes;
    }
    @Override public String getNotes() {
        return mNotes;
    }
    @Override public void setChangeScript(boolean isChangeScript) {this.mIsChangeScript = isChangeScript;}
    @Override public boolean isChangeScript() {return mIsChangeScript;}
}
