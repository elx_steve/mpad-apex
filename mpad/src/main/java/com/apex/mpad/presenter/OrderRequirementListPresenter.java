package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.OrderRequirementListActivity;
import com.apex.mpad.activity.PatientScriptsActivity;
import com.apex.mpad.activity.SpecimenRequirementListActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.TubeInfo;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.OrderRequirementListPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class OrderRequirementListPresenter extends BasePresenter implements OrderRequirementListPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private OrderRequirementListActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (OrderRequirementListActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        mActivity.startActivity(new Intent(mActivity, SpecimenRequirementListActivity.class));
    }
    @Override public void back() {
        mActivity.startActivity(new Intent(mActivity, PatientScriptsActivity.class));
    }

    @Override public String[] getOrderRequirementItems() {
        List<String> list = new ArrayList<String>();
        if (mApp.getCurrentVisit() != null && mApp.getCurrentVisit().getCurrentPatientWorkOrders() != null) {
            for (WorkOrderInterface order : mApp.getCurrentVisit().getCurrentPatientWorkOrders()) {
                for (SpecimenRequirementInterface specimen : order.getSpecimenRequirements()) {
                    if (specimen.getTubeTypeId() != TubeInfo.EXTRA_TUBE_ID) {
                        list.add(specimen.getTubeCount() + " " + specimen.getTubeType());
                    }
                }
            }
        }
        String[] result = new String[list.size()];
        result = list.toArray(result);
        return result;
    }
}
