package com.apex.mpad.presenter;

import android.content.Intent;

import com.apex.mpad.activity.PatientDOBActivity;
import com.apex.mpad.activity.PatientInformationActivity;
import com.apex.mpad.activity.VisitInformationActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.PatientDOBPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.DateUtil;
import com.apex.mpad.util.ImageUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

/**
 * Created by John on 6/13/2016.
 */
public class PatientDOBPresenter  extends BasePresenter implements PatientDOBPresenterInterface {
    @Inject
    @ForApplication
    ApexApplication mApp;

    @Inject
    ImageUtility mImageUtility;


    private PatientDOBActivity mActivity;
    private WorkOrderInterface mWorkOrder;
    private String mDob;
    private String mOriginalNotes;
    private String mNewNote;
    private File mAudioFile;

    private List<String> mDOBs;

    public static final String NOTA = "None of the Above";

    private static final Logger mLogger = LoggerFactory.getLogger(PatientDOBPresenter.class);

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (PatientDOBActivity) activity;
        mWorkOrder = mApp.getCurrentWorkOrder();
        mOriginalNotes = mWorkOrder.getWorkOrderNotes();
        mDob = mWorkOrder.getDob();

        buildList(false);
    }

    public boolean doValidateDropdown() {
        mWorkOrder = mApp.getCurrentWorkOrder();
        return mWorkOrder.isDobValidationReqd();
    }

    public boolean doVoiceRecording() {
        mWorkOrder = mApp.getCurrentWorkOrder();
        return mWorkOrder.isVoiceRecReqd();
    }

    /**
     * Builds the DOB list for the drop-down
     */
    @Override public void buildList(boolean isBack) {
        mDOBs = new ArrayList<String>(4);
        WorkOrderInterface wo = getWorkOrder();
        if (wo != null) {
            //first use the DOB from the WO
            String dob1 = wo.getDob();
            //see if the tech has already entered a diff DOB
            String dob2 = wo.getNewDOB();
            //if there is a diff dob, use that as the entry
            if (dob2 != null && dob2.length() > 0) dob1 = dob2;
            //make sure the wo dob is a real date.  If not, choose a random date between 1900 and 1901
            Date ddob = DateUtil.getDate(dob1, DateUtil.randomDate(1900, 1901));
            dob1 = DateUtil.getDateString(ddob);
            mDOBs.add(dob1);
            //create a dob between 1-2 years before the dob in the wo
            String dob3 = DateUtil.getDateString(DateUtil.randomDate(DateUtil.getYear(ddob) - 2, DateUtil.getYear(ddob) - 1));
            //create a dob between 2-3 years before the dob in the wo
            String dob4 = DateUtil.getDateString(DateUtil.randomDate(DateUtil.getYear(ddob) - 3, DateUtil.getYear(ddob) - 2));
            //create a dob between 2-3 years after the dob in the wo
            String dob5 = DateUtil.getDateString(DateUtil.randomDate(DateUtil.getYear(ddob) + 1, DateUtil.getYear(ddob) + 2));
            mDOBs.add(dob3);
            mDOBs.add(dob4);
            mDOBs.add(dob5);

            //randomize the list
            if (!isBack) {
                long seed = System.nanoTime();
                Collections.shuffle(mDOBs, new Random(seed));
            }
            mDOBs.add(NOTA);
        }
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public String getDob() {
        mWorkOrder = mApp.getCurrentWorkOrder();
        return mWorkOrder.getDob();
    }

    @Override public String getNewDob() {
        mWorkOrder = mApp.getCurrentWorkOrder();
        return mWorkOrder.getNewDOB();
    }

    @Override public void setDOB(String s) {
        this.mDob = s;
        mWorkOrder.setNewDOB(s);
    }

    @Override public void showPatientInformation() {
        if (mApp.getCurrentVisit() == null) {
            mActivity.showNoDataDialog();
        }
        else {
            //mWorkOrder.setWorkOrderNotes(mOriginalNotes + mNewNote);
            mActivity.startActivity(new Intent(mActivity, PatientInformationActivity.class));
        }
    }

    private void deleteFile() {
        try {
            mAudioFile.delete();
        } catch (Exception e) {
            mLogger.warn("Failed to delete audio file: {}", e.getMessage());
        }
    }

    @Override public void back() {
        if (this.mAudioFile != null) {
            if (mAudioFile.exists()) {
                deleteFile();
            }
        }
        showPatientInformation();
    }

    @Override public void next() {
        if (mAudioFile != null) {
            if (mAudioFile.exists()) {
                if (mImageUtility.saveAudioForUpload(mApp.getSessionId(), "APID", mAudioFile.getAbsolutePath(), mWorkOrder.getSpecimenNumber(), "", mApp.getTechId())) {
                    deleteFile();
                }
            }
        }
        mActivity.startActivity(new Intent(mActivity, VisitInformationActivity.class));
    }

    @Override public void setAudioFile(File f) {
        this.mAudioFile = f;
    }

    @Override public List<String> getDOBList() {

        List<String> out = new ArrayList<String>(mDOBs);
        //out.add(0, "Select...");
        return out;
    }

    @Override public void setNewNote(String s) {
        this.mNewNote = s;
    }

}
