package com.apex.mpad.presenter;

import android.content.Intent;

import com.apex.mpad.activity.PatientDOBActivity;
import com.apex.mpad.activity.PatientInformationActivity;
import com.apex.mpad.activity.PatientListActivity;
import com.apex.mpad.activity.VisitInformationActivity;
import com.apex.mpad.activity.VisitRouteLocationActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.PatientInformationPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class PatientInformationPresenter extends BasePresenter implements PatientInformationPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private PatientInformationActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (PatientInformationActivity) activity;
        mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void back() {
        if (mApp.getCurrentVisit() == null) {
            mActivity.showNoDataDialog();
        } else if (mApp.getCurrentVisit().getNumPatients() > 1) {
            mActivity.startActivity(new Intent(mActivity, PatientListActivity.class));
        } else {
            mActivity.startActivity(new Intent(mActivity, VisitRouteLocationActivity.class));
        }
    }
    @Override public void next() {
        if (mApp.getCurrentVisit() == null) {
            mActivity.showNoDataDialog();
        } else {
            if (mWorkOrder.isDobValidationReqd() || mWorkOrder.isVoiceRecReqd()) {
                mActivity.startActivity(new Intent(mActivity, PatientDOBActivity.class));
            } else {
                mWorkOrder.setNewDOB(mWorkOrder.getDob());
                mActivity.startActivity(new Intent(mActivity, VisitInformationActivity.class));
            }
        }
    }
}
