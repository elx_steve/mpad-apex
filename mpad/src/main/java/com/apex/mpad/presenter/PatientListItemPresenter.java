package com.apex.mpad.presenter;

import android.content.Intent;

import com.apex.mpad.activity.PatientInformationActivity;
import com.apex.mpad.activity.PatientListActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.PatientListItemPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/24/15.
 */
public class PatientListItemPresenter implements PatientListItemPresenterInterface {
    @Inject @ForApplication ApexApplication mApp;

    private WorkOrderInterface mWorkOrder;
    private PatientListActivity mActivity;

    private String mButtonText = "Go";

    public PatientListItemPresenter(WorkOrderInterface wo, BaseActivity activity) {
        Injector.INSTANCE.inject(this);
        this.mWorkOrder = wo;
        this.mActivity = (PatientListActivity) activity;
    }
    @Override public void startOrder() {
        mApp.setCurrentWorkOrder(mWorkOrder);
        mApp.getCurrentVisit().setCurrentWorkOrder(mWorkOrder);
        mActivity.startActivity(new Intent(mActivity, PatientInformationActivity.class));
    }
    @Override public String getLocation() {
        return mWorkOrder.getAddressLine2() == null || mWorkOrder.getAddressLine2().isEmpty() ? "N/A" : mWorkOrder.getAddressLine2();
    }
    @Override public void setButtonText(String text) {this.mButtonText = text;}
    @Override public String getButtonText() {
        return mButtonText;
    }
    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }
    @Override public WorkOrder.WorkOrderStatus getStatus() {
        return mWorkOrder.getStatus();
    }
    @Override public String getNameAndDob() {return mWorkOrder.getPatientName() + " - " + mWorkOrder.getDob();}
    @Override public String getName() {return mWorkOrder.getPatientName();}
    @Override public String getDob() {return mWorkOrder.getDob();}
}
