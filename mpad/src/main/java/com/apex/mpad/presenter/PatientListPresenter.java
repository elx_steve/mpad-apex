package com.apex.mpad.presenter;

import com.apex.mpad.activity.PatientListActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.VisitInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.PatientListItemPresenterInterface;
import com.apex.mpad.presenter.iface.PatientListPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class PatientListPresenter extends BasePresenter implements PatientListPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private List<PatientListItemPresenterInterface> mItems;

    private PatientListActivity mActivity;
    private WorkOrderInterface mWorkOrder;
    private VisitInterface mVisit;

    private String mFacilityName;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (PatientListActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
        this.mVisit = mApp.getCurrentVisit();
    }

    private void createPatientListItems() {
        List<PatientListItemPresenterInterface> items = new ArrayList<PatientListItemPresenterInterface>();
        String patientName = "";
        mFacilityName = "";

        for (WorkOrderInterface wo : mVisit.getWorkOrders()) {
            if (mFacilityName.isEmpty()) {
                mFacilityName = wo.getAddressLine1() == null || wo.getAddressLine1().isEmpty() ? "N/A" : wo.getAddressLine1();
            }
            if (!patientName.equals(wo.getPatientName())) {
                items.add(new PatientListItemPresenter(wo, mActivity));
            }
            patientName = wo.getPatientName();
        }
        mItems = items;
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }
    @Override public String getFacilityName() {return mFacilityName;}
    @Override public List<PatientListItemPresenterInterface> getItems() {
        if (mItems == null) {
            createPatientListItems();
        }
        return mItems;
    }

    @Override public void next() {

    }
}
