package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.*;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.PatientScriptsPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class PatientScriptsPresenter extends BasePresenter implements PatientScriptsPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private WorkOrderInterface mWorkOrder;
    private PatientScriptsActivity mActivity;
    private int mNumberAdditionalScripts;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (PatientScriptsActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public int getNumberAdditionalScripts() {
        return mNumberAdditionalScripts;
    }

    @Override public void setNumberAdditionalScripts(int number) {
        this.mNumberAdditionalScripts = number;
    }

    @Override public void next() {
        if (mWorkOrder == null) {
            mActivity.showNoDataDialog();
            return;
        }
        mWorkOrder.setNumberAdditionalScripts(mNumberAdditionalScripts);
        mWorkOrder.setNumberAdditionalScriptsCollected(0);
        Intent i = new Intent();

        if (mNumberAdditionalScripts > 0) {
            i.setClass(mActivity, NewPatientScriptActivity.class);
        } else {
            i.setClass(mActivity, OrderRequirementListActivity.class);
        }
        mActivity.startActivity(i);
    }
    @Override public void back() {
        mActivity.startActivity(new Intent(mActivity, PatientInformationActivity.class));
    }
}
