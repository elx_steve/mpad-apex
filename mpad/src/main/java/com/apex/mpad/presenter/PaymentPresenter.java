package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.*;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.PaymentPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class PaymentPresenter extends BasePresenter implements PaymentPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private PaymentActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    private String mAmountCollected;
    private String mNotes;

    @Inject public PaymentPresenter() {}

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (PaymentActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        mActivity.sendFormInfo();

        mWorkOrder.setPaymentCollectionNotes(mNotes);
        mWorkOrder.setAmountActuallyCollected(mAmountCollected);
        if (mApp.patientHasMultipleOpenWorkOrders()) {
            mActivity.startActivity(new Intent(mActivity, VisitCompletedMoreOrdersActivity.class));
        } else {
            mActivity.startActivity(new Intent(mActivity, VisitCompletedActivity.class));
        }

    }

    @Override public void back() {
        if (mWorkOrder.getTubeDrawFailReasonId() == null || mWorkOrder.getTubeDrawFailReasonId().isEmpty()) {
            mActivity.startActivity(new Intent(mActivity, SpecimenRequirementListActivity.class));
        } else {
            mActivity.startActivity(new Intent(mActivity, TubeFailReasonActivity.class));
        }
    }

    @Override public void setAmountCollected(String s) {
        this.mAmountCollected = s;
    }

    @Override public void setNotes(String s) {
        this.mNotes = s;
    }
}
