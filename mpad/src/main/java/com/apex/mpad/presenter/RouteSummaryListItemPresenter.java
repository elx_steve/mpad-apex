package com.apex.mpad.presenter;

import com.apex.mpad.presenter.iface.RouteSummaryListItemPresenterInterface;

/**
 * Created by will on 5/14/15.
 */
public class RouteSummaryListItemPresenter implements RouteSummaryListItemPresenterInterface {

    private String mText;
    private boolean mIsComplete;

    public RouteSummaryListItemPresenter(String text, boolean isComplete) {
        this.mText = text;
        this.mIsComplete = isComplete;
    }
    @Override public String getText() {
        return mText;
    }

    @Override public boolean isComplete() {
        return mIsComplete;
    }
}
