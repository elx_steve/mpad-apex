package com.apex.mpad.presenter;

import android.content.Intent;

import com.apex.mpad.activity.RouteSummaryListActivity;
import com.apex.mpad.activity.WelcomeActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.VisitInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.RouteSummaryListItemPresenterInterface;
import com.apex.mpad.presenter.iface.RouteSummaryListPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class RouteSummaryListPresenter extends BasePresenter implements RouteSummaryListPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private RouteSummaryListActivity mActivity;

    private List<RouteSummaryListItemPresenterInterface> mRouteSummaryItems;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (RouteSummaryListActivity) activity;
    }

    private void createRouteSummaryItems() {
        mRouteSummaryItems = new ArrayList<RouteSummaryListItemPresenterInterface>();
        for (VisitInterface visit : mApp.getVisits()) {
            String text = "";
            WorkOrderInterface wo = visit.getWorkOrders().get(0);
            if (visit.getAreaType().toUpperCase().equals(HOUSE_CALL)) {
                if (visit.getNumPatients() > 1) {
                    text += "(HC Multi-Patent) ";
                } else {
                    text += wo.getPatientName() + " ";
                }

            } else {
                text = "(" + visit.getArea() + ") ";
            }
            text += wo.getAddressLine1() + " " + wo.getAddressLine2();

            RouteSummaryListItemPresenterInterface item = new RouteSummaryListItemPresenter(text, visit.isComplete());
            mRouteSummaryItems.add(item);

        }

    }

    @Override public WorkOrderInterface getWorkOrder() {
        return null;
    }


    @Override public void next() {

    }

    @Override public List<RouteSummaryListItemPresenterInterface> getRouteSummaryItems() {
        if (mRouteSummaryItems == null) {
            createRouteSummaryItems();
        }
        return mRouteSummaryItems;
    }

    @Override public void close(boolean isFirstLaunch) {
        mActivity.finish();
        if (isFirstLaunch) mActivity.startActivity(new Intent(mActivity, WelcomeActivity.class));
    }
}
