package com.apex.mpad.presenter;

import android.widget.Button;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.SpecimenRequirementListItemPresenterInterface;
import com.apex.mpad.service.PrintService;
import com.apex.mpad.util.ApexApplication;

import javax.inject.Inject;

/**
 * Created by will on 4/24/15.
 */
public class SpecimenRequirementListItemPresenter implements SpecimenRequirementListItemPresenterInterface {
    @Inject @ForApplication ApexApplication mApp;
    @Inject PrintService mPrintService;

    private SpecimenRequirementInterface mSpecimen;

    private boolean mHasNotPrinted = true;
    private String mButtonText = "Print";
    private Integer mTubeCountCollected;
    private Button mButton;
    private WorkOrderInterface mWorkOrder;

    public SpecimenRequirementListItemPresenter(SpecimenRequirementInterface specimen, WorkOrderInterface wo) {
        Injector.INSTANCE.inject(this);
        this.mSpecimen = specimen;
        this.mTubeCountCollected = 0;
        this.mWorkOrder = wo;
    }

    public String getButtonText() {
        return mButtonText;
    }
    public SpecimenRequirementInterface getSpecimen() {
        return mSpecimen;
    }
    public Integer getTubeCountCollected() {
        return mTubeCountCollected;
    }
    public void setTubeCountCollected(Integer count) {
        this.mTubeCountCollected = count;
        this.mSpecimen.setTubeCountCollected(count);
    }

    public boolean hasNotPrinted() {
        return mHasNotPrinted;
    }
    public void setHasNotPrinted(boolean hasNotPrinted) {
        this.mHasNotPrinted = hasNotPrinted;
        this.mButtonText = !hasNotPrinted ? "Reprint" : "Print";
        int i = 0;
    }
    public boolean hasPrinted() {
        return (!mHasNotPrinted || mSpecimen.getTubeCountCollected() == null || mSpecimen.getTubeCountCollected() == 0);
    }

    public void print() {
        boolean allPrinted = true;
        for (int i = 0; i < mSpecimen.getTubeCountCollected(); i++) {
            if (!mPrintService.printTubeLabel(mWorkOrder, mSpecimen)) {
                allPrinted = false;
            }
        }
        mSpecimen.setTubeCountCollected(mTubeCountCollected);
        setHasNotPrinted(!allPrinted);
    }
    public void setButton(Button button) {
        this.mButton = button;
    }
    public Button getButton() {
        return mButton;
    }

    public void disconnectPrinter() {
        mPrintService.disconnect();
    }
}
