package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.*;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.SpecimenRequirement;
import com.apex.mpad.model.TubeInfo;
import com.apex.mpad.model.iface.DrawSiteInterface;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.SpecimenRequirementListItemPresenterInterface;
import com.apex.mpad.presenter.iface.SpecimenRequirementListPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.async.PrintAllAsync;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class SpecimenRequirementListPresenter extends BasePresenter implements SpecimenRequirementListPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private SpecimenRequirementListActivity mActivity;
    private WorkOrderInterface mWorkOrder;
    private WorkOrderInterface mClonedWorkOrder;
    private List<DrawSiteInterface> mDrawSites;
    private List<SpecimenRequirementListItemPresenterInterface> mSpecimens;

    private boolean canMoveNext;
    private String drawSite;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (SpecimenRequirementListActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
        this.mClonedWorkOrder = mWorkOrder.copy();
        mDrawSites = mApp.getDrawSites();
        mSpecimens = null;
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public List<SpecimenRequirementListItemPresenterInterface> getSpecimenRequirementList() {
        if (mSpecimens == null) {
            createSpecimenList();
        }
        return mSpecimens;
    }

    @Override public void printAll() {
        if (!mApp.isPrinting()) {
            PrintAllAsync print = new PrintAllAsync(mActivity, mSpecimens);
            print.execute();
        }
    }

    private void createSpecimenList() {
        List<SpecimenRequirementListItemPresenterInterface> result = new ArrayList<SpecimenRequirementListItemPresenterInterface>();
        boolean hasExtra = checkHasExtra();

        String extraBarcode = "";
        boolean extraPrintBarcode = true;

        for (SpecimenRequirementInterface specimen : getNotGreySpecimenList()) {
            extraBarcode = specimen.getBarcode();
            extraPrintBarcode = specimen.printBarcode();
            break;
        }

        if (!hasExtra) {
            SpecimenRequirementInterface extraReq = new SpecimenRequirement();
            extraReq.setBarcode(extraBarcode);
            extraReq.setBarcodeName(extraBarcode);
            extraReq.setTubeCount(0);
            extraReq.setPrintBarcode(extraPrintBarcode);
            extraReq.setTubeType("EXTRA");
            extraReq.setTubeTypeId(TubeInfo.EXTRA_TUBE_ID);
            if (mWorkOrder != null)
                mWorkOrder.getSpecimenRequirements().add(extraReq);
        }
        if (mWorkOrder != null) {
            for (SpecimenRequirementInterface req : mWorkOrder.getSpecimenRequirements()) {
                result.add(new SpecimenRequirementListItemPresenter(req, mClonedWorkOrder));
            }
        }
        mSpecimens = result;
    }

    private boolean checkHasExtra() {
        if (mWorkOrder != null) {
            for (SpecimenRequirementInterface specimen : mWorkOrder.getSpecimenRequirements()) {
                if (specimen.getTubeTypeId() == TubeInfo.EXTRA_TUBE_ID) {
                    return true;
                }
            }
        }
        return false;
    }

    private List<SpecimenRequirementInterface> getNotGreySpecimenList() {
        List<SpecimenRequirementInterface> result = new ArrayList<SpecimenRequirementInterface>();
        if (mWorkOrder != null) {
            for (SpecimenRequirementInterface specimen : mWorkOrder.getSpecimenRequirements()) {
                if (!specimen.getTubeType().equalsIgnoreCase("grey")) {
                    result.add(specimen);
                }
            }
        }
        return result;
    }

    @Override public DrawSiteInterface getDrawSiteWithName(String name) {
        for (DrawSiteInterface drawSite : mDrawSites) {
            if (drawSite.getDescription().equals(name)) {
                return drawSite;
            }
        }
        return null;
    }

    @Override public List<DrawSiteInterface> getDrawSites() {
        return mDrawSites;
    }

    @Override public List<String> getDrawSiteNames() {
        List<String> result = new ArrayList<String>();
        for (DrawSiteInterface drawSite : mDrawSites) {
            result.add(drawSite.getDescription());
        }
        return result;
    }

    public boolean hasAllPrinted() {
        for (SpecimenRequirementListItemPresenterInterface specimen : mSpecimens) {
            if (!specimen.hasPrinted()) {
                return false;
            }
        }
        return true;
    }

    @Override public void next() {
        if (!this.canMoveNext()) {
            return;
        }
        mWorkOrder.setDrawSite(drawSite);
        boolean hasCountDiscrepancy = false;
        for (SpecimenRequirementInterface requirement : mWorkOrder.getSpecimenRequirements()) {
            if (requirement.hasCountDiscrepancy()) {
                hasCountDiscrepancy = true;
                break;
            }
        }
        Intent i = new Intent();
        if (hasCountDiscrepancy) {
            i.setClass(mActivity, TubeFailReasonActivity.class);
        } else if (mWorkOrder.isCashCheck()) {
            i.setClass(mActivity, PaymentActivity.class);
        } else {
            if (mApp.patientHasMultipleOpenWorkOrders()) {
                i.setClass(mActivity, VisitCompletedMoreOrdersActivity.class);
            } else {
                i.setClass(mActivity, VisitCompletedActivity.class);
            }
        }
        mActivity.startActivity(i);
    }
    @Override public void back() {
        mActivity.startActivity(new Intent(mActivity, OrderRequirementListActivity.class));
    }

    public void setCanMoveNext(boolean canMoveNext) {
        this.canMoveNext = canMoveNext;
    }
    public boolean canMoveNext() {
        return canMoveNext;
    }
    public void setDrawSite(String drawSite) {
        this.drawSite = drawSite;
        this.canMoveNext = !(drawSite == null || drawSite.isEmpty());
    }
    public String getDrawSite() {
        return drawSite;
    }
}
