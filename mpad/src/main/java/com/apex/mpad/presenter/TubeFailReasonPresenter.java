package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.*;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.TubeDrawFailReasonInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.TubeFailReasonPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class TubeFailReasonPresenter extends BasePresenter implements TubeFailReasonPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private TubeFailReasonActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    private List<String> mFailReasons;

    private String mFailReason;
    private String mNotes;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (TubeFailReasonActivity) activity;
        mWorkOrder = mApp.getCurrentWorkOrder();

        mFailReasons = new ArrayList<String>();
        for (TubeDrawFailReasonInterface reason : mApp.getTubeDrawFailReasons()) {
            mFailReasons.add(reason.getFailReason());
        }
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        mWorkOrder.setTubeDrawFailNotes(mNotes);
        mWorkOrder.setTubeDrawFailReasonId(getTubeFailReasonId());

        Intent i = new Intent();

        if (mWorkOrder.isCashCheck()) {
            i.setClass(mActivity, PaymentActivity.class);
        } else {
            if (mApp.patientHasMultipleOpenWorkOrders()) {
                i.setClass(mActivity, VisitCompletedMoreOrdersActivity.class);
            } else {
                i.setClass(mActivity, VisitCompletedActivity.class);
            }
        }
        mActivity.startActivity(i);
    }

    private String getTubeFailReasonId() {
        for (TubeDrawFailReasonInterface reason : mApp.getTubeDrawFailReasons()) {
            if (reason.getFailReason().equals(mFailReason)) {
                return reason.getFailReasonId();
            }
        }
        return mFailReason;
    }
    @Override public void back() {
        mActivity.startActivity(new Intent(mActivity, SpecimenRequirementListActivity.class));
    }

    @Override public List<String> getFailReasons() {
        return mFailReasons;
    }

    @Override public void setFailReason(String s) {
        this.mFailReason = s;
    }

    @Override public void setNotes(String s) {
        this.mNotes = s;
    }
}
