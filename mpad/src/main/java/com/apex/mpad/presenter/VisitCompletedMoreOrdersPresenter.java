package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.SpecimenRequirementListActivity;
import com.apex.mpad.activity.VisitCompletedMoreOrdersActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.CompletedWorkOrderInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.VisitCompletedMoreOrdersPresenterInterface;
import com.apex.mpad.service.rest.iface.RestClientInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class VisitCompletedMoreOrdersPresenter extends BasePresenter implements VisitCompletedMoreOrdersPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;
    @Inject RestClientInterface mRestClient;

    private VisitCompletedMoreOrdersActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (VisitCompletedMoreOrdersActivity) activity;
        WorkOrderInterface wo = mApp.getCurrentWorkOrder();

        if (wo != null) {
            CompletedWorkOrderInterface completedOrder = wo.getSuccessfulOrder();
            mRestClient.saveWorkOrder(mApp.getSessionId(), completedOrder, mApp.getTechId());
            wo.setStatus(WorkOrder.WorkOrderStatus.Completed);
            mApp.advanceToNextWorkOrder();
            mWorkOrder = mApp.getCurrentWorkOrder();
        }
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        mActivity.startActivity(new Intent(mActivity, SpecimenRequirementListActivity.class));
    }

    @Override public boolean patientHasMoreWorkOrders() {
        return mApp.patientHasMoreWorkOrders();
    }
}
