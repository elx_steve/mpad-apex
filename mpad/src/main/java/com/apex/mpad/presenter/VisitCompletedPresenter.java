package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.*;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.CompletedWorkOrderInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.VisitCompletedPresenterInterface;
import com.apex.mpad.service.FileUploaderService;
import com.apex.mpad.service.rest.iface.RestClientInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.FileUtility;
import com.apex.mpad.util.ImageUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;

/**
 * Created by will on 4/10/15.
 */
public class VisitCompletedPresenter extends BasePresenter implements VisitCompletedPresenterInterface {

    private static final String MESSAGE_NOT_SIGNED = "Signature required. Sign UTS if Patient can not sign";
    private static final String MESSAGE_CANCEL_NEW_PATIENT = "Going back will discard the new patient info. Click OK to continue";
    @Inject @ForApplication ApexApplication mApp;
    @Inject RestClientInterface mRestClient;
    @Inject ImageUtility mImageUtility;

    private VisitCompletedActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    private boolean mHasSignature;
    private boolean mPatientUnableToSign;
    private String mCompletedWorkOrderNotes;
    private File mImageFile;

    private static final Logger mLogger = LoggerFactory.getLogger(VisitCompletedPresenter.class);

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (VisitCompletedActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        if (!mHasSignature) {
            mActivity.showMessageDialog(MESSAGE_NOT_SIGNED);
        } else {
            mActivity.showLoading();
            mWorkOrder.setCompletedWorkOrderNotes(mCompletedWorkOrderNotes);
            mWorkOrder.setPatientUnableToSign(mPatientUnableToSign);

            if (mImageFile != null) {
                if (mImageFile.exists()) {
                    if (mImageUtility.saveImageForUpload(mApp.getSessionId(), "SG", mImageFile.getAbsolutePath(), mWorkOrder.getSpecimenNumber(), "", mApp.getTechId())) {
                        try {
                            mImageFile.delete();
                            FileUtility.deleteFile( mImageFile.getAbsolutePath() );
                        } catch (Exception e) {
                            mLogger.warn("Failed to delete image file: {}", e.getMessage());
                        }
                    }
                }
            }

            CompletedWorkOrderInterface completedOrder = mWorkOrder.getSuccessfulOrder();
            boolean result = mRestClient.saveWorkOrder(mApp.getSessionId(), completedOrder, mApp.getTechId());
            mApp.getCurrentWorkOrder().setStatus(WorkOrder.WorkOrderStatus.Completed);

            mActivity.startService(new Intent(mActivity, FileUploaderService.class));
            Intent i = new Intent();

            if (mApp.getCurrentVisit().hasMoreWorkOrders()) {
                i.setClass(mActivity, PatientListActivity.class);
            } else {
                mApp.advanceToNextWorkOrder();
                i.setClass(mActivity, WelcomeActivity.class);
            }
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mActivity.stopLoading();
                }
            });
            mActivity.startActivity(i);
            mActivity.finish();
        }
    }

    @Override public void back() {
        if (!(mApp.getCurrentWorkOrder().getPatientLastName() == null || mApp.getCurrentWorkOrder().getPatientLastName().isEmpty())) {
            mActivity.showConfirmDialog(MESSAGE_CANCEL_NEW_PATIENT);
        } else {
            Intent i = new Intent();
            if (mWorkOrder.isCashCheck()) {
                i.setClass(mActivity, PaymentActivity.class);
            } else {
                i.setClass(mActivity, SpecimenRequirementListActivity.class);
            }
            mActivity.startActivity(i);
        }
    }

    @Override public void setImageFile(File f) {
        this.mImageFile = f;
    }

    @Override public void setCompletedWorkOrderNotes(String notes) {
        this.mCompletedWorkOrderNotes = notes;
    }

    @Override public void setHasSignature(boolean hasSignature) {
        this.mHasSignature = hasSignature;
    }

    @Override public void setPatientUnableToSign(boolean unableToSign) {
        this.mPatientUnableToSign = unableToSign;
    }
}
