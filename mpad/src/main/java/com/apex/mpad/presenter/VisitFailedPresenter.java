package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.PatientListActivity;
import com.apex.mpad.activity.VisitFailedActivity;
import com.apex.mpad.activity.WelcomeActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.FailureReasonInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.VisitFailedPresenterInterface;
import com.apex.mpad.service.FileUploaderService;
import com.apex.mpad.service.PrintService;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class VisitFailedPresenter extends BasePresenter implements VisitFailedPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;
    @Inject PrintService mPrintService;

    private VisitFailedActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    private List<String> mFailureReasons;
    private String mFailureReason;
    private String mFailureComments;

    @Override public void setFailureReason(String reason) {
        this.mFailureReason = reason;
    }
    @Override public void setFailureComments(String comments) {
        this.mFailureComments = comments;
    }

    @Override public void print() {
        mPrintService.printFailedVisitInfo(mApp.getTechName(), mApp.getCurrentWorkOrder(), mFailureReason);
        mPrintService.disconnect();
    }

    @Override public void back() {
        mActivity.finish();
    }


    @Override public List<String> getFailureReasons() {
        return mFailureReasons;
    }

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (VisitFailedActivity) activity;
        mWorkOrder = mApp.getCurrentWorkOrder();

        mFailureReasons = new ArrayList<String>();
        for (FailureReasonInterface reason : mApp.getFailureReasons()) {
            mFailureReasons.add(reason.getDescription());
        }
    }

    private String getFailureReasonId() {
        for (FailureReasonInterface reason : mApp.getFailureReasons()) {
            if (reason.getDescription().equals(mFailureReason)) {
                return reason.getId();
            }
        }
        return mFailureReason;
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        String failureReasonId = getFailureReasonId();
        mApp.failCurrentOrder(failureReasonId, mFailureComments);
        Intent i = new Intent();

        mActivity.startService(new Intent(mActivity, FileUploaderService.class));

        if (mApp.getCurrentVisit().hasMoreWorkOrders()) {
            i.setClass(mActivity, PatientListActivity.class);
            mActivity.runOnUiThread(new Runnable() {
                @Override public void run() {
                    mActivity.stopLoading();
                }
            });
            mActivity.startActivity(i);
        } else {
            mApp.advanceToNextWorkOrder();
            //if (mApp.getCurrentWorkOrder() != null) {
                i.setClass(mActivity, WelcomeActivity.class);
                mActivity.runOnUiThread(new Runnable() {
                    @Override public void run() {
                        mActivity.stopLoading();
                    }
                });
                mActivity.startActivity(i);
            //} else {
            //    mActivity.runOnUiThread(new Runnable() {
            //        @Override public void run() {
            //            mActivity.stopLoading();
            //        }
            //    });
            //    mActivity.showCompleteDialog();
            //}
        }
    }
}
