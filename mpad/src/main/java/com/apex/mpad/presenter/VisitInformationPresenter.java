package com.apex.mpad.presenter;

import android.content.Intent;

import com.apex.mpad.activity.PatientDOBActivity;
import com.apex.mpad.activity.PatientInformationActivity;
import com.apex.mpad.activity.PatientScriptsActivity;
import com.apex.mpad.activity.VisitInformationActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.VisitInformationPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class VisitInformationPresenter extends BasePresenter implements VisitInformationPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private VisitInformationActivity mActivity;
    private WorkOrderInterface mWorkOrder;

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (VisitInformationActivity) activity;
        this.mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        mActivity.startActivity(new Intent(mActivity, PatientScriptsActivity.class));
    }
    @Override public void back() {
        if (mWorkOrder.isDobValidationReqd() || mWorkOrder.isVoiceRecReqd()) {
            mActivity.startActivity(new Intent(mActivity, PatientDOBActivity.class));
        } else {
            mActivity.startActivity(new Intent(mActivity, PatientInformationActivity.class));
        }
    }

}
