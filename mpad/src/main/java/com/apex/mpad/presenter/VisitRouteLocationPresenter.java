package com.apex.mpad.presenter;

import android.content.Intent;
import com.apex.mpad.activity.*;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.VisitRouteLocationPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class VisitRouteLocationPresenter extends BasePresenter implements VisitRouteLocationPresenterInterface {

    @Inject @ForApplication ApexApplication mApp;

    private VisitRouteLocationActivity mActivity;
    private WorkOrderInterface mWorkOrder;
    private boolean mUseRouting = true;

    @Inject public VisitRouteLocationPresenter() {}

    public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (VisitRouteLocationActivity) activity;
        mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public void showPatientInformation() {
        if (mApp.getCurrentVisit() == null) {
            mActivity.showNoDataDialog();
        }
        else {
            if (mApp.getCurrentVisit().getNumPatients() > 1) {
                mActivity.startActivity(new Intent(mActivity, PatientListActivity.class));
            } else {
                mActivity.startActivity(new Intent(mActivity, PatientInformationActivity.class));
            }
        }
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void next() {
        showPatientInformation();
    }
}
