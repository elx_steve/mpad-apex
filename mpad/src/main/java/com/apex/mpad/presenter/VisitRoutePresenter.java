package com.apex.mpad.presenter;

import android.content.Intent;

import com.apex.mpad.activity.VisitFailedActivity;
import com.apex.mpad.activity.VisitRouteActivity;
import com.apex.mpad.activity.VisitRouteLocationActivity;
import com.apex.mpad.activity.WelcomeActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.VisitRoutePresenterInterface;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/10/15.
 */
public class VisitRoutePresenter extends BasePresenter implements VisitRoutePresenterInterface {

    @Inject @ForApplication ApexApplication mApp;
    @Inject NavigationManager mNavManager;

    private VisitRouteActivity mActivity;
    private WorkOrderInterface mWorkOrder;
    private boolean mUseRouting = true; //false for testing only

    @Inject public VisitRoutePresenter() {}

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (VisitRouteActivity) activity;
        mWorkOrder = mApp.getCurrentWorkOrder();
    }

    @Override public void showRouteLocation() {
        // Call the Visit location activity here. If the map is used, then the map activity is called right after here
        // The latest startActivity take precedence on foreground
        mActivity.startActivity(new Intent(mActivity, VisitRouteLocationActivity.class));

        if (mUseRouting) {
            mNavManager.setIsNavigating(true);
            mNavManager.routeTrip("trip", null, null, null, mWorkOrder.getLatitude(), mWorkOrder.getLongitude());
        }
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return mWorkOrder;
    }

    @Override public void failVisit() {
        mActivity.startActivity(new Intent(mActivity, VisitFailedActivity.class));
    }

    @Override public void skipVisit() {
        mApp.skipVisit();
        mActivity.startActivity(new Intent(mActivity, WelcomeActivity.class));
    }

    @Override public void next() {
        showRouteLocation();
    }
}
