package com.apex.mpad.presenter;

import com.apex.mpad.activity.WelcomeActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.WelcomePresenterInterface;
import com.apex.mpad.service.FileUploader;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.service.rest.RestClient;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class WelcomePresenter extends BasePresenter implements WelcomePresenterInterface {
    @Inject @ForApplication ApexApplication mApp;
    @Inject FileUploader mFileUploaderService;
    @Inject NavigationManager mNavManager;
    @Inject RestClient mRestClient;

    private WelcomeActivity mActivity;

    private boolean mVisitsComplete;
    private boolean mHasSkippedPatients;
    private int pendingFileThreshold = 10;

    private String patientName;
    private String stopsRemaining;
    private String patientsRemaining;
    private String patientsSkipped;
    private String pendingUploads;

    @Inject public WelcomePresenter() {}

    @Override public void onCreate(BaseActivity activity) {
        super.onCreate(activity);
        this.mActivity = (WelcomeActivity) activity;
        calcIsComplete();
    }

    public void calcIsComplete() {
        mVisitsComplete = mApp.getRemainingPatientCount() == 0 || mApp.getRemainingPatientCount() == mApp.getSkippedPatientCount();
        mHasSkippedPatients = mApp.getSkippedPatientCount() > 0;
        //mVisitsComplete = true; //for testing
        //mHasSkippedPatients = false; //for testing
    }

    public void uploadFiles() {
        mFileUploaderService.uploadFiles();
    }

    public void refreshOrders(boolean showDialog) {
        mActivity.showLoading();
        boolean isSuccessful = false;
        uploadFiles();
        if (mFileUploaderService.getPendingWorkOrdersCount() == 0) {
            List<WorkOrderInterface> workOrders = mRestClient.getWorkOrders(mApp.getSessionId());
            if (workOrders != null) {
                mApp.loadWorkOrders(workOrders);
                isSuccessful = true;
            }
        }
        mActivity.stopLoading();
        if (showDialog) mActivity.showRefreshResultMessage(isSuccessful);
        if (isSuccessful) {
            mApp.setHasMoreWorkOrdersNotification(false);
            calcIsComplete();
        }
        mActivity.handleRefreshButton();
    }

    public void routeBackToApex() {
        mNavManager.routeTrip("trip", null, null, null, mApp.getOfficeLat(), mApp.getOfficeLong());
    }

    public boolean hasSkippedPatients() {
        return mHasSkippedPatients;
    }

    public boolean getVisitsComplete() {
        return mVisitsComplete;
    }

    public String getPatientName() {
        String result = "";
        if (mApp.getCurrentWorkOrder() == null) {
            mApp.advanceToNextWorkOrder();
        }
        if (mApp.getCurrentWorkOrder() != null) {
            result = mApp.getCurrentWorkOrder().getPatientName();
        }

        return result;
    }

    public String getDrawAreaUnit() {
        String result = "";
        if (mApp.getCurrentWorkOrder() == null) {
            mApp.advanceToNextWorkOrder();
        }
        if (mApp.getCurrentWorkOrder() != null) {
            result = mApp.getCurrentWorkOrder().getDrawAreaUnit();
        }

        return result;
    }

    public String getDrawAreaType() {
        String result = "";
        if (mApp.getCurrentWorkOrder() == null) {
            mApp.advanceToNextWorkOrder();
        }
        if (mApp.getCurrentWorkOrder() != null) {
            result = mApp.getCurrentWorkOrder().getDrawAreaType();
        }

        return result;
    }

    public int getNextPatientCount() {
        int res = 1;
        if (mApp.getCurrentWorkOrder() == null) {
            mApp.advanceToNextWorkOrder();
        }
        if (mApp.getCurrentWorkOrder() != null) {
            res = mApp.getCurrentVisit().getNumPatients();
        }
        return res;
    }

    public String getStopsRemaining() {
        return String.valueOf(mApp.getRemainingVisitCount());
    }

    public String getPatientsRemaining() {
        return String.valueOf(mApp.getRemainingPatientCount());
    }

    public String getPatientsSkipped() {
        return String.valueOf(mApp.getSkippedPatientCount());
    }

    public String getPendingUploads() {
        if (mFileUploaderService.getPendingUploadCount() < pendingFileThreshold) {
            return "";
        } else {
            return mFileUploaderService.getPendingUploadCount() + " pending uploads";
        }
    }

    @Override public WorkOrderInterface getWorkOrder() {
        return null;
    }
    @Override public void next() {

    }

    @Override public boolean hasMultiplePatients() {
        return mApp.getCurrentVisit().getWorkOrders().size() > 1 && mApp.getCurrentVisit().getNumPatients() > 1;
    }

    @Override public boolean hasMoreWorkOrdersNotification() {
        return mApp.hasMoreWorkOrdersNotification();
    }
}
