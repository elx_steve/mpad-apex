package com.apex.mpad.presenter.iface;

import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.util.base.BaseActivity;

/**
 * Created by will on 4/23/15.
 */
public interface BasePresenterInterface {
    void onCreate(BaseActivity activity);
    WorkOrderInterface getWorkOrder();
    void failVisit();
    void skipVisit();
    void next();
    void back();

    boolean hasMultiplePatients();
}
