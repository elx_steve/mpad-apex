package com.apex.mpad.presenter.iface;

/**
 * Created by will on 4/10/15.
 */
public interface HomePresenterInterface extends BasePresenterInterface {
    void addPrinter();
    void checkPrinter();
    void addPatient();
    void refreshWorkOrders();
    void routeSummary();
    boolean hasMoreWorkOrdersNotification();
}
