package com.apex.mpad.presenter.iface;

/**
 * Created by will on 4/9/15.
 */
public interface LoginPresenterInterface extends BasePresenterInterface {
    void login(String username, String password);
}
