package com.apex.mpad.presenter.iface;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public interface NewPatientPresenterInterface extends BasePresenterInterface {
    List<String> getDrawSites();

    void print();
    void setDrawSite(String s);

    int getTubeCount();
    boolean hasPrinted();

    void setFirstName(String s);
    void setLastName(String s);
    void setDob(String s);

    void setNotes(String s);

    void setTubeCount(int i);
}
