package com.apex.mpad.presenter.iface;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;

/**
 * Created by will on 4/10/15.
 */
public interface NewPatientScriptPresenterInterface extends BasePresenterInterface {
    File getFileForImage();

    int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight);

    void setNotes(String notes);
    String getNotes();
    void saveImage(Bitmap bitmap);
    void setChangeScript(boolean isChangeScript);
    boolean isChangeScript();
}
