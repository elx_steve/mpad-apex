package com.apex.mpad.presenter.iface;

import java.io.File;
import java.util.List;

/**
 * Created by John on 6/13/2016.
 */
public interface PatientDOBPresenterInterface  extends BasePresenterInterface {
    void showPatientInformation();
    void setNewNote(String note);
    void setDOB(String dob);
    List<String> getDOBList();
    void setAudioFile(File f);
    String getDob();
    String getNewDob();
    void buildList(boolean isBack);
    boolean doValidateDropdown();
    boolean doVoiceRecording();
}
