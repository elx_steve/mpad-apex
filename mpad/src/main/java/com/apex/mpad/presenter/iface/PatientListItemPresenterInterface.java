package com.apex.mpad.presenter.iface;

import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.WorkOrderInterface;

/**
 * Created by will on 4/24/15.
 */
public interface PatientListItemPresenterInterface {
    String getLocation();
    WorkOrderInterface getWorkOrder();
    String getButtonText();
    void setButtonText(String text);
    void startOrder();

    WorkOrder.WorkOrderStatus getStatus();

    String getNameAndDob();

    String getName();

    String getDob();
}
