package com.apex.mpad.presenter.iface;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public interface PatientListPresenterInterface extends BasePresenterInterface {
    String getFacilityName();
    List<PatientListItemPresenterInterface> getItems();
}
