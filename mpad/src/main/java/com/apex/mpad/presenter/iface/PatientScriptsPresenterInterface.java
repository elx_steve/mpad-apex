package com.apex.mpad.presenter.iface;

/**
 * Created by will on 4/10/15.
 */
public interface PatientScriptsPresenterInterface extends BasePresenterInterface {
    int getNumberAdditionalScripts();
    void setNumberAdditionalScripts(int number);
}
