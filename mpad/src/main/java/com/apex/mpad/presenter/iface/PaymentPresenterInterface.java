package com.apex.mpad.presenter.iface;

/**
 * Created by will on 4/10/15.
 */
public interface PaymentPresenterInterface extends BasePresenterInterface {
    void setAmountCollected(String s);

    void setNotes(String s);

    void back();
}
