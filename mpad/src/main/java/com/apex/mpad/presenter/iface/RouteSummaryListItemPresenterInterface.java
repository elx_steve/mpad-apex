package com.apex.mpad.presenter.iface;

/**
 * Created by will on 5/14/15.
 */
public interface RouteSummaryListItemPresenterInterface {
    String getText();
    boolean isComplete();
}
