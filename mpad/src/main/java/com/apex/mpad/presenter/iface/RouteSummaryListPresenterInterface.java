package com.apex.mpad.presenter.iface;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public interface RouteSummaryListPresenterInterface extends BasePresenterInterface {
    List<RouteSummaryListItemPresenterInterface> getRouteSummaryItems();

    void close(boolean isFirstLaunch);

}
