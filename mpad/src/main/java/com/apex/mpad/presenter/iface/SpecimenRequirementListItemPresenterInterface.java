package com.apex.mpad.presenter.iface;

import android.widget.Button;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;

/**
 * Created by will on 4/24/15.
 */
public interface SpecimenRequirementListItemPresenterInterface {
    public SpecimenRequirementInterface getSpecimen();
    public Integer getTubeCountCollected();
    public void setTubeCountCollected(Integer count);
    boolean hasPrinted();

    void print();

    String getButtonText();
    void setButton(Button button);
    Button getButton();

    void disconnectPrinter();
}
