package com.apex.mpad.presenter.iface;

import com.apex.mpad.model.iface.DrawSiteInterface;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public interface SpecimenRequirementListPresenterInterface extends BasePresenterInterface {
    List<SpecimenRequirementListItemPresenterInterface> getSpecimenRequirementList();

    DrawSiteInterface getDrawSiteWithName(String name);

    List<DrawSiteInterface> getDrawSites();

    List<String> getDrawSiteNames();
    void setDrawSite(String drawSite);

    void printAll();

    boolean hasAllPrinted();
}
