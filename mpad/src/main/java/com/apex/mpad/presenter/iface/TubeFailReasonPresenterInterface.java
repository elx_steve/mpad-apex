package com.apex.mpad.presenter.iface;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public interface TubeFailReasonPresenterInterface extends BasePresenterInterface {
    List<String> getFailReasons();
    void setFailReason(String s);
    void setNotes(String s);
}
