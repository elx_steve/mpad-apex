package com.apex.mpad.presenter.iface;

import com.apex.mpad.model.iface.WorkOrderInterface;

import java.io.File;

/**
 * Created by will on 4/10/15.
 */
public interface VisitCompletedPresenterInterface extends BasePresenterInterface {
    void back();
    void setCompletedWorkOrderNotes(String notes);
    void setHasSignature(boolean hasSignature);
    void setPatientUnableToSign(boolean unableToSign);
    WorkOrderInterface getWorkOrder();

    void setImageFile(File f);
}
