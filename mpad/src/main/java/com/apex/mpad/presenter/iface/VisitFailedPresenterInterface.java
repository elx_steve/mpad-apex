package com.apex.mpad.presenter.iface;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public interface VisitFailedPresenterInterface extends BasePresenterInterface {
    List<String> getFailureReasons();
    void setFailureReason(String reason);
    void setFailureComments(String comments);

    void print();

    void back();

}
