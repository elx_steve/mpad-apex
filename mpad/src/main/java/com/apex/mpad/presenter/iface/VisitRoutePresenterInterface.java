package com.apex.mpad.presenter.iface;

/**
 * Created by will on 4/10/15.
 */
public interface VisitRoutePresenterInterface extends BasePresenterInterface {
    boolean hasMultiplePatients();
    void showRouteLocation();
}
