package com.apex.mpad.presenter.iface;

/**
 * Created by will on 4/10/15.
 */
public interface WelcomePresenterInterface extends BasePresenterInterface {
    boolean hasSkippedPatients();
    boolean getVisitsComplete();
    String getPatientName();
    String getDrawAreaUnit();
    String getDrawAreaType();
    int getNextPatientCount();
    String getStopsRemaining();
    String getPatientsRemaining();
    String getPatientsSkipped();
    String getPendingUploads();
    void routeBackToApex();
    void refreshOrders(boolean showDialog);
    void calcIsComplete();
    void uploadFiles();
    boolean hasMultiplePatients();
    boolean hasMoreWorkOrdersNotification();
}
