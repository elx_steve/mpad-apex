package com.apex.mpad.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.service.rest.RestClient;
import com.apex.mpad.service.rest.message.*;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseRecordingActivity;
import com.apex.mpad.util.FileUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by will on 2/28/16.
 */
public class FileUploader {
    private static final String TAG = "FileUploader";

    private File[] workOrderPendingFiles;
    private File[] imagePendingFiles;
    private File[] gpsPendingFiles;
    private File[] audioPendingFiles;
    private File[] shutdownFiles;

    public static final double interval = 240;

    @Inject FileUtility mFileUtility;
    @Inject ObjectMapper mObjectMapper;
    @Inject RestClient mClient;
    @Inject NavigationManager mNavManager;
    @Inject @ForApplication Context mAppContext;

    private ApexApplication mApp;
    private static final Logger mLogger = LoggerFactory.getLogger(FileUploaderService.class);

    @Inject public FileUploader() {
        Injector.INSTANCE.inject(this);
        getPendingFiles();
        mApp = (ApexApplication) mAppContext;
    }

    private void getPendingFiles() {
        workOrderPendingFiles = mFileUtility.getFiles("", "SP-\\d+\\.txt", false);
        imagePendingFiles = mFileUtility.getFiles("", ".+\\.imgmsg", false);
        gpsPendingFiles = mFileUtility.getFiles("", "gps(-\\d{4}-\\d{2}-\\d{2}-)[[:ascii:]]+.txt", true);
        audioPendingFiles = mFileUtility.getFiles(BaseRecordingActivity.AUDIO_RECORDER_FOLDER, ".+\\.mp4msg", false);
        shutdownFiles = mFileUtility.getFiles("", "SD-\\d+\\.txt", false);
    }

    public boolean uploadFiles() {
        boolean isPrinting = mApp.isPrinting();
        boolean isConnected = isConnected();
        boolean isUploading = mApp.isUploading();
        mLogger.debug("Starting upload. Uploading: " + isUploading + "Printing: " + isPrinting + ", Connected: " + isConnected);
        if (isConnected && !isPrinting && !isUploading) {
            mApp.setUploading(true);
            mLogger.debug("Uploading", "Connected, Beginning Upload");
            getPendingFiles();
            mLogger.debug("Uploading, Work Order Files: " + getPendingWorkOrdersCount() + ", Images: " + getPendingImageCount() + ", GPS: " + getPendingGPSCount() + ", AUDIO: " + getPendingAudioCount() + ", SHUTDOWN: " + getPendingShutdownCount());
            uploadWorkOrders();
            uploadImages();
            uploadGpsData();
            uploadShutdowns();
            mApp.setUploading(false);
            return true;
        } else {
            mApp.setUploading(false);
            mLogger.warn("Upload Failed", "No internet connection");
            return false;
        }
    }

    public int getPendingUploadCount() {
        return getPendingWorkOrdersCount() + getPendingImageAPICount() + getPendingShutdownCount();
    }

    private int getPendingImageAPICount() {
        return getPendingImageCount() + getPendingAudioCount();
    }

    private int getPendingImageCount() {
        int imageFileLength = imagePendingFiles == null ? 0 : imagePendingFiles.length;
        return imageFileLength;
    }

    private int getPendingAudioCount() {
        int audioCount = audioPendingFiles == null ? 0 : audioPendingFiles.length;
        return audioCount;
    }

    public int getPendingWorkOrdersCount() {
        int workOrderLength = workOrderPendingFiles == null ? 0 : workOrderPendingFiles.length;
        return workOrderLength;
    }

    private int getPendingGPSCount() {
        int gpsCount = gpsPendingFiles == null ? 0 : gpsPendingFiles.length;
        return gpsCount;
    }

    private int getPendingShutdownCount() {
        int sdCount = shutdownFiles == null ? 0 : shutdownFiles.length;
        return sdCount;
    }

    private void uploadWorkOrders() {
        mLogger.debug("uploadWorkOrders: length: " + getPendingWorkOrdersCount() + "app: " + mApp.toString() + "sessionId: " + mApp.getSessionId());

        if (getPendingWorkOrdersCount() == 0 || mApp == null || mApp.getSessionId() == null || mApp.getSessionId().isEmpty()) {
            return;
        }
        mLogger.debug("Starting Work Order Upload");
        for (File file : workOrderPendingFiles) {
            String filename = file.getName();

            String data = mFileUtility.textFromFile(filename, true);
            SaveWorkOrderRequestMessage message = null;
            SaveWorkOrderResponseMessage response = null;
            try {
                mLogger.debug("Beginning Uploading Work Order, Raw Data: {}", data);
                message = mObjectMapper.readValue(data, SaveWorkOrderRequestMessage.class);
            } catch (Exception e) {
                mLogger.error("Read error: {}", e.getMessage() != null ? e.getMessage() : "");
            }
            if (message != null) {
                try {
                    message.setSessionId(mApp.getSessionId());
                    String outgoingMessage = "";
                    try {
                        outgoingMessage = mObjectMapper.writeValueAsString(message);
                    } catch (Exception e) {
                        mLogger.warn("Error writing message to string for logging");
                    }
                    mLogger.debug("Uploading Work Order, Message: {}", outgoingMessage);
                    response = mClient.sendWorkOrder(message);
                } catch (Exception e) {
                    mFileUtility.saveToFile(Environment.getExternalStorageDirectory().getPath(), filename, data);
                    mLogger.error("Sending error: {}", e.getMessage() != null ? e.getMessage() : "");
                }
                if (response != null) {
                    if (response.getError() == null) {
                        mLogger.debug("Work Order Sent. Response: OK");
                    } else {
                        mLogger.debug("Work Order Sent, received error: {}", response.getError().getDescription() != null ? response.getError().getDescription() : "Error");
                        try {
                            mFileUtility.saveToFile(Environment.getExternalStorageDirectory().getPath(), filename, data);
                        } catch (Exception e) {
                            mLogger.error("Save file error: {}", e.getMessage() != null ? e.getMessage() : "");
                        }
                    }
                } else {
                    mLogger.warn("Error during upload, saving file");
                    try {
                        mFileUtility.saveToFile(Environment.getExternalStorageDirectory().getPath(), filename, data);
                    } catch (Exception e) {
                        mLogger.error("Save file error: {}", e.getMessage() != null ? e.getMessage() : "");
                    }
                }
            }
        }
    }

    private void uploadImages() {
        if ( getPendingImageAPICount() == 0
                || mApp == null
                || mApp.getSessionId() == null
                || mApp.getSessionId().isEmpty()) {
            return;
        }

        List<File> pendingFiles = new ArrayList<File>(getPendingImageAPICount());
        if (getPendingImageCount() > 0) pendingFiles.addAll(Arrays.asList(imagePendingFiles));
        if (getPendingAudioCount() > 0) pendingFiles.addAll(Arrays.asList(audioPendingFiles));

        mLogger.debug("Starting Image/Audio Uploading");

        for (File file : pendingFiles) {
            String fullPath = file.getAbsolutePath();
            String data = mFileUtility.textFromFilePath(fullPath, true);
            uploadImageData(data, fullPath);
        }

    }

    private void uploadImageData(String data, String fullPath) {
        SaveImageRequestMessage message = null;
        ErrorResponseMessage error = null;
        try {
            message = mObjectMapper.readValue(data, SaveImageRequestMessage.class);
        } catch (Exception e) {
            mLogger.error("Read error: {}", e.getMessage() != null ? e.getMessage() : "");
        }
        if (message != null) {
            try {
                message.setSessionId(mApp.getSessionId());
                mLogger.debug("Uploading Image/Audio: {}", message.toString());
                error = mClient.sendImage(message);
            } catch (Exception e) {
                mLogger.error("Image/Audio Upload error: {}", e.getMessage() != null ? e.getMessage() : "");
            }
            mLogger.debug("Image/Audio Uploaded, Response: {}", error == null ? "OK" : error.toString());
            if (error != null) {
                try {
                    mFileUtility.saveToFile(fullPath, data);
                } catch (Exception e) {
                    mLogger.error("File error: {}", e.getMessage() != null ? e.getMessage() : "");
                }
            }
        }
    }


    private void uploadShutdowns() {
        if ( getPendingShutdownCount() == 0
                || mApp == null
                || mApp.getSessionId() == null
                || mApp.getSessionId().isEmpty()) {
            return;
        }

        List<File> pendingFiles = new ArrayList<File>(getPendingShutdownCount());
        if (getPendingShutdownCount() > 0) pendingFiles.addAll(Arrays.asList(shutdownFiles));


        mLogger.debug("Starting Shutdown Uploading");

        for (File file : pendingFiles) {
            String fullPath = file.getAbsolutePath();
            String data = mFileUtility.textFromFilePath(fullPath, true);
            uploadShutdownData(data, fullPath);
        }
    }

    private void uploadShutdownData(String data, String fullPath) {
        SaveShutdownRequestMessage message = null;
        ErrorResponseMessage error = null;
        try {
            message = mObjectMapper.readValue(data, SaveShutdownRequestMessage.class);
        } catch (Exception e) {
            mLogger.error("Read error: {}", e.getMessage() != null ? e.getMessage() : "");
        }
        if (message != null) {
            try {
                message.setSessionId(mApp.getSessionId());
                mLogger.debug("Uploading Shutdown: {}", message.toString());
                error = mClient.sendShutdown(message);
            } catch (Exception e) {
                mLogger.error("Shutdown Upload error: {}", e.getMessage() != null ? e.getMessage() : "");
            }
            mLogger.debug("Shutdown Uploaded, Response: {}", error == null ? "OK" : error.toString());
            if (error != null) {
                try {
                    mFileUtility.saveToFile(fullPath, data);
                } catch (Exception e) {
                    mLogger.error("File error: {}", e.getMessage() != null ? e.getMessage() : "");
                }
            }
        }
    }

    private void uploadGpsData() {
        if (mApp == null || mApp.getSessionId() == null || mApp.getSessionId().isEmpty()) {
            return;
        }

        List<GpsDataPointInterface> gpsMessages = new ArrayList<GpsDataPointInterface>();
        if (gpsPendingFiles != null) {
            for (File file : gpsPendingFiles) {
                String filename = file.getName();
                String data = mFileUtility.textFromFile(filename, true);
                mLogger.debug("GPS File Found: {}", data != null ? data : "");
                try {
                    SaveGpsDataCollectionRequestMessage message = mObjectMapper.readValue(data, SaveGpsDataCollectionRequestMessage.class);
                    message.setSessionId(mApp.getSessionId());

                    gpsMessages = new ArrayList<GpsDataPointInterface>(message.getGpsData());
                } catch (Exception e) {
                    mLogger.warn("GPS File Error: {}", e.getMessage() != null ? e.getMessage() : "");
                }

            }
        }
        if (gpsMessages.size() > 0) {
            mLogger.debug("GPS Files Found: {}", gpsMessages.size());
        }
        List<GpsDataPointInterface> dataPoints = new ArrayList<GpsDataPointInterface>();
        if (mNavManager.getGpsInfoListCount() > 0) {
            dataPoints = new ArrayList<GpsDataPointInterface>(mNavManager.getGpsInfoList());
            mLogger.debug("GPS Data Points Found: {}", dataPoints.size());
        }
        for (GpsDataPointInterface dataPoint : dataPoints) {
            gpsMessages.add(dataPoint);
        }

        if (!gpsMessages.isEmpty()) {
            mLogger.debug("GPS Data Upload, {} records", gpsMessages.size());
            SaveGpsDataCollectionRequestMessage message = new SaveGpsDataCollectionRequestMessage();
            message.setSessionId(mApp.getSessionId());
            message.setGpsData(gpsMessages);

            String outgoingString = null;
            ErrorResponseMessage error = null;
            try {
                outgoingString = mObjectMapper.writeValueAsString(message);
            } catch (Exception e) {
                mLogger.error("GPS File Write error: {}", e == null ? "" : e);
            }
            if (outgoingString != null) {
                try {
                    mLogger.debug("Uploading GPS: {}", outgoingString);
                    error = mClient.sendGpsData(message);
                } catch (Exception e) {
                    mLogger.error("GPS Upload Error: {}", e == null ? "" : e);
                }
                mLogger.debug("GPS Uploaded, Response: {}", error == null ? "OK" : error.toString());

                if (error != null) {
                    String messageString = null;
                    try {
                        messageString = mObjectMapper.writeValueAsString(message);
                    } catch (Exception e) {
                        mLogger.error("GPS File Write error: {}", e == null ? "" : e);
                    }
                    if (messageString != null) {
                        try {
                            mFileUtility.saveToFile(Environment.getExternalStorageDirectory().getPath(), "gps-" + getDateTimeForGps() + ".txt", messageString);
                        } catch (Exception e) {
                            mLogger.error("GPS File Save error: {}", e == null ? "" : e);
                        }
                    }
                }
            }
        } else {
            mLogger.debug("GPS Upload Occurred, no GPS Points to upload");
        }
    }

    public void getNotifications() {
        if (mApp == null || mApp.getSessionId() == null || mApp.getSessionId().isEmpty()) {
            return;
        }

        Log.d(TAG, "getNotifications");
        NotificationsRequestMessage message = new NotificationsRequestMessage();
        message.setSessionId(mApp.getSessionId());

        String outgoingString = null;
        ErrorResponseMessage error = null;
        NotificationsResponseMessage response = null;
        try {
            outgoingString = mObjectMapper.writeValueAsString(message);
        } catch (Exception e) {
            mLogger.error("Notifications Write error: {}", e == null ? "" : e);
        }
        if (outgoingString != null) {
            try {
                mLogger.debug("Getting Notifications: {}", outgoingString);
                response = mClient.getNotifications(message);
            } catch (Exception e) {
                mLogger.error("Getting Notifications Error: {}", e == null ? "" : e);
            }
            mLogger.debug("Getting Notifications, Response: {}", error == null ? "OK" : error.toString());
            boolean isOK = false;
            if (response != null) {
                isOK = true;
                error = response.getError();
                if (error != null) {
                    isOK = false;
                    if (error.getCode() == null && error.getDescription() == null) {
                        isOK = true;
                    } else {
                        mLogger.error("Getting Notifications Error: {} " + error.getDescription());
                    }
                }
            }
            //we have a response back.  Set the value of the response in the app
            // ONLY IF there wasn't a recent notification.  Meaning, only a refresh or a logout can clear
            // a previous mApp notification flag once set to true.
            if (isOK && !mApp.hasMoreWorkOrdersNotification()) {
                // To simulate the arrival of an order, force the flag hnwo true  (see the commented code below)
                boolean hnwo = (response.getHasNewWorkOrders() != null && response.getHasNewWorkOrders().toLowerCase().equals("true"));
//                hnwo = true;        // TEMP TEMP KEEP COMMENT OUT THIS FOR NORMAL RUN !!! ONLY FOR DEBUG !

                mApp.setHasMoreWorkOrdersNotification(hnwo);
                Log.d(TAG, "notification received : " + hnwo);
            }
        } else {
            mLogger.debug("Get notifications, unexpected unknown error");
        }

    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager)mAppContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    private String getDateTimeForGps() {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd-HH:mm:ss.SSSS");
        return DateTime.now().toString(format);
    }
}
