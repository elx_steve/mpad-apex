package com.apex.mpad.service;

import android.app.IntentService;
import android.content.Intent;
import com.apex.mpad.injection.Injector;

import javax.inject.Inject;

/**
 * Created by will on 4/21/15.
 */
public class FileUploaderService extends IntentService {

    @Inject FileUploader mUploader;

    public FileUploaderService() {
        super("FileUploaderService");
    }

    @Override protected void onHandleIntent(Intent intent) {
        mUploader.uploadFiles();
    }

    @Override public void onCreate() {
        super.onCreate();
        Injector.INSTANCE.inject(this);
    }
}
