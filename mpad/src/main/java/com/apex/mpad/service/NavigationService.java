package com.apex.mpad.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.navigation.NavigationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by will on 4/21/15.
 */
public class NavigationService extends BroadcastReceiver {
    private GpsDataPointInterface mLastGpsDataPoint;
    private Float mInterval;

    @Inject NavigationManager mNavManager;
    private static final Logger mLogger = LoggerFactory.getLogger(NavigationService.class);

    @Override public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getBooleanExtra("fromAlarm", false)) {
            Injector.INSTANCE.inject(this);

            mLogger.debug("Breadcrumbing Intent Received");

            GpsDataPointInterface dataPoint = mNavManager.getCurrentGpsDataPoint();
            GpsDataPointInterface previousDataPoint = mNavManager.getPreviousGpsDataPoint();
            if (dataPoint == null) {
                return;
            } else if (previousDataPoint == null || !previousDataPoint.getDateTime().equals(dataPoint.getDateTime())) {
                mNavManager.setPreviousGpsDataPoint(dataPoint);
            }
            mNavManager.getGpsInfoList().add(dataPoint.clone());
        }
    }

}
