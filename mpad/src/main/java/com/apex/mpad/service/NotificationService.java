package com.apex.mpad.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by will on 4/21/15.
 */
public class NotificationService extends IntentService {

    @Inject @ForApplication
    ApexApplication mApp;
    @Inject FileUploader mUploader;

    Handler handler;

    public NotificationService() {
        super("NotificationService");
    }

    @Override protected void onHandleIntent(Intent intent) {
        mUploader.getNotifications();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BaseActivity current = (BaseActivity)mApp.getCurrentActivity();
                if (current != null) {
                    current.handleRefreshIndicator();
                    current.handleRefreshButton();
                }
            }
        });
    }

    @Override public void onCreate() {
        super.onCreate();
        Injector.INSTANCE.inject(this);
        handler = new Handler(Looper.getMainLooper());
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }
}
