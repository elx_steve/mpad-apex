package com.apex.mpad.service;

import android.graphics.Color;
import android.os.Environment;
import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.ApexSleeper;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.BluetoothConnectionInsecure;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;

/**
 * Created by will on 4/24/15.
 */
public class PrintService {

    @Inject @ForApplication ApexApplication mApp;
    @Inject @ForApplication ApexPrefs mPrefs;

    private static final Logger mLogger = LoggerFactory.getLogger(PrintService.class);

    private BluetoothConnection mConnection;
    private ZebraPrinter mPrinter;
    private File mTubeLabelPrintFile;
    private File mFailedVisitPrintFile;

    private static final int NUM_TRIES = 3;

    public PrintService() {
        Injector.INSTANCE.inject(this);
        mTubeLabelPrintFile = new File(Environment.getExternalStorageDirectory() + "/label.txt");
        mFailedVisitPrintFile = new File(Environment.getExternalStorageDirectory() + "/failedVisit.txt");
    }

    public void setConnection(BluetoothConnection cnxn) {
        mConnection = cnxn;
    }

    public BluetoothConnection getConnection() {
        if (mConnection == null) {
            mLogger.debug("Bluetooth", "Getting Bluetooth Connection");
            mConnection = new BluetoothConnection(getMacAddress());
            mLogger.debug("Bluetooth", "Bluetooth Connection: {}", mConnection.toString());
        }
        return mConnection;
    }

    public void setPrinter(ZebraPrinter printer) {
        mPrinter = printer;
    }

    public void disconnect() {
        try {
            if (mConnection != null) {
                mConnection.close();
            }
        } catch (ConnectionException e) {
            mLogger.debug("Printer Disconnect Error", e);
        } finally {
        }
    }

    public ZebraPrinter connect() {
        if (mConnection != null) disconnect();
        mConnection = null;
        mConnection = getConnection();


        try {
            mConnection.open();
        } catch (ConnectionException e) {
            mLogger.debug("Printer Connection Error", e);
            disconnect();
        }

        ZebraPrinter printer = null;

        if (mConnection.isConnected()) {
            try {

                printer = ZebraPrinterFactory.getInstance(mConnection);
            } catch (ConnectionException e) {
                mLogger.debug("Printer Connection Error", e);
                printer = null;
                ApexSleeper.sleep(1000);
                disconnect();
            } catch (ZebraPrinterLanguageUnknownException e) {
                mLogger.debug("Printer Connection Error", e);
                printer = null;
                ApexSleeper.sleep(1000);
                disconnect();
            }
        }

        return printer;
    }


    public ZebraPrinter getPrinter() {
        if (mPrinter == null) {
            mPrinter = connect();
        }
        return mPrinter;
    }
    public String getMacAddress() {
        return mPrefs.getMacAddress();
    }

    public boolean printTubeLabel(WorkOrderInterface currentWorkOrder, SpecimenRequirementInterface specimen) {
        int retry = 0;
        while (retry < NUM_TRIES) {

            mLogger.debug("Tube Printing: " + "Begin Tube Printing");
            ZebraPrinter printer = null;

            if (!mApp.isBluetoothEnabled()) {
                mApp.showPrinterErrorDialog(ApexApplication.ERROR_BLUETOOTH);
                return false;
            }

            try {
                mLogger.debug("Tube Printing: " + "Getting Connection");

                printer = getPrinter();
                if (printer == null) {
                    mApp.showPrinterErrorDialog("Failed to open printer. Please ensure it is turned on and try again.");
                    return false;
                }

                if (checkPrinterStatus(printer)) {
                    mLogger.debug("Tube Printing: " + "Printer Status OK: {}", printer.toString());
                    boolean isOK = sendTubeLabel(printer, currentWorkOrder, specimen);
                    return isOK;
                } else {
                    mConnection = null;
                    mPrinter = null;
                    retry++;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }

                    if (retry == NUM_TRIES) {
                        mApp.showPrinterErrorDialog("Failed to get printer status. Please ensure it is turned on and try again.");
                        return false;
                    }
                }
            } catch (Exception e) {
                mLogger.warn("Printer Connection: {}", e.getMessage());
                try {
                    mConnection.close();
                } catch (ConnectionException e1) {
                    e1.printStackTrace();
                }
                mConnection = null;
                mPrinter = null;
                retry++;
                if (retry == NUM_TRIES) {
                    mApp.showPrinterErrorDialog("Failed to connect to printer. Please ensure it is turned on and try again.");
                    return false;
                } else {
                    //Wait for connection to reset
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    private boolean checkPrinterStatus(ZebraPrinter printer) {
        mLogger.debug("Printer Status: " + "Begin Checking Printer Status");
        boolean isOK = true;

        if (mPrefs.getPrinterName() == null) {
            mApp.showPrinterErrorDialog("No printer is paired.");
            return false;
        }
        try {
            ZebraPrinterLinkOs linkOsPrinter = ZebraPrinterFactory.createLinkOsPrinter(printer);

            PrinterStatus printerStatus = (linkOsPrinter != null) ? linkOsPrinter.getCurrentStatus() : printer.getCurrentStatus();

            if (printerStatus.isReadyToPrint) {
                PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();
                SGD.SET("device.languages", "cpcl", mConnection);
            } else if (printerStatus.isHeadOpen) {
                mLogger.debug("Printer Head Open");
                isOK = false;
            } else if (printerStatus.isPaused) {
                mLogger.debug("Printer is Paused");
                isOK = false;
            } else if (printerStatus.isPaperOut) {
                mLogger.debug("Printer Media Out");
                isOK = false;
            }
        } catch (ConnectionException e) {
            mLogger.debug(e.getMessage());
            isOK = false;
        } finally {
            if (!isOK) disconnect();
        }
        return isOK;
    }

    public boolean sendTubeLabel(ZebraPrinter printer, WorkOrderInterface wo, SpecimenRequirementInterface specimen) {
        mLogger.debug("Sending Tube Label: " + "Begin Sending Tube Label");

        String newFilePath = createTubeLabel(wo, specimen);
        mLogger.debug("Sending Tube Label: " + "Label created: {}", newFilePath);

        if (newFilePath == null || newFilePath.isEmpty()) {
            mApp.showPrinterErrorDialog("Error Generating Tube Label");
            mLogger.warn("Tube Label Error");
            return false;
        }

        try {
            mLogger.debug("Sending Tube Label: " + "Begin Sending Label To Printer");
            printer.sendFileContents(newFilePath);
            ApexSleeper.sleep(500);
            mLogger.debug("Sending Tube Label: " + "Label Sent");
            return true;
        } catch (Exception e) {
            mApp.showPrinterErrorDialog("Error Generating Tube Label.");
            mPrinter = null;
            mLogger.debug("Tube Label Error: {}", e);
            return false;
        }
    }

    public String createTubeLabel(WorkOrderInterface wo, SpecimenRequirementInterface specimen) {
        FileOutputStream os = null;
        mLogger.debug("Creating Tube Label: " + "Begin Sending Label To Printer");

        InputStream labelStream = specimen.printBarcode() ? getTubeLabelTemplateFile() : getTubeLabelNoBarcodeTemplateFile();

        if (mTubeLabelPrintFile.exists()) {
            mTubeLabelPrintFile.delete();
        }
        if (labelStream == null) {
            return "";
        }
        String labelTemplate = "";
        InputStreamReader is = new InputStreamReader(labelStream);
        BufferedReader reader = new BufferedReader(is);
        String s = "";
        try {
            while ((s = reader.readLine()) != null) {
                labelTemplate += s + "\n";
            }
        } catch (IOException e) {
            mLogger.warn("IO: {}", e.getMessage());
        }

        labelTemplate = fixName(labelTemplate, wo);  // Fix name

        labelTemplate = labelTemplate.replace("[BARCODETYPE]", mPrefs.getBarcodeType());    // AM-49
        // Note ! This replace existst but IS NOT USED since the label template DOES NOT contains BARCODETYPE.
        // Is left for possible future use in this version of the code

        labelTemplate = labelTemplate.replace("[BARCODE]", specimen.getBarcode() == null ? "" : specimen.getBarcode());
        labelTemplate = labelTemplate.replace("[SPECIMEN]", specimen.getBarcodeName() == null ? "" : specimen.getBarcodeName());
        labelTemplate = labelTemplate.replace("[TIMESTAMP]", getTime() == null ? "" : getTime());
        labelTemplate = labelTemplate.replace("[COLOR]", specimen.getTubeType() == null ? "" : specimen.getTubeType());
        labelTemplate = labelTemplate.replace("[CITY]", wo.getCity() == null ? "" : wo.getCity());
        labelTemplate = labelTemplate.replace("[STATE]", wo.getState() == null ? "" : wo.getState());
        labelTemplate = labelTemplate.replace("[ZIP]", wo.getZipcode() == null ? "" : wo.getZipcode());
        labelTemplate = labelTemplate.replace("[STREET]", wo.getAddressLine1() == null ? "" : wo.getAddressLine1());
        labelTemplate = labelTemplate.replace("[DOB]", wo.getDob() == null ? "" : wo.getDob());

        mLogger.debug("Creating Tube Label: " + "Label: {}", labelTemplate);

        byte[] zplLabel = labelTemplate.getBytes();
        try {
            os = new FileOutputStream(mTubeLabelPrintFile);
        } catch (FileNotFoundException e) {
            mLogger.warn("FNFE: {}", e.getMessage());
        }
        try {
            if (os != null) {
                mLogger.debug("Creating Tube Label: " + "Begin Writing To Output Stream");
                os.write(zplLabel);
                os.close();
                mLogger.debug("Creating Tube Label: " + "Finished Writing To Output Stream");
            }
        } catch (IOException e) {
            mLogger.warn("ioe: {}", e.getMessage());
        }

        return mTubeLabelPrintFile.toString();
    }

    public boolean printFailedVisitInfo(String techName, WorkOrderInterface wo, String failureReason) {
        int retry = 0;

        if (failureReason == null)
        {
            mApp.showPrinterErrorDialog(ApexApplication.ERROR_NO_REASON);
            return false;
        }

        while (retry < NUM_TRIES) {
            boolean result = false;
            mLogger.debug("Print Failed Visit: " + "Begin Printing Failed Visit");

            ZebraPrinter printer;
            if (!mApp.isBluetoothEnabled()) {
                mApp.showPrinterErrorDialog(ApexApplication.ERROR_BLUETOOTH);
                return result;
            }

            try {
                mLogger.debug("Print Failed Visit: " + "Opening Connection");

                printer = getPrinter();

                if (printer == null) {
                    throw new Exception();
                }
                mLogger.debug("Print Failed Visit: " + "Printer Connected: {}", printer);

                if (!checkPrinterStatus(printer)) {
                    mConnection = null;
                    mPrinter = null;
                    retry++;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }

                    if (retry == NUM_TRIES) {
                        mApp.showPrinterErrorDialog("Failed to get printer status. Please ensure it is turned on and try again.");
                        return false;
                    }
                } else {
                    result = sendFailedVisitLabel(printer, techName, wo, failureReason);
                    return result;
                }

            } catch (Exception e) {
                mConnection = null;
                mPrinter = null;
                retry++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                if (retry == NUM_TRIES) {
                    mLogger.warn("Print Failed Visit: " + "Connection Failure");
                    mApp.showPrinterErrorDialog("Failed to connect to printer. Please ensure it is turned on and try again.");
                    return false;
                }
            }
        }
        return false;
    }

    public boolean sendFailedVisitLabel(ZebraPrinter printer, String techName, WorkOrderInterface wo, String failureReason) {
        try {
            mLogger.debug("Sending Failed Visit: " + "Begin Sending Failed Visit");
            String newFilePath = createFailedVisitLabel(techName, wo, failureReason);
            mLogger.debug("Sending Failed Visit: " + "Sending Failed Visit: {}", newFilePath);

            if (newFilePath == null || newFilePath.isEmpty()) {
                mApp.showPrinterErrorDialog("Error Generating Failed Visit Label");
                mLogger.warn("Sending Failed Visit: " + "Sending Failed Visit Failure, File Not Created");
                return false;
            }
            mLogger.debug("Sending Failed Visit: " + "Begin Sending Label To Printer");
            printer.sendFileContents(newFilePath);
            Thread.sleep(500);

            mLogger.debug("Sending Failed Visit: " + "Label Sent");
            return true;
        } catch (Exception e) {
            mApp.showPrinterErrorDialog(e.getMessage());
            mLogger.warn("Sending Failed Visit: {}", e);
            return false;
        }
    }

    private String createFailedVisitLabel(String techName, WorkOrderInterface wo, String failureReason) {
        String result = "";

        mLogger.debug("Create Failed Visit Label: " + "Beginning Label Creation");

        if (failureReason == null)
        {
            mLogger.debug("ERROR ! No failure reason !");
            return result;
        }

        if (mFailedVisitPrintFile.exists()) {
            mFailedVisitPrintFile.delete();
        }
        InputStream stream = getFailedVisitTemplateFile();
        if (stream == null) {
            return result;
        }

        String labelTemplate = "";
        InputStreamReader is = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(is);
        String s = "";
        try {
            while ((s = reader.readLine()) != null) {
                labelTemplate += s + "\n";
            }
        } catch (IOException e) {
            mLogger.warn("read", e.getMessage());
        }
        String reason1 = failureReason;
        String reason2 = null;
        if (failureReason.length() > 27) {
            reason1 = failureReason.substring(0, 26);
            reason2 = failureReason.substring(26);
        }

        labelTemplate = buildLabelTemplate(labelTemplate, reason1, reason2, wo, techName);

        mLogger.debug("Create Failed Visit Label: " + "Label Created: {}", labelTemplate);

        byte[] zplLabel = labelTemplate.getBytes();
        try {
            FileOutputStream os = new FileOutputStream(mFailedVisitPrintFile);
            mLogger.debug("Creating Tube Label: " + "Begin Writing To Output Stream");
            os.write(zplLabel);
            os.close();
            mLogger.debug("Creating Tube Label: " + "Finished Writing To Output Stream");
        } catch (Exception e) {
            mLogger.warn("File Error: {}", e.getMessage());
        }
        return mFailedVisitPrintFile.toString();
    }

    private String buildLabelTemplate(String origTemplate, String reason1, String reason2, WorkOrderInterface activeWo, String techName) {

        String labelTemplate = origTemplate;
        WorkOrderInterface wo = activeWo;

        //for testing
        if (wo == null) {
            wo = new WorkOrder();
            wo.setPatientName("Test");
            wo.setConfirmed(false);
            wo.setAppointmentTime(getTime());
        }

        labelTemplate = fixName(labelTemplate, wo);  // Fix name

        labelTemplate = labelTemplate.replace("[REASON]", reason1);
        labelTemplate = labelTemplate.replace("[REASON1]", reason2 == null ? "" : reason2);
        labelTemplate = labelTemplate.replace("[CURRDATE]", getTime() == null ? "" : getTime());
        labelTemplate = labelTemplate.replace("[TECH]", techName == null ? "" : techName);
        labelTemplate = labelTemplate.replace("[CONFIRM]", wo.isConfirmed() ? "Confirmed" : "Not Confirmed");
        labelTemplate = labelTemplate.replace("[WINDOW]", wo.getAppointmentTime() == null ? "" : wo.getAppointmentTime());
        return labelTemplate;
    }

    private String getTime() {
        DateTimeFormatter format = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm aa");
        return DateTime.now().toString(format);
    }
    private InputStream getFailedVisitTemplateFile() {
        return mApp.getResources().openRawResource(R.raw.failedvisittemplate);
    }
    private InputStream getTubeLabelTemplateFile() {
        return mApp.getResources().openRawResource(R.raw.labeltemplate);
    }
    private InputStream getTubeLabelNoBarcodeTemplateFile() {
        return mApp.getResources().openRawResource(R.raw.labelnobarcodetemplate);
    }

    // AM-32 - fix name if too long for label
    // With the default template max 31 character are allowed for the name.
    private String fixName(String inputString, WorkOrderInterface wo) {
        String resultString = "";
        String firstName, lastName;

        // Debug - check to avoid crashes
        if (inputString == null) inputString = "";
        if (wo.getPatientName() == null) wo.setPatientName("");
        if (wo.getPatientFirstName() == null) wo.setPatientFirstName("");
        if (wo.getPatientLastName() == null) wo.setPatientLastName("");

        // Check for the template.
        if (inputString.contains("T 5 0 20 17 [NAME]")) {
            // Not fail template - check for max lenght to 31

            if (wo.getPatientName().length() <= 31) {
                // Name long OK for normal font
                resultString = inputString.replace("[NAME]", wo.getPatientName());
            } else {
                // Name too long !  Change font then if still too long for the new font, trim it
                // Check for template ?
                inputString = inputString.replace("T 5 0 20 17 [NAME]", "T 0 0 20 17 [NAME]");

                if (wo.getPatientName().length() <= 42) {
                    resultString = inputString.replace("[NAME]", wo.getPatientName());
                } else {
                    // Try to cut it - pick up first 20 char from lastname and 20 char from firstname

                    if (wo.getPatientFirstName().length() > 20) {
                        firstName = wo.getPatientFirstName().substring(0, 20);
                    } else {
                        firstName = wo.getPatientFirstName();
                    }

                    if (wo.getPatientLastName().length() > 20) {
                        lastName = wo.getPatientLastName().substring(0, 20);
                    } else {
                        lastName = wo.getPatientLastName();
                    }

                    resultString = inputString.replace("[NAME]", lastName + ", " + firstName);
                }
            }
        } else {
            // fail template
            if (wo.getPatientName().length() <= 12) {
                // Name long OK for normal font
                resultString = inputString.replace("[NAME]", wo.getPatientName());
            } else {
                // Name too long !  Just trim name !
                if (wo.getPatientFirstName().length() > 6) {
                    firstName = wo.getPatientFirstName().substring(0, 6);
                } else {
                    firstName = wo.getPatientFirstName();
                }

                if (wo.getPatientLastName().length() > 4) {
                    lastName = wo.getPatientLastName().substring(0, 4);
                } else {
                    lastName = wo.getPatientLastName();
                }

                resultString = inputString.replace("[NAME]", lastName + ", " + firstName);
            }
        }

        return (resultString);
    }
}
