package com.apex.mpad.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.util.ApexApplication;

import javax.inject.Inject;

/**
 * Created by will on 5/8/15.
 */
public class ScanService extends IntentService {

    public static final String LABEL_TYPE = "com.motorolasolutions.emdk.datawedge.label_type";
    public static final String DATA_STRING = "com.motorolasolutions.emdk.datawedge.data_string";
    public static final String TYPE_128 = "LABEL-TYPE-CODE128";

    @Inject @ForApplication ApexApplication mApp;

    private Activity mActivity;

    public ScanService() {
        super("ScanService");
        Injector.INSTANCE.inject(this);
    }

    @Override protected void onHandleIntent(Intent intent) {
        mActivity = mApp.getCurrentActivity();
        if (intent != null) {
            String type = intent.getStringExtra(LABEL_TYPE);
            String data = intent.getStringExtra(DATA_STRING);

            if (type.equals(TYPE_128)) {
                processScan(data);
            }
        }
    }

    public void processScan(final String scanData) {
        View currentFocus = mActivity.getCurrentFocus();
        if (currentFocus != null) {
            final EditText edit = (EditText) currentFocus;
            edit.post(new Runnable() {
                @Override public void run() {
                    edit.setText(scanData.trim());
                    edit.setSelection(edit.getText().length());
                }
            });
        }
    }
}
