package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.DrawSiteInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class DrawSiteListConverter implements ParcelConverter<List<DrawSiteInterface>> {
    @Override public void toParcel(List<DrawSiteInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (DrawSiteInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<DrawSiteInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<DrawSiteInterface> items = new ArrayList<DrawSiteInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((DrawSiteInterface) Parcels.unwrap(parcel.readParcelable(DrawSiteInterface.class.getClassLoader())));
        }
        return items;
    }
}
