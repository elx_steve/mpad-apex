package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.FailInfoInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class FailInfoListConverter implements ParcelConverter<List<FailInfoInterface>> {
    @Override public void toParcel(List<FailInfoInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (FailInfoInterface fail : list) {
                parcel.writeParcelable(Parcels.wrap(fail), 0);
            }
        }
    }

    @Override public List<FailInfoInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<FailInfoInterface> items = new ArrayList<FailInfoInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((FailInfoInterface) Parcels.unwrap(parcel.readParcelable(FailInfoInterface.class.getClassLoader())));
        }
        return items;
    }
}
