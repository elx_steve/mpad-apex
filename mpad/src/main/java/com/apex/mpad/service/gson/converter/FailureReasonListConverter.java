package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.FailureReasonInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class FailureReasonListConverter implements ParcelConverter<List<FailureReasonInterface>> {
    @Override public void toParcel(List<FailureReasonInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (FailureReasonInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<FailureReasonInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<FailureReasonInterface> items = new ArrayList<FailureReasonInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((FailureReasonInterface) Parcels.unwrap(parcel.readParcelable(FailureReasonInterface.class.getClassLoader())));
        }
        return items;
    }
}
