package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.GpsDataPointInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

/**
 * Created by will on 4/19/15.
 */
public class GpsDataPointConverter implements ParcelConverter<GpsDataPointInterface> {
    @Override public void toParcel(GpsDataPointInterface point, Parcel parcel) {
        parcel.writeParcelable(Parcels.wrap(point), 0);
    }

    @Override public GpsDataPointInterface fromParcel(Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(GpsDataPointInterface.class.getClassLoader()));
    }
}
