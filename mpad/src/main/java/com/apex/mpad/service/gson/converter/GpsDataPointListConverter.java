package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.GpsDataPointInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class GpsDataPointListConverter implements ParcelConverter<List<GpsDataPointInterface>> {
    @Override public void toParcel(List<GpsDataPointInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (GpsDataPointInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<GpsDataPointInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<GpsDataPointInterface> items = new ArrayList<GpsDataPointInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((GpsDataPointInterface) Parcels.unwrap(parcel.readParcelable(GpsDataPointInterface.class.getClassLoader())));
        }
        return items;
    }
}
