package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import org.joda.time.DateTime;
import org.parceler.ParcelConverter;

/**
 * Created by will on 4/10/15.
 */
public class JodaDateTimeConverter implements ParcelConverter<DateTime> {
    @Override public void toParcel(DateTime time, Parcel parcel) {
        if (time == null) {
            parcel.writeInt(-1);
        } else {
            parcel.writeInt(1);
            parcel.writeLong(time.getMillis());
        }
    }

    @Override public DateTime fromParcel(Parcel parcel) {
        int nullCheck = parcel.readInt();
        DateTime result;

        if (nullCheck < 0) {
            result = null;
        } else {
            result = new DateTime(parcel.readLong());
        }

        return result;
    }
}
