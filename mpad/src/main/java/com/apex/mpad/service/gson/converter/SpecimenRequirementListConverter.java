package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.SpecimenRequirementInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class SpecimenRequirementListConverter implements ParcelConverter<List<SpecimenRequirementInterface>> {
    @Override public void toParcel(List<SpecimenRequirementInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (SpecimenRequirementInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<SpecimenRequirementInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<SpecimenRequirementInterface> items = new ArrayList<SpecimenRequirementInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((SpecimenRequirementInterface) Parcels.unwrap(parcel.readParcelable(SpecimenRequirementInterface.class.getClassLoader())));
        }
        return items;
    }
}
