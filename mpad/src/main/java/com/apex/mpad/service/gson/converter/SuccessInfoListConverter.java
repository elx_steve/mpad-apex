package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.SuccessInfoInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class SuccessInfoListConverter implements ParcelConverter<List<SuccessInfoInterface>> {
    @Override public void toParcel(List<SuccessInfoInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (SuccessInfoInterface success : list) {
                parcel.writeParcelable(Parcels.wrap(success), 0);
            }
        }
    }

    @Override public List<SuccessInfoInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<SuccessInfoInterface> items = new ArrayList<SuccessInfoInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((SuccessInfoInterface) Parcels.unwrap(parcel.readParcelable(SuccessInfoInterface.class.getClassLoader())));
        }
        return items;
    }
}
