package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.TestInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class TestListConverter implements ParcelConverter<List<TestInterface>> {
    @Override public void toParcel(List<TestInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (TestInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<TestInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<TestInterface> items = new ArrayList<TestInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((TestInterface) Parcels.unwrap(parcel.readParcelable(TestInterface.class.getClassLoader())));
        }
        return items;
    }
}
