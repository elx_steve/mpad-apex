package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.TubeDrawFailReasonInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class TubeDrawFailReasonsListConverter implements ParcelConverter<List<TubeDrawFailReasonInterface>> {
    @Override public void toParcel(List<TubeDrawFailReasonInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (TubeDrawFailReasonInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<TubeDrawFailReasonInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<TubeDrawFailReasonInterface> items = new ArrayList<TubeDrawFailReasonInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((TubeDrawFailReasonInterface) Parcels.unwrap(parcel.readParcelable(TubeDrawFailReasonInterface.class.getClassLoader())));
        }
        return items;
    }
}
