package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.TubeInfoInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class TubeInfoListConverter implements ParcelConverter<List<TubeInfoInterface>> {
    @Override public void toParcel(List<TubeInfoInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (TubeInfoInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<TubeInfoInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<TubeInfoInterface> items = new ArrayList<TubeInfoInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((TubeInfoInterface) Parcels.unwrap(parcel.readParcelable(TubeInfoInterface.class.getClassLoader())));
        }
        return items;
    }
}
