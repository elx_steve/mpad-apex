package com.apex.mpad.service.gson.converter;

import android.os.Parcel;
import com.apex.mpad.model.iface.WorkOrderInterface;
import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/19/15.
 */
public class WorkOrderListConverter implements ParcelConverter<List<WorkOrderInterface>> {
    @Override public void toParcel(List<WorkOrderInterface> list, Parcel parcel) {
        if (list == null) {
            parcel.writeInt(-1);
        }
        else {
            parcel.writeInt(list.size());
            for (WorkOrderInterface wo : list) {
                parcel.writeParcelable(Parcels.wrap(wo), 0);
            }
        }
    }

    @Override public List<WorkOrderInterface> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if (size < 0) return null;
        List<WorkOrderInterface> items = new ArrayList<WorkOrderInterface>();
        for (int i = 0; i < size; ++i) {
            items.add((WorkOrderInterface) Parcels.unwrap(parcel.readParcelable(WorkOrderInterface.class.getClassLoader())));
        }
        return items;
    }
}
