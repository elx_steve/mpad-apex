package com.apex.mpad.service.gson.instance;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by will on 4/19/15.
 */
public final class InterfaceAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {
    public JsonElement serialize(T object, Type interfaceType, JsonSerializationContext context) {
        final JsonObject wrapper = new JsonObject();
        wrapper.addProperty("type", object.getClass().getName());
        wrapper.add("data", context.serialize(object));
        return wrapper;
    }

    public T deserialize(JsonElement elem, Type interfaceType, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject wrapper = (JsonObject) elem;
        final JsonElement data = get(wrapper, "data");
        final JsonElement typeName = get(wrapper, "type");
        final Type actualType = typeForName(typeName);

        if (data != null && actualType != null) {
            return context.deserialize(data, actualType);
        } else {
            return context.deserialize(elem, interfaceType);
        }
    }

    private Type typeForName(final JsonElement typeElem) {
        Type type = null;
        String typeName = typeElem != null ? typeElem.getAsString() : "";
        try {
            type = Class.forName(typeName);
        } catch (ClassNotFoundException e) {
            type = null;
        }
        return type;
    }

    private JsonElement get(final JsonObject wrapper, String memberName) {
        final JsonElement elem = wrapper.get(memberName);
//        if (elem == null) throw new JsonParseException("no '" + memberName + "' member found in what was expected to be an interface wrapper");
        return elem;
    }
}
