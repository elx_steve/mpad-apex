package com.apex.mpad.service.gson.instance;

import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.google.gson.InstanceCreator;

import java.lang.reflect.Type;

/**
 * Created by will on 4/19/15.
 */
public class WorkOrderInstanceCreator implements InstanceCreator<WorkOrderInterface> {
    @Override public WorkOrderInterface createInstance(Type type) {
        return new WorkOrder();
    }
}
