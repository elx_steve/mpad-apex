package com.apex.mpad.service.jackson.date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;

import java.io.IOException;

/**
 * Created by will on 4/19/15.
 */
public class DateDeserializer extends StdScalarDeserializer<DateTime> {

    public DateDeserializer() {
        super(DateTime.class);
    }

    @Override public DateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonToken currentToken = jsonParser.getCurrentToken();
        DateTime result = null;
        if (currentToken == JsonToken.VALUE_STRING) {
            String dateTimeAsString = jsonParser.getText().trim();
            String patternForDate = "MM/dd/yyyy";
            String patternForExtendedDate = "EEEE, MMMM dd, yyyy";
            String patternForFullDate = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
            String patternForFullDateWithoutMillis = "yyyy-MM-dd'T'HH:mm:ss";
            DateTimeParser[] parsers = {
                    DateTimeFormat.forPattern(patternForDate).getParser(),
                    DateTimeFormat.forPattern(patternForExtendedDate).getParser(),
                    DateTimeFormat.forPattern(patternForFullDate).getParser(),
                    DateTimeFormat.forPattern(patternForFullDateWithoutMillis).getParser()
            };
            DateTimeFormatter formatter = new DateTimeFormatterBuilder().append(null, parsers).toFormatter();
            result = formatter.parseDateTime(dateTimeAsString);
        }
        return result;
    }
}
