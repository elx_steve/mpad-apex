package com.apex.mpad.service.jackson.date;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;
import org.joda.time.DateTime;
import org.joda.time.format.*;

import java.io.IOException;

public class DateSerializer extends StdScalarSerializer<DateTime> {

    public DateSerializer() {
        super(DateTime.class);
    }

    @Override public void serialize(DateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
        String patternForDate = "MM/dd/yyyy";
        String patternForExtendedDate = "EEEE, MMMM dd, yyyy";
        String patternForFullDate = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        DateTimeParser[] parsers = {
                DateTimeFormat.forPattern(patternForDate).getParser(),
                DateTimeFormat.forPattern(patternForExtendedDate).getParser(),
                DateTimeFormat.forPattern(patternForFullDate).getParser()
        };
        DateTimePrinter printer =
                new DateTimeFormatterBuilder()
                        .append(ISODateTimeFormat.date())
                        .appendLiteral('T')
                        .append(ISODateTimeFormat.hourMinuteSecond())
                                // omit fraction of second
                        .toPrinter();

        DateTimeFormatter formatter = new DateTimeFormatterBuilder().append(printer, parsers).toFormatter();

        String date = formatter.print(dateTime);
        jsonGenerator.writeString(formatter.print(dateTime));
    }
}


//2015-05-18T14:53:13.202-04:00