package com.apex.mpad.service.jackson.date;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.time.DateTime;

/**
 * Created by will on 4/19/15.
 */
public class DateTimeConverter extends SimpleModule {
    public DateTimeConverter() {
        super();
        addSerializer(DateTime.class, new DateSerializer());
        addDeserializer(DateTime.class, new DateDeserializer());
    }
}
