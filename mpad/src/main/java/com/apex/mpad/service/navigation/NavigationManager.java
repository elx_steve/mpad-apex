package com.apex.mpad.service.navigation;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import com.apex.mpad.activity.VisitRouteLocationActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.injection.NavigationModule;
import com.apex.mpad.model.GpsDataPoint;
import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.util.ApexApplication;
import dagger.ObjectGraph;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NavigationManager implements LocationListener {
    @Inject @ForApplication LocationManager locationManager;
    @Inject @ForApplication Context appContext;

    private static final Logger mLogger = LoggerFactory.getLogger(NavigationManager.class);

    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private static final int ONE_MINUTE = 1000 * 60;

    private ApexApplication mApp;
    private String mStatus;
    private boolean mUseRouting;
    private boolean mUseGps;
    private boolean mIsNavigating;

    private GpsDataPointInterface mCurrentGpsDataPoint;
    private GpsDataPointInterface mPreviousGpsDataPoint;
    private Location mPreviousLocation;

    private List<GpsDataPointInterface> mDataPoints = new ArrayList<GpsDataPointInterface>();

    public static final float THRESHOLD_DISTANCE = 100f;
    private final LocationListener mLocationListener = new LocationListener() {
        @Override public void onLocationChanged(Location location) {
            NavigationManager.this.onLocationChanged(location);
        }

        @Override public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override public void onProviderEnabled(String provider) {

        }

        @Override public void onProviderDisabled(String provider) {

        }
    };

    @Inject public NavigationManager() {
        ObjectGraph activityGraph = Injector.INSTANCE.add(new NavigationModule());
        activityGraph.inject(this);
        mApp = (ApexApplication) appContext;
        LocationProvider gps = locationManager.getProvider(LocationManager.GPS_PROVIDER);

        if (gps != null) {
//            locationManager.requestSingleUpdate(gps.getName(), mLocationListener, Looper.myLooper());
            locationManager.requestLocationUpdates(gps.getName(), 60000, 0, mLocationListener);
        }

        mUseGps = true;
        mUseRouting = true;
        mIsNavigating = false;
    }


    @Override public void onLocationChanged(Location location) {
        if (mApp == null || mApp.getSessionId() == null || mApp.getSessionId().isEmpty()) {
            return;
        }
        if (isBetterLocation(location, mPreviousLocation)) {
            mLogger.debug("New Location Found: {}", location.toString());
            mPreviousLocation = location;
            mCurrentGpsDataPoint = new GpsDataPoint(location.getLatitude(), location.getLongitude(), location.getBearing(), location.getSpeed(), new DateTime(), mApp.getTechId());
            mDataPoints.add(mCurrentGpsDataPoint);

            if (mApp.getCurrentWorkOrder() != null) {
                Location loc = new Location("custom");
                loc.setLatitude(mApp.getCurrentWorkOrder().getLatitude());
                loc.setLongitude(mApp.getCurrentWorkOrder().getLongitude());
                float distance = location.distanceTo(loc);
                if (distance < THRESHOLD_DISTANCE && mIsNavigating && !mApp.isPrinting()) {
                    mIsNavigating = false;
                    Intent i = new Intent(mApp, VisitRouteLocationActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mApp.startActivity(i);
                }
            }
        }
    }

    public boolean routeTrip(String name, String streetNumber, String street, String zip, double lat, double lon) {
//        Uri intentUri;
//        if (lat == -1 && lon == -1){
//            intentUri = Uri.parse("google.navigation:q=" + name + ",+" + streetNumber + " " + street + ",+" + zip);
//        } else {
//            intentUri = Uri.parse("google.navigation:q=" + Double.toString(lat) + "," + Double.toString(lon));
//        }
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, intentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");
//        mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f", lat, lon);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        appContext.startActivity(intent);
        return true;
    }

    public GpsDataPointInterface getCurrentGpsDataPoint() {
        if (mCurrentGpsDataPoint == null) {
            return null;
        }
        return mCurrentGpsDataPoint.clone();
    }

    public List<GpsDataPointInterface> getGpsInfoList() {
        List<GpsDataPointInterface> list = new ArrayList<GpsDataPointInterface>();

        if (mDataPoints.size() == 0) {
            return list;
        }

        List<GpsDataPointInterface> copy;

        copy = new ArrayList<GpsDataPointInterface>(mDataPoints);
        mDataPoints.clear();

        return copy;
    }

    public int getGpsInfoListCount() {
        return mDataPoints.size();
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > ONE_MINUTE;

        return isNewer;
        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
//        if (isSignificantlyNewer) {
//            return true;
//            // If the new location is more than two minutes older, it must be worse
//        } else if (isSignificantlyOlder) {
//            return false;
//        }
//
//        // Check whether the new location fix is more or less accurate
//        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
//        boolean isLessAccurate = accuracyDelta > 0;
//        boolean isMoreAccurate = accuracyDelta < 0;
//        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
//
//        // Check if the old and new location are from the same provider
//        boolean isFromSameProvider = isSameProvider(location.getProvider(),
//                currentBestLocation.getProvider());
//
//        // Determine location quality using a combination of timeliness and accuracy
////        if (isMoreAccurate) {
////            return true;
////        }
////        else
//        if (isNewer && !isLessAccurate) {
//            return true;
//        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
//            return true;
//        }
//        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public void clearGpsInfoList() {
        mDataPoints.clear();
    }

    public GpsDataPointInterface getPreviousGpsDataPoint() {
        return mPreviousGpsDataPoint;
    }

    public void setPreviousGpsDataPoint(GpsDataPointInterface previousGpsDataPoint) {
        mPreviousGpsDataPoint = previousGpsDataPoint;
    }

    @Override public void onStatusChanged(String provider, int status, Bundle extras) {
        int i = 0;
    }

    @Override public void onProviderEnabled(String provider) {
        int i = 0;
    }

    @Override public void onProviderDisabled(String provider) {
        int i = 0;
    }

    public void setIsNavigating(boolean isNavigating) {this.mIsNavigating = isNavigating;}
}
