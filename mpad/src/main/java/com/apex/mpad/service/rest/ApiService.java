package com.apex.mpad.service.rest;

import com.apex.mpad.service.rest.message.*;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by will on 4/10/15.
 */
public interface ApiService {

    @POST("/login")
    LoginResponseMessage login(@Body LoginRequestMessage message);

    @POST("/getWorkOrder")
    WorkOrdersResponseMessage getWorkOrders(@Body WorkOrdersRequestMessage message);

    @POST("/getSpecimenFailReasons")
    FailureReasonsResponseMessage getFailureReasons(@Body FailureReasonsRequestMessage message);

    @POST("/getTubeDrawFailReasons")
    TubeDrawFailReasonsResponseMessage getTubeDrawFailReasons(@Body TubeDrawFailReasonsRequestMessage message);

    @POST("/getDrawSites")
    DrawSitesResponseMessage getDrawSites(@Body DrawSitesRequestMessage message);

    @POST("/saveWorkOrder")
    SaveWorkOrderResponseMessage saveWorkOrder(@Body SaveWorkOrderRequestMessage message);

    @POST("/saveImage")
    SaveImageResponseMessage saveImage(@Body SaveImageRequestMessage message);

    @POST("/saveGpsData") SaveGpsDataCollectionResponseMessage saveGpsData(@Body SaveGpsDataCollectionRequestMessage message);

    @POST("/shutdown")  SaveShutdownResponseMessage saveShutdown(@Body SaveShutdownRequestMessage message);

    @POST("/getNotifications")
    NotificationsResponseMessage getNotifications(@Body NotificationsRequestMessage message);

}
