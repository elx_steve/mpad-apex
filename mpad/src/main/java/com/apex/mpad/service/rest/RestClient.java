package com.apex.mpad.service.rest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.FailInfo;
import com.apex.mpad.model.SuccessInfo;
import com.apex.mpad.model.TubeInfo;
import com.apex.mpad.model.iface.*;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.service.rest.iface.RestClientInterface;
import com.apex.mpad.service.rest.message.*;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.DateUtil;
import com.apex.mpad.util.FileUtility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit.RetrofitError;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by will on 4/10/15.
 * Modified by TFG on 2/22/2020.
 */
public class RestClient implements RestClientInterface {
    private static final String TAG = "RestClientInterface";
    private static final Logger mLogger = LoggerFactory.getLogger(RestClient.class);

    public static final String NOTES_MSG = "\nDOB chosen: ";

    @Inject @ForApplication Context mAppContext;
    @Inject @ForApplication ApexPrefs mPrefs;

    @Inject @Named("device_id") String deviceId;

    @Inject ApiService mApiService;
    @Inject FileUtility mFileUtility;
    @Inject ObjectMapper mObjectMapper;
    @Inject NavigationManager mNavService;

    @Inject public RestClient(ApiService service) {
        this.mApiService = service;
        Injector.INSTANCE.inject(this);
    }

    @Override public LoginResponseMessage authenticate(String username, String password) {
        LoginResponseMessage response = null;
        if (isConnected()) {
            LoginRequestMessage message = new LoginRequestMessage(username, password, deviceId);
            try {
                GpsDataPointInterface gps = mNavService.getCurrentGpsDataPoint();
                message.setLoginGpsInfo(((gps == null) ? mNavService.getPreviousGpsDataPoint() : gps));
                response = mApiService.login(message);
            } catch (RetrofitError error) {
                mLogger.warn("Authenticate Retrofit Error: " + error);
                Log.w(TAG, "Authenticate Retrofit Error: " + error);
                response = null;
            }
        }
        return response;
    }

    @Override public List<WorkOrderInterface> getWorkOrders(String sessionId) {
        List<WorkOrderInterface> result = null;
        WorkOrdersResponseMessage response = null;
        if (isConnected()) {
            WorkOrdersRequestMessage message = new WorkOrdersRequestMessage();
            message.setSessionId(sessionId);
            message.setAllWorkOrders(false);

            Log.d(TAG, "getWorkOrders - START");

            try {
                response = mApiService.getWorkOrders(message);
            } catch (RetrofitError error) {
                Log.d(TAG, "getWorkOrders - END - error");
                mLogger.warn("getWorkOrders Retrofit Error: " + error);
                Log.w(TAG, "getWorkOrders Retrofit Error: " + error);

                response = null;
            }
            if (response != null) {
                Log.d(TAG, "getWorkOrders - END - Ok");

                mLogger.debug("getWorkOrders - retrieved  " + response.workOrders.size() + " workorders");
                Log.d(TAG, "getWorkOrders - retrieved " + response.workOrders.size() + " workorders");

                result = response.getWorkOrders();
            }
        }
        return result;
    }

    @Override public List<FailureReasonInterface> getFailureReasons(String sessionId) {
        List<FailureReasonInterface> result = null;
        FailureReasonsResponseMessage response = null;
        if (isConnected()) {
            FailureReasonsRequestMessage message = new FailureReasonsRequestMessage();
            message.setSessionId(sessionId);
            try {
                response = mApiService.getFailureReasons(message);
            } catch (RetrofitError error) {
                mLogger.warn("getFailureReason Retrofit Error: " + error);
                Log.w(TAG, "getFailureReason Retrofit Error: " + error);
                response = null;
            }
            if (response != null) {
                result = response.getSpecimenFailReasons();
            }
        }
        return result;
    }

    @Override public List<TubeDrawFailReasonInterface> getTubeDrawFailureReasons(String sessionId) {
        List<TubeDrawFailReasonInterface> result = null;
        TubeDrawFailReasonsResponseMessage response = null;
        if (isConnected()) {
            TubeDrawFailReasonsRequestMessage message = new TubeDrawFailReasonsRequestMessage();
            message.setSessionId(sessionId);

            try {
                response = mApiService.getTubeDrawFailReasons(message);
            } catch (RetrofitError error) {
                mLogger.warn("getTubeDrawFailReason Retrofit Error: " + error);
                Log.w(TAG, "getTubeDrawFailReason Retrofit Error: " + error);

                response = null;
            }
            if (response != null) {
                result = response.getTubeDrawFailReasons();
            }
        }
        return result;
    }

    @Override public List<DrawSiteInterface> getDrawSites(String sessionId) {
        List<DrawSiteInterface> result = null;
        DrawSitesResponseMessage response = null;
        if (isConnected()) {
            DrawSitesRequestMessage message = new DrawSitesRequestMessage();
            message.setSessionId(sessionId);
            try {
                response = mApiService.getDrawSites(message);
            } catch (RetrofitError error) {
                mLogger.warn("getDrawSites Retrofit Error: " + error);
                Log.w(TAG, "getDrawSites Retrofit Error: " + error);

                response = null;
            }
            if (response != null) {
                result = response.getDrawSites();
            }
        }
        return result;
    }

    @Override public boolean saveWorkOrder(String sessionId, CompletedWorkOrderInterface wo, String techId) {
        SaveWorkOrderRequestMessage message = new SaveWorkOrderRequestMessage();
        SaveGpsDataCollectionRequestMessage gpsMessage = new SaveGpsDataCollectionRequestMessage();

        GpsDataPointInterface currentGps = mNavService.getCurrentGpsDataPoint();
        if (currentGps != null) {
            message.setWorkorderGpsInfo(currentGps);
        }
        message.setSessionId(sessionId);
        message.setTechId(techId);
        message.setSpecimenNumber(wo.getSpecimenNumber());
        message.setConfirmed(wo.isConfirmed());
        message.setHasAdditionalScript(wo.hasAdditionalScript());
        message.setAmountCollected(wo.getAmountCollected());
        message.setPaymentCollectionNotes(wo.getPaymentCollectionNotes());
        message.setPatientUnableToSign(wo.isPatientUnableToSign());
        message.setWorkorderNotes(wo.getCompletedWorkOrderNotes());
        message.setIsAddOnPatient(wo.isAddOnPatient());
        message.setPatientFirstName(wo.getPatientFirstName());
        message.setPatientLastName(wo.getPatientLastName());
        message.setPatientDob(wo.getDob());
        message.setIsDrawSuccessful(wo.isDrawSuccessfull());

        if (wo.getNewDOB() != null) {
            boolean areTrulyEqual = DateUtil.areDateStringsEqual(wo.getDob(), wo.getNewDOB());
            if (!areTrulyEqual) {
                message.setWorkorderNotes(message.getWorkorderNotes() + NOTES_MSG + wo.getNewDOB());
            }
        }

        FailInfoInterface failInfo = new FailInfo();
        failInfo.setFailedDateTime(wo.getFailedCollection().getFailedDateTime());
        failInfo.setReasonId(wo.getFailedCollection().getReasonId());
        failInfo.setFailComments(wo.getFailedCollection().getFailComments());

        message.setFailedCollection(failInfo);

        SuccessInfoInterface successInfo = new SuccessInfo();
        successInfo.setCollectionTime(wo.getSuccessCollection().getCollectionTime());
        successInfo.setDrawSite(wo.getSuccessCollection().getDrawSite());

        List<TubeInfoInterface> tubesCollected = new ArrayList<TubeInfoInterface>();

        if (wo.getSuccessCollection() != null && wo.getSuccessCollection().getTubesCollected() != null) {
            for (TubeInfoInterface tube : wo.getSuccessCollection().getTubesCollected()) {
                if (tube.getTubeTypeId() == TubeInfo.EXTRA_TUBE_ID || tube.getTubeCount() > 0) {
                    TubeInfoInterface tubeInfo = new TubeInfo();
                    tubeInfo.setTubeCount(tube.getTubeCount());
                    tubeInfo.setTubeTypeId(tube.getTubeTypeId());
                    tubesCollected.add(tubeInfo);
                }
            }
            successInfo.setTubesCollected(tubesCollected);
            successInfo.setTubeFailReasonId(wo.getSuccessCollection().getTubeFailReasonId());
            successInfo.setTubeFailReasonComment(wo.getSuccessCollection().getTubeFailReasonComment());

            message.setSuccessCollection(successInfo);
        }

        try {
            mFileUtility.saveToFile(Environment.getExternalStorageDirectory().getPath(), "SP-" + message.getSpecimenNumber() + ".txt", mObjectMapper.writeValueAsString(message));

        } catch (JsonProcessingException e) {
            StackTraceElement[] elements = e.getStackTrace();
            mLogger.warn("saveWorkOrder json", e.getMessage());
            Log.w(TAG, "saveWorkOrder json " + e.getMessage());
            return false;
        }
        return true;
    }

    @Override public SaveWorkOrderResponseMessage sendWorkOrder(SaveWorkOrderRequestMessage message) {
        SaveWorkOrderResponseMessage response = null;
        if (isConnected()) {
            try {
                response = mApiService.saveWorkOrder(message);
            } catch (RetrofitError error) {
                mLogger.warn("sendWorkOrder Retrofit Error: " + error);
                Log.w(TAG, "sendWorkOrders Retrofit Error: " + error);
                response = null;
            }
        }
        return response;
    }

    @Override public ErrorResponseMessage sendImage(SaveImageRequestMessage message) {
        SaveImageResponseMessage response = null;
        ErrorResponseMessage out = null;
        if (isConnected()) {
            try {
                response = mApiService.saveImage(message);
                if (response.getSuccess() != null) {
                    if (!response.getSuccess().isSuccess())
                        out = response.getError();
                } else {
                    out = response.getError();
                }
            } catch (RetrofitError error) {
                response = null;
                out = generateError(error.getMessage());
            }
        } else {
            out = generateError("Not connected");
        }
        return out;
    }

    @Override public ErrorResponseMessage sendGpsData(SaveGpsDataCollectionRequestMessage message) {
        SaveGpsDataCollectionResponseMessage response = null;
        ErrorResponseMessage out = null;
        if (isConnected()) {
            try {
                response = mApiService.saveGpsData(message);
                out = response.getError();
            } catch (RetrofitError error) {
                out = generateError(error.getMessage());
            }
        } else {
            out = generateError("Not connected");
        }
        return out;
    }

    private ErrorResponseMessage generateError(String desc) {
        ErrorResponseMessage out = new ErrorResponseMessage();
        out.setDescription(desc);
        return out;
    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager)mAppContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (!isConnected) {
//            ((ApexApplication) mAppContext).showConnectionErrorDialog();
            return false;
        } else {
            return true;
        }
    }

    @Override public boolean saveShutdown(String sessionId, boolean isAtLab, String techId) {
        if (sessionId != null && !sessionId.isEmpty() && techId != null && !techId.isEmpty()) {
            SaveShutdownRequestMessage message = new SaveShutdownRequestMessage();
            message.setTechId(techId);
            message.setSessionId(sessionId);
            message.setIsArriveAtLab(isAtLab);
            GpsDataPointInterface gps = mNavService.getCurrentGpsDataPoint();
            message.setShutdownGpsInfo(((gps == null) ? mNavService.getPreviousGpsDataPoint() : gps));
            message.setShutdownDateTime(new DateTime());
            DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMddHHmmss");
            String dt = message.getShutdownDateTime().toString(format);

            try {
                mFileUtility.saveToFile(Environment.getExternalStorageDirectory().getPath(), "SD-" + dt + ".txt", mObjectMapper.writeValueAsString(message));

            } catch (JsonProcessingException e) {
                StackTraceElement[] elements = e.getStackTrace();
                mLogger.warn("saveShutdown json", e.getMessage());
                Log.w(TAG, "saveShutdown json " + e.getMessage());
                return false;
            }
        }
        return true;
    }

    @Override public ErrorResponseMessage sendShutdown(SaveShutdownRequestMessage message) {
        ErrorResponseMessage err = null;

        if (isConnected()) {
            try {
                SaveShutdownResponseMessage response = mApiService.saveShutdown(message);
                err = response.getError();
            } catch (RetrofitError error) {
                err = generateError(error.getMessage());
            }
        } else {
            err = generateError("Not connected");
        }
        return err;
    }

    @Override public NotificationsResponseMessage getNotifications(NotificationsRequestMessage message) {
        NotificationsResponseMessage response = null;
        if (isConnected()) {
            try {
                response = mApiService.getNotifications(message);
            } catch (RetrofitError error) {
                mLogger.warn("getNotifications Retrofit Error: " + error);
                Log.w(TAG, "getNotifications Retrofit Error: " + error);
                response = null;
            }
        }
        return response;
    }
}
