package com.apex.mpad.service.rest.iface;

import com.apex.mpad.model.iface.*;
import com.apex.mpad.service.rest.message.*;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public interface RestClientInterface {
    LoginResponseMessage authenticate(String username, String password);
    List<WorkOrderInterface> getWorkOrders(String sessionId);
    List<FailureReasonInterface> getFailureReasons(String sessionId);
    List<TubeDrawFailReasonInterface> getTubeDrawFailureReasons(String sessionId);
    List<DrawSiteInterface> getDrawSites(String sessionId);
    boolean saveWorkOrder(String sessionId, CompletedWorkOrderInterface completedWorkOrder, String techId);
    SaveWorkOrderResponseMessage sendWorkOrder(SaveWorkOrderRequestMessage message);
    ErrorResponseMessage sendImage(SaveImageRequestMessage message);
    ErrorResponseMessage sendGpsData(SaveGpsDataCollectionRequestMessage message);
    boolean saveShutdown(String sessionId, boolean isAtLab, String techId);
    ErrorResponseMessage sendShutdown(SaveShutdownRequestMessage message);
    NotificationsResponseMessage getNotifications(NotificationsRequestMessage message);
}
