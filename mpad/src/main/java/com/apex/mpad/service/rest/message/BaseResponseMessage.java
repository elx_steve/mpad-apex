package com.apex.mpad.service.rest.message;

/**
 * Created by John on 9/20/2016.
 */

public class BaseResponseMessage {
    private SuccessResponseMessage success;
    private ErrorResponseMessage error;

    public SuccessResponseMessage getSuccess() {
        return success;
    }

    public void setSuccess(SuccessResponseMessage success) {
        this.success = success;
    }

    public ErrorResponseMessage getError() {
        return error;
    }

    public void setError(ErrorResponseMessage error) {
        this.error = error;
    }
}
