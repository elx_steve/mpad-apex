package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.DrawSiteInterface;
import com.apex.mpad.service.gson.converter.DrawSiteListConverter;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class DrawSitesResponseMessage {

    @ParcelPropertyConverter(DrawSiteListConverter.class)
    public List<DrawSiteInterface> drawSites;

    public List<DrawSiteInterface> getDrawSites() {
        return drawSites;
    }

    public void setDrawSites(List<DrawSiteInterface> drawSites) {
        this.drawSites = drawSites;
    }
}
