package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.FailureReasonInterface;
import com.apex.mpad.service.gson.converter.FailureReasonListConverter;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class FailureReasonsResponseMessage {
    @ParcelPropertyConverter(FailureReasonListConverter.class)
    public List<FailureReasonInterface> specimenFailReasons;

    public List<FailureReasonInterface> getSpecimenFailReasons() {
        return specimenFailReasons;
    }

    public void setSpecimenFailReasons(List<FailureReasonInterface> specimenFailReasons) {
        this.specimenFailReasons = specimenFailReasons;
    }
}
