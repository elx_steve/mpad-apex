package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.gson.converter.GpsDataPointConverter;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class LoginRequestMessage {
    public String username;
    public String password;
    public String deviceId;

    @ParcelPropertyConverter(GpsDataPointConverter.class)
    public GpsDataPointInterface loginGpsInfo;

    public LoginRequestMessage() {}

    public LoginRequestMessage(String username, String password, String deviceId) {
        this.username = username;
        this.password = password;
        this.deviceId = deviceId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public GpsDataPointInterface getLoginGpsInfo() {
        return loginGpsInfo;
    }

    public void setLoginGpsInfo(GpsDataPointInterface loginGpsInfo) {
        this.loginGpsInfo = loginGpsInfo;
    }


}