package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.gson.converter.GpsDataPointConverter;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class LoginResponseMessage {
    ErrorResponseMessage error;
    String sessionId;
    String techName;
    String techId;

    @ParcelPropertyConverter(GpsDataPointConverter.class)
    public GpsDataPointInterface officeLocation;

    public ErrorResponseMessage getError() {
        return error;
    }

    public void setError(ErrorResponseMessage error) {
        this.error = error;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTechName() {
        return techName;
    }

    public void setTechName(String techName) {
        this.techName = techName;
    }

    public String getTechId() {
        return techId;
    }

    public void setTechId(String techId) {
        this.techId = techId;
    }

    public GpsDataPointInterface getOfficeLocation() {
        return officeLocation;
    }

    public void setOfficeLocation(GpsDataPointInterface officeLocation) {
        this.officeLocation = officeLocation;
    }
}
