package com.apex.mpad.service.rest.message;

import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
//TODO: extend BaseResponseMessage
@Parcel(Parcel.Serialization.BEAN)
public class NotificationsResponseMessage {
    private String hasNewWorkOrders;
    private ErrorResponseMessage error;

    public boolean hasNewWorkOrders() {
        return hasNewWorkOrders.toLowerCase().equals("true");
    }

    public void setHasNewWorkOrders(String hnwo) {
        this.hasNewWorkOrders = hnwo;
    }

    public String getHasNewWorkOrders() { return this.hasNewWorkOrders; }

    public ErrorResponseMessage getError() {
        return error;
    }

    public void setError(ErrorResponseMessage error) {
        this.error = error;
    }
}
