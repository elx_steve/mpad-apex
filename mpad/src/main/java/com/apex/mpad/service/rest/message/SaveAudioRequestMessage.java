package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.gson.converter.GpsDataPointListConverter;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class SaveAudioRequestMessage {
    public String sessionId;
    public String imageType;
    public String notes;
    public String imageData;
    public String imageReferenceId;
    public String techId;

    @ParcelPropertyConverter(GpsDataPointListConverter.class)
    public List<GpsDataPointInterface> gpsData;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public String getImageReferenceId() {
        return imageReferenceId;
    }

    public void setImageReferenceId(String imageReferenceId) {
        this.imageReferenceId = imageReferenceId;
    }

    public List<GpsDataPointInterface> getGpsData() {
        return gpsData;
    }

    public void setGpsData(List<GpsDataPointInterface> gpsData) {
        this.gpsData = gpsData;
    }

    public String getTechId() {
        return techId;
    }

    public void setTechId(String techId) {
        this.techId = techId;
    }

}
