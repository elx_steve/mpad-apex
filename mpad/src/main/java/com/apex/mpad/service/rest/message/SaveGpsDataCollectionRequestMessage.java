package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.gson.converter.GpsDataPointListConverter;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class SaveGpsDataCollectionRequestMessage {
    public String sessionId;

    @ParcelPropertyConverter(GpsDataPointListConverter.class)
    public List<GpsDataPointInterface> gpsData;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<GpsDataPointInterface> getGpsData() {
        return gpsData;
    }

    public void setGpsData(List<GpsDataPointInterface> gpsData) {
        this.gpsData = gpsData;
    }
}
