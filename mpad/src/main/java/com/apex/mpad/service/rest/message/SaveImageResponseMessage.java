package com.apex.mpad.service.rest.message;

import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
//TODO: extend BaseResponseMessage
@Parcel(Parcel.Serialization.BEAN)
public class SaveImageResponseMessage {
    private SuccessResponseMessage success;
    private ErrorResponseMessage error;

    public SuccessResponseMessage getSuccess() {
        return success;
    }

    public void setSuccess(SuccessResponseMessage success) {
        this.success = success;
    }

    public ErrorResponseMessage getError() {
        return error;
    }

    public void setError(ErrorResponseMessage error) {
        this.error = error;
    }
}
