package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.service.gson.converter.GpsDataPointConverter;
import com.apex.mpad.service.gson.converter.GpsDataPointListConverter;
import com.apex.mpad.service.gson.converter.JodaDateTimeConverter;

import org.joda.time.DateTime;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by John on 6/10/16.
 */
@Parcel
public class SaveShutdownRequestMessage  {
    public String sessionId;
    public String techId;
    public boolean isArriveAtLab; //TODO: rename
    @ParcelPropertyConverter(JodaDateTimeConverter.class)
    public DateTime shutdownDateTime;

    @ParcelPropertyConverter(GpsDataPointConverter.class)
    public GpsDataPointInterface shutdownGpsInfo;

    @ParcelPropertyConverter(GpsDataPointListConverter.class)
    public List<GpsDataPointInterface> gpsInfo;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTechId() {
        return techId;
    }

    public void setTechId(String techId) {
        this.techId = techId;
    }

    public String getIsArriveAtLab() {
        return (isArriveAtLab) ? "true" : "false";
    }

    public void setIsArriveAtLab(boolean isAtLab) {
        this.isArriveAtLab = isAtLab;
    }

    public GpsDataPointInterface getShutdownGpsInfo() {
        return shutdownGpsInfo;
    }

    public void setShutdownGpsInfo(GpsDataPointInterface shutdownGpsInfo) {
        this.shutdownGpsInfo = shutdownGpsInfo;
    }

    public List<GpsDataPointInterface> getGpsInfo() {
        return gpsInfo;
    }

    public void setGpsInfo(List<GpsDataPointInterface> gpsInfo) {
        this.gpsInfo = gpsInfo;
    }

    public DateTime getShutdownDateTime() {
        return shutdownDateTime;
    }

    public void setShutdownDateTime(DateTime shutdownDateTime) {
        this.shutdownDateTime = shutdownDateTime;
    }



}
