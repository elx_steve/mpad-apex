package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.FailInfoInterface;
import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.model.iface.SuccessInfoInterface;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
public class SaveWorkOrderRequestMessage {
    public String sessionId;
    public String specimenNumber;
    public boolean isConfirmed;
    public FailInfoInterface failedCollection;
    public SuccessInfoInterface successCollection;
    public boolean hasAdditionalScipt;
    public double amountCollected;
    public String paymentCollectionNotes;
    public String workorderNotes;
    public String patientComments;
    public boolean patientUnableToSign;
    public boolean isDrawSuccessfull;
    public boolean isAddOnPatient;
    public String patientFirstName;
    public String patientLastName;
    public String patientDob;
    public GpsDataPointInterface workorderGpsInfo;
    public List<GpsDataPointInterface> gpsData;
    public String techId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSpecimenNumber() {
        return specimenNumber;
    }

    public void setSpecimenNumber(String specimenNumber) {
        this.specimenNumber = specimenNumber;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    public FailInfoInterface getFailedCollection() {
        return failedCollection;
    }

    public void setFailedCollection(FailInfoInterface failedCollection) {
        this.failedCollection = failedCollection;
    }

    public SuccessInfoInterface getSuccessCollection() {
        return successCollection;
    }

    public void setSuccessCollection(SuccessInfoInterface successCollection) {
        this.successCollection = successCollection;
    }

    public boolean isHasAdditionalScript() {
        return hasAdditionalScipt;
    }

    public void setHasAdditionalScript(boolean hasAdditionalScript) {
        this.hasAdditionalScipt = hasAdditionalScript;
    }

    public double getAmountCollected() {
        return amountCollected;
    }

    public void setAmountCollected(double amountCollected) {
        this.amountCollected = amountCollected;
    }

    public String getPaymentCollectionNotes() {
        return paymentCollectionNotes;
    }

    public void setPaymentCollectionNotes(String paymentCollectionNotes) {
        this.paymentCollectionNotes = paymentCollectionNotes;
    }

    public String getWorkorderNotes() {
        return workorderNotes;
    }

    public void setWorkorderNotes(String workorderNotes) {
        this.workorderNotes = workorderNotes;
    }

    public String getPatientComments() {
        return patientComments;
    }

    public void setPatientComments(String patientComments) {
        this.patientComments = patientComments;
    }

    public boolean isPatientUnableToSign() {
        return patientUnableToSign;
    }

    public void setPatientUnableToSign(boolean patientUnableToSign) {
        this.patientUnableToSign = patientUnableToSign;
    }

    public boolean isDrawSuccessfull() {
        return isDrawSuccessfull;
    }

    public void setIsDrawSuccessful(boolean isDrawSuccessful) {
        this.isDrawSuccessfull = isDrawSuccessful;
    }

    public boolean isAddOnPatient() {
        return isAddOnPatient;
    }

    public void setIsAddOnPatient(boolean isAddOnPatient) {
        this.isAddOnPatient = isAddOnPatient;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientDob() {
        return patientDob;
    }

    public void setPatientDob(String patientDob) {
        this.patientDob = patientDob;
    }

    public GpsDataPointInterface getWorkorderGpsInfo() {
        return workorderGpsInfo;
    }

    public void setWorkorderGpsInfo(GpsDataPointInterface workorderGpsInfo) {
        this.workorderGpsInfo = workorderGpsInfo;
    }

    public List<GpsDataPointInterface> getGpsData() {
        return gpsData;
    }

    public void setGpsData(List<GpsDataPointInterface> gpsData) {
        this.gpsData = gpsData;
    }

    public String getTechId() {
        return techId;
    }

    public void setTechId(String techId) {
        this.techId = techId;
    }


}
