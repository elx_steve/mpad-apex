package com.apex.mpad.service.rest.message;

import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class TubeDrawFailReasonsRequestMessage {
    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}

