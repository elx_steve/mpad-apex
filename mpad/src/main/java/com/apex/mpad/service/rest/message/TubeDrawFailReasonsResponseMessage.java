package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.TubeDrawFailReasonInterface;
import com.apex.mpad.service.gson.converter.TubeDrawFailReasonsListConverter;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
@Parcel
public class TubeDrawFailReasonsResponseMessage {
    @ParcelPropertyConverter(TubeDrawFailReasonsListConverter.class)
    public List<TubeDrawFailReasonInterface> tubeDrawFailReasons;

    public List<TubeDrawFailReasonInterface> getTubeDrawFailReasons() {
        return tubeDrawFailReasons;
    }

    public void setTubeDrawFailReasons(List<TubeDrawFailReasonInterface> tubeDrawFailReasons) {
        this.tubeDrawFailReasons = tubeDrawFailReasons;
    }
}
