package com.apex.mpad.service.rest.message;

import org.parceler.Parcel;

/**
 * Created by will on 4/10/15.
 */
@Parcel(Parcel.Serialization.BEAN)
public class WorkOrdersRequestMessage {
    private String sessionId;
    private boolean allWorkOrders;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean getAllWorkOrders() {
        return allWorkOrders;
    }

    public void setAllWorkOrders(boolean allWorkOrders) {
        this.allWorkOrders = allWorkOrders;
    }
}
