package com.apex.mpad.service.rest.message;

import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.service.gson.converter.WorkOrderListConverter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

/**
 * Created by will on 4/10/15.
 */
@Parcel
@JsonIgnoreProperties
public class WorkOrdersResponseMessage {

    @ParcelPropertyConverter(WorkOrderListConverter.class)
    @JsonProperty("work_orders")
    public List<WorkOrderInterface> workOrders;

    public List<WorkOrderInterface> getWorkOrders() {
        return workOrders;
    }

    public void setWorkOrders(List<WorkOrderInterface> workOrders) {
        this.workOrders = workOrders;
    }
}
