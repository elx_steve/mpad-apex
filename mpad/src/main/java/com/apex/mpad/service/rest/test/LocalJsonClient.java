package com.apex.mpad.service.rest.test;

import android.content.Context;
import android.util.Log;
import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by will on 4/19/15.
 */
public class LocalJsonClient implements Client {
    private Context context;

    private String scenario = null;

    public LocalJsonClient(Context ctx) {
        this.context = ctx;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    @Override
    public Response execute(Request request) throws IOException {
        URL requestedUrl = new URL(request.getUrl());
        String requestedMethod = request.getMethod();

        String prefix = "";
        if (this.scenario != null) {
            prefix = scenario + "_";
        }

        String fileName = (requestedUrl.getPath()).replace("/", "");
        fileName = fileName.toLowerCase();

        int resourceId = context.getResources().getIdentifier(fileName, "raw",
                context.getPackageName());

        if (resourceId == 0) {
            Log.wtf("YourTag", "Could not find res/raw/" + fileName + ".json");
            throw new IOException("Could not find res/raw/" + fileName + ".json");
        }


        InputStream inputStream = context.getResources().openRawResource(resourceId);


        //testing with json
//        InputStream inputStream = new ByteArrayInputStream(testJson.getBytes());

        String mimeType = URLConnection.guessContentTypeFromStream(inputStream);
        if (mimeType == null) {
            mimeType = "application/json";
        }

        TypedInput body = new TypedInputStream(mimeType, inputStream.available(), inputStream);
        return new Response(request.getUrl(), 200, "Content from res/raw/" + fileName, new ArrayList<Header>(), body);
    }

    private static class TypedInputStream implements TypedInput {
        private final String mimeType;
        private final long length;
        private final InputStream stream;

        private TypedInputStream(String mimeType, long length, InputStream stream) {
            this.mimeType = mimeType;
            this.length = length;
            this.stream = stream;
        }

        @Override
        public String mimeType() {
            return mimeType;
        }

        @Override
        public long length() {
            return length;
        }

        @Override
        public InputStream in() throws IOException {
            return stream;
        }
    }


    String testJson = "{\n" +
            "    \"error\": null,\n" +
            "    \"work_orders\": [\n" +
            "        {\n" +
            "            \"facility_id\": \"0\",\n" +
            "            \"route_number\": \"1\",\n" +
            "            \"stop_number\": \"1\",\n" +
            "            \"specimen_number\": \"151032144\",\n" +
            "            \"is_complete\": \"false\",\n" +
            "            \"is_confirmed\": \"true\",\n" +
            "            \"not_confirmed_reason\": \"\",\n" +
            "            \"visit_date\": \"4/13/2015\",\n" +
            "            \"patient_id\": \"522373\",\n" +
            "            \"patient_name\": \"TEST0010, TESTABC\",\n" +
            "            \"dob\": \"1/1/1900\",\n" +
            "            \"gender\": \"M\",\n" +
            "            \"patient_phone\": \"5610000000\",\n" +
            "            \"alt_contact_name\": \"\",\n" +
            "            \"alt_phone_no\": \"5615555555\",\n" +
            "            \"address_line_1\": \"110 Central Avenue\",\n" +
            "            \"address_line_2\": \"#203\",\n" +
            "            \"city\": \"Farmingdale\",\n" +
            "            \"state\": \"NY\",\n" +
            "            \"zipcode\": \"11735\",\n" +
            "            \"latitude\": \"40.753482\",\n" +
            "            \"longitude\": \"-73.4095019\",\n" +
            "            \"patient_comments\": \"\",\n" +
            "            \"work_order_notes\": \"testing for Andy. Leave on schedule\",\n" +
            "            \"appointment_time\": \"06:00-08:00\",\n" +
            "            \"acs_information\": \"\",\n" +
            "            \"is_fasting\": \"false\",\n" +
            "            \"insurance_type\": \"CASH/CHECK\",\n" +
            "            \"is_cash_check_pmt\": \"true\",\n" +
            "            \"doctor_name\": \"tom atwell\",\n" +
            "            \"agency_name\": \"\",\n" +
            "            \"additional_doctor_1_name\": \"\",\n" +
            "            \"additional_doctor_2_name\": \"\",\n" +
            "            \"frequency\": \"Every 1 Week Mon, Tue, Wed, Thu, Fri, Sat\",\n" +
            "            \"order_end_date\": \"7/9/2015\",\n" +
            "            \"upcoming_visit_date\": \"7/9/2015\",\n" +
            "            \"collect_amount_description\": \"Patient is SELF PAY.\",\n" +
            "            \"collect_amount_total\": \"0\",\n" +
            "            \"patient_specimen_count\": \"1\",\n" +
            "            \"specimen_requirements\": [\n" +
            "                {\n" +
            "                    \"tube_type_id\": 1,\n" +
            "                    \"tube_type\": \"SST\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132144\",\n" +
            "                    \"barcode_name\": null\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tests\": [\n" +
            "                {\n" +
            "                    \"test_name\": \"IRON PRO\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"THYROID PRO\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"facility_id\": \"0\",\n" +
            "            \"route_number\": \"2\",\n" +
            "            \"stop_number\": \"2\",\n" +
            "            \"specimen_number\": \"151032140\",\n" +
            "            \"is_complete\": \"false\",\n" +
            "            \"is_confirmed\": \"true\",\n" +
            "            \"not_confirmed_reason\": \"\",\n" +
            "            \"visit_date\": \"4/13/2015\",\n" +
            "            \"patient_id\": \"522532\",\n" +
            "            \"patient_name\": \"TEST0012, TEST\",\n" +
            "            \"dob\": \"1/1/1900\",\n" +
            "            \"gender\": \"M\",\n" +
            "            \"patient_phone\": \"5610000000\",\n" +
            "            \"alt_contact_name\": \"\",\n" +
            "            \"alt_phone_no\": \"\",\n" +
            "            \"address_line_1\": \"80 Second Avenue\",\n" +
            "            \"address_line_2\": \"\",\n" +
            "            \"city\": \"Brentwood\",\n" +
            "            \"state\": \"NY\",\n" +
            "            \"zipcode\": \"11717\",\n" +
            "            \"latitude\": \"40.7783162\",\n" +
            "            \"longitude\": \"-73.2420058\",\n" +
            "            \"patient_comments\": \"\",\n" +
            "            \"work_order_notes\": null,\n" +
            "            \"appointment_time\": \"06:00-08:00\",\n" +
            "            \"acs_information\": \"\",\n" +
            "            \"is_fasting\": \"false\",\n" +
            "            \"insurance_type\": \"CASH/CHECK\",\n" +
            "            \"is_cash_check_pmt\": \"true\",\n" +
            "            \"doctor_name\": \"tom atwell\",\n" +
            "            \"agency_name\": \"\",\n" +
            "            \"additional_doctor_1_name\": \"\",\n" +
            "            \"additional_doctor_2_name\": \"\",\n" +
            "            \"frequency\": \"Every 1 Week Sun, Mon, Tue, Wed, Thu, Fri, Sat\",\n" +
            "            \"order_end_date\": \"10/13/2015\",\n" +
            "            \"upcoming_visit_date\": \"10/13/2015\",\n" +
            "            \"collect_amount_description\": \"Patient is SELF PAY.\",\n" +
            "            \"collect_amount_total\": \"0\",\n" +
            "            \"patient_specimen_count\": \"2\",\n" +
            "            \"specimen_requirements\": [\n" +
            "                {\n" +
            "                    \"tube_type_id\": 6,\n" +
            "                    \"tube_type\": \"LT.BLUE\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132140\",\n" +
            "                    \"barcode_name\": null\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tests\": [\n" +
            "                {\n" +
            "                    \"test_name\": \"PT/INR\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"facility_id\": \"0\",\n" +
            "            \"route_number\": \"2\",\n" +
            "            \"stop_number\": \"2\",\n" +
            "            \"specimen_number\": \"151032143\",\n" +
            "            \"is_complete\": \"false\",\n" +
            "            \"is_confirmed\": \"true\",\n" +
            "            \"not_confirmed_reason\": \"\",\n" +
            "            \"visit_date\": \"4/13/2015\",\n" +
            "            \"patient_id\": \"522532\",\n" +
            "            \"patient_name\": \"TEST0012, TEST\",\n" +
            "            \"dob\": \"1/1/1900\",\n" +
            "            \"gender\": \"M\",\n" +
            "            \"patient_phone\": \"5610000000\",\n" +
            "            \"alt_contact_name\": \"\",\n" +
            "            \"alt_phone_no\": \"\",\n" +
            "            \"address_line_1\": \"80 Second Avenue\",\n" +
            "            \"address_line_2\": \"\",\n" +
            "            \"city\": \"Brentwood\",\n" +
            "            \"state\": \"NY\",\n" +
            "            \"zipcode\": \"11717\",\n" +
            "            \"latitude\": \"40.7783162\",\n" +
            "            \"longitude\": \"-73.2420058\",\n" +
            "            \"patient_comments\": \"\",\n" +
            "            \"work_order_notes\": null,\n" +
            "            \"appointment_time\": \"06:00-08:00\",\n" +
            "            \"acs_information\": \"\",\n" +
            "            \"is_fasting\": \"false\",\n" +
            "            \"insurance_type\": \"CASH/CHECK\",\n" +
            "            \"is_cash_check_pmt\": \"true\",\n" +
            "            \"doctor_name\": \"tom atwell\",\n" +
            "            \"agency_name\": \"\",\n" +
            "            \"additional_doctor_1_name\": \"\",\n" +
            "            \"additional_doctor_2_name\": \"\",\n" +
            "            \"frequency\": \"Every 1 Week Sun, Mon, Tue, Wed, Thu, Fri, Sat\",\n" +
            "            \"order_end_date\": \"10/13/2015\",\n" +
            "            \"upcoming_visit_date\": \"10/13/2015\",\n" +
            "            \"collect_amount_description\": \"Patient is SELF PAY.\",\n" +
            "            \"collect_amount_total\": \"0\",\n" +
            "            \"patient_specimen_count\": \"2\",\n" +
            "            \"specimen_requirements\": [\n" +
            "                {\n" +
            "                    \"tube_type_id\": 65,\n" +
            "                    \"tube_type\": \"Sterile Cup\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132143\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 66,\n" +
            "                    \"tube_type\": \"UA Tube\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132143\",\n" +
            "                    \"barcode_name\": null\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tests\": [\n" +
            "                {\n" +
            "                    \"test_name\": \"UA\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"facility_id\": \"0\",\n" +
            "            \"route_number\": \"3\",\n" +
            "            \"stop_number\": \"3\",\n" +
            "            \"specimen_number\": \"151032141\",\n" +
            "            \"is_complete\": \"false\",\n" +
            "            \"is_confirmed\": \"true\",\n" +
            "            \"not_confirmed_reason\": \"\",\n" +
            "            \"visit_date\": \"4/13/2015\",\n" +
            "            \"patient_id\": \"525296\",\n" +
            "            \"patient_name\": \"TEST0020, TEST\",\n" +
            "            \"dob\": \"1/1/1900\",\n" +
            "            \"gender\": \"M\",\n" +
            "            \"patient_phone\": \"9149633279\",\n" +
            "            \"alt_contact_name\": \"\",\n" +
            "            \"alt_phone_no\": \"\",\n" +
            "            \"address_line_1\": \"170 Finn Court\",\n" +
            "            \"address_line_2\": \"\",\n" +
            "            \"city\": \"Farmingdale\",\n" +
            "            \"state\": \"NY\",\n" +
            "            \"zipcode\": \"11735\",\n" +
            "            \"latitude\": \"40.7591628\",\n" +
            "            \"longitude\": \"-73.4053897\",\n" +
            "            \"patient_comments\": \"\",\n" +
            "            \"work_order_notes\": \"TESTING\",\n" +
            "            \"appointment_time\": \"10:00-13:00\",\n" +
            "            \"acs_information\": \"\",\n" +
            "            \"is_fasting\": \"false\",\n" +
            "            \"insurance_type\": \"CASH/CHECK\",\n" +
            "            \"is_cash_check_pmt\": \"true\",\n" +
            "            \"doctor_name\": \"tom atwell\",\n" +
            "            \"agency_name\": \"\",\n" +
            "            \"additional_doctor_1_name\": \"\",\n" +
            "            \"additional_doctor_2_name\": \"\",\n" +
            "            \"frequency\": \"Every 1 Week Sun, Mon, Tue, Wed, Thu, Fri, Sat\",\n" +
            "            \"order_end_date\": \"10/13/2015\",\n" +
            "            \"upcoming_visit_date\": \"10/13/2015\",\n" +
            "            \"collect_amount_description\": \"Patient is SELF PAY.\",\n" +
            "            \"collect_amount_total\": \"0\",\n" +
            "            \"patient_specimen_count\": \"2\",\n" +
            "            \"specimen_requirements\": [\n" +
            "                {\n" +
            "                    \"tube_type_id\": 93,\n" +
            "                    \"tube_type\": \"Bld Cult Aerobic Btl\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132141\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 94,\n" +
            "                    \"tube_type\": \"Bld Cult Anaerobic Btl\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132141\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 9,\n" +
            "                    \"tube_type\": \"GREY\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"13214199\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 5,\n" +
            "                    \"tube_type\": \"LAV\",\n" +
            "                    \"tube_count\": 2,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132141\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 7,\n" +
            "                    \"tube_type\": \"LAV ON ICE\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132141\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 14,\n" +
            "                    \"tube_type\": \"RYL BLUE PLSM\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132141\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 1,\n" +
            "                    \"tube_type\": \"SST\",\n" +
            "                    \"tube_count\": 3,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132141\",\n" +
            "                    \"barcode_name\": null\n" +
            "                },\n" +
            "                {\n" +
            "                    \"tube_type_id\": 2,\n" +
            "                    \"tube_type\": \"SST TAPE\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132141\",\n" +
            "                    \"barcode_name\": null\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tests\": [\n" +
            "                {\n" +
            "                    \"test_name\": \"ALBUMIN\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"AMMONIA\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"BMP\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"BLOOD CULTURE\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"CBC\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"HEAVY METAL SCREEN (\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"HGBA1C\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"IONIZED CALCIUM (NEE\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"IRON\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"IRON PRO\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"LIPID PRO\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"LITHIUM\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"LIVER PRO\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"SED RATE\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"test_name\": \"VIT D,25-OH\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"facility_id\": \"0\",\n" +
            "            \"route_number\": \"3\",\n" +
            "            \"stop_number\": \"3\",\n" +
            "            \"specimen_number\": \"151032142\",\n" +
            "            \"is_complete\": \"false\",\n" +
            "            \"is_confirmed\": \"true\",\n" +
            "            \"not_confirmed_reason\": \"\",\n" +
            "            \"visit_date\": \"4/13/2015\",\n" +
            "            \"patient_id\": \"525296\",\n" +
            "            \"patient_name\": \"TEST0020, TEST\",\n" +
            "            \"dob\": \"1/1/1900\",\n" +
            "            \"gender\": \"M\",\n" +
            "            \"patient_phone\": \"9149633279\",\n" +
            "            \"alt_contact_name\": \"\",\n" +
            "            \"alt_phone_no\": \"\",\n" +
            "            \"address_line_1\": \"170 Finn Court\",\n" +
            "            \"address_line_2\": \"\",\n" +
            "            \"city\": \"Farmingdale\",\n" +
            "            \"state\": \"NY\",\n" +
            "            \"zipcode\": \"11735\",\n" +
            "            \"latitude\": \"40.7591628\",\n" +
            "            \"longitude\": \"-73.4053897\",\n" +
            "            \"patient_comments\": \"\",\n" +
            "            \"work_order_notes\": \"TESTING\",\n" +
            "            \"appointment_time\": \"10:00-13:00\",\n" +
            "            \"acs_information\": \"\",\n" +
            "            \"is_fasting\": \"false\",\n" +
            "            \"insurance_type\": \"CASH/CHECK\",\n" +
            "            \"is_cash_check_pmt\": \"true\",\n" +
            "            \"doctor_name\": \"tom atwell\",\n" +
            "            \"agency_name\": \"\",\n" +
            "            \"additional_doctor_1_name\": \"testing\",\n" +
            "            \"additional_doctor_2_name\": \"\",\n" +
            "            \"frequency\": \"Every 1 Week Sun, Mon, Tue, Wed, Thu, Fri, Sat\",\n" +
            "            \"order_end_date\": \"10/13/2015\",\n" +
            "            \"upcoming_visit_date\": \"10/13/2015\",\n" +
            "            \"collect_amount_description\": \"Patient is SELF PAY.\",\n" +
            "            \"collect_amount_total\": \"0\",\n" +
            "            \"patient_specimen_count\": \"2\",\n" +
            "            \"specimen_requirements\": [\n" +
            "                {\n" +
            "                    \"tube_type_id\": 3,\n" +
            "                    \"tube_type\": \"PLAIN RED\",\n" +
            "                    \"tube_count\": 1,\n" +
            "                    \"print_barcode\": \"true\",\n" +
            "                    \"barcode\": \"132142\",\n" +
            "                    \"barcode_name\": null\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tests\": [\n" +
            "                {\n" +
            "                    \"test_name\": \"AMIKACIN TROUGH\"\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "     ]\n" +
            "}";
}
