package com.apex.mpad.util;

import com.apex.mpad.event.AbstractEvent;
import com.squareup.otto.Bus;

/**
 * Created by will on 4/11/15.
 */
public abstract class AbstractBus extends Bus {
    private Bus activeBus;

    protected AbstractBus(Bus bus) {
        this.activeBus = bus;
    }

    protected Bus getActiveBus() {
        return activeBus;
    }

    protected void post(AbstractEvent event) {
        this.activeBus.post(event);
    }
}
