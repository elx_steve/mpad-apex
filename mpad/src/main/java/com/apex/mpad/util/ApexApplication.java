package com.apex.mpad.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.apex.mpad.R;
import com.apex.mpad.activity.ExitActivity;
import com.apex.mpad.activity.LoginActivity;
import com.apex.mpad.activity.PermissionsActivity;
import com.apex.mpad.injection.AppModule;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.Visit;
import com.apex.mpad.model.WorkOrder;
import com.apex.mpad.model.iface.CompletedWorkOrderInterface;
import com.apex.mpad.model.iface.DrawSiteInterface;
import com.apex.mpad.model.iface.FailureReasonInterface;
import com.apex.mpad.model.iface.GpsDataPointInterface;
import com.apex.mpad.model.iface.TubeDrawFailReasonInterface;
import com.apex.mpad.model.iface.VisitInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.service.FileUploader;
import com.apex.mpad.service.navigation.NavigationManager;
import com.apex.mpad.service.rest.RestClient;
import com.apex.mpad.util.base.BaseActivity;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.ProfileConfig;
import com.symbol.emdk.ProfileManager;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by will on 4/9/15.
 */
public class ApexApplication extends Application implements EMDKManager.EMDKListener {
    private static final String TAG = "Application";
    public static final String printerConnectionFile = Environment.getExternalStorageDirectory() + "/apex/printer_mac.txt";
    public static final String ENDPOINT = "www.apexlabinc.com/ApexMPAD2/IApexMPADService/";
    public static final IntentFilter FILTER_NAV = new IntentFilter("com.apex.mpad.FILTER_NAV");
    public static final IntentFilter FILTER_UPLOAD = new IntentFilter("com.apex.mpad.FILTER_UPLOAD");
    public static final String ERROR_BLUETOOTH = "Bluetooth is not enabled. Please enable Bluetooth in the Android Settings and try again.";
    public static final String ERROR_NO_REASON = "Need to specify the reason. Please select a reason and try again.";
    public static final String CONF_FILE = "mpadConf.txt";
    public static final String EXTLOG_FILE = Environment.getExternalStorageDirectory() + "/log/mpad.log";

    public static final String DEFAULT_PROFILE = "Profile0 (default)";
    public static final String SCAN_PROFILE = "ApexEnable";
    private ProfileManager mProfileManager;
    private EMDKResults mEMDKResults;
    private String[] mModifyData;
    private EMDKManager mEMDKManager;

    private List<TubeDrawFailReasonInterface> mTubeDrawFailReasons;
    private List<FailureReasonInterface> mFailureReasons;
    private List<DrawSiteInterface> mDrawSites;
    private List<VisitInterface> mVisits;
    private VisitInterface mCurrentVisit;
    private VisitInterface mPreviousVisit;
    private WorkOrderInterface mCurrentWorkOrder;

    private List<GpsDataPointInterface> mDataPoints = new ArrayList<GpsDataPointInterface>();

    private Activity mCurrentActivity;
    private int mRunningActivities;

    private boolean mIsAlarmSet = false;

    private double mOfficeLat;
    private double mOfficeLong;
    private String mSessionId;
    private String mUsername;
    private String mTechName;
    private String mTechId;

    private boolean mPatientHasMoreWorkOrders;
    private boolean mIsPrinting;
    private boolean mIsUploading;

    private boolean mHasMoreWorkOrdersNotification;

    private static final Logger mLogger = LoggerFactory.getLogger(ApexApplication.class);

    private static ApexApplication INSTANCE;
    @Inject RestClient restClient;
    @Inject NavigationManager mNavManager;
    @Inject FileUploader mFileUploaderService;

    private ApexPrefs mPrefs;

    private Thread.UncaughtExceptionHandler mSysUncaughtExceptionHandler;
    private Thread.UncaughtExceptionHandler mUeHandler;

    public boolean isInit = false;
    public boolean isLoggedIn = false;
    // EMDK status - tells about EMDK readyness
    //   -1 -> status unknown
    //   0 -> present - all OK
    //   1 -> present - initialization errors - should not have impact
    //   2 -> present - failed acquire scan profile - potential problem !
    public byte EMDKstatus = -1;

    @Inject public ApexApplication() {}

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        if ((ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED)) {

            // Start EMDK at this stage only if all the permissions are OK !

            startEMDK();
        }
    }

    @Override public void onOpened(EMDKManager emdkManager) {
        mEMDKManager = emdkManager;
        mProfileManager = (ProfileManager) mEMDKManager.getInstance(EMDKManager.FEATURE_TYPE.PROFILE);

        if (mProfileManager != null) {
            mModifyData = new String[1];

            ProfileConfig profileConfig = new ProfileConfig();
            mEMDKResults = mProfileManager.processProfile(DEFAULT_PROFILE, ProfileManager.PROFILE_FLAG.GET, profileConfig);
            if(mEMDKResults.statusCode == EMDKResults.STATUS_CODE.FAILURE) {
                mLogger.warn("failed to acquire default profile");
                EMDKstatus = 1;
            }

            profileConfig.dataCapture.barcode.scanner_input_enabled = ProfileConfig.ENABLED_STATE.FALSE;
            mEMDKResults = mProfileManager.processProfile(DEFAULT_PROFILE, ProfileManager.PROFILE_FLAG.SET, profileConfig);

            if(mEMDKResults.statusCode == EMDKResults.STATUS_CODE.FAILURE) {
                mLogger.warn("failed to set default profile");
                EMDKstatus = 1;
            }

            mEMDKResults = mProfileManager.processProfile(SCAN_PROFILE, ProfileManager.PROFILE_FLAG.SET, mModifyData);
            if(mEMDKResults.statusCode == EMDKResults.STATUS_CODE.FAILURE) {
                mLogger.warn("failed to acquire scan profile");
                EMDKstatus = 2;

            }
        }

        initApp();      // Initialize the app regardless the state of EMDK
    }
    @Override public void onClosed() {
        mEMDKManager = null;
        mProfileManager = null;
        EMDKstatus = -1;
    }

    // The function starts EMDK
    public void startEMDK() {
        EMDKResults result = EMDKManager.getEMDKManager(this, this);
        EMDKstatus = (byte) ((result.statusCode.name().compareTo("SUCCESS") == 0) ? 0 : -1);
        mPrefs = ApexPrefs.getInstance(this);

        if (EMDKstatus == -1) {
            mLogger.error("EMDK early check - NOT OK - ERROR !");
            // If we arrive here, EMDK Service app is not working !
            // It could be not installed, or disabled or need to be updated.
            // Basically the app remains stuck forever.  A message is shown on the initial activity
            // page.
            // TBD - find why the app is stuck and eventually bypass EMDK - requires more investigation
            // on EMDK

            // initApp();   something not yet ready at this stage - if calling here null pointer error ahead
        }
    }


    private void initApp() {
        //must read config file before injection, so RestModule loads the correct Endpoint
        parseConfig();
        displayConfiguration();
        Injector.INSTANCE.init(new AppModule(this));
        Injector.INSTANCE.inject(this);
        isInit = true;
        isLoggedIn = false;
        initLists();
        initUeHandler();
        cancelTimedEvents();
        gotoLoginActivity(true);
    }

    // Debug function to display configuration after reading the configuration file
    private void displayConfiguration() {
        Log.d(TAG,"--- terminal configuration ---");
        Log.d(TAG,"    Mac address           : " + mPrefs.getMacAddress());
        Log.d(TAG,"    Printer name          : " + mPrefs.getPrinterName());

        Log.d(TAG,"    API endpoint          : " + mPrefs.getEndpoint());
        Log.d(TAG,"    Local Dev             : " + mPrefs.getLocalDev());
        Log.d(TAG,"    Default DOB           : " + mPrefs.getDefaultDob());
        Log.d(TAG,"    Notification interval : " + mPrefs.getNotificationInterval());
        Log.d(TAG,"    Type barcode          : " + mPrefs.getBarcodeType());
        Log.d(TAG,"    SSL                   : " + mPrefs.getSsl());

        mLogger.info("--- terminal configuration ---");
        mLogger.info("    Mac address           : " + mPrefs.getMacAddress());
        mLogger.info("    Printer name          : " + mPrefs.getPrinterName());

        mLogger.info("    API endpoint          : " + mPrefs.getEndpoint());
        mLogger.info("    Local Dev             : " + mPrefs.getLocalDev());
        mLogger.info("    Default DOB           : " + mPrefs.getDefaultDob());
        mLogger.info("    Notification interval : " + mPrefs.getNotificationInterval());
        mLogger.info("    Type barcode          : " + mPrefs.getBarcodeType());
        mLogger.info("    SSL                   : " + mPrefs.getSsl());
    }

    private void gotoLoginActivity(boolean loggedOut) {
        ((PermissionsActivity)mCurrentActivity).startLogin();
    }

    private void parseConfig() {
        File storage = Environment.getExternalStorageDirectory();
        File logDir = new File(storage.getAbsolutePath() + "/log");

        if (!logDir.exists()) {
            try {
               logDir.mkdir();
            } catch (Exception e) {
                // Can't create the log dir
                mLogger.error("Can't create Logdir :", e);
                Toast.makeText(this, "Warning ! Can't create log directory :" + logDir.toString(), Toast.LENGTH_LONG).show();
            }
        }
        try {
//            File logFile = new File(logDir.getAbsolutePath() + "/mpad.log");
            File logFile = new File(EXTLOG_FILE);
            if (!logFile.exists()) {
                mLogger.debug("Create log file on "+logFile.toString());
                logFile.createNewFile();
            }
        } catch (Exception e) {
            // Can't create the log
            mLogger.error("Can't create logfile  :", e);
            Toast.makeText(this, "Warning ! Can't create log file :" + EXTLOG_FILE, Toast.LENGTH_LONG).show();
        }

        File confFile = new File(storage, CONF_FILE);
        List<String> confLines = null;
        try {
            confLines = FileUtils.readLines(confFile, System.getProperty("file.encoding", "UTF-8"));
        } catch (IOException e) {
            confLines = null;
        }
        try {
            mPrefs.setEndpoint(ENDPOINT);
            mPrefs.setDefaultDob(ApexPrefs.DEFAULT_DOB);
            mPrefs.setTimerInterval(ApexPrefs.DEFAULT_UPLOAD_INTERVAL_MIN);
            if (confLines != null) {
                for (String s : confLines) {
                    String[] split;
                    s = s.trim();
                    if (s.startsWith("api_endpoint")) {
                        split = s.split("=");
                        if (split.length > 1) {
                            String endpoint = split[1];
                            if (endpoint.startsWith("https://") || endpoint.startsWith("http://"))
                                endpoint = endpoint.substring(endpoint.indexOf("//") + 2);
                            mPrefs.setEndpoint(endpoint);
                        }
                    } else if (s.startsWith("dob_date_picker_start")) {
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
                                Date d = fmt.parse(split[1]);
                                if (d != null) {
                                    mPrefs.setDefaultDob(fmt.format(d));
                                }
                            } catch (ParseException pe) {
                                mPrefs.setDefaultDob(ApexPrefs.DEFAULT_DOB);
                            }
                        }
                    } else if (s.startsWith("upload_frequency")) {
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                Long l = Long.parseLong(split[1]);
                                mPrefs.setTimerInterval(l.toString());
                            } catch (NumberFormatException nfe) {
                                mPrefs.setTimerInterval(ApexPrefs.DEFAULT_UPLOAD_INTERVAL_MIN);
                            }
                        }
                    } else if (s.startsWith("notifications_frequency")) {
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                Long l = Long.parseLong(split[1]);
                                mPrefs.setNotificationInterval(l.toString());
                            } catch (NumberFormatException nfe) {
                                mPrefs.setNotificationInterval(ApexPrefs.DEFAULT_UPLOAD_INTERVAL_MIN);
                            }
                        }
                    } else if (s.startsWith("connection_timeout")) {
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                Long l = Long.parseLong(split[1]);
                                mPrefs.setPrefRestConnTimeout(l.intValue());
                            } catch (NumberFormatException nfe) {
                                mPrefs.setPrefRestConnTimeout(ApexPrefs.DEFAULT_REST_CONN_TIMEOUT);
                            }
                        }
                    } else if (s.startsWith("sending_timeout")) {
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                Long l = Long.parseLong(split[1]);
                                mPrefs.setPrefRestTxTimeout(l.intValue());
                            } catch (NumberFormatException nfe) {
                                mPrefs.setPrefRestTxTimeout(ApexPrefs.DEFAULT_REST_TX_TIMEOUT);
                            }
                        }
                    } else if (s.startsWith("receiving_timeout")) {
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                Long l = Long.parseLong(split[1]);
                                mPrefs.setPrefRestRxTimeout(l.intValue());
                            } catch (NumberFormatException nfe) {
                                mPrefs.setPrefRestRxTimeout(ApexPrefs.DEFAULT_REST_RX_TIMEOUT);
                            }
                        }
                    } else if (s.startsWith("barcode_type")) {      // AM-49
                        // Note !
                        // This version support the parameter however is NOT used since the label
                        // is fixed to use the Code 128.
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                String barType = split[1];
                                if (barType.compareTo("128") == 0 || barType.compareTo("39") == 0) {
                                    mPrefs.setPrefBarcodeType(barType);
                                } else {
                                    mPrefs.setPrefBarcodeType(ApexPrefs.DEFAULT_BARCODE_TYPE);
                                }
                            } catch (NumberFormatException nfe) {
                                mPrefs.setPrefBarcodeType(ApexPrefs.DEFAULT_BARCODE_TYPE);
                            }
                        }
                    } else if (s.startsWith("ssl_selection")) {      // AM-50
                        split = s.split("=");
                        if (split.length > 1) {
                            try {
                                String sslType = split[1];
                                if (sslType.compareTo("1.1") != 0 || sslType.compareTo("1.2") != 0) {
                                    mPrefs.setSsl(sslType);
                                } else {
                                        mPrefs.setSsl(ApexPrefs.DEFAULT_SSL);
                                }
                            } catch (NumberFormatException nfe) {
                                mPrefs.setSsl(ApexPrefs.DEFAULT_SSL);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private void initUeHandler() {
        final ApexApplication self = this;
        mSysUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();

        mUeHandler = new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                if (e != null) {
                    if (e.getStackTrace() != null && e.getStackTrace().length > 0) {
                        mLogger.warn("Unhandled Exception: " + e.getStackTrace()[0].toString(), e);
                    } else {
                        mLogger.warn("Unhandled Exception", e);
                    }
                } else {
                    mLogger.warn("Unhandled Exception", (Throwable)null);
                }
                restClient.saveShutdown(self.mSessionId, false, self.mTechId);
                if (mSysUncaughtExceptionHandler != null) {
                    mSysUncaughtExceptionHandler.uncaughtException(t, e);
                }
            }
        };

        Thread.setDefaultUncaughtExceptionHandler(mUeHandler);
    }

    public void resetTimedEvents() {
        cancelTimedEvents();
        startTimedEvents();
    }

    public void startTimedEvents() {
        startUploadTimedEvent();
        startNotificationsEvent();
        mIsAlarmSet = true;
//        startGpsTimedEvent();
    }

    public static ApexApplication get() {
        return INSTANCE;
    }

    public void cancelTimedEvents() {
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent nav = new Intent("com.apex.mpad.FILTER_NAV");
        PendingIntent navPending = PendingIntent.getBroadcast(this, 54321, nav, 0);

        Intent notification = new Intent("com.apex.mpad.FILTER_NOTIFICATIONS");
        PendingIntent notificationPending = PendingIntent.getBroadcast(this, 67890, notification, 0);

        Intent upload = new Intent("com.apex.mpad.FILTER_UPLOAD");
        PendingIntent uploadPending = PendingIntent.getBroadcast(this, 12345, upload, 0);

        am.cancel(navPending);
        am.cancel(notificationPending);
        am.cancel(uploadPending);

        navPending.cancel();
        notificationPending.cancel();
        uploadPending.cancel();
    }

    /**
     * Send GPS data every second
     *
     * NOTE: Not used afai can tell
     */
    private void startGpsTimedEvent() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent("com.apex.mpad.FILTER_NAV");
        i.putExtra("fromAlarm", true);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 54321, i, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000, pendingIntent);
    }

    /**
     * Query notification data every X minutes
     *
     *
     */
    private void startNotificationsEvent() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent("com.apex.mpad.FILTER_NOTIFICATIONS");
        i.putExtra("fromAlarm", true);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 67890, i, PendingIntent.FLAG_CANCEL_CURRENT);
        long numMin = Long.parseLong(ApexPrefs.getInstance(getApplicationContext()).getNotificationInterval());
        alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), (numMin * 60000),
                pendingIntent);
    }

    /**
     * Every X min, try to send data
     */
    private void startUploadTimedEvent() {
        AlarmManager alarmManager=(AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent("com.apex.mpad.FILTER_UPLOAD");
        intent.putExtra("fromAlarm", true);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 12345, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        long numMin = Long.parseLong(ApexPrefs.getInstance(getApplicationContext()).getTimerInterval());
        alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), (numMin * 60000),
                pendingIntent);
    }

    private void initLists() {
        mTubeDrawFailReasons = new ArrayList<TubeDrawFailReasonInterface>();
        mFailureReasons = new ArrayList<FailureReasonInterface>();
        mDrawSites = new ArrayList<DrawSiteInterface>();
        mVisits = new ArrayList<VisitInterface>();
    }

    public int getRemainingVisitCount() {
        int result = 0;
        for (VisitInterface visit : mVisits) {
            if (!visit.isComplete()) {
                result++;
            }
        }
        return result;
    }

    public int getRemainingWorkOrderCount() {
        int result = 0;
        for (VisitInterface visit : mVisits) {
            if (!visit.isComplete()) {
                result += visit.getOpenWorkOrderCount();
            }
        }
        return result;
    }

    public int getSkippedPatientCount() {
        int result = 0;
        for (VisitInterface visit : mVisits) {
            result += visit.getNumSkippedPatients();
        }
        return result;
    }

    public int getRemainingPatientCount() {
        int result = 0;
        for (VisitInterface visit : mVisits) {
            if (!visit.isComplete() || visit.hasSkips()) {
                result += visit.getNumPatients();
            }
        }
        return result;
    }

    public boolean patientHasMultipleOpenWorkOrders() {
        boolean result = false;
        if (mCurrentVisit != null) {
            result = mCurrentVisit.patientHasMultipleOpenWorkOrders();
        }
        return result;
    }

    public void loadWorkOrders(List<WorkOrderInterface> workOrders) {
        int previousStopNumber = -1;
        String previousSpecimenNumber = "";

        // AM-38 - we reach this point because an initial login or a refresh is issued.
        // We erase the list before to process received orders.  If NO orders are present then
        // the list will remains empty.

        mLogger.debug("Loading from backend " + workOrders.size() + " workorders - local list erased");

        mVisits.clear();

        if (workOrders.size() > 0) {
            for (WorkOrderInterface wo : workOrders) {
                if (previousStopNumber != wo.getStopNumber()) {
                    previousStopNumber = wo.getStopNumber();

                    mCurrentVisit = new Visit();
                    mCurrentVisit.setStopNumber(wo.getStopNumber());
                    mCurrentVisit.setArea(wo.getDrawAreaUnit());
                    mCurrentVisit.setAreaType(wo.getDrawAreaType());
                    mVisits.add(mCurrentVisit);
                }
                if (!previousSpecimenNumber.equals(wo.getSpecimenNumber())) {
                    mCurrentVisit.addWorkOrder(wo);
                }
                previousSpecimenNumber = wo.getSpecimenNumber();
            }
        }

        if (mVisits.size() > 0) {
            mCurrentVisit = mVisits.get(0);
            mCurrentWorkOrder = null;
        }
    }

    public boolean advanceToNextWorkOrder() {
        boolean result;
        if (mCurrentVisit == null) {
            result = false;
        } else {
            mCurrentWorkOrder = mCurrentVisit.getNextOrder();
            if (mCurrentWorkOrder == null) {
                advanceCurrentVisit();
                mCurrentWorkOrder = mCurrentVisit.getNextOrder();
            }
            if (mCurrentVisit != null) {
                mPatientHasMoreWorkOrders = mCurrentVisit.patientHasMultipleOpenWorkOrders();
            }
            result = mCurrentWorkOrder != null;
        }
        return result;
    }

    public void failCurrentOrder(String failureReasonId, String failureComments) {
        for (WorkOrderInterface wo : mCurrentVisit.getCurrentPatientWorkOrders()) {
            if (wo.getStatus() != WorkOrder.WorkOrderStatus.Open) {
                continue;
            }

            CompletedWorkOrderInterface failedOrder = wo.getFailedOrder(failureReasonId, failureComments);
            wo.setStatus(WorkOrder.WorkOrderStatus.Failed);
            restClient.saveWorkOrder(mSessionId, failedOrder, mTechId);
        }
    }

    public boolean skipVisit() {
        mLogger.info("Skipping visit");
        if (mCurrentWorkOrder == null) {
            return false;
        }
        for (WorkOrderInterface wo : mCurrentVisit.getWorkOrders()) {
            if (wo.getStatus() == WorkOrder.WorkOrderStatus.Open) {
                wo.setStatus(WorkOrder.WorkOrderStatus.Skipped);
            }
        }
        advanceCurrentVisit();
        if (mCurrentVisit == null) {
            return false;
        }
        mCurrentWorkOrder = mCurrentVisit.getNextOrder();
        return mCurrentWorkOrder != null;
    }

    public void advanceCurrentVisit() {
        if (mCurrentVisit == null && mVisits != null && mVisits.size() > 0) {
            mCurrentVisit = mVisits.get(0);
            return;
        }
        if (mCurrentVisit == null) {
            return;
        }
        mPreviousVisit = mCurrentVisit;

        //TODO: Refresh Orders

        for (VisitInterface visit : mVisits) {
            if (!visit.isComplete() && visit.getStopNumber() != mPreviousVisit.getStopNumber()) {
                mCurrentVisit = visit;
                break;
            }
        }
    }

    public void loadFailureReasons(List<FailureReasonInterface> failureReasons) {
        if (failureReasons.size() > 0) {
            mFailureReasons.clear();
            for (FailureReasonInterface reason : failureReasons) {
                mFailureReasons.add(reason);
            }
        }
    }

    public void loadTubeDrawFailureReasons(List<TubeDrawFailReasonInterface> tubeDrawFailReasons) {
        if (tubeDrawFailReasons.size() > 0) {
            mTubeDrawFailReasons.clear();
            for (TubeDrawFailReasonInterface reason : tubeDrawFailReasons) {
                mTubeDrawFailReasons.add(reason);
            }
        }
    }

    public void loadDrawSites(List<DrawSiteInterface> drawSites) {
        if (drawSites.size() > 0) {
            mDrawSites.clear();
            for (DrawSiteInterface site : drawSites) {
                mDrawSites.add(site);
            }
        }
    }

    public boolean patientHasMoreWorkOrders() {
        return mPatientHasMoreWorkOrders;
    }

    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity activity) {
        this.mCurrentActivity = activity;
    }

    public double getOfficeLat() {
        return mOfficeLat;
    }
    public void setOfficeLat(double latitude) {
        this.mOfficeLat = latitude;
    }

    public double getOfficeLong() {
        return mOfficeLong;
    }
    public void setOfficeLong(double longitude) {
        this.mOfficeLong = longitude;
    }

    public String getSessionId() {
        mLogger.info("getSessionId, sessionId: " + mSessionId);
        return mSessionId;
    }

    public void setSessionId(String mSessionId) {
        mLogger.debug("setSessionId, current: " + this.mSessionId + " , new: " + mSessionId);
        this.mSessionId = mSessionId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getTechName() {
        return mTechName;
    }

    public void setTechName(String mTechName) {
        this.mTechName = mTechName;
    }

    public String getTechId() {
        return mTechId;
    }

    public void setTechId(String mTechId) {
        this.mTechId = mTechId;
    }

    public List<TubeDrawFailReasonInterface> getTubeDrawFailReasons() {
        return mTubeDrawFailReasons;
    }

    public void setTubeDrawFailReasons(List<TubeDrawFailReasonInterface> mTubeDrawFailReasons) {
        this.mTubeDrawFailReasons = mTubeDrawFailReasons;
    }

    public List<FailureReasonInterface> getFailureReasons() {
        return mFailureReasons;
    }

    public void setFailureReasons(List<FailureReasonInterface> mFailureReasons) {
        this.mFailureReasons = mFailureReasons;
    }

    public List<DrawSiteInterface> getDrawSites() {
        return mDrawSites;
    }

    public void setDrawSites(List<DrawSiteInterface> mDrawSites) {
        this.mDrawSites = mDrawSites;
    }

    public List<VisitInterface> getVisits() {
        return mVisits;
    }

    public void setVisits(List<VisitInterface> mVisits) {
        this.mVisits = mVisits;
    }

    public VisitInterface getCurrentVisit() {
        return mCurrentVisit;
    }

    public void setCurrentVisit(VisitInterface mCurrentVisit) {
        this.mCurrentVisit = mCurrentVisit;
    }

    public VisitInterface getPreviousVisit() {
        return mPreviousVisit;
    }

    public void setPreviousVisit(VisitInterface mPreviousVisit) {
        this.mPreviousVisit = mPreviousVisit;
    }

    public WorkOrderInterface getCurrentWorkOrder() {
        return mCurrentWorkOrder;
    }

    public void setCurrentWorkOrder(WorkOrderInterface mCurrentWorkOrder) {
        this.mCurrentWorkOrder = mCurrentWorkOrder;
    }

    public boolean isBluetoothEnabled(){
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        return btAdapter != null && btAdapter.isEnabled();
    }


    /*
    Clear Top launches the given activity, and clears all others in the stack
    Login activity then looksfor SHUTDOWN = true, and calls finish() to close.
    */
    public void shutdown(boolean showDialog, boolean isAtLab) {
        mLogger.info("Shutting Down, dialog: " + showDialog);
        setPrinting(false);
//        mFileUploaderService.uploadFiles();
        if (showDialog) {
            showShutdownConfirmDialog(isAtLab);
        } else {
            BaseActivity activity = (BaseActivity) getCurrentActivity();
            activity.showLoading();
            restClient.saveShutdown(this.mSessionId, isAtLab, this.mTechId);
            //startService(new Intent(this, FileUploaderService.class));
            mFileUploaderService.uploadFiles();
            activity.stopLoading();
            ExitActivity.exitApplication(mCurrentActivity);
        }
    }

    public void showShutdownConfirmDialog(final boolean isAtLab) {
        BaseActivity activity = (BaseActivity) getCurrentActivity();
        if (activity != null) {
            ApexDialog dialog = new ApexDialog() {
                @Override public void confirm() {
                    onShutdownConfirm(isAtLab);
                }

                @Override public void cancel() {
                    dismiss();
                }
            };
            dialog.setArgs("Shutdown", getResources().getString(R.string.shutdown_request), "Yes", "No");
            activity.showDialog(dialog);
        }
    }

    private void onShutdownConfirm(boolean isAtLab) {
        BaseActivity activity = (BaseActivity) getCurrentActivity();
        if (activity != null) {
            activity.showLoading();
            restClient.saveShutdown(this.mSessionId, isAtLab, this.mTechId);
            //startService(new Intent(this, FileUploaderService.class));
            mFileUploaderService.uploadFiles();
            activity.stopLoading();
            mSessionId = null;
            cancelTimedEvents();
            mLogger.info("Shutdown confirmed");
            isLoggedIn = false;
            ExitActivity.exitApplication(mCurrentActivity);
        }
    }

    public void showNewOrdersNotificationDialog() {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                this.dismiss();
            }
        };

        dialog.setArgs("New orders", getResources().getString(R.string.new_orders_notification));
        BaseActivity activity = (BaseActivity) getCurrentActivity();
        if (activity != null) {
            activity.showDialog(dialog);
        }
    }

    public void showConnectionErrorDialog() {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                this.dismiss();
            }
        };

        dialog.setArgs("No Connection", getResources().getString(R.string.no_network_warning));
        BaseActivity activity = (BaseActivity) getCurrentActivity();
        if (activity != null) {
             activity.showDialog(dialog);
        }
    }

    public void showPrinterErrorDialog(String message) {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                this.dismiss();
            }
        };
        dialog.setArgs("Print Error", message);
        BaseActivity activity = (BaseActivity) getCurrentActivity();
        if (activity != null) {
            activity.showDialog(dialog);
        }
    }

    private void goToLogin(BaseActivity activity){
        Intent i = new Intent(activity, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("SHUTDOWN", true);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        cancelTimedEvents();
        activity.startActivity(i);
    }

    public boolean hasMoreWorkOrdersNotification() {
        return mHasMoreWorkOrdersNotification;
    }
    public void setHasMoreWorkOrdersNotification(boolean hmwo) {
        this.mHasMoreWorkOrdersNotification = hmwo;
    }

    public boolean isPrinting() {
        return mIsPrinting;
    }
    public void setPrinting(boolean printing) {
        this.mIsPrinting = printing;
    }

    public int getRunningActivities() {
        return mRunningActivities;
    }

    public void setRunningActivities(int runningActivities) {
        mRunningActivities = runningActivities;
    }

    public boolean isAlarmSet() {
        return mIsAlarmSet;
    }

    public void setAlarmSet(boolean isAlarmSet) {
        mIsAlarmSet = isAlarmSet;
    }

    public boolean isUploading() {
        return mIsUploading;
    }

    public void setUploading(boolean isUploading) {
        mIsUploading = isUploading;
    }
}
