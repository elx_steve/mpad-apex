package com.apex.mpad.util;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by will on 4/11/15.
 */
public class ApexBus extends AbstractBus {

    public ApexBus(ThreadEnforcer enforcer) {
        super(new Bus(enforcer));
    }
}
