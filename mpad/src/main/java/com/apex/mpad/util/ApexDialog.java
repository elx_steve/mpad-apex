package com.apex.mpad.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.apex.mpad.R;

/**
 * Created by will on 5/11/15.
 */
public abstract class ApexDialog extends DialogFragment {

    public void setArgs(String message) {
        setArgs("", message);
    }
    public void setArgs(String title, String message) {
        setArgs(title, message, "", null);
    }
    public void setArgs(String title, String message, String posButtonText) {
        setArgs(title, message, posButtonText, null);
    }
    public void setArgs(String title, String message, String posButtonText, String negButtonText) {
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("pos", posButtonText);
        args.putString("neg", negButtonText);
        setArguments(args);
    }
    //must override confirm for ok button
    public abstract void confirm();

    //optional, override cancel to perform action on cancel
    public void cancel() {

    }

    public class DialogViewHolder {
        @InjectView(R.id.dialog_title) TextView title;
        @InjectView(R.id.dialog_message) TextView message;
        @InjectView(R.id.dialog_button_positive) Button positive;
        @InjectView(R.id.dialog_button_negative) Button negative;

        public DialogViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    @Override public Dialog onCreateDialog(Bundle bun) {
        super.onCreateDialog(bun);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MpadDialog);
        builder.setIcon(getResources().getDrawable(R.drawable.apex_logo_dialog));

        View titleView = getActivity().getLayoutInflater().inflate(R.layout.dialog_title, null);
        String title = getArguments().getString("title");

        if (!title.equals("")) {
            ((TextView)titleView.findViewById(R.id.dialog_title)).setText(title);
            builder.setCustomTitle(titleView);
        }
        builder.setMessage(getArguments().getString("message"));
        String pos = getArguments().getString("pos");
        builder.setPositiveButton(pos.equals("") ? "Ok" : pos, new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
                confirm();
            }
        });

        final String neg = getArguments().getString("neg");
        if (neg != null) {
            builder.setNegativeButton(neg.equals("") ? "Cancel" : neg, new DialogInterface.OnClickListener() {
                @Override public void onClick(DialogInterface dialog, int which) {
                    cancel();
                }
            });
        }

        AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override public void onShow(DialogInterface dialog) {
                final Button posButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                final Button negButton = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_NEGATIVE);
                if (posButton != null) {
                    posButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(
                            R.drawable.dialog_positive), null, null, null);
                    centerImageAndTextInButton(posButton);
                    posButton.setBackground(getResources().getDrawable(R.drawable.dialog_left_button));


                }
                if (neg != null && !neg.equals("") && negButton != null) {
                    negButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(
                            R.drawable.dialog_negative), null, null, null);
                    centerImageAndTextInButton(negButton);
                    negButton.setBackground(getResources().getDrawable(R.drawable.dialog_right_button));

                } else {
                    if (posButton != null) {
                        posButton.setBackground(getResources().getDrawable(R.drawable.dialog_single_button));
                    }
                }

            }
        });

        dialog.show();

        fixDialogWidth(dialog);
        return dialog;
    }

    private void fixDialogWidth(Dialog dialog) {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = (int) (width * .9);

        dialog.getWindow().setAttributes(params);
    }
    private void centerImageAndTextInButton(Button button) {
        Rect textBounds = new Rect();
        //Get text bounds
        CharSequence text = button.getText();
        if (text != null && text.length() > 0) {
            TextPaint textPaint = button.getPaint();
            textPaint.getTextBounds(text.toString(), 0, text.length(), textBounds);
        }
        //Set left drawable bounds
        Drawable leftDrawable = button.getCompoundDrawables()[0];
        if (leftDrawable != null) {
            Rect leftBounds = leftDrawable.copyBounds();
            int width = button.getWidth() - (button.getPaddingLeft() + button.getPaddingRight());
            int leftOffset = (width - (textBounds.width() + leftBounds.width()) - button.getCompoundDrawablePadding()) / 2 - button.getCompoundDrawablePadding();
            leftBounds.offset(leftOffset - 20, 0);
            leftDrawable.setBounds(leftBounds);
        }
    }

}
