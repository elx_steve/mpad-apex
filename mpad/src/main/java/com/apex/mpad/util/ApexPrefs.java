package com.apex.mpad.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.apex.mpad.R;
import com.apex.mpad.annotation.ForApplication;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;

import static android.content.SharedPreferences.Editor;

@Singleton
public class ApexPrefs {

    private static ApexPrefs mInstance = null;
    private SharedPreferences mPrefs;
    Context mAppContext;

    private static final String TAG = ApexPrefs.class.getName();
    private static final String PREF_SESSION_ID = TAG + ".PREF_SESSION_ID";
    private static final String PREF_USER_NAME = TAG + ".PREF_USER_NAME";
    private static final String PREF_TECH_NAME = TAG + ".PREF_TECH_NAME";
    private static final String PREF_TECH_ID = TAG + ".PREF_TECH_ID";
    private static final String PREF_OFFICE_LAT = TAG + ".PREF_OFFICE_LAT";
    private static final String PREF_OFFICE_LONG = TAG + ".PREF_OFFICE_LONG";
    private static final String PREF_MAC_ADDRESS = TAG + ".PREF_MAC_ADDRESS";
    private static final String PREF_PRINTER_NAME = TAG + ".PREF_PRINTER_NAME";
    private static final String PREF_DEFAULT_DOB = TAG + ".PREF_DEFAULT_DOB";

    public static final String PREF_API_ENDPOINT = TAG + ".PREF_API_ENDPOINT";
    public static final String PREF_LOCAL_DEV = TAG + ".PREF_LOCAL_DEV";
    public static final String PREF_TRANSMIT_INTERVAL = TAG + ".PREF_TRANSMIT_INTERVAL";
    public static final String PREF_NOTIFICATION_INTERVAL = TAG + ".PREF_NOTIFICATION_INTERVAL";

    public static final String PREF_REST_CONN_TIMEOUT = TAG + ".PREF_REST_CONN_TIMEOUT";// AM-48
    public static final String PREF_REST_RX_TIMEOUT = TAG + ".PREF_REST_RX_TIMEOUT";    // AM-48
    public static final String PREF_REST_TX_TIMEOUT = TAG + ".PREF_REST_TX_TIMEOUT";    // AM-48

    public static final String PREF_BARCODE_TYPE = TAG + ".PREF_BARCODE_TYPE";          // AM-49
    public static final String PREF_SSL = TAG + ".PREF_SSL";                            // AM-50

    private static String DEFAULT_ENDPOINT = "initializing...";
    public static String DEFAULT_DOB = "01/01/1930";
    public static String DEFAULT_UPLOAD_INTERVAL_MIN = "4";
    public static String DEFAULT_BARCODE_TYPE = "128";      // AM-49 - Default barcode type : Code 128
    public static String DEFAULT_SSL = "1.2";       // AM-50
                                                    // Options :
                                                    //  none --> no SSL
                                                    //  1.1  --> TLS 1.1
                                                    //  1.2  --> TLS 1.2  (default)

    private static final String DEFAULT_ADMIN_PASSWORD = "8p3x8dm1n!";
    public static final int DEFAULT_REST_CONN_TIMEOUT = 10;   // AM-48
    public static final int DEFAULT_REST_RX_TIMEOUT = 10;     // AM-48
    public static final int DEFAULT_REST_TX_TIMEOUT = 10;     // AM-48


    public static ApexPrefs getInstance(Context appContext) {
        if (mInstance == null) {
            mInstance = new ApexPrefs(appContext.getApplicationContext());
        }
        return mInstance;
    }


    @Inject public ApexPrefs(@ForApplication Context appContext) {
        mAppContext = appContext;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mAppContext);
        DEFAULT_ENDPOINT = mAppContext.getResources().getString(R.string.pref_default_endpoint);
    }

    private void putInt(String tag, int data) {
        Editor editor = mPrefs.edit();
        editor.putInt(tag, data);
        editor.commit();
    }

    private void putString(String tag, String data) {
        Editor editor = mPrefs.edit();
        editor.putString(tag, data);
        editor.commit();
    }

    private void putStringList(String tag, List<String> list) {
        Editor editor = mPrefs.edit();
        Gson gson = new Gson();
        String data = gson.toJson(list);
        editor.putString(tag, data);
        editor.commit();
    }

    private List<String> getStringList(String tag) {
        String data = mPrefs.getString(tag, null);
        if (data == null) {
            return null;
        } else {
            String[] array = new Gson().fromJson(data, String[].class);
            return Arrays.asList(array);
        }

    }

    private void putBoolean(String tag, boolean val) {
        Editor editor = mPrefs.edit();
        editor.putBoolean(tag, val);
        editor.commit();
    }

    public String getSessionId() {
        return mPrefs.getString(PREF_SESSION_ID, null);
    }
    public void setSessionId(String sessionId) {
        putString(PREF_SESSION_ID, sessionId);
    }

    public String getUserName() {
        return mPrefs.getString(PREF_USER_NAME, null);
    }
    public void setUserName(String userName) {
        putString(PREF_USER_NAME, userName);
    }

    public String getTechName() {
        return mPrefs.getString(PREF_TECH_NAME, null);
    }
    public void setTechName(String techName) {
        putString(PREF_TECH_NAME, null);
    }

    public String getTechId() {
        return mPrefs.getString(PREF_TECH_ID, null);
    }
    public void setTechId(String techId) {
        putString(PREF_TECH_ID, techId);
    }

    public String getOfficeLat() {
        return mPrefs.getString(PREF_OFFICE_LAT, null);
    }
    public void setOfficeLat(String officeLat) {
        putString(PREF_OFFICE_LAT, officeLat);
    }
    public String getOfficeLong() {
        return mPrefs.getString(PREF_OFFICE_LONG, null);
    }
    public void setOfficeLong(String officeLong) {
        putString(PREF_OFFICE_LONG, officeLong);
    }

    public void setMacAddress(String mac) {
        putString(PREF_MAC_ADDRESS, mac);
    }
    public String getMacAddress() {
        return mPrefs.getString(PREF_MAC_ADDRESS, null);
    }
    public void setPrinterName(String name) {
        putString(PREF_PRINTER_NAME, name);
    }
    public String getPrinterName() {
        return mPrefs.getString(PREF_PRINTER_NAME, null);
    }

    public String getTimerInterval() { return mPrefs.getString(PREF_TRANSMIT_INTERVAL, DEFAULT_UPLOAD_INTERVAL_MIN); }
    public void setTimerInterval(String interval) { putString(PREF_TRANSMIT_INTERVAL, interval); }

    public String getEndpoint() {
        return mPrefs.getString(PREF_API_ENDPOINT, DEFAULT_ENDPOINT);
    }
    public void setEndpoint(String ep) {
        putString(PREF_API_ENDPOINT, ep);
    }

    public boolean getLocalDev() {
        return mPrefs.getBoolean(PREF_LOCAL_DEV, false);
    }
    public void setLocalDev(boolean isDev) {
        putBoolean(PREF_LOCAL_DEV, isDev);
    }

    public String getHiddenAdminPassword() { return DEFAULT_ADMIN_PASSWORD; }

    public String getDefaultDob() {
        return mPrefs.getString(PREF_DEFAULT_DOB, DEFAULT_DOB);
    }
    public void setDefaultDob(String ep) {
        putString(PREF_DEFAULT_DOB, ep);
    }

    public String getNotificationInterval() { return mPrefs.getString(PREF_NOTIFICATION_INTERVAL, DEFAULT_UPLOAD_INTERVAL_MIN); }
    public void setNotificationInterval(String interval) { putString(PREF_NOTIFICATION_INTERVAL, interval); }

    // AM-48
    public int getPrefRestConnTimeout() { return mPrefs.getInt(PREF_REST_CONN_TIMEOUT, DEFAULT_REST_CONN_TIMEOUT); }
    public void setPrefRestConnTimeout(int timeout) { putInt(PREF_REST_CONN_TIMEOUT, timeout); }

    public int getPrefRestRxTimeout() { return mPrefs.getInt(PREF_REST_RX_TIMEOUT, DEFAULT_REST_RX_TIMEOUT); }
    public void setPrefRestRxTimeout(int timeout) { putInt(PREF_REST_RX_TIMEOUT, timeout); }

    public int getPrefRestTxTimeout() { return mPrefs.getInt(PREF_REST_TX_TIMEOUT, DEFAULT_REST_TX_TIMEOUT); }
    public void setPrefRestTxTimeout(int timeout) { putInt(PREF_REST_TX_TIMEOUT, timeout); }

    // AM-49
    public String getBarcodeType() { return mPrefs.getString(PREF_BARCODE_TYPE, DEFAULT_BARCODE_TYPE); }
    public void setPrefBarcodeType(String type) { putString(PREF_BARCODE_TYPE, type); }

    // AM-50
    public String getSsl() { return mPrefs.getString(PREF_SSL, DEFAULT_SSL); }
    public void setSsl(String type) { putString(PREF_SSL, type); }
}
