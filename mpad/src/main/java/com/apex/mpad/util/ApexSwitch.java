package com.apex.mpad.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Switch;

/**
 * Created by John on 5/17/2016.
 */
public class ApexSwitch extends Switch {

    private String parentKey;

    public ApexSwitch(Context context) {
        super(context);
    }

    public ApexSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ApexSwitch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setParentKey(String id) {
        this.parentKey = id;
    }

    public String getParentKey() {
        return parentKey;
    }
}
