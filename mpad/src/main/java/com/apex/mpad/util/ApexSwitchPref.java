package com.apex.mpad.util;

import android.content.Context;
import android.preference.SwitchPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Switch;

import com.apex.mpad.R;

public class ApexSwitchPref extends SwitchPreference {
    private ApexSwitch prefSwitch;
    private String textOn;
    private String textOff;

    private OnSwitchPrefCreatedListener listener;

    public ApexSwitchPref(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayoutResource(R.layout.layout_pref_switch);
    }
    @Override protected void onBindView(View view) {
        super.onBindView(view);
        prefSwitch = (ApexSwitch) view.findViewById(R.id.switch_item);
        prefSwitch.setTextOn(textOn);
        prefSwitch.setTextOff(textOff);
        prefSwitch.setParentKey(this.getKey());
        listener.onSwitchPrefCreated(this);
    }
    public void setSwitchText(String textOn, String textOff) {
        this.textOn = textOn;
        this.textOff = textOff;
        notifyChanged();
    }
    public Switch getSwitch() {
        return prefSwitch;
    }
    public void setListener(OnSwitchPrefCreatedListener listener) {
        this.listener = listener;
    }
    public interface OnSwitchPrefCreatedListener {
        void onSwitchPrefCreated(ApexSwitchPref switchPref);
    }

}
