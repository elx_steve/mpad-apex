package com.apex.mpad.util;

/**
 * Created by whansen on 4/11/15.
 * Holder class for String Constants for @Named annotations
 */
public final class Constants {
    public static final String BUS_REST = "bus_rest";
    public static final String BUS_UI = "bus_ui";
}
