package com.apex.mpad.util;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by will on 5/14/15.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    DatePickerResult mListener;

    public static final String BUNDLE_ARG_DEFAULT_DATE = "def";

    public interface DatePickerResult {
        public void onDateSet(DatePicker view, int year, int month, int day);
    }

    @Override public Dialog onCreateDialog(Bundle bun) {
        Date defaultDate = new Date();
        if (bun == null) {
            bun = this.getArguments();
        }
        if (bun != null) {
            if (bun.getLong(BUNDLE_ARG_DEFAULT_DATE) > Long.MIN_VALUE) {
                defaultDate = new Date(bun.getLong(BUNDLE_ARG_DEFAULT_DATE));
            }
        }
        final Calendar c = new GregorianCalendar();
        c.setTime(defaultDate);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        mDialog.getDatePicker().setMaxDate((new Date()).getTime());

        return mDialog;
    }
    @Override public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (mListener != null) {
            mListener.onDateSet(view, year, monthOfYear, dayOfMonth);
        }
    }
    public void setListener(DatePickerResult listener) {
        this.mListener = listener;
    }


}
