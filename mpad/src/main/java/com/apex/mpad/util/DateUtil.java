package com.apex.mpad.util;

import org.joda.time.LocalDateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class DateUtil {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");


    public static Date randomDate(int startYear, int endYear) {

        GregorianCalendar gc = new GregorianCalendar();

        int year = randBetween(startYear, endYear);

        gc.set(gc.YEAR, year);

        int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));

        gc.set(gc.DAY_OF_YEAR, dayOfYear);

        return gc.getTime();

    }

    private static int randBetween(int start, int end) {
        Random generator = new Random((new Date()).getTime());
        int rnd = generator.nextInt((end - start) + 1) + start;
        return rnd;
    }

    public static Date getDate(String dt, Date defaultDt) {
        Date ddob = null;
        try {
            ddob = dateFormat.parse(dt);
        } catch(ParseException pe) {
            ddob = defaultDt;
        }
        return ddob;
    }

    public static int getYear(Date dt) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(dt);
        return gc.get(gc.YEAR);
    }

    public static String getDateString(Date dt) {
        return dateFormat.format(dt);
    }


    /**
     * Gets the current date and time using JOdatime
     * @return default string format
     */
    public static String getTimestamp() {
        String out = null;
        LocalDateTime result = LocalDateTime.now();
        out = result.toDateTime().toString();
        return out;
    }

    /**
     * DOB's may be formatted differently, so force to dates
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static boolean areDateStringsEqual(String dateString1, String dateString2) {
        boolean out = false;
        Date date1 = getDate(dateString1, null);
        Date date2 = getDate(dateString2, null);

        if (date1 != null && date2 != null)
            out = getDateString(date1).equals(getDateString(date2));

        return out;
    }
}