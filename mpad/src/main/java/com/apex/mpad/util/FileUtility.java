package com.apex.mpad.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;

/**
 * Created by will on 4/21/15.
 */
public class FileUtility {

    @Inject @ForApplication Context appContext;

    public static final DateTimeFormatter fileDate = DateTimeFormat.forPattern("yyyy-MM-dd-hh.mm.ss.SSSS");
    private static final Logger mLogger = LoggerFactory.getLogger(FileUtility.class);
    private static final String TAG = "FileUtility";

    public FileUtility() {
        Injector.INSTANCE.inject(this);
    }

    public boolean prepareDirectory(String file, boolean deleteExisting) {
        if (file == null || file.isEmpty()) {
            return false;
        }

        File f = new File(file);
        File dir = f.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }

        if (!deleteExisting) {
            return true;
        }

        if (f.exists()) {
            f.delete();
        }
        return true;
    }

    public void saveDirectToFile(String fullFileName, String requestJson) {
        saveAsText(fullFileName, requestJson);
    }

    public void saveToFile(String fullPath, String requestJson) {
        saveAsText(fullPath, requestJson);
    }

    public void saveToFile(String folderPath, String fileName, String requestJson) {
        File f = new File(folderPath, fileName);
        saveAsText(f.getAbsolutePath(), requestJson);
    }

    public String textFromFilePath(String fullPath, boolean delete) {
        File f = new File(fullPath);
        return textFromFile(f, delete);
    }

    public String textFromFile(String filename, boolean delete) {
        File f = new File(Environment.getExternalStorageDirectory(), filename);
        return textFromFile(f, delete);
    }

    private String textFromFile(File f, boolean delete) {
        String result = "";
        if (!f.exists()) {
            return result;
        } else {
            try {
                result = FileUtils.readFileToString(f, "UTF-8");
                if (delete) {
                    f.delete();
                }
            } catch (Exception e) {
                return "";
            }
        }
        return result;
    }

    public File[] getFiles(String path, final String regex, Boolean log) {
        File f;
        final Boolean innerLog = log;
        //String dir = Environment.getExternalStorageDirectory().toString() + path;
        if (path != null && !path.trim().isEmpty())
            f = new File(Environment.getExternalStorageDirectory().toString(), path);
        else
            f = new File(Environment.getExternalStorageDirectory().toString());

        return f.listFiles(new FilenameFilter() {
            @Override public boolean accept(File dir, String filename) {
//                Log.d(TAG, "matches regex " + regex + " - " + filename.matches(regex));
                if (innerLog)
                    mLogger.debug("matches regex " + regex + " : {}", filename.matches(regex));
                return filename.matches(regex);
            }
        });
    }

    private void saveAsText(String filename, String requestJson) {
        try {
            File f = new File(filename);
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(requestJson.getBytes());
            fos.close();
        } catch (Exception e) {
            mLogger.warn("Text Save Error: {}", e.getMessage());
        }
    }

    public static File getFile(String path) {
        try {
            File f = new File(path);
            return f;
        } catch (Exception e) {
            return null;
        }
    }

    public static File getFileOrCreate(String path) {
        try {
            File f = new File(path);
            if (!f.exists()) {
                if (!f.createNewFile()) {
                    mLogger.warn("Unable to create file");
                }
            }
            return getFile(path);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean deleteFile(String path) {
        boolean out = true;
        try {
            File f = getFile(path);
            if (f != null && f.exists()) {
                f.delete();
            }
        } catch(Exception e) {
            out = false;
        }
        return out;
    }

    public static boolean fileExists(String path) {
        boolean out = false;
        try {
            File f = getFile(path);
            out = (f != null && f.exists());
        } catch(Exception e) {
            out = false;
        }
        return out;
    }

    public static boolean fileExistsNonZero(String path) {
        boolean out = false;
        try {
            File f = getFile(path);
            out = (f != null && f.exists() && f.length() > 0);
        } catch(Exception e) {
            out = false;
        }
        return out;
    }

    public static String getApplicationDir(Context appContext) {
        PackageManager m = appContext.getPackageManager();
        String s = appContext.getPackageName();
        try {
            PackageInfo p = m.getPackageInfo(s, 0);
            s = p.applicationInfo.dataDir;
        } catch (PackageManager.NameNotFoundException e) {
            s = null;
        }
        return s;
    }

}
