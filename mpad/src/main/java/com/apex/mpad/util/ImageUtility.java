package com.apex.mpad.util;

import com.apex.mpad.injection.Injector;
import com.apex.mpad.service.rest.message.SaveAudioRequestMessage;
import com.apex.mpad.service.rest.message.SaveImageRequestMessage;
import com.apex.mpad.util.base.BaseActivity;
import com.apex.mpad.util.base.BaseAudioRecordingActivity;
import com.apex.mpad.util.base.BaseMediaRecordingActivity;
import com.apex.mpad.util.base.BaseRecordingActivity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import android.util.Base64;

import javax.inject.Inject;
import java.io.*;

/**
 * Created by will on 4/23/15.
 */
public class ImageUtility {

    @Inject FileUtility mFileUtility;
    @Inject ObjectMapper mMapper;
    private static final Logger mLogger = LoggerFactory.getLogger(ImageUtility.class);

    public ImageUtility() {
        Injector.INSTANCE.inject(this);
    }

    /**
     * Saves a "bitmap" image as byte array to a JSON message file
     *
     * @param sessionId
     * @param imageType
     * @param fileData
     * @param newPath
     * @param imageRefId
     * @param notes
     * @param techId
     * @return
     */
    private boolean saveFileForUpload(String sessionId, String imageType, byte[] fileData, String newPath, String imageRefId, String notes, String techId){
        SaveImageRequestMessage message = new SaveImageRequestMessage();
        message.setSessionId(sessionId);
        message.setImageType(imageType);
        message.setNotes(notes);
        message.setImageReferenceId(imageRefId);
        message.setImageData(fileData);
        message.setTechId(techId);

        File file = new File(newPath);
        if (file.exists()) {
            file.delete();
        }
        String output = "";
        try {
            output = mMapper.writeValueAsString(message);
        } catch (Exception e) {
            mLogger.warn("JSON Error: {}", e.getMessage());
        }
        mFileUtility.saveDirectToFile(newPath, output);
        return true;
    }

    /**
     * Saves a "bitmap" image as byte array to a JSON message file
     *
     * @param sessionId
     * @param imageType
     * @param fileData
     * @param newPath
     * @param imageRefId
     * @param notes
     * @param techId
     * @return
     */
    private boolean saveFileForUpload(String sessionId, String imageType, String fileData, String newPath, String imageRefId, String notes, String techId){
        SaveAudioRequestMessage message = new SaveAudioRequestMessage();
        message.setSessionId(sessionId);
        message.setImageType(imageType);
        message.setNotes(notes);
        message.setImageReferenceId(imageRefId);
        message.setImageData(fileData);
        message.setTechId(techId);

        File file = new File(newPath);
        if (file.exists()) {
            file.delete();
        }
        String output = "";
        try {
            output = mMapper.writeValueAsString(message);
        } catch (Exception e) {
            mLogger.warn("JSON Error: {}", e.getMessage());
        }
        mFileUtility.saveDirectToFile(newPath, output);
        return true;
    }

    public String getFullAudioPath(String origPath) {
        String res = origPath.replace(BaseMediaRecordingActivity.AUDIO_RECORDER_FILE_EXT_MP4,".mp4msg");
        if (!res.endsWith("mp4msg")) {
            res = origPath.replace(SoundUtil.AUDIO_RECORDER_FILE_EXT_WAV,".mp4msg");
        }
        return res;
    }

    public boolean saveImageForUpload(String sessionId, String imageType, String imagePath, String imageRefId, String notes, String techId) {
        byte[] fileData = convertImageToByteArray(imagePath);
        String newPath = imagePath.replace(BaseActivity.IMAGE_FORMAT_EXT,".imgmsg");
        return saveFileForUpload(sessionId, imageType, fileData, newPath, imageRefId, notes, techId);
    }

    public boolean saveAudioForUpload(String sessionId, String imageType, String imagePath, String imageRefId, String notes, String techId) {
        byte[] fileData = convertImageToByteArray(imagePath);
        String newPath = getFullAudioPath(imagePath);
        String base64Audio = Base64.encodeToString(fileData, Base64.DEFAULT);
        return saveFileForUpload(sessionId, imageType, base64Audio, newPath, imageRefId, notes, techId);
    }

    public byte[] convertImageToByteArray(String imagePath) {
        InputStream in = null;
        byte[] fileData = new byte[0];
        try {
            in = new FileInputStream(imagePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (in != null) {
            try {
                fileData = IOUtils.toByteArray(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileData;
    }
}
