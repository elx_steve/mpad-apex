package com.apex.mpad.util;

import android.content.Context;
import android.location.LocationManager;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.injection.NavigationModule;
import dagger.ObjectGraph;

import javax.inject.Inject;

/**
 * Created by will on 4/15/15.
 */
public class NavigationUpdateRunnable implements Runnable {

    @Inject @ForApplication Context appContext;
    @Inject @ForApplication LocationManager locationManager;

    public NavigationUpdateRunnable() {
        ObjectGraph activityGraph = Injector.INSTANCE.add(new NavigationModule());
        activityGraph.inject(this);
    }

    @Override public void run() {

    }
}
