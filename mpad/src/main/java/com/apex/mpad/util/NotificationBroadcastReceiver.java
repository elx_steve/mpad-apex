package com.apex.mpad.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.apex.mpad.injection.Injector;
import com.apex.mpad.service.FileUploaderService;
import com.apex.mpad.service.NotificationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by will on 8/3/15.
 */
public class NotificationBroadcastReceiver extends BroadcastReceiver {

    private static final Logger mLogger = LoggerFactory.getLogger(NotificationBroadcastReceiver.class);
    private static Handler mHandler;

    public NotificationBroadcastReceiver() {
        Injector.INSTANCE.inject(this);
        mHandler = new Handler();
    }
    @Override public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getBooleanExtra("fromAlarm", false)) {
            mLogger.debug("Notification Intent Received");
            Intent i = new Intent(context, NotificationService.class);
            context.startService(i);
        }
    }
}
