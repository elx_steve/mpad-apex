package com.apex.mpad.util;

/**
 * Created by stefanobodini on 1/17/18.
 */

import android.os.Build;
import android.util.Log;

import com.squareup.okhttp.ConnectionSpec;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.TlsVersion;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

public class SSLUtil {

    public static OkHttpClient enableTls12(OkHttpClient client) {
        Log.d("enableTls12", "build version SDK : " + Build.VERSION.SDK_INT);
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                client.setSslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.setConnectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("enableTls12", "Error while setting TLS 1.2 : " +  exc);
            }
        }

        return client;
    }

    public static OkHttpClient enableTls11(OkHttpClient client) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.1");
                sc.init(null, null, null);
                client.setSslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                                        .tlsVersions(TlsVersion.TLS_1_1)
                                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.setConnectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.1", exc);
            }
        }
        return client;
    }

    public static OkHttpClient enableNoTls(OkHttpClient client) {
        return client;
    }
}

