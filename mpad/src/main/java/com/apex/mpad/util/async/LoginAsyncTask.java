package com.apex.mpad.util.async;

import android.os.AsyncTask;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.model.iface.DrawSiteInterface;
import com.apex.mpad.model.iface.FailureReasonInterface;
import com.apex.mpad.model.iface.TubeDrawFailReasonInterface;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.service.rest.iface.RestClientInterface;
import com.apex.mpad.service.rest.message.LoginResponseMessage;
import com.apex.mpad.util.ApexApplication;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by will on 5/14/15.
 */
public class LoginAsyncTask extends AsyncTask<Void, Void, Boolean> {

    @Inject @ForApplication
    ApexApplication mApp;
    @Inject RestClientInterface mRestClient;

    private String mUsername;
    private String mPassword;
    private String mSessionId;

    private LoginResponseMessage mLoginMessage;

    private LoginComplete mLoginComplete;

    public interface LoginComplete {
        public void onLoginComplete(boolean isSuccess, LoginResponseMessage message);
    }

    public LoginAsyncTask(String username, String password, LoginComplete complete) {
        Injector.INSTANCE.inject(this);
        this.mUsername = username;
        this.mPassword = password;
        this.mLoginComplete = complete;
    }

    @Override protected Boolean doInBackground(Void... params) {
        mLoginMessage = mRestClient.authenticate(mUsername, mPassword);
        if (mLoginMessage != null) {
            if (mLoginMessage.getError() != null && !mLoginMessage.getError().getDescription().isEmpty()) {
                return false;
            }

            if (mLoginMessage.getOfficeLocation() != null) {
                mApp.setOfficeLat(mLoginMessage.getOfficeLocation().getLatitude());
                mApp.setOfficeLong(mLoginMessage.getOfficeLocation().getLongitude());
            }

            mSessionId = mLoginMessage.getSessionId();
            mApp.setUsername(mUsername);
            mApp.setSessionId(mSessionId);
            mApp.setTechName(mLoginMessage.getTechName());
            mApp.setTechId(mLoginMessage.getTechId());

            mApp.setHasMoreWorkOrdersNotification(false);

            List<WorkOrderInterface> workOrders = mRestClient.getWorkOrders(mSessionId);
            if (workOrders != null) {
                mApp.loadWorkOrders(workOrders);
            }

            List<FailureReasonInterface> failureReasons = mRestClient.getFailureReasons(mSessionId);
            if (failureReasons != null) {
                mApp.loadFailureReasons(failureReasons);
            }

            List<TubeDrawFailReasonInterface> tubeDrawFailReasons = mRestClient.getTubeDrawFailureReasons(mSessionId);
            if (tubeDrawFailReasons != null) {
                mApp.loadTubeDrawFailureReasons(tubeDrawFailReasons);
            }

            List<DrawSiteInterface> drawSites = mRestClient.getDrawSites(mSessionId);
            if (drawSites != null) {
                mApp.loadDrawSites(drawSites);
            }
            return true;
        }
        return false;
    }

    @Override protected void onPostExecute(Boolean result) {
        mLoginComplete.onLoginComplete(result, mLoginMessage);
    }
}
