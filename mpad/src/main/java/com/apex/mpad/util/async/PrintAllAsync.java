package com.apex.mpad.util.async;

import android.os.AsyncTask;
import android.widget.Button;
import com.apex.mpad.activity.SpecimenRequirementListActivity;
import com.apex.mpad.presenter.iface.SpecimenRequirementListItemPresenterInterface;
import com.apex.mpad.util.ApexApplication;

import java.util.HashMap;
import java.util.List;

/**
 * Created by will on 5/15/15.
 */
public class PrintAllAsync extends AsyncTask<Void, Void, Void> {

    private SpecimenRequirementListActivity mActivity;
    private List<SpecimenRequirementListItemPresenterInterface> mSpecimens;
    private HashMap<SpecimenRequirementListItemPresenterInterface, Button> mButtons = new HashMap<SpecimenRequirementListItemPresenterInterface, Button>();

    public PrintAllAsync(SpecimenRequirementListActivity activity, List<SpecimenRequirementListItemPresenterInterface> specimens) {
        this.mActivity = activity;
        this.mSpecimens = specimens;
    }

    @Override protected void onPreExecute() {
        ((ApexApplication) mActivity.getApplication()).setPrinting(true);
        mActivity.showLoading();
    }
    @Override protected Void doInBackground(Void... params) {
        boolean allPrinted = true;
        SpecimenRequirementListItemPresenterInterface specimen = null;
        for (int i = 0; i < mSpecimens.size(); i++) {
            specimen = mSpecimens.get(i);
            mButtons.put(specimen, specimen.getButton());
            specimen.print();
            if (!specimen.hasPrinted()) allPrinted = false;
        }
        //use the last specimen to disconnect from the printer
        if (allPrinted && specimen != null) specimen.disconnectPrinter();
        return null;
    }

    @Override protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        mActivity.stopLoading();
        mActivity.updateList();
        ((ApexApplication) mActivity.getApplication()).setPrinting(false);

//        for (final Map.Entry entry : mButtons.entrySet()) {
//            ((Button)entry.getValue()).setText(((SpecimenRequirementListItemPresenterInterface) entry.getKey()).getButtonText());
//        }
    }
}
