package com.apex.mpad.util.async;

import android.os.AsyncTask;
import android.widget.Button;
import com.apex.mpad.presenter.iface.SpecimenRequirementListItemPresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.base.BaseActivity;

/**
 * Created by will on 5/15/15.
 */
public class PrintAsync extends AsyncTask<Void, Void, Void> {

    private BaseActivity mActivity;
    private SpecimenRequirementListItemPresenterInterface mSpecimen;
    private Button mButton;

    public PrintAsync(BaseActivity activity, SpecimenRequirementListItemPresenterInterface specimen, Button button) {
        this.mActivity = activity;
        this.mSpecimen = specimen;
        this.mButton = button;
    }

    @Override protected void onPreExecute() {
        ((ApexApplication) mActivity.getApplication()).setPrinting(true);
        mActivity.showLoading();
    }
    @Override protected Void doInBackground(Void... params) {
        mSpecimen.print();
        mButton.post(new Runnable() {
            @Override public void run() {
                mButton.setText(mSpecimen.getButtonText());
            }
        });
        return null;
    }

    @Override protected void onPostExecute(Void result) {
        mActivity.stopLoading();
        ((ApexApplication) mActivity.getApplication()).setPrinting(false);
    }
}
