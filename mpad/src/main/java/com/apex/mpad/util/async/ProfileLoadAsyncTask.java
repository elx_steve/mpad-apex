package com.apex.mpad.util.async;

import android.os.AsyncTask;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.util.ApexApplication;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.ProfileConfig;
import com.symbol.emdk.ProfileManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by will on 5/8/15.
 */
public class ProfileLoadAsyncTask extends AsyncTask<Void, Void, Void> implements EMDKManager.EMDKListener {

    public static final String DEFAULT_PROFILE = "Profile0 (default)";
    public static final String SCAN_PROFILE = "ApexEnable";
    private static final Logger mLogger = LoggerFactory.getLogger(ProfileLoadAsyncTask.class);


    @Inject @ForApplication
    ApexApplication mApp;
    private ProfileManager mProfileManager;
    private EMDKResults mEMDKResults;
    private String[] mModifyData;
    private EMDKManager mEMDKManager;

    public ProfileLoadAsyncTask() {
        Injector.INSTANCE.inject(this);
        EMDKResults result = EMDKManager.getEMDKManager(mApp, this);
    }

    @Override protected Void doInBackground(Void... params) {
        return null;
    }

    @Override public void onOpened(EMDKManager emdkManager) {
        mEMDKManager = emdkManager;
        mProfileManager = (ProfileManager) mEMDKManager.getInstance(EMDKManager.FEATURE_TYPE.PROFILE);
        if (mProfileManager != null) {
            mModifyData = new String[1];

            ProfileConfig profileConfig = new ProfileConfig();
            mEMDKResults = mProfileManager.processProfile(DEFAULT_PROFILE, ProfileManager.PROFILE_FLAG.GET, profileConfig);
            if(mEMDKResults.statusCode == EMDKResults.STATUS_CODE.FAILURE) {
                mLogger.info("failed to acquire default profile");
            }
            profileConfig.dataCapture.barcode.scanner_input_enabled = ProfileConfig.ENABLED_STATE.FALSE;
            mEMDKResults = mProfileManager.processProfile(DEFAULT_PROFILE, ProfileManager.PROFILE_FLAG.SET, profileConfig);

            if(mEMDKResults.statusCode == EMDKResults.STATUS_CODE.FAILURE) {
                mLogger.info("failed to set default profile");
            }
            mEMDKResults = mProfileManager.processProfile(SCAN_PROFILE, ProfileManager.PROFILE_FLAG.SET, mModifyData);
            if(mEMDKResults.statusCode == EMDKResults.STATUS_CODE.FAILURE) {
                mLogger.info("failed to acquire scan profile");
            }
        }
    }
    @Override public void onClosed() {
        mEMDKManager = null;
        mProfileManager = null;
    }
}
