package com.apex.mpad.util.base;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTouch;
import butterknife.Optional;
import com.apex.mpad.R;
import com.apex.mpad.activity.HelpActivity;
import com.apex.mpad.activity.HomeActivity;
import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.injection.Injector;
import com.apex.mpad.injection.PresenterModule;
import com.apex.mpad.model.iface.WorkOrderInterface;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexDialog;
import com.apex.mpad.util.ApexScrollView;

import dagger.ObjectGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by will on 4/9/15.
 */
public class BaseActivity extends Activity implements PopupMenu.OnMenuItemClickListener {

    protected static final String TEXT_MULTIPLE = "Multiple Patients";
    protected static final String TEXT_NA = "N/A";
    protected static final String TEXT_EMPTY = "";
    public static final String IMAGE_FORMAT_EXT = ".png";

    @Optional @InjectView(R.id.button_back) Button backButton;
    @Optional @InjectView(R.id.button_left) protected Button leftButton;
    @Optional @InjectView(R.id.button_right) protected Button rightButton;
    @Optional @InjectView(R.id.button_help)  protected Button helpButton;
    @Optional @InjectView(R.id.progress_spinner) ProgressBar progress;

    @Optional @InjectView(R.id.header_patient_name) TextView patientName;
    @Optional @InjectView(R.id.header_dob_label) protected TextView dobLbl;
    @Optional @InjectView(R.id.header_dob) protected TextView header_dob;
    @Optional @InjectView(R.id.header_sex) TextView sex;

    @Optional @InjectView(R.id.scroll)
    ApexScrollView scroll;

    @Inject @ForApplication Context mAppContext;

    protected Toast problemToast;

    protected MenuItem refreshIndicator;

    private ApexApplication mApp;
    protected PopupMenu mPopupMenu;
    protected Thread.UncaughtExceptionHandler mUeHandler;
    protected Thread.UncaughtExceptionHandler mSysHandler;

    private static final Logger mLogger = LoggerFactory.getLogger(BaseActivity.class);

    @Inject public BaseActivity() {}

    @Override protected void onCreate(Bundle bun) {
        super.onCreate(bun);
        ButterKnife.inject(this);
        ObjectGraph activityGraph = Injector.INSTANCE.add(new PresenterModule(this));
        activityGraph.inject(this);
        mApp = (ApexApplication) mAppContext;
        setOrientation();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        initUeHandler();
    }

    protected void setOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    protected void checkForUpload() {
        while(mApp.isUploading()) {
            this.showLoading();
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
        this.stopLoading();
    }

    private void initUeHandler() {
        mSysHandler = Thread.getDefaultUncaughtExceptionHandler();
        mUeHandler = new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                if (e != null) {
                    if (e.getStackTrace() != null && e.getStackTrace().length > 0) {
                        mLogger.warn("Unhandled Exception: " + e.getStackTrace()[0].toString(), e);
                    } else {
                        mLogger.warn("Unhandled Exception", e);
                    }
                } else {
                    mLogger.warn("Unhandled Exception");
                }
                mSysHandler.uncaughtException(t, e);
//                showUncaughtExceptionDialog();
            }
        };

        Thread.setDefaultUncaughtExceptionHandler(mUeHandler);
    }

    protected void initHeader() {
        if (patientName != null && header_dob != null && sex != null) {
            BasePresenterInterface presenter = getPresenter();
            if (presenter != null) {
                WorkOrderInterface wo = presenter.getWorkOrder();
                if (wo != null) {
                    patientName.setText(wo.getPatientName());
                    setDOB(wo);
                    sex.setText(wo.getGender());
                } else {
                    showNoDataDialog();
                }
            }
        }
    }

    protected void setDOB(WorkOrderInterface wo) {
        dobLbl.setVisibility(View.VISIBLE);
        String dobStr = wo.getDob();
        if (wo.getNewDOB() != null) {
            dobStr = wo.getNewDOB();
        }
        header_dob.setText(dobStr);
    }

    /**
     * This creates the X close button in the header
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        refreshIndicator = menu.findItem(R.id.action_refresh);
        if (refreshIndicator != null) {
            refreshIndicator.setVisible(false);
            handleRefreshIndicator();
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void handleRefreshIndicator() {
        if (refreshIndicator != null) {
            refreshIndicator.setVisible(mApp.hasMoreWorkOrdersNotification());
        }
    }

    /**
     * This is the handler for the X close button
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_refresh:
                // action refresh button - it can come here only if a notification was receievd and
                // the button make visible - notify the user about the notification received
                mApp.showNewOrdersNotificationDialog();
                return true;
            case R.id.action_close:
                mApp.shutdown(true, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void handleRefreshButton() {}


    @Override
    protected void onResume() {
        super.onResume();
        String localClass = this.getLocalClassName();
        String[] split = null;
        String className = "";
        if (localClass != null) {
            split = localClass.split("\\.");
        }
        if (split.length > 1) {
            className = split[1];
        }
        mLogger.info("Starting Activity: " + className);
        mApp.setCurrentActivity(this);
        if (mApp.getRunningActivities() == 0) {
            //mApp.startTimedEvents();
        }
        mApp.setRunningActivities(mApp.getRunningActivities() + 1);
    }

    @Override
    protected void onPause() {
        clearReferences();
        mApp.setPrinting(false);

        super.onPause();
    }

    @Override protected void onStop() {
        int acts = mApp.getRunningActivities();
        mApp.setRunningActivities(acts - 1);
        if (mApp.getRunningActivities() == 0) {
            //mApp.setAlarmSet(false);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    @Override public void onBackPressed() {
    }

    protected void focusOnView(final View v) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {Thread.sleep(100);} catch (InterruptedException e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        scroll.scrollTo(0, v.getBottom() + 5);
                    }
                });
            }
        }).start();
    }

    protected void focusOnTop(final View v) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {Thread.sleep(500);} catch (InterruptedException e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (scroll != null)
                        scroll.scrollTo(0, v.getTop());
                    }
                });
            }
        }).start();
    }

//    protected void focusOnView(final View v){
//        if (scroll != null) {
//            scroll.post(new Runnable() {
//                @Override public void run() {
//                    scroll.smoothScrollTo(0, scroll.getBottom());
//                }
//            });
//        }
//    }

    private void clearReferences(){
        Activity currActivity = mApp.getCurrentActivity();
        if (currActivity == null) return;
        if (currActivity.equals(this))
            mApp.setCurrentActivity(null);
    }

    protected BasePresenterInterface getPresenter() {return null;}

    protected void initPopupMenu(int menuResource) {
        // AM-30 workaround
        if (menuResource == R.menu.specimen_requirement && mPopupMenu != null)
        {
            mPopupMenu.dismiss();
            mPopupMenu = null;
        }

        if (mPopupMenu == null && leftButton != null) {
            mPopupMenu = new PopupMenu(this, leftButton);
        }
        if (mPopupMenu != null) {
            mPopupMenu.getMenuInflater().inflate(menuResource, mPopupMenu.getMenu());
            mPopupMenu.setOnMenuItemClickListener(this);
        }
    }

    protected void setupButtonBar(String leftLabel, String rightLabel, boolean hasBackButton) {
        if (hasBackButton) {
            if (backButton != null) {
                backButton.setVisibility(View.VISIBLE);
            }
        }
        if (leftLabel != null && leftButton != null) {
            leftButton.setVisibility(View.VISIBLE);
            leftButton.setText(leftLabel);
        }
        else {
            leftButton.setVisibility(View.INVISIBLE);
        }

        if (rightLabel != null && rightButton != null) {
            rightButton.setVisibility(View.VISIBLE);
            rightButton.setText(rightLabel);
        }
        else {
            rightButton.setVisibility(View.INVISIBLE);
        }
    }

    protected void manageButtonBar(boolean left, boolean right, boolean back) {
        backButton.setEnabled(back);
        leftButton.setEnabled(left);
        rightButton.setEnabled(right);
    }

    public void showLoading() {
        if (progress != null) {
            this.runOnUiThread(new Runnable() {
                @Override public void run() {
                    if (progress.getVisibility() != View.VISIBLE) {
                        progress.setVisibility(View.VISIBLE);
                        progress.bringToFront();
                    }
                }
            });
        }
    }
    public void stopLoading() {
        if (progress != null) {
            this.runOnUiThread(new Runnable() {
                @Override public void run() {
                    if (progress.getVisibility() != View.GONE) {
                        progress.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    public void makeCall(String number) {
        String text   = "tel:"+number;
        Uri uri = Uri.parse(text);
        startActivity(new Intent(Intent.ACTION_DIAL, uri));
    }


    public void showMessageDialog(String message) {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                dismiss();
            }
        };
        dialog.setArgs(message);
        showDialog(dialog);
    }

    public void showDialog(DialogFragment dialog) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack("dialog");
        dialog.setCancelable(false);
        dialog.show(ft, "dialog");
    }
    
    public void showNoDataDialog() {
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                mApp.shutdown(false, false);
            }
        };
        dialog.setArgs("","Work Orders could not be accessed. Please shutdown and try again.","Shutdown","Cancel");
        showDialog(dialog);
    }

    public void showUncaughtExceptionDialog(String message) {
        String msg = message;
        if (msg == null || msg.isEmpty()) msg = "An unknown error has occurred. Please shutdown and try again.";
        ApexDialog dialog = new ApexDialog() {
            @Override public void confirm() {
                mApp.shutdown(false, false);
            }
        };
        dialog.setArgs("",msg,"Shutdown","Cancel");
        showDialog(dialog);
    }

    @Optional @OnTouch(R.id.button_help) protected boolean onHelpClick(View v, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                v.setPressed(true);
                startActivity(new Intent(this, HelpActivity.class));
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_OUTSIDE:
            case MotionEvent.ACTION_CANCEL:
                v.setPressed(false);
                // Stop action ...
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }

        return true;
    }

    @Override public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_home) {
            startActivity(new Intent(this, HomeActivity.class));
            return true;
        } else if (id == R.id.menu_fail) {
            getPresenter().failVisit();
            return true;
        } else if (id == R.id.menu_skip) {
            getPresenter().skipVisit();
            return true;
        } else if (id == R.id.menu_back) {
            getPresenter().back();
            return true;
        }
        return false;
    }


    @Optional @OnTouch(R.id.background_layout) boolean onLayoutTouch() {
        if (getCurrentFocus() != null && getCurrentFocus() instanceof EditText) {
            hideKeyboard((EditText) getCurrentFocus());
        }
        return true;
    }
    @Optional @OnTouch(R.id.scroll) boolean onScrollTouch(MotionEvent event) {
        return onTouchEvent(event);
    }

    private float mDownX;
    private float mDownY;
    private final float SCROLL_THRESHOLD = 10;
    private boolean isOnClick = false;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mDownX = ev.getX();
                mDownY = ev.getY();
                isOnClick = true;
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (isOnClick) {
                    if (getCurrentFocus() != null && getCurrentFocus() instanceof EditText) {
                        hideKeyboard((EditText) getCurrentFocus());
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (isOnClick && (Math.abs(mDownX - ev.getX()) > SCROLL_THRESHOLD || Math.abs(mDownY - ev.getY()) > SCROLL_THRESHOLD)) {
                    isOnClick = false;
                }
                break;
            default:
                break;
        }
        return false;
    }

    public void hideKeyboard(EditText edit) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
    }

}
