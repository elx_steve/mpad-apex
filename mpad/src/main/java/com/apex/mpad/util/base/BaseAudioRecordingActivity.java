package com.apex.mpad.util.base;

import android.media.AudioRecord;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.apex.mpad.util.FileUtility;
import com.apex.mpad.util.SoundUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;


/**
 * Created by John on 9/23/2016.
 */

public abstract class BaseAudioRecordingActivity extends BaseRecordingActivity {
    private static final String TAG = "BaseAudioRecordingActivity";
    private static final Logger mLogger = LoggerFactory.getLogger(BaseAudioRecordingActivity.class);

    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    private boolean isRecording = false;    // Used to avoid to start multiple recording threads !
    private boolean isRecordingRun = false;
    public static final String AUDIO_RECORDER_TEMP_FILE = SoundUtil.AUDIO_RECORDER_TEMP_FILE;

    short[] audioData;

    protected Integer bufferSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private String getTempFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    @Override
    protected String getAudioImageName() {
        return SoundUtil.getAudioImageName(SoundUtil.AUDIO_RECORDER_FILE_EXT_WAV, fileKey);
    }

    @Override
    protected void prepareRecording() {
        recorder = SoundUtil.getAudioRecord();
        if (recorder == null) {
            String msg = "Could not connect to the Audio Recorder. Try restarting the application.";
            mLogger.warn(msg);
            showUncaughtExceptionDialog(msg);
        } else {
            bufferSize = recorder.getMinBufferSize(recorder.getSampleRate(), recorder.getChannelConfiguration(), recorder.getAudioFormat());
            audioData = new short[bufferSize]; //short array that pcm data is put into.
            deleteTempFile();
        }
    }

    @Override
    protected void startRecording() {
        // Arrives here when the rec button is touched.
        if (!isRecordingRun) {
            isRecordingRun = true;
            prepareRecording();

            isRecording = true;
            RecordAudio recordTask = new RecordAudio();
            recordTask.execute(recorder);

            startMinRecordingTimer();   // Start minimum recording timer
            startMaxRecordingTimer();   // Start maximum recording timer
        }
    }

    @Override
    protected void stopRecording() {
        // Arrives here when we want ot stop the recording for real
        isRecording = false;

        // Stop timers
        stopMaxRecordingTimer();
        stopMinRecordingTimer();
    }

    private void deleteTempFile() {
        File file = new File(getTempFilename());
        file.delete();
    }

    protected boolean isRecordingRunning() {
        return isRecording;
    }

        protected abstract void setPresenterFile();

    private class RecordAudio extends AsyncTask<AudioRecord, Integer, Void> {
        @Override
        protected Void doInBackground(AudioRecord... params) {
            AudioRecord audioRecord = params[0];

            Log.d(TAG, "Recording thread - START");

            isRecording = true;
            try {
                File recordingFile = new File(getTempFilename());
                DataOutputStream dos = new DataOutputStream(
                        new BufferedOutputStream(new FileOutputStream(
                                recordingFile)));
                int bufferSize = AudioRecord.getMinBufferSize(audioRecord.getSampleRate(), audioRecord.getChannelConfiguration(), audioRecord.getAudioFormat());

                short[] buffer = new short[bufferSize];
                audioRecord.startRecording();
                int r = 0;
                while (isRecording) {
                    int bufferReadResult = audioRecord.read(buffer, 0,
                            bufferSize);
                    for (int i = 0; i < bufferReadResult; i++) {
                       dos.writeShort(buffer[i]);
                    }
                    publishProgress(new Integer(r));
                    r++;
                }
                audioRecord.stop();
                dos.flush();
                dos.close();
                SoundUtil.rawToWave(recordingFile,FileUtility.getFile(getAudioImageName()), audioRecord.getSampleRate());
                deleteTempFile();
                audioRecord.release();
                isRecordingRun = false;
                Log.d(TAG, "Recording thread - END");

            } catch (Throwable t) {
                mLogger.error("AudioRecord", "Recording Failed");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            drawRecButtonOff();
            setPresenterFile();
        }

    }
}
