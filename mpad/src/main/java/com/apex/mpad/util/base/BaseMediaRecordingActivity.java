package com.apex.mpad.util.base;

import android.media.MediaRecorder;
import android.os.Bundle;

import com.apex.mpad.R;
import com.apex.mpad.util.FileUtility;
import com.apex.mpad.util.SoundUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by John on 9/23/2016.
 */

public abstract class BaseMediaRecordingActivity extends BaseRecordingActivity {

    private static final Logger mLogger = LoggerFactory.getLogger(BaseMediaRecordingActivity.class);


    private MediaRecorder mRecorder;
    public static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    public static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";

    private int currentFormat = 0;
    private int output_formats[] = { MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP };
    private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            showToast(TOAST_NO_RECORDNG);
        }
    };

    protected void prepareRecording() {
        String filename = getAudioImageName();
        FileUtility.deleteFile(filename);
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(output_formats[currentFormat]);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(filename);
        mRecorder.setOnErrorListener(errorListener);
        //mRecorder.setOnInfoListener(infoListener);

        try {
            showLoading();
            mRecorder.prepare();
            Thread.sleep(1000);
            stopLoading();
        } catch (Exception ex) {
            mLogger.warn("Unable to prepare recording", ex);
            showToast(TOAST_NO_RECORDNG);
        }
    }

    @Override
    protected void startRecording(){

        if (mRecorder == null) {
            String filename = getAudioImageName();
            if (FileUtility.fileExists(filename)) FileUtility.deleteFile(filename);
            prepareRecording();
        }

        try {
            mRecorder.start();
            startMaxRecordingTimer();
        } catch (Exception ex) {
            mLogger.warn("Unable to record audio", ex);
            showToast(TOAST_NO_RECORDNG);
        }
    }

    @Override
    protected void stopRecording() {
        if (null != mRecorder) {
            try {
                mRecorder.stop();
                mRecorder.reset();
                mRecorder.release();
            } catch (Exception ex) {
                mLogger.warn("Unable to stop recording", ex);
            }
            finally {
                mRecorder = null;
            }
        }
        enablePlay();
        recordingButton.setPressed(false);
        recordingButton.setBackgroundResource(R.drawable.record_button_idle);


    }

    @Override
    protected String getAudioImageName() {
        return SoundUtil.getAudioImageName(file_exts[currentFormat], fileKey);
    }
}
