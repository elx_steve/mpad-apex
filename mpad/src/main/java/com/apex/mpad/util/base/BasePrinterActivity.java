package com.apex.mpad.util.base;

import android.os.Bundle;
import android.os.Looper;
import android.widget.Toast;

import com.apex.mpad.annotation.ForApplication;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.base.BaseActivity;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.discovery.BluetoothDiscoverer;
import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveredPrinterBluetooth;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by John on 3/9/2017.
 */

public class BasePrinterActivity extends BaseActivity implements DiscoveryHandler {

    protected List<DiscoveredPrinter> foundPrinters;
    @Inject @ForApplication
    protected ApexApplication mApp;
    @Inject @ForApplication
    protected ApexPrefs mPrefs;

    protected ZebraPrinter printer;
    protected Connection connection;


    protected boolean showDiscoveryMessage = false;

    protected boolean printersFound = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void discoverPrinters() {
        foundPrinters = new ArrayList<DiscoveredPrinter>();
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                try {
                    printersFound = false;
                    BluetoothDiscoverer.findPrinters(BasePrinterActivity.this, BasePrinterActivity.this);
                } catch (ConnectionException e) {
                    discoveryError(e.getMessage());
                } finally {
                    Looper.myLooper().quit();
                }
            }
        }).start();
    }

    @Override
    public void foundPrinter(final DiscoveredPrinter printer) {
        runOnUiThread(new Runnable() {
            public void run() {
                foundPrinters.add((DiscoveredPrinter) printer);
            }
        });
    }
    @Override
    public void discoveryFinished() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (showDiscoveryMessage) showMessageDialog(" Discovered " + foundPrinters.size() + " printers");
                if (foundPrinters.size() > 0) {
                    printersFound = true;
                    DiscoveredPrinterBluetooth printer = (DiscoveredPrinterBluetooth)foundPrinters.get(0);
                    if (printer.friendlyName != null) {
                        mPrefs.setMacAddress(printer.address);
                        mPrefs.setPrinterName(printer.friendlyName);
                    }
                }
            }
        });
    }

    @Override
    public void discoveryError(final String s) {
        runOnUiThread(new Runnable() {
            public void run() {
                showMessageDialog(s);
            }
        });
    }
}
