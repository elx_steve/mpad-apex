package com.apex.mpad.util.base;

import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.apex.mpad.R;
import com.apex.mpad.presenter.iface.BasePresenterInterface;
import com.apex.mpad.util.ApexApplication;
import com.apex.mpad.util.ApexPrefs;
import com.apex.mpad.util.FileUtility;
import com.apex.mpad.util.SoundUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.InjectView;
import butterknife.Optional;

/**
 * Created by John on 6/14/2016.
 */
public abstract class BaseRecordingActivity extends BaseActivity {
    private static final String TAG = "BaseRecordingActivity";

    @Optional @InjectView(R.id.recording_label_view) protected TextView recordingLabel;
    @Optional @InjectView(R.id.record_button) protected ImageButton recordingButton;
    @Optional @InjectView(R.id.play_button) protected ImageButton playButton;
    @Optional @InjectView(R.id.recording_status_label_view) protected TextView recordedLabel;

    private static final Logger mLogger = LoggerFactory.getLogger(BaseRecordingActivity.class);

    public static final String AUDIO_RECORDER_FOLDER = SoundUtil.AUDIO_RECORDER_FOLDER;
    protected static boolean shadowRecButton = false;

    protected static int MAX_RECORDING_TIMEOUT = 4000; //5 seconds
    protected static int MIN_RECORDING_TIMEOUT = 2000; //3 seconds

    protected static boolean isMinTimerRunning = false;
    protected static boolean isMaxTimerRunning = false;

    protected final String TOAST_NO_RECORDNG = "Unable to Record Audio";
    protected final String TOAST_NO_DATA = "Unable to Play Audio";
    protected final String TOAST_BAD_DATA = "Unable to Play Audio - corrupted file";

    protected static MaxRecordingTimeoutRunnable maxRecordingTimeout;
    protected static MinRecordingTimeoutRunnable minRecordingTimeout;

    // TIMERS
    // The timers are used to create the time management for the recording.
    // There are 4 events to handle :
    // 1) Rec button pressed
    // 2) Rec button released
    // 3) 3 sec timer expiring
    // 4) 5 sec timer expiring

    // 5 seconds timer
    protected final class MaxRecordingTimeoutRunnable implements Runnable {
        public void run() {
            Log.d(TAG, "Recording - Event 4 - Max recording timer expired");
            isMaxTimerRunning = false;
            stopRecording();
        }
    };

    // 3 second timer
    protected final class MinRecordingTimeoutRunnable implements Runnable {
        public void run() {
            isMinTimerRunning = false;

            // If the Rec button is still pressed, do nothing - ignore
            if (!shadowRecButton) {
                Log.d(TAG, "Recording - Event 3 - Min recording timer expired");
                stopRecording();
            } else {
                Log.d(TAG, "Recording - Event 3 - Min recording timer expired - button still pressed");
            }
        }
    };

    protected static Handler handler = new Handler();

    protected String fileKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Create timers
        maxRecordingTimeout = new MaxRecordingTimeoutRunnable();
        minRecordingTimeout = new MinRecordingTimeoutRunnable();
    }

    protected abstract void setFileKey();

    protected void showToast(String msg) {
        problemToast = Toast.makeText(ApexApplication.get(), msg, Toast.LENGTH_SHORT);
        problemToast.setGravity(Gravity.TOP, 0, 40);
        problemToast.show();
    }

    protected void initView() {
        setFileKey();
        if (recordingButton != null && playButton != null) {
            recordingButton.setEnabled(true);
            recordingButton.setBackgroundResource(R.drawable.record_button_idle);

            playButton.setBackgroundResource(R.drawable.play_button_idle);
            disablePlay();

            recordingButton.setOnClickListener(null);
            recordingButton.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    boolean myreturn = false;
                    // TODO Auto-generated method stub
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            // Pressed Recording button event ! Change button aspect and start recording
                            if (!isRecordingRunning()){
                                Log.d(TAG, "Recording - Event 1 - Rec button pressed");

                                shadowRecButton = true;
                                drawRecButtonOn();
                                startRecording();
                                myreturn = true;
                            }
                            else
                            {
                                Log.d(TAG, "Recording - Event 1 - Rec button pressed but ignored");
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            // Released Recording button event !
                            shadowRecButton = false;

                            // If the Minimum timer is still running ignore the release
                            // TBD How to ceck if the timer is running
                            if (!isMinTimerRunning) {
                                Log.d(TAG, "Recording - Event 2 - Rec button released");

                                drawRecButtonOff();
                                stopRecording();
                                myreturn = true;
                            }
                            else {
                                Log.d(TAG, "Recording - Event 2 - Rec button released ignored");
                            }
                            break;
                    }
                    return myreturn;
                }
            });

            playButton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) throws IllegalArgumentException, SecurityException, IllegalStateException {
                    MediaPlayer m = new MediaPlayer();
                    m.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                            mp.release();
                        }

                    });
                    boolean isError = false;
                    try {
                        m.setDataSource(getAudioImageName());
                        showLoading();
                    } catch (Exception e) {
                        mLogger.warn("Unable to find playback file", e);
                        showToast(TOAST_NO_DATA);
                        isError = true;
                    }

                    if (!isError) {
                        try {
                            m.prepare();
                            Thread.sleep(1000);
                        } catch (Exception ex) {
                            mLogger.warn("Unable to prepare playback file", ex);
                            showToast(TOAST_NO_DATA);
                            isError = true;
                        } finally {
                            stopLoading();
                        }
                    }

                    if (!isError) {
                        try {
                            m.start();
                            Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
                        } catch (Exception ex) {
                            mLogger.warn("Unable to play playback file", ex);
                            showToast(TOAST_NO_DATA);
                        }
                    }

                }
            });
            boolean recordingExists = FileUtility.fileExistsNonZero(getAudioImageName());
            if (recordingExists) {
                enablePlay();
            }
        }
    }

    // Draw the Recording button ON
    protected void drawRecButtonOn() {
        recordingButton.setPressed(false);
        recordingButton.setBackgroundResource(R.drawable.record_button_pressed);
        disablePlay();
    }

    // Draw the Recording button OFF
    protected void drawRecButtonOff() {
        recordingButton.setPressed(false);
        recordingButton.setBackgroundResource(R.drawable.record_button_idle);
        enablePlay();
    }

    protected void enablePlay() {
        playButton.setEnabled(true);
        recordedLabel.setVisibility(View.VISIBLE);
        recordingLabel.setText(getString(R.string.play_label));
        playButton.setBackgroundResource(R.drawable.play_button_idle);
    }

    protected void disablePlay() {
        playButton.setEnabled(false);
        recordedLabel.setVisibility(View.INVISIBLE);
        recordingLabel.setText(getString(R.string.recording_label));
        playButton.setBackgroundResource(R.drawable.play_button_disabled);
    }

    // Start Max recording timer
    protected void startMaxRecordingTimer() {
        try {
            if (MAX_RECORDING_TIMEOUT > 0) {
                handler.postDelayed(maxRecordingTimeout, MAX_RECORDING_TIMEOUT);
                isMaxTimerRunning = true;
            }
        } catch(Exception e) {

        }
    }

    // Start Min recording timer
    protected void startMinRecordingTimer() {
        try {
            if (MIN_RECORDING_TIMEOUT > 0) {
                handler.postDelayed(minRecordingTimeout, MIN_RECORDING_TIMEOUT);
                isMinTimerRunning = true;
            }
        } catch(Exception e) {

        }
    }

    // Stop Max recording timer
    protected void stopMaxRecordingTimer() {
        try {
            handler.removeCallbacks(maxRecordingTimeout);
            isMaxTimerRunning = false;
        } catch(Exception e) {

        }
    }

    // Stop Min recording timer
    protected void stopMinRecordingTimer() {
        try {
            handler.removeCallbacks(minRecordingTimeout);
            isMinTimerRunning = true;
        } catch(Exception e) {

        }
    }

    protected abstract void startRecording();

    protected abstract void stopRecording();

    protected abstract void prepareRecording();

    protected abstract boolean isRecordingRunning();

    protected abstract String getAudioImageName();

    protected abstract BasePresenterInterface getPresenter();
}
